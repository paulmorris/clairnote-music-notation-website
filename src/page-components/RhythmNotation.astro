---
import Layout from "../layouts/Layout.astro";
import NextPageLink from "../components/NextPageLink.astro";
import type { PageComponentProps } from "../types/types";

import ChromaticScaleDn from "../images/svg-dn/Clairnote-chromatic-scale-ascending-sharps.svg";

import DurationNotesDn from "../images/svg-dn/Clairnote-duration-notes.svg";
import DurationNotesSn from "../images/svg-sn/Clairnote-duration-notes.svg";

import HalfNotesQuarterNotesDn from "../images/svg-dn/Clairnote-half-notes-quarter-notes.svg";
import HalfNotesQuarterNotesSn from "../images/svg-sn/Clairnote-half-notes-quarter-notes.svg";

import DurationRestsDn from "../images/svg-dn/Clairnote-duration-rests.svg";
import DurationRestsSn from "../images/svg-sn/Clairnote-duration-rests.svg";

import PageTitle from "../components/PageTitle.astro";
import pageStyles from "../styles/page.module.css";
import LilyPondSvg from "../components/LilyPondSvg.astro";

type Props = PageComponentProps;
const props = Astro.props;

const title = "Rhythm Notation";
const description = `Discussion and illustration of rhythm notation symbols in ${
  props.clairnoteName
}. ${
  props.dn
    ? "(Traditional duration symbols except hollow and solid notes show pitch, and half notes have double stems.)"
    : "(The same as the duration symbols in traditional music notation.)"
}`;
---

<Layout title={title} description={description} {...props}>
  <article class="page">
    <PageTitle>{title}</PageTitle>
    <div>
      {
        props.dn ? (
          <>
            <p>
              {props.clairnoteName} uses the same rhythm notation symbols as
              traditional notation, except:
            </p>
            <ul>
              <li>
                Hollow and solid notes are used to help indicate pitch instead
                of duration.
              </li>
              <li>
                Half notes have a double stem to distinguish them from quarter
                notes.
              </li>
            </ul>
            <p>Here is a comparison showing notes of different durations:</p>
          </>
        ) : (
          <p>
            {props.clairnoteName} uses the same rhythm notation symbols as
            traditional notation. Here is a comparison showing notes of
            different durations:
          </p>
        )
      }
      <div class={pageStyles.imageContainer}>
        <LilyPondSvg
          data={props.dn ? DurationNotesDn : DurationNotesSn}
          alt={"Note durations in " + props.clairnoteName + " music notation"}
        />
      </div>
      {
        props.dn && (
          <p>And a comparison focusing on half notes and quarter notes:</p>
        )
      }
      <div class={pageStyles.imageContainer}>
        <LilyPondSvg
          data={props.dn ? HalfNotesQuarterNotesDn : HalfNotesQuarterNotesSn}
          alt={"Half notes and quarter notes in " +
            props.clairnoteName +
            " music notation"}
        />
      </div>
      <p>
        Dotted notes, tied notes, rests, slurs, and other articulations are the
        same in {props.clairnoteName} as they are in traditional notation. Here is
        another comparison showing rests of different durations.
      </p>
      <div class={pageStyles.imageContainer}>
        <LilyPondSvg
          data={props.dn ? DurationRestsDn : DurationRestsSn}
          alt={"Rests of different durations in " +
            props.clairnoteName +
            " music notation"}
        />
      </div>
      {
        props.dn && (
          <>
            <h2 class="title is-5">
              Distinguishing Half Notes and Quarter Notes
            </h2>
            <p>
              Using double stems for half notes distinguishes them from quarter
              notes, and symbolizes how their duration is twice as long. It also
              has the benefit of consolidating duration symbols into variations
              on the stem. The rest of the duration and rhythm symbols are the
              same as in traditional notation, making it easier to read both{" "}
              {props.clairnoteName} and traditional notation "bilingually."
              Since quarter notes generally occur more frequently than half
              notes it makes sense for them to look the same as in traditional
              notation, with a single stem and no flag.
            </p>
            <h2 class="title is-5">
              Why Use Hollow and Solid Notes to Indicate Pitch?
            </h2>
            <p>
              In traditional notation hollow and solid notes are used simply to
              differentiate between half notes and quarter notes. (Other note
              durations are distinguished through other symbols like flags or
              lack of stem as shown above.) But consider that the one thing that
              is most visually apparent about a note is whether it is solid or
              hollow. At a glance it is much easier to see that a note is solid
              or hollow than to determine its position on the staff.
            </p>
            <p>
              Isn't it a poor use of this prominent visual feature to use it
              simply to differentiate between half notes and quarter notes?
              Wouldn't it be better to use hollow and solid notes to make it
              easier to identify a note's pitch? And also to provide the
              benefits of the{" "}
              <a
                title="6-6 Pitch Pattern"
                href={props.urlDir + "staff/#six-six"}
              >
                6-6 pitch pattern
              </a>
              ? This is {props.clairnoteName}
              's approach.
            </p>
            <p>
              Using hollow and solid noteheads to help indicate pitch makes it
              easier to quickly identify notes and tell them apart. It also
              provides the benefits of the{" "}
              <a
                title="6-6 Pitch Pattern"
                href={props.urlDir + "staff/#six-six"}
              >
                6-6 pitch pattern
              </a>
              , namely making interval relationships clearer and easier to
              distinguish from one another. This is illustrated on the{" "}
              <a title="Scales" href={props.urlDir + "scales/"}>
                Scales
              </a>{" "}
              and{" "}
              <a title="Intervals" href={props.urlDir + "intervals/"}>
                Intervals
              </a>{" "}
              pages.
            </p>
            <div class={pageStyles.imageContainer}>
              <LilyPondSvg
                data={ChromaticScaleDn}
                alt={
                  "Chromatic scale in " +
                  props.clairnoteName +
                  " music notation"
                }
              />
            </div>
          </>
        )
      }

      <h2 class="title is-5">
        Why Not Try to Improve Upon Traditional Rhythm Notation?
      </h2>
      <p>
        It would probably be possible to come up with a novel rhythm notation
        system that was more consistent or systematic. This has not been a
        priority with {props.clairnoteName}, mainly because it would increase
        the learning curve for those who already know traditional notation or
        who want to learn it alongside {props.clairnoteName}. Using the
        traditional rhythm notation{" "}
        {props.dn && "(with one small change that brings big benefits)"}
        provides greater continuity with traditional notation and makes it easier
        for musicians to learn to read both systems interchangeably.
      </p>
      <p>
        That said, anyone who wants to further innovate and use{" "}
        {props.clairnoteName} with an alternative rhythm notation system is welcome
        to do so (provided they communicate that it is not{" "}
        {props.clairnoteName}
        's standard rhythm notation).
      </p>
      <p>
        {
          props.dn && (
            <em>
              For further discussion see the Music Notation Project's tutorial
              on{" "}
              <a
                href="http://musicnotation.org/tutorials/noteheads-and-pitch/"
                target="_blank"
                rel="external noopener noreferrer"
              >
                Using Notehead Color for Pitch
              </a>
              .
            </em>
          )
        }{" "}
        <em>
          Traditional rhythm notation is documented on Wikipedia pages such as
          these:{" "}
          <a
            href="http://en.wikipedia.org/wiki/Note_value"
            target="_blank"
            rel="external noopener noreferrer"
          >
            Note Value
          </a>
          ,{" "}
          <a
            href="http://en.wikipedia.org/wiki/Dotted_note"
            target="_blank"
            rel="external noopener noreferrer"
          >
            Dotted Note
          </a>
          , and{" "}
          <a
            href="http://en.wikipedia.org/wiki/Tie_(music)"
            target="_blank"
            rel="external noopener noreferrer"
          >
            Tie
          </a>
          .
        </em>
      </p>

      <NextPageLink title="FAQ" href={props.urlDir + "faq/"} />
    </div>
  </article>
</Layout>
