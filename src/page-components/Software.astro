---
import Layout from "../layouts/Layout.astro";
import type { PageComponentProps } from "../types/types";

import DoubleLilyModified3 from "../images/png/double-lily-modified3.png";

import CMajorScaleDn from "../images/svg-dn/Clairnote-C-major-scale.svg";
import CMajorScaleSn from "../images/svg-sn/Clairnote-C-major-scale.svg";

import CMajorScaleLilyPondNotesDn from "../images/svg-dn/Clairnote-C-major-scale-lilypond-notes.svg";
import CMajorScaleLilyPondNotesSn from "../images/svg-sn/Clairnote-C-major-scale-lilypond-notes.svg";

import CMajorScaleFunksolNotesDn from "../images/svg-dn/Clairnote-C-major-scale-funksol-notes.svg";
import CMajorScaleFunksolNotesSn from "../images/svg-sn/Clairnote-C-major-scale-funksol-notes.svg";

import AccidentalsQuarterToneCustomSn from "../images/svg-sn/Clairnote-accidentals-quarter-tone-custom.svg";
import AccidentalsQuarterToneCustomDn from "../images/svg-dn/Clairnote-accidentals-quarter-tone-custom.svg";

import AccidentalsQuarterToneCustomDefaultsSn from "../images/svg-sn/Clairnote-accidentals-quarter-tone-custom-defaults.svg";
import AccidentalsQuarterToneCustomDefaultsDn from "../images/svg-dn/Clairnote-accidentals-quarter-tone-custom-defaults.svg";

import PageTitle from "../components/PageTitle.astro";
import utilStyles from "../styles/util.module.css";
import pageStyles from "../styles/page.module.css";
import LilyPondSvg from "../components/LilyPondSvg.astro";

type Props = PageComponentProps;
const props = Astro.props;
const title = "Software: LilyPond";
const description = `Learn how to use LilyPond (free and open-source music notation software) to create sheet music in ${props.clairnoteName} music notation.`;
---

<Layout title={title} description={description} {...props}>
  <article class="page">
    <PageTitle>{title}</PageTitle>
    <div>
      <p class={`${utilStyles.textAlignCenter}`}>
        <img
          src={DoubleLilyModified3.src}
          alt="LilyPond Logo"
          width="321"
          height="192"
        />
      </p>
      <div class={pageStyles.listTitle}>On this page:</div>
      <ul>
        <li>
          <a href="#about">About LilyPond</a>
        </li>
        <li>
          <a href="#creating">
            Creating {props.clairnoteName} Sheet Music with LilyPond
          </a>
        </li>
        <li>
          <a href="#templates">{props.clairnoteName} Template Files</a>
        </li>
        <li>
          <a href="#octaves">Staves Spanning More Than Two Octaves</a>
        </li>
        <li>
          <a href="#same-file">
            {props.clairnoteName} and Traditional Music Notation in the Same
            File
          </a>
        </li>
        <li>
          <a href="#noteheads">Notehead Styles</a>
        </li>
        <li>
          <a href="#custom-quarter-tone-accidental-signs"
            >Custom Quarter Tone Accidental Signs</a
          >
        </li>
        <li>
          <a href="#more">More Examples and Documentation</a>
        </li>
      </ul>
      <h2 class="title is-5" id="about">About LilyPond</h2>
      <div class={pageStyles.listTitle}>LilyPond:</div>
      <ul>
        <li>
          Is music notation software that can create sheet music in{" "}
          {props.clairnoteName} music notation.
        </li>
        <li>Is free to download and use.</li>
        <li>
          Runs on all major desktop operating systems (Windows, macOS, and
          GNU/Linux).
        </li>
        <li>
          Can be used via web apps like{" "}
          <a
            href="https://lilybin.clairnote.org/"
            target="_blank"
            rel="noopener noreferrer"
          >
            LilyBin + Clairnote
          </a>{" "}
           (also{" "}
          <a
            href="https://www.hacklily.org/"
            target="_blank"
            rel="noopener noreferrer"
          >
            HackLily
          </a>{" "}
           and{" "}
          <a
            href="http://lilybin.com/"
            target="_blank"
            rel="noopener noreferrer"
          >
            LilyBin
          </a>
           ).
        </li>
        <li>Can convert MusicXML, ABC, and MIDI files into LilyPond files.</li>
        <li>
          Is highly customizable, making it an excellent tool for creating sheet
          music in alternative notation systems.
        </li>
        <li>
          Is "devoted to producing the highest-quality sheet music possible".
        </li>
        <li>
          Is{" "}
          <a
            href="https://www.gnu.org/philosophy/free-sw"
            target="_blank"
            rel="noopener noreferrer"
          >
            free software
          </a>{" "}
           and part of the{" "}
          <a
            href="http://www.gnu.org/"
            target="_blank"
            rel="noopener noreferrer"
          >
            GNU project
          </a>
           .
        </li>
      </ul>
      <p>
        LilyPond has a{" "}
        <a
          href="http://lilypond.org/text-input.html"
          target="_blank"
          rel="external noopener noreferrer"
        >
          text-based
        </a>{" "}
         interface rather than a graphical user interface. (Like{" "}
        <a
          href="https://www.latex-project.org/"
          target="_blank"
          rel="noopener noreferrer"
        >
          LaTeX
        </a>{" "}
         but for sheet music.) LilyPond files are simply plain text files
        written in LilyPond syntax and named using the ".ly" file extension.
        Learn more about LilyPond at{" "}
        <a
          href="http://lilypond.org"
          target="_blank"
          rel="external noopener noreferrer"
        >
          lilypond.org
        </a>
         .
      </p>
      <p>
        Popular graphical applications like the free and open-source{" "}
        <a
          href="https://musescore.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          MuseScore
        </a>{" "}
         (also Finale, Sibelius, and Dorico) can export MusicXML files that can
        be converted into LilyPond files to produce sheet music in{" "}
        {props.clairnoteName}. For details on using this approach see:{" "}
        {
          /* </p>
          <p> */
        }
        <ul>
          <li>
            <a
              href="/blog/2021/11/creating-clairnote-sheet-music-musescore-frescobaldi-and-lilypond/"
            >
              Creating Clairnote Sheet Music with MuseScore, Frescobaldi, and
              LilyPond
            </a>
          </li>
          <li>
            <a
              href="/blog/2022/03/converting-standard-sheet-music-in-a-pdf-file-into-clairnote-notation/"
            >
              Converting Standard Sheet Music in a PDF File into Clairnote
              Notation
            </a>
          </li>
        </ul>
      </p>
      <h2 class="title is-5" id="creating">
        Creating {props.clairnoteName} Sheet Music with LilyPond
      </h2>
      <p>
        Here are two ways to use LilyPond to create {props.clairnoteName}{" "}
         sheet music:
      </p>
      <ul>
        <li>Use LilyBin, a website interface for LilyPond</li>
        <li>Install LilyPond</li>
      </ul>
      <p>
        LilyBin lets you try LilyPond before installing it and lets you use
        LilyPond from a tablet or phone. For more extensive use, install
        LilyPond. Both work for converting existing LilyPond files and for
        creating new ones from scratch.
      </p>
      <p>
        <strong>Option 1: Use LilyBin</strong>
      </p>
      <p style="color: rgb(180, 0, 0);">
        NOTE: LilyBin + Clairnote is not working and needs repair. Please
        install LilyPond as described in "Option 2" below.
      </p>
      <p>
        1. Go to{" "}
        <a
          href="http://lilybin.clairnote.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          LilyBin + Clairnote
        </a>{" "}
         (
        <a
          href="http://lilybin.clairnote.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          lilybin.clairnote.org
        </a>
         ) a version of LilyBin that supports {props.clairnoteName} music
        notation.
      </p>
      <p>
        {
          props.sn ? (
            <>
              2. Note the <code>\include "clairnote.ly"</code> line at the top
              of your LilyPond file.
            </>
          ) : (
            <>
              2. Near the top of the LilyPond file, delete the <code>%</code> in
              the line <code>% clairnote-type = dn</code>. This uncomments the
              line, which sets the <code>clairnote-type</code> variable to{" "}
              <code>dn</code>. (Leave the <code>\include "clairnote.ly"</code>{" "}
              line as it is.)
            </>
          )
        }
      </p>
      <p>
        3. Edit and engrave your file (click "Preview") and it will be rendered
        in {props.clairnoteName} music notation.{" "}
        {
          props.sn && (
            <>
              (For {props.clairnoteName}, you will need to use the unstable
              (development) version of LilyPond.)
            </>
          )
        }
      </p>
      <p>
        <strong>Option 2: Install LilyPond</strong>
      </p>
      <p>
        1. Download and install{" "}
        <a
          href="http://lilypond.org"
          target="_blank"
          rel="external noopener noreferrer"
        >
          LilyPond
        </a>
         . (The latest stable version is best. Version 2.20 or later should
        work.)
      </p>
      <p>
        2. Download and install{" "}
        <a
          href="http://www.frescobaldi.org/"
          target="_blank"
          rel="external noopener noreferrer"
        >
          Frescobaldi
        </a>{" "}
         (optional but highly recommended). Frescobaldi is an editor designed
        specifically for use with LilyPond that offers a lot of nice features
        like PDF preview, text highlighting, auto-complete, midi playback, etc.
        It helps make LilyPond easier to use and learn.
      </p>
      <p>3. Download the following file (right-click on the link):</p>
      <p style={{ paddingLeft: "30px" }}>
        <strong>
          <a
            href={"/lilypond/clairnote.ly"}
            target="_blank"
            rel="noopener noreferrer"
          >
            clairnote.ly
          </a>
        </strong>
        <br />
         Released under the{" "}
        <a
          href="http://www.gnu.org/licenses/gpl.html"
          target="_blank"
          rel="noopener noreferrer"
        >
          GNU General Public License
        </a>{" "}
         version 3 (
        <a
          href="http://www.gnu.org/licenses/gpl-3.0-standalone.html"
          target="_blank"
          rel="noopener noreferrer"
        >
          html
        </a>{" "}
        <a
          href="http://www.gnu.org/licenses/gpl-3.0.txt"
          target="_blank"
          rel="noopener noreferrer"
        >
          txt
        </a>
         ).
        <br />
         Development:{" "}
        <a
          href="https://gitlab.com/paulmorris/lilypond-clairnote"
          target="_blank"
          rel="noopener noreferrer"
        >
          git repo
        </a>
         .
      </p>
      {
        props.dn && (
          <p>
            (If you are still using LilyPond 2.18.2 you can download this{" "}
            <a href="https://clairnote.org/files/20170714/clairnote.ly">
              earlier version of clairnote.ly
            </a>
            .)
          </p>
        )
      }
      <p>
        4. Open a LilyPond music file (.ly) (using Frescobaldi) and add the
        following line near the top.{" "}
        {props.dn && <>(They must be in this order.)</>}
      </p>
      {
        props.sn ? (
          <pre>\include "clairnote.ly"</pre>
        ) : (
          <pre>{`clairnote-type = dn
\\include "clairnote.ly"`}</pre>
        )
      }
      <p>
        {
          props.sn ? (
            <>
              This <em>includes</em> the <code>clairnote.ly</code> file in your
              LilyPond file,
            </>
          ) : (
            <>
              This sets the <code>clairnote-type</code> variable to{" "}
              <code>dn</code> and includes the
              <code>clairnote.ly</code> file in your LilyPond file,
            </>
          )
        }{" "}
         providing additional code that allows LilyPond to render/engrave the
        file in {props.clairnoteName} music notation. Including a file works the
        same as if you had cut and pasted the contents of one file into another
        file (see{" "}
        <a
          href="http://lilypond.org/doc/v2.22/Documentation/notation/including-lilypond-files.html"
          target="_blank"
          rel="external noopener noreferrer"
        >
          <code>\include</code>
        </a>
         ).
      </p>
      <p>
        5. Place the <code>clairnote.ly</code> file in the same directory as
        your LilyPond file(s) or somewhere else where LilyPond can access it
        (see{" "}
        <a
          href="http://lilypond.org/doc/v2.22/Documentation/notation/including-lilypond-files.html"
          target="_blank"
          rel="external noopener noreferrer"
        >
          <code>\include</code>
        </a>
         ). In{" "}
        <a
          href="http://www.frescobaldi.org/"
          target="_blank"
          rel="external noopener noreferrer"
        >
          Frescobaldi
        </a>{" "}
         you can designate a directory that contains files that you want to
        include and then you can always include those files when using
        Frescobaldi. To set this up go to Preferences &gt; LilyPond Preferences.
      </p>
      <p>
        6. Edit your LilyPond file and engrave it with LilyPond. It will be
        rendered in {props.clairnoteName} music notation.
      </p>
      <p>
        Note: if you are converting an existing LilyPond file (.ly), make sure
        it is for LilyPond version 2.19.49 or higher.{" "}
        {
          props.dn && (
            <>
              (If you are still using LilyPond 2.18.2 then it can be 2.18.2 or
              higher.)
            </>
          )
        }{" "}
         Look for a <code>\version</code> statement that should be at the top of
        the file. If it is an earlier version, update the file using{" "}
        <a
          href="http://www.lilypond.org/doc/v2.22/Documentation/usage/updating-files-with-convert_002dly"
          target="_blank"
          rel="external noopener noreferrer"
        >
          convert-ly
        </a>
         . (The{" "}
        <a
          href="http://www.mutopiaproject.org/"
          target="_blank"
          rel="external noopener noreferrer"
        >
          Mutopia Project
        </a>{" "}
         offers LilyPond files for free download, but some will need to be
        updated.)
      </p>

      <h2 class="title is-5" id="templates">
        {props.clairnoteName} Template Files
      </h2>
      <p>
        You may want to use one of these template files to get started. They are
        just variations on some of the official{" "}
        <a
          href="http://www.lilypond.org/doc/v2.22/Documentation/learning/templates"
          target="_blank"
          rel="external noopener noreferrer"
        >
          LilyPond Template Files
        </a>
         . Note the line <code>\include "clairnote.ly"</code> at the top.{" "}
        {
          props.dn && (
            <>
              Delete the <code>%</code> in the{" "}
              <code>% clairnote-type = dn</code> line to uncomment that line.
              (In a LilyPond file a "%" signals that the rest of the line is a
              comment, which is ignored when the score is rendered.)
            </>
          )
        }
      </p>
      <ul>
        <li>
          <a href={"/lilypond-templates/clairnote-staff.ly"}>
            clairnote-staff.ly
          </a>{" "}
           – 2 octave staff (the standard staff)
        </li>
        <li>
          <a href={"/lilypond-templates/clairnote-staff-3-octaves.ly"}>
            clairnote-staff-3-octaves.ly
          </a>{" "}
           – 3 octave staff (for guitar, etc.)
        </li>
        <li>
          <a href={"/lilypond-templates/clairnote-staff-4-octaves.ly"}>
            clairnote-staff-4-octaves.ly
          </a>{" "}
           – 4 octave staff (for piano, etc.)
        </li>
      </ul>
      <ul>
        <li>
          <a href={"/lilypond-templates/clairnote-staff-lyrics.ly"}>
            clairnote-staff-lyrics.ly
          </a>{" "}
           – staff with lyrics
        </li>
        <li>
          <a href={"/lilypond-templates/clairnote-staff-chords.ly"}>
            clairnote-staff-chords.ly
          </a>{" "}
           – staff with chords
        </li>
        <li>
          <a href={"/lilypond-templates/clairnote-staff-lyrics-chords.ly"}>
            clairnote-staff-lyrics-chords.ly
          </a>{" "}
           – staff with lyrics and chords
        </li>
      </ul>
      <ul>
        <li>
          <a href={"/lilypond-templates/clairnote-solo-piano.ly"}>
            clairnote-solo-piano.ly
          </a>{" "}
           – piano staff (separate bass and treble staves)
        </li>
        <li>
          <a href={"/lilypond-templates/clairnote-piano-lyrics.ly"}>
            clairnote-piano-lyrics.ly
          </a>{" "}
           – piano staff with lyrics
        </li>
        <li>
          <a href={"/lilypond-templates/clairnote-piano-melody-lyrics.ly"}>
            clairnote-piano-melody-lyrics.ly
          </a>{" "}
           – piano staff with melody staff and lyrics
        </li>
      </ul>
      <p>
        By default these templates show {props.clairnoteName}
         's alternative{" "}
        <a href={props.urlDir + "key-signatures/"}> key signatures</a> and{" "}
        <a href={props.urlDir + "accidental-signs/"}>accidental signs</a>. If
        you would rather not use them, just delete the "%" at the beginning of
        the following line(s) in the template file:
      </p>
      <pre>{`% \\remove Accidental_engraver
% \\remove Key_engraver`}</pre>
      <p>
        Restore the "%" to show them again.{" "}
        {
          props.sn && (
            <>
              (In a LilyPond file a "%" signals that the rest of the line is a
              comment, which is ignored when the score is rendered.)
            </>
          )
        }
      </p>
      <h2 class="title is-5" id="octaves">
        Staves Spanning More Than Two Octaves
      </h2>
      <p>
        The standard {props.clairnoteName} staff spans two octaves. For music
        with a wider pitch range you can optionally use a larger staff with the <code
          >\cnStaffOctaveSpan</code
        > command. For example, you might use <code>\cnStaffOctaveSpan 3</code>
         for guitar or{" "}
        <code>\cnStaffOctaveSpan 4</code> for piano. This command should be
        entered in a staff's <code>\with</code> block or at the very beginning
        of the music.
      </p>
      <p>
        To change a staff in the middle of the music – to temporarily extend it
        up or down an octave to avoid ledger lines – use the commands{" "}
        <code>\cnExtendStaffUp</code> and <code>\cnExtendStaffDown</code>. Then
        "unextend" the staff with <code>\cnUnextendStaffUp</code> and{" "}
        <code>\cnUnextendStaffDown</code>. See the{" "}
        <a title="Staff" href={props.urlDir + "staff/"}> Staff</a>{" "}
         page for more about staves with different octave ranges.
      </p>
      <pre>{`\\new Staff \\with {
  \\cnStaffOctaveSpan 3
} {
  e4 f g a
  \\cnExtendStaffUp
  e''' f''' g''' a'''
  \\cnUnextendStaffUp
  a g f e
}`}</pre>
      <p>
        Usually you would use different clefs to change the octaves of the
        staff, but in some cases you might want to have the clef appear at a
        different octave on the staff. You can move a clef up or down one or
        more octaves (bringing all the notes with it) with the{" "}
        <code>\cnClefPositionShift</code> command. The key signature and time
        signature will also move with the clef. When combined, all these
        commands make a wide range of staff and clef configurations possible.
      </p>
      <pre>{`\\new Staff \\with {
  \\cnStaffOctaveSpan 3
  \\cnClefPositionShift -1
} {
  e4 f g a
  e''' f''' g''' a'''
  a g f e
}`}</pre>
      <h2 class="title is-5" id="same-file">
        {props.clairnoteName} and Traditional Music Notation in the Same File
      </h2>
      <p>
        Sometimes you may want to produce files that contain music in both{" "}
        {props.clairnoteName} and traditional music notation, say for easy
        comparison. Instead of the following which will produce a{" "}
        {props.clairnoteName} staff:
      </p>
      <pre>{`\\new Staff {
  % music goes here...
}`}</pre>
      <p>Use this to produce a traditional staff:</p>
      <pre>{`\\new TradStaff {
  % music goes here...
}`}</pre>
      <p>
        "TradStaff" is the name of a custom staff context. It tells LilyPond
        that you want a standard traditional staff instead of a{" "}
        {props.clairnoteName} staff. ( <code>\context TradStaff</code> can also
        be used where you would use <code>\context Staff</code>)
      </p>
      <h2 class="title is-5" id="noteheads">Notehead Styles</h2>
      <p>
        Notehead shapes can be customized with the{" "}
        <code>\cnNoteheadStyle</code> command. (See discussion in this{" "}
        <a href="https://clairnote.org/blog/2016/02/notehead-shapes-refined/">
          blog post
        </a>
         .) Here are the available styles:
      </p>
      <div class={pageStyles.imageContainer}>
        <figure>
          <LilyPondSvg
            data={props.dn ? CMajorScaleDn : CMajorScaleSn}
            alt={"C major scale in " +
              props.clairnoteName +
              " music notation with default noteheads"}
          />
          <figcaption>"default"</figcaption>
        </figure>
      </div>
      <div class={pageStyles.imageContainer}>
        <figure>
          <LilyPondSvg
            data={props.dn
              ? CMajorScaleLilyPondNotesDn
              : CMajorScaleLilyPondNotesSn}
            alt={"C major scale in " +
              props.clairnoteName +
              " music notation with lilypond noteheads"}
          />
          <figcaption>"lilypond"</figcaption>
        </figure>
      </div>
      <div class={pageStyles.imageContainer}>
        <figure>
          <LilyPondSvg
            data={props.dn
              ? CMajorScaleFunksolNotesDn
              : CMajorScaleFunksolNotesSn}
            alt={"C major scale in " +
              props.clairnoteName +
              " music notation with funksol noteheads"}
          />
          <figcaption>"funksol"</figcaption>{" "}
        </figure>
      </div>
      <p>Here's how to change the notehead style for a given staff.</p>
      <pre>{`\\new Staff \\with {
  % options are "funksol" "lilypond" and "default"
  \\cnNoteheadStyle "funksol"
} {
  % music goes here...
}`}</pre>
      <h2 class="title is-5" id="custom-quarter-tone-accidental-signs">
        Custom Quarter Tone Accidental Signs
      </h2>
      <p>
        {props.clairnoteName}'s quarter-tone and three-quarter-tone accidental
        signs can be customized with the <code>\cnQuarterToneSharpGlyph</code>
         and <code>\cnQuarterToneFlatGlyph</code> commands. Any of <a
          href="https://lilypond.org/doc/v2.25/Documentation/notation/accidental-glyphs"
          target="_blank">LilyPond's accidental glyphs</a
        > can be used. Here is an example that shows how to use these commands
        to switch to the <a
          href="https://www.w3.org/2019/03/smufl13/tables/gould-arrow-quartertone-accidentals-24-edo.html"
          target="_blank">Gould quarter-tone accidental glyphs</a
        >.
      </p>
      <pre>{`\\new Staff \\with {
  \\cnQuarterToneSharpGlyph "accidentals.natural.arrowup"
  \\cnQuarterToneFlatGlyph "accidentals.flat.arrowup"
} {
  % music goes here...
}`}</pre>
      <p>
        The LilyPond code above produces the following custom accidental signs.
      </p>
      <div class={pageStyles.imageContainer}>
        <figure>
          <LilyPondSvg
            data={props.dn
              ? AccidentalsQuarterToneCustomDn
              : AccidentalsQuarterToneCustomSn}
            alt={`Custom quarter-tone and three-quarter-tone sharp and flat signs in ${props.clairnoteName}`}
          />
        </figure>
      </div>
      <p>
        Note how the specified quarter-tone glyphs are also used in the
        three-quarter-tone accidental signs, just as they are in the default
        accidental signs, which are shown below for reference. The default
        accidental signs use the <a
          href="https://www.w3.org/2019/03/smufl13/tables/stein-zimmermann-accidentals-24-edo.html"
          target="_blank">Stein-Zimmermann quarter-tone accidental glyphs</a
        >.
      </p>
      <div class={pageStyles.imageContainer}>
        <figure>
          <LilyPondSvg
            data={props.dn
              ? AccidentalsQuarterToneCustomDefaultsDn
              : AccidentalsQuarterToneCustomDefaultsSn}
            alt={`Default Stein-Zimmerman quarter-tone and three-quarter-tone sharp and flat signs in ${props.clairnoteName}`}
          />
        </figure>
      </div>
      <p>
        Additional options for customization are provided by LilyPond's standard
        mechanisms for using <a
          href="https://lilypond.org/doc/v2.24/Documentation/notation/displaying-pitches#alternate-accidental-glyphs"
          target="_blank">alternate accidental glyphs</a
        > (namely the <code>alterationGlyphs</code> property of the <code
          >Staff</code
        > context).
      </p>
      <p>
        See the
        <a
          href={props.urlDir + "accidental-signs/#microtonal-accidental-signs"}
          title="Accidental Signs">Accidental Signs</a
        > page for more about {props.clairnoteName}'s microtonal accidentals.
      </p>
      <h2 class="title is-5" id="more">More Examples and Documentation</h2>
      <p>
        To use LilyPond you will need to refer to the{" "}
        <a
          href="http://www.lilypond.org/website/manuals.html"
          target="_blank"
          rel="external noopener noreferrer"
        >
          LilyPond manuals
        </a>{" "}
         which are quite comprehensive. See{" "}
        <a href={props.urlDir + "sheet-music/"}>Sheet Music</a> for more
        examples of music typeset in {props.clairnoteName} using LilyPond. The{
          " "
        }
        <a href="http://musicnotation.org/wiki/LilyPond" rel="external">
          LilyPond page
        </a>{" "}
         of the Music Notation Project Wiki offers some additional information
        about using LilyPond with alternative notation systems like {
          props.clairnoteName
        }.
      </p>
      <p>
        Please <a href={props.urlDir + "contact/"}>contact us</a> if you are
        interested in using LilyPond to create sheet music in{" "}
        {props.clairnoteName}. If there is a piece of music you would like to
        see in {props.clairnoteName}, we can help. See{" "}
        <a href={props.urlDir + "sheet-music/"}>Sheet Music</a> for more
        details.
      </p>
    </div>
  </article>
</Layout>
