---
import Layout from "../layouts/Layout.astro";
import type { PageComponentProps } from "../types/types";
import NextPageLink from "../components/NextPageLink.astro";

import AccidentalsGDn from "../images/svg-dn/Clairnote-accidentals-G.svg";
import AccidentalsGSn from "../images/svg-sn/Clairnote-accidentals-G.svg";

import AccidentalsGABDn from "../images/svg-dn/Clairnote-accidentals-G-A-B.svg";
import AccidentalsGABSn from "../images/svg-sn/Clairnote-accidentals-G-A-B.svg";

import AccidentalsFewerGGsADn from "../images/svg-dn/Clairnote-accidentals-fewer-G-Gs-A.svg";
import AccidentalsFewerGGsASn from "../images/svg-sn/Clairnote-accidentals-fewer-G-Gs-A.svg";

import AccidentalsFewerEChordsDn from "../images/svg-dn/Clairnote-accidentals-fewer-E-chords.svg";
import AccidentalsFewerEChordsSn from "../images/svg-sn/Clairnote-accidentals-fewer-E-chords.svg";

import AccidentalsNaturalsDn from "../images/svg-dn/Clairnote-accidentals-naturals.svg";
import AccidentalsNaturalsSn from "../images/svg-sn/Clairnote-accidentals-naturals.svg";

import AccidentalsDoubleSharpsFlatsDn from "../images/svg-dn/Clairnote-accidentals-double-sharps-flats.svg";
import AccidentalsDoubleSharpsFlatsSn from "../images/svg-sn/Clairnote-accidentals-double-sharps-flats.svg";

import AccidentalsChromaticScaleDn from "../images/svg-dn/Clairnote-accidentals-chromatic-scale.svg";
import AccidentalsChromaticScaleSn from "../images/svg-sn/Clairnote-accidentals-chromatic-scale.svg";

import AccidentalsQuarterToneSharpFlatGSn from "../images/svg-sn/Clairnote-accidentals-quarter-tone-sharp-flat-G.svg";
import AccidentalsQuarterToneSharpFlatGDn from "../images/svg-dn/Clairnote-accidentals-quarter-tone-sharp-flat-G.svg";

import AccidentalsQuarterToneSharpsGDn from "../images/svg-dn/Clairnote-accidentals-quarter-tone-sharps-G.svg";
import AccidentalsQuarterToneSharpsGSn from "../images/svg-sn/Clairnote-accidentals-quarter-tone-sharps-G.svg";

import AccidentalsQuarterToneFlatsGDn from "../images/svg-dn/Clairnote-accidentals-quarter-tone-flats-G.svg";
import AccidentalsQuarterToneFlatsGSn from "../images/svg-sn/Clairnote-accidentals-quarter-tone-flats-G.svg";

import AccidentalsQuarterToneSharpsFlatsEFDn from "../images/svg-dn/Clairnote-accidentals-quarter-tone-sharps-flats-E-F.svg";
import AccidentalsQuarterToneSharpsFlatsEFSn from "../images/svg-sn/Clairnote-accidentals-quarter-tone-sharps-flats-E-F.svg";

import AccidentalsQuarterToneScaleAscendingDn from "../images/svg-dn/Clairnote-accidentals-quarter-tone-scale-ascending.svg";
import AccidentalsQuarterToneScaleAscendingSn from "../images/svg-sn/Clairnote-accidentals-quarter-tone-scale-ascending.svg";

import AccidentalsQuarterToneScaleDescendingDn from "../images/svg-dn/Clairnote-accidentals-quarter-tone-scale-descending.svg";
import AccidentalsQuarterToneScaleDescendingSn from "../images/svg-sn/Clairnote-accidentals-quarter-tone-scale-descending.svg";

import pageStyles from "../styles/page.module.css";
import PageTitle from "../components/PageTitle.astro";
import PageFootnotes from "../components/PageFootnotes.astro";
import PageFootnoteLink from "../components/PageFootnoteLink.astro";
import LilyPondSvg from "../components/LilyPondSvg.astro";

type Props = PageComponentProps;
const props = Astro.props;

const { clairnoteName, dn, sn, urlDir } = props;

const title = "Accidental Signs";
const description = `Discussion and illustration of accidental signs in ${clairnoteName} music notation, where they simply indicate the note's traditional name and that it is an accidental.`;
---

<Layout title={title} description={description} {...props}>
  <article class="page">
    <PageTitle>{title}</PageTitle>
    <div>
      <p>
        {clairnoteName} has accidental signs that are similar to the standard
        sharp, flat and natural signs used in traditional music notation, but
        they work a little bit differently. Much like the standard accidental
        signs they indicate:
      </p>
      <ul>
        <li>That a note is an accidental (i.e. not in the current key).</li>
        <li>
          The note's name, whether it is a sharp, flat, natural, double sharp,
          double flat, etc.
        </li>
      </ul>
      <p>
        The difference is that unlike traditional accidental signs they do not
        indicate that the following note(s) should be played a semitone higher
        or lower (an essential instruction the musician must follow to play the
        right notes). Instead they effectively indicate that the following note
        has <em>already</em> been sharpened or flattened. They communicate the
        names of accidental notes but do not (typically) affect their pitch.<PageFootnoteLink
          num={1}
        />
        {" "}
         As such they play a very minimal role when it comes to reading and
        playing notes. For example, a piano or guitar player could ignore the
        accidental signs and still play the correct notes.
      </p>
      <p>Here is an illustration of accidental signs in both systems:</p>
      <div class={pageStyles.imageContainer}>
        <LilyPondSvg
          data={dn ? AccidentalsGDn : AccidentalsGSn}
          alt={"Sharp and Flat Accidentals in " +
            clairnoteName +
            " music notation"}
        />
      </div>
      <p>
        Since {clairnoteName}'s sharp and flat signs have a different meaning
        than traditional sharp and flat signs, new symbols are used — simple
        matchstick-style symbols pointing up for sharp or down for flat. These
        symbols are more visually subtle and take up less horizontal space than
        traditional accidental symbols.
      </p>
      <div class={pageStyles.imageContainer}>
        <LilyPondSvg
          data={dn ? AccidentalsGABDn : AccidentalsGABSn}
          alt={"Enharmonic accidentals in " + clairnoteName + " music notation"}
        />
      </div>
      <p>
        Above is an illustration of two pairs of enharmonically equivalent notes
        — notes like the black keys on a piano that have different names but
        have the same pitch (in 12-tone equal temperament<PageFootnoteLink
          num={1}
        />
        {" "}
         ). In {clairnoteName} it is much clearer that they have the same pitch.
        The alternative accidental signs simply indicate their different names
        and do not affect their pitch. (Differentiating between enharmonically
        equivalent notes using
        {clairnoteName}'s accidental signs and{" "}
        <a href={urlDir + "key-signatures/"}>Key Signatures</a> makes it
        possible to use the standard{" "}
        <a href={urlDir + "names-of-notes-and-intervals/"}>
          Names of Notes and Intervals
        </a>.<PageFootnoteLink num={2} />
        {" "}
         )
      </p>
      <p>
        {clairnoteName}'s accidental signs remain in effect until the end of the
        measure in which they appear, just like traditional accidental signs.
        Fewer accidental signs are needed because there are twelve staff
        positions per octave instead of seven. The following measure illustrates
        this point with its repeating melodic pattern of G, G-sharp, and A,
        where only the first G-sharp gets an accidental sign. Notice how{" "}
        {clairnoteName} uses less horizontal space because it requires fewer
        accidental signs.
      </p>
      <div class={pageStyles.imageContainer}>
        <LilyPondSvg
          data={dn ? AccidentalsFewerGGsADn : AccidentalsFewerGGsASn}
          alt={"Melody showing fewer accidental signs in " + clairnoteName}
        />
      </div>
      <p>
        Below is a similar illustration showing alternating E-minor and E-major
        chords (triads in{" "}
        <a
          href="https://en.wikipedia.org/wiki/Inversion_%28music%29"
          target="_blank"
          rel="noopener noreferrer"
        >
          second inversion
        </a>).
      </p>
      <div class={pageStyles.imageContainer}>
        <LilyPondSvg
          data={dn ? AccidentalsFewerEChordsDn : AccidentalsFewerEChordsSn}
          alt={"Chords showing fewer accidental signs in " + clairnoteName}
        />
      </div>
      <p>
        Traditional notation often requires accidental signs that cancel
        previous ones in a given measure, but this is very rare in Clairnote SN.
        For it to happen there would have to be two different enharmonically
        equivalent notes in the same measure. For example, F-sharp and G-flat,
        or E and F-flat.
      </p>
      <p>
        {clairnoteName} has natural signs that indicate natural notes that are
        outside of the current key, just as in traditional notation. For
        example, an F natural note in the key of G major (where F is sharp)
        would have a natural sign, as shown below.
        <PageFootnoteLink num={3} />{" "}
         (See also {clairnoteName}
         's <a href={urlDir + "key-signatures/"}>Key Signatures</a>
         .)
      </p>
      <div class={pageStyles.imageContainer}>
        <LilyPondSvg
          data={dn ? AccidentalsNaturalsDn : AccidentalsNaturalsSn}
          alt={"Natural signs in " + clairnoteName + " music notation"}
        />
      </div>
      <p>
        Double sharps and double flats are indicated by doubled sharp and flat
        symbols.
      </p>
      <div class={pageStyles.imageContainer}>
        <LilyPondSvg
          data={dn
            ? AccidentalsDoubleSharpsFlatsDn
            : AccidentalsDoubleSharpsFlatsSn}
          alt={"Double sharps and double flats in " +
            clairnoteName +
            " music notation"}
        />
      </div>
      <p>
        Here is how accidental signs are used to notate an ascending and
        descending chromatic scale in {clairnoteName}
         and traditional notation.
      </p>
      <div class={pageStyles.imageContainer}>
        <LilyPondSvg
          data={dn ? AccidentalsChromaticScaleDn : AccidentalsChromaticScaleSn}
          alt={"Ascending chromatic scale in " +
            clairnoteName +
            " with accidentals"}
        />
      </div>
      <h2 class="title is-5" id="microtonal-accidental-signs">
        Microtonal Accidental Signs
      </h2>
      <p>
        Like standard music notation, {clairnoteName} employs special microtonal
        accidental signs to
        <a
          href="https://en.wikipedia.org/wiki/Accidental_(music)#Microtonal_notation"
          target="_blank">notate</a
        >
        <a
          href="https://en.wikipedia.org/wiki/Microtone_(music)"
          target="_blank">microtonal music</a
        > that includes <a
          href="https://en.wikipedia.org/wiki/Quarter_tone"
          target="_blank">quarter tones</a
        >. For quarter-tone sharp and quarter-tone flat notes (also called
        half-sharp and half-flat), {clairnoteName} adopts the
        <a
          href="https://www.w3.org/2019/03/smufl13/tables/stein-zimmermann-accidentals-24-edo.html"
          target="_blank">Stein-Zimmerman</a
        > accidental signs that <a
          href="http://lilypond.org/doc/v2.25/Documentation/notation/accidentals"
          target="_blank">LilyPond</a
        > uses by default. These symbols work the same way in {clairnoteName} as
        they do in standard music notation, and they carry the same meaning:
        <i>play the following notes a quarter tone higher or lower.</i>
      </p>
      <div class={pageStyles.imageContainer}>
        <LilyPondSvg
          data={sn
            ? AccidentalsQuarterToneSharpFlatGSn
            : AccidentalsQuarterToneSharpFlatGDn}
          alt={`The notes G, G-half-sharp, and G-half-flat in ${clairnoteName} and traditional music notation.`}
        />
      </div>
      <p>
        However, {clairnoteName} introduces its own three-quarter-tone sharp and
        three-quarter-tone flat accidental signs (also called sharp-and-a-half
        and flat-and-a-half). These new signs are needed because the ones from
        standard notation would not make sense given the way sharp and flat
        notes are positioned on {clairnoteName}'s chromatic staff. Visually, the
        new signs combine {clairnoteName}'s sharp sign with its quarter-tone
        sharp sign and its flat sign with its quarter-tone flat sign.
        Semantically, they also combine the meanings of the two signs, namely:
      </p>
      <p>
        <ul>
          <li>
            <i
              >The following notes have already been raised or lowered by one
              semitone (two quarter tones sharp or flat).</i
            >
          </li>
          <li>
            <i>Play them an additional quarter tone higher or lower.</i>
          </li>
        </ul>
      </p>
      <div class={pageStyles.imageContainer}>
        <LilyPondSvg
          data={sn
            ? AccidentalsQuarterToneSharpsGSn
            : AccidentalsQuarterToneSharpsGDn}
          alt={`The notes G, G-half-sharp, G-sharp, G-sharp-and-a-half and G-double-sharp in ${clairnoteName} and traditional music notation.`}
        />
      </div>
      <div class={pageStyles.imageContainer}>
        <LilyPondSvg
          data={sn
            ? AccidentalsQuarterToneFlatsGSn
            : AccidentalsQuarterToneFlatsGDn}
          alt={`The notes G, G-half-flat, G-flat, G-flat-and-a-half and G-double-flat in ${clairnoteName} and traditional music notation.`}
        />
      </div>
      <p>
        These new "combined" accidental signs are similar to the corresponding
        Stein-Zimmerman signs used in standard notation which also combine the
        sharp and quarter-tone sharp signs and the flat and quarter-tone flat
        signs (visually and semantically).
      </p>
      <p>
        This system of distinct quarter-tone and three-quarter-tone accidental
        signs allows {clairnoteName} to distinguish between different microtonal
        notes that share the same staff position (and may or may not be
        enharmonically equivalent, depending on the tuning system). For example:
      </p>
      <div class={pageStyles.imageContainer}>
        <LilyPondSvg
          data={sn
            ? AccidentalsQuarterToneSharpsFlatsEFSn
            : AccidentalsQuarterToneSharpsFlatsEFDn}
          alt={`The notes F-half-sharp, E-sharp-and-a-half, E-half-flat and F-flat-and-a-half in ${clairnoteName} and traditional music notation.`}
        />
      </div>
      <p>
        An "F quarter-tone sharp" and an "E three-quarter-tone sharp" both share
        the same space on the staff but can be distinguished by their accidental
        sign. Similarly, an "E quarter-tone flat" and "F three-quarter-tone
        flat" can be distinguished by their accidental sign despite sharing the
        same staff line. Having distinct accidental signs also provides a
        one-to-one correspondence between the accidental sign and the note's
        name (which is consistent with Clairnote's other accidental signs).
      </p>
      <p>
        Here is how quarter-tone sharp and three-quarter-tone sharp accidental
        signs are used to notate an ascending quarter tone scale in
        {clairnoteName} and in traditional notation.
      </p>
      <div class={pageStyles.imageContainer}>
        <LilyPondSvg
          data={sn
            ? AccidentalsQuarterToneScaleAscendingSn
            : AccidentalsQuarterToneScaleAscendingDn}
          alt={`An ascending quarter tone scale in ${clairnoteName} and traditional music notation.`}
        />
      </div>
      <p>
        And here is how quarter-tone flat and three-quarter-tone flat accidental
        signs are used to notate a descending quarter tone scale.
      </p>
      <div class={pageStyles.imageContainer}>
        <LilyPondSvg
          data={sn
            ? AccidentalsQuarterToneScaleDescendingSn
            : AccidentalsQuarterToneScaleDescendingDn}
          alt={`A descending quarter tone scale in ${clairnoteName} and traditional music notation.`}
        />
      </div>
      <p>
        It is possible to use different quarter-tone accidental glyphs with {
          clairnoteName
        }. See the
        <a href={urlDir + "software/#custom-quarter-tone-accidental-signs"}
          >Software: LilyPond</a
        > page for details.
      </p>
      <NextPageLink
        title="Names of Notes and Intervals"
        href={urlDir + "names-of-notes-and-intervals/"}
      />
      <PageFootnotes>
        <ol>
          <li id="ftn1">
            This assumes the standard tuning system of 12-tone equal
            temperament. In other less commonly used tuning systems the pitch or
            intonation of "enharmonically equivalent" notes may differ slightly.
            In that case {clairnoteName}'s accidental signs and key signatures
            indicate these subtle shifts in pitch/intonation, as well as the
            different names of the notes. See the{" "}
            <a
              href="http://musicnotation.org/tutorials/enharmonic-equivalents/"
              target="_blank"
              rel="external noopener noreferrer"
            >
              Enharmonic Equivalents
            </a>{" "}
             tutorial on the Music Notation Project's site.{" "}
            <a class={"footnoteReturn"} href="#ftnref1"> Return</a>
          </li>
          <li id="ftn2">
            Even if someone has no need to distinguish enharmonic equivalents
            and uses an{" "}
            <a
              href="http://musicnotation.org/wiki/nomenclature/nomenclatures-overview/"
              target="_blank"
              rel="external noopener noreferrer"
            >
              alternative note naming system
            </a>{" "}
             that does not differentiate between them, {clairnoteName}'s
            accidental signs are still useful since they indicate which notes
            are outside of the current key. This reassures musicians that they
            are playing the correct note, even if it may sound out of place.
            However, musicians can always just ignore them or even omit them,
            for example if they are creating sheet music with{" "}
            <a title="Software: LilyPond" href={urlDir + "software/"}>
              LilyPond
            </a>
             . Omitting them may be especially appropriate for use with atonal
            or non-diatonic music.{" "}
            <a class={"footnoteReturn"} href="#ftnref2"> Return</a>
          </li>
          <li id="ftn3">
            A different approach would be to use the same "matchstick"
            accidental symbols for natural notes. For example, indicating that a
            note was raised from B-flat (in the key signature) to a B-natural
            (as an accidental outside the key) by using an upwards-pointing
            matchstick accidental sign rather than a natural sign. This would
            more directly indicate that such accidental notes have been lowered
            or raised. However, it makes it harder to determine the names of
            accidental notes since there would no longer be a one-to-one
            correspondence between the name of a note and its accidental sign.
            Since {clairnoteName}
             's accidental signs are largely about communicating the names of
            notes, it uses natural signs to indicate natural notes. Thus the
            note's name (sharp, flat, natural) is always directly indicated by
            the accidental sign.{" "}
            <a class={"footnoteReturn"} href="#ftnref3"> Return</a>
          </li>
        </ol>
      </PageFootnotes>
    </div>
  </article>
</Layout>
