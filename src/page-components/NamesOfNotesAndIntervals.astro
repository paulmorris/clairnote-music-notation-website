---
import PageTitle from "../components/PageTitle.astro";
import Layout from "../layouts/Layout.astro";
import NextPageLink from "../components/NextPageLink.astro";
import type { PageComponentProps } from "../types/types";

import NoteNamesWhiteKeyNotesDn from "../images/svg-dn/Clairnote-note-names-white-key-notes.svg";
import NoteNamesWhiteKeyNotesSn from "../images/svg-sn/Clairnote-note-names-white-key-notes.svg";

import NoteNamesBlackKeyNotesDn from "../images/svg-dn/Clairnote-note-names-black-key-notes.svg";
import NoteNamesBlackKeyNotesSn from "../images/svg-sn/Clairnote-note-names-black-key-notes.svg";
import PageFootnotes from "../components/PageFootnotes.astro";
import PageFootnoteLink from "../components/PageFootnoteLink.astro";

import pageStyles from "../styles/page.module.css";
import LilyPondSvg from "../components/LilyPondSvg.astro";

type Props = PageComponentProps;
const props = Astro.props;

const title = "Names of Notes and Intervals";
const description = `A discussion of using the standard names for notes and intervals with ${props.clairnoteName} music notation and the option of using alternative nomenclatures.`;
---

<Layout title={title} description={description} {...props}>
  <article class="page">
    <PageTitle>{title}</PageTitle>
    <div>
      <p>
        {props.clairnoteName} can be used with the standard names for notes (C, D,
        E, ...) and intervals (major 3rd, minor 3rd, ...). This offers practical
        benefits for musicians who want to, or may{" "}
        <em>need</em> to:
      </p>
      <ul>
        <li>Be able to easily communicate with other musicians</li>
        <li>Be fluent in the common language of music theory</li>
        <li>
          Learn to read traditional notation in addition to{" "}
          {props.clairnoteName}
        </li>
      </ul>
      <p>
        Of course, it is also possible to use {props.clairnoteName} with an alternative
        nomenclature instead.
        <PageFootnoteLink num={1} />
      </p>
      <div class={pageStyles.imageContainer}>
        <LilyPondSvg
          data={props.dn ? NoteNamesWhiteKeyNotesDn : NoteNamesWhiteKeyNotesSn}
          alt={"White-key notes in " + props.clairnoteName + " music notation"}
        />
      </div>
      <p>
        {props.clairnoteName}
        's{" "}
        <a title="Key Signatures" href={props.urlDir + "key-signatures/"}>
          key signatures
        </a>{" "}
        and{" "}
        <a title="Accidental Signs" href={props.urlDir + "accidental-signs/"}>
          accidental signs
        </a>{" "}
        make it possible to use the standard nomenclature. They clarify the names
        of{" "}
        <a
          href="http://musicnotation.org/tutorials/enharmonic-equivalents/"
          target="_blank"
          rel="external noopener noreferrer"
        >
          enharmonically equivalent
        </a>{" "}
        notes — notes that have the same pitch (in twelve-tone equal temperament)
        and the same position on the {props.clairnoteName}{" "}
        staff. A good example of these enharmonically equivalent notes are the black
        keys on a piano keyboard:
      </p>
      <div class={pageStyles.imageContainer}>
        <LilyPondSvg
          data={props.dn ? NoteNamesBlackKeyNotesDn : NoteNamesBlackKeyNotesSn}
          alt={"Black-key notes in " + props.clairnoteName + " music notation"}
        />
      </div>
      <p>
        Each of these "black-key" notes can have one of two different names,
        assuming no double-sharps or double-flats. We are not used to having
        notes on the staff that do not have a single primary name, but we are
        used to this when it comes to instruments, so it helps to think of it in
        those terms. The black keys on a piano have two "equally primary" names,
        as do the "black key" frets on the guitar, etc.
      </p>
      <p>
        When you see one of these "black-key" notes on the{" "}
        {props.clairnoteName} staff you would refer to the accidental sign (if there
        is one) or the key signature to determine the name of the note. This is very
        similar to how it works in traditional notation. To determine the name of
        any note on the traditional staff you always need to refer to the accidental
        sign (if there is one) and the key signature.
      </p>
      <p>
        The difference is that in {props.clairnoteName} you already know enough to
        play the note based on its staff position alone (without referring to an
        accidental sign or the key signature). Also, the only thing you have to remember
        about the key signature is whether it is a sharp key or flat key, since that
        is enough to tell you whether the note is a sharp or flat. This is because
        any given key may contain either sharp notes or flat notes but not both.
        For example, the key of D major (D E F# G A B C#) is a sharp key because
        it has two sharps. A musician playing in D major that sees an F# note will
        know that it is an F# and not a Gb because the key is a sharp key, as indicated
        by the key signature.
      </p>
      <p>
        See{" "}
        <a title="Key Signatures" href={props.urlDir + "key-signatures/"}>
          key signatures
        </a>{" "}
        and{" "}
        <a title="Accidental Signs" href={props.urlDir + "accidental-signs/"}>
          accidental signs
        </a>{" "}
        for more details.
      </p>
      <p>
        Thanks to {props.clairnoteName}
        's accidental signs and key signatures, {props.clairnoteName} can be used
        with the standard nomenclature and convey all of the information conveyed
        by traditional notation. This includes differentiating between enharmonic
        equivalents. Whether that differentiation is relevant or not is a matter
        of long-standing debate. Fortunately, anyone can use {
          props.clairnoteName
        } no matter which side of that debate they are on.
      </p>

      <NextPageLink
        title="Rhythm Notation"
        href={props.urlDir + "rhythm-notation/"}
      />

      <PageFootnotes>
        <ol>
          <li id="ftn1">
            There are various{" "}
            <a
              href="http://musicnotation.org/wiki/Nomenclatures_Overview"
              target="_blank"
              rel="external noopener noreferrer"
            >
              alternative nomenclatures
            </a>{" "}
            that re-name the notes and intervals using a simpler twelve-notes-per-octave
            approach, and eliminate distinctions between “enharmonically equivalent”
            notes and intervals. This entails a revision and simplification of much
            of traditional music theory as well. {props.clairnoteName} can be used
            with such an alternative nomenclature by simply ignoring or removing
            the key signatures and accidental signs that indicate the traditional
            names of notes. Distinctions between “enharmonic equivalents” would no
            longer be necessary since there would always be a simple one-to-one correspondence
            between each note’s name and its visual representation on the staff.{
              " "
            }
            {props.clairnoteName} would work particularly well with an alternative
            nomenclature that highlights the{" "}
            <a title="6-6 Pitch Pattern" href={props.urlDir + "staff/#six-six"}>
              6-6 pitch pattern
            </a>{" "}
            (like Dan Lindgren's{" "}
            <a
              href="http://nydana.se/salata.pdf"
              target="_blank"
              rel="external noopener noreferrer"
            >
              SaLaTa
            </a>
            ).{" "}
            <a class={"footnoteReturn"} href="#ftnref1"> Return</a>
          </li>
        </ol>
      </PageFootnotes>
    </div>
  </article>
</Layout>
