// @ts-ignore
import { mutopiaItems } from "./data-mutopia-items";
// @ts-ignore
import { mutopiaComposerLookup } from "./data-mutopia-composer-lookup";
// @ts-ignore
import { sessionItems } from "./data-session-items";
// @ts-ignore
import { miscItems } from "./data-misc-items";
import {
  idPrefixIsMisc,
  idPrefixIsMutopia,
  idPrefixIsSession,
  removeIdPrefix,
} from "./utils";

import musicStyles from "./sheet-music.module.css";
import { For } from "solid-js";

type ClairnoteType = "sn" | "dn";

type LicenseCode = "pd0" | "by3" | "by4" | "by-sa3" | "by-sa4";

const licenseLookup: Record<
  LicenseCode,
  { name: string; shortName: string; url: string }
> = {
  pd0: {
    name: "Public Domain",
    shortName: "PD",
    url: "http://creativecommons.org/publicdomain/zero/1.0/",
  },
  by3: {
    name: "Creative Commons Attribution 3.0",
    shortName: "CC BY 3.0",
    url: "http://creativecommons.org/licenses/by/3.0/",
  },
  by4: {
    name: "Creative Commons Attribution 4.0",
    shortName: "CC BY 4.0",
    url: "http://creativecommons.org/licenses/by/4.0/",
  },
  "by-sa3": {
    name: "Creative Commons Attribution-ShareAlike 3.0",
    shortName: "CC BY-SA 3.0",
    url: "http://creativecommons.org/licenses/by-sa/3.0/",
  },
  "by-sa4": {
    name: "Creative Commons Attribution-ShareAlike 4.0",
    shortName: "CC BY-SA 4.0",
    url: "http://creativecommons.org/licenses/by-sa/4.0/",
  },
};

const getSessionResult = (id: string, clairnoteType: ClairnoteType) => {
  // undefinedFive,
  // licenseId,
  const [title, style, fileName, settingId, settings]: [
    string,
    string,
    string,
    number,
    number[]
  ] = sessionItems[id];

  // fileName does not include the extension
  const path = `https://session-sheet-music.clairnote.org/${fileName}`;

  /* const licenseData = licenseLookup[licenseId] */

  const settingLines = settings.map((setnum: number) => {
    const settingPath = path + "-" + setnum;
    const pathWithType = settingPath + "-" + clairnoteType;

    return (
      <div class={musicStyles.resultsLine}>
        <span class={musicStyles.resultsLineItem}>{"Setting " + setnum}</span>
        <span class={musicStyles.resultsLineItem}>
          <a
            href={pathWithType + "-let.pdf"}
            title={"Letter Size PDF File"}
            target="_blank"
          >
            PDF Letter
          </a>
        </span>
        <span class={musicStyles.resultsLineItem}>
          <a
            href={pathWithType + "-a4.pdf"}
            title={"A4 Size PDF File"}
            target="_blank"
          >
            PDF A4
          </a>
        </span>
        <span class={musicStyles.resultsLineItem}>
          <a href={settingPath + ".mid"} title={"MIDI File"} target="_blank">
            MIDI
          </a>
        </span>
        <span class={musicStyles.resultsLineItem}>
          <a href={settingPath + ".ly"} title={"LilyPond File"} target="_blank">
            LY
          </a>
        </span>
      </div>
    );
  });

  const source = (
    <a
      href={`https://thesession.org/tunes/${id}#setting${settingId}`}
      title={'Source on "The Session" website'}
      target="_blank"
    >
      Source: The Session
    </a>
  );

  return (
    <li class={musicStyles.searchResult}>
      <div>
        <strong>{title}</strong>
      </div>
      <div class={musicStyles.resultsLine}>{style}</div>
      {settingLines}
      <div class={musicStyles.resultsLine}>{source}</div>
    </li>
  );
};

const getMutopiaResult = (id: string, clairnoteType: ClairnoteType) => {
  const [
    style,
    composerId,
    title,
    instruments,
    shortPath,
    fileName,
    licenseId,
    opus,
    words, // poet/lyricist
    date,
    arrangement,
  ]: [
    string,
    string,
    string,
    string[],
    string,
    string,
    LicenseCode,
    string,
    string,
    string,
    string
  ] = mutopiaItems[id];
  const pathToDir = `https://mutopia-sheet-music.clairnote.org/${shortPath}/`;

  const path = fileName ? pathToDir + fileName : pathToDir;
  const composer = mutopiaComposerLookup[composerId][0];

  const pathWithType = path + "-" + clairnoteType;
  const licenseData = licenseLookup[licenseId];

  const titleLine = title + (opus.trimStart() ? " (" + opus + ")" : "");

  // " Traditional" ends up with an extra space in front because it has no
  // first initial.
  const composerItem =
    composer === " Traditional" ? composer.trimStart() : "By " + composer;

  const instrumentItem = "For " + instruments.join(", ");

  const styleItem = (
    <>
      <span>{style}</span>
      {date ? <span>{`, ${date}`}</span> : undefined}
      {words ? <span>{`, Words: ${words}`}</span> : undefined}
      {arrangement ? <span>{`, Arrangement: ${arrangement}`}</span> : undefined}
    </>
  );

  const links = fileName ? (
    <>
      <span class={musicStyles.resultsLineItem}>
        <a
          href={pathWithType + "-let.pdf"}
          title={"Letter Size PDF File"}
          target="_blank"
        >
          PDF Letter
        </a>
      </span>
      <span class={musicStyles.resultsLineItem}>
        <a
          href={pathWithType + "-a4.pdf"}
          title={"A4 Size PDF File"}
          target="_blank"
        >
          PDF A4
        </a>
      </span>
      <span class={musicStyles.resultsLineItem}>
        <a href={path + ".mid"} title={"MIDI File"} target="_blank">
          MIDI
        </a>
      </span>
      <span class={musicStyles.resultsLineItem}>
        <a href={path + ".ly"} title={"LilyPond File"} target="_blank">
          LY
        </a>
      </span>
    </>
  ) : (
    <span class={musicStyles.resultsLineItem}>
      <a
        href={pathToDir}
        title={"This work involves multiple PDF, MIDI, and/or LilyPond Files"}
        target="_blank"
      >
        PDF, MIDI, and LY Files
      </a>
    </span>
  );

  const source = (
    <span class={musicStyles.resultsLineItem}>
      <a
        href={"http://www.mutopiaproject.org/cgibin/piece-info.cgi?id=" + id}
        title={'Source on the "Mutopia Project" website'}
        target="_blank"
      >
        Source: Mutopia Project
      </a>
    </span>
  );

  const license = (
    <span class={musicStyles.resultsLineItem}>
      <a href={licenseData.url} title={licenseData.name} target="_blank">
        {licenseData.shortName}
      </a>
    </span>
  );

  return (
    <li class={musicStyles.searchResult}>
      <div>
        <strong>{titleLine}</strong>
      </div>
      <div class={musicStyles.resultsLine}>
        <div class={musicStyles.resultsLineItem}>{composerItem}</div>
        <div class={musicStyles.resultsLineItem}>{instrumentItem}</div>
        <div class={musicStyles.resultsLineItem}>{styleItem}</div>
      </div>
      <div class={musicStyles.resultsLine}>
        {links}
        {license}
      </div>
      <div class={musicStyles.resultsLine}>{source}</div>
    </li>
  );
};

const getMiscResult = (id: string, clairnoteType: ClairnoteType) => {
  const path = "https://sheet-music.clairnote.org/";
  const [title, fileNameLetter, fileNameA4, lyUrlPart, source, description]: [
    string,
    string,
    string,
    string,
    string,
    string
  ] = miscItems[id];

  const letterUrl = `${path}${fileNameLetter}-${clairnoteType}-let.pdf`;
  const a4Url = `${path}${fileNameA4}-${clairnoteType}-a4.pdf`;
  const lyUrl = lyUrlPart.slice(0, 4) === "http" ? lyUrlPart : path + lyUrlPart;

  return (
    <li class={musicStyles.searchResult}>
      <div>
        <strong>{title}</strong>
      </div>
      {description && <div>{description}</div>}
      <div class={musicStyles.resultsLine}>
        {fileNameLetter && (
          <span class={musicStyles.resultsLineItem}>
            <a href={letterUrl} title="Letter Size PDF File" target="_blank">
              PDF Letter
            </a>
          </span>
        )}
        {fileNameA4 && (
          <span class={musicStyles.resultsLineItem}>
            <a href={a4Url} title="A4 Size PDF File" target="_blank">
              PDF A4
            </a>
          </span>
        )}
        {lyUrlPart && (
          <span class={musicStyles.resultsLineItem}>
            <a href={lyUrl} title="LilyPond File(s)" target="_blank">
              LY
            </a>
          </span>
        )}
        {source && (
          <span class={musicStyles.resultsLineItem}>
            <a href={source} title="Source" target="_blank">
              Source
            </a>
          </span>
        )}
      </div>
    </li>
  );
};

export const SearchResults = (props: {
  resultIdsToShow: string[];
  currentCollectionName: string;
  clairnoteType: ClairnoteType;
}) => {
  return (
    <ul class={musicStyles.searchResults}>
      <For
        each={props.resultIdsToShow}
        fallback={
          <li class={musicStyles.searchResult}>
            No results found in {props.currentCollectionName} collection.
          </li>
        }
      >
        {(id) => {
          const idNoPrefix = removeIdPrefix(id);

          if (idPrefixIsMutopia(id)) {
            return getMutopiaResult(idNoPrefix, props.clairnoteType);
          } else if (idPrefixIsSession(id)) {
            return getSessionResult(idNoPrefix, props.clairnoteType);
          } else if (idPrefixIsMisc(id)) {
            return getMiscResult(idNoPrefix, props.clairnoteType);
          }
          // Should never happen.
          return null;
        }}
      </For>
    </ul>
  );
};
