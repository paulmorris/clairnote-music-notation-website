// This file is used to pre-build the lunr index for searching all sheet music.
// It is not runtime code.

// Adapted from the script here:
// https://lunrjs.com/guides/index_prebuilding.html

// Use this file like this:
// $ node build-index-all.mjs > index-all.js

// Use .mjs file extension so node can use ECMAScript modules.
import { mutopiaItems } from "./data-mutopia-items.mjs";
import { mutopiaComposerLookup } from "./data-mutopia-composer-lookup.mjs";
import { sessionItems } from "./data-session-items.mjs";
import { miscItems } from "./data-misc-items.mjs";
import lunr from "lunr";

// NOTE: must be kept in sync with definitions in `./utils.tsx`.
const addIdPrefixMutopia = (id) => "m" + id;
const addIdPrefixSession = (id) => "s" + id;
const addIdPrefixMisc = (id) => "i" + id;

const indexAll = lunr(function () {
  this.ref("id");

  // "t" for "title". Use a single letter to reduce index file size.
  this.field("t", { boost: 10 });

  // "d" for "document", basically a string of all searchable words.
  this.field("d");

  Object.entries(mutopiaItems).forEach(([id, item]) => {
    const composerSlug = item[1];
    const title = item[2];
    const style = item[0];
    const composer = mutopiaComposerLookup[composerSlug][0];
    const instruments = item[3].join(" ");
    const opus = item[7];
    const poet = item[8];
    const date = item[9];
    const arranger = item[10];
    const doc = [style, composer, instruments, opus, poet, date, arranger].join(
      " "
    );
    this.add({
      id: addIdPrefixMutopia(id),
      t: title,
      d: doc,
    });
  });

  Object.entries(sessionItems).forEach(([id, item]) => {
    const [title, meter] = item;
    this.add({
      id: addIdPrefixSession(id),
      t: title,
      d: meter,
    });
  });

  Object.entries(miscItems).forEach(([id, item]) => {
    const title = item[0];
    const description = item[5];
    this.add({
      id: addIdPrefixMisc(id),
      t: title,
      d: description,
    });
  });
});

const json = JSON.stringify(indexAll);

process.stdout.write(`export const indexAll = ${json};`);
