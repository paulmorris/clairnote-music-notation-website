import { For, Show } from "solid-js";
import styles from "./sheet-music.module.css";

export const Pagination = (props: {
  navigateOnParamChange: () => void;
  pageNumber: number;
  setPageNumber: (n: number) => void;
  paginationNumbers: number[];
}) => {
  const navigateToNewPage = (newPage: number) => {
    props.setPageNumber(newPage);
    props.navigateOnParamChange();
    window.scrollTo(0, 0);
  };

  return (
    <ul>
      <Show when={props.pageNumber > 1}>
        <li class={styles.paginationItem}>
          <button
            class={styles.paginationLink}
            onClick={() => navigateToNewPage(props.pageNumber - 1)}
          >
            Previous
          </button>
        </li>
      </Show>

      <For each={props.paginationNumbers}>
        {(paginationNumber) => (
          <li class={styles.paginationItem}>
            {paginationNumber === props.pageNumber ? (
              <button class={styles.paginationLinkCurrent}>
                {paginationNumber}
              </button>
            ) : (
              <button
                class={styles.paginationLink}
                onClick={() => navigateToNewPage(paginationNumber)}
              >
                {paginationNumber}
              </button>
            )}
          </li>
        )}
      </For>

      <Show when={props.pageNumber !== props.paginationNumbers.at(-1)}>
        <li class={styles.paginationItem}>
          <button
            class={styles.paginationLink}
            onClick={() => navigateToNewPage(props.pageNumber + 1)}
          >
            Next
          </button>
        </li>
      </Show>
    </ul>
  );
};
