// @ts-ignore
import { miscItems } from "./data-misc-items";

export const miscIdsSorted: string[] = Object.keys(miscItems);
