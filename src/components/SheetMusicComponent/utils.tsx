// To allow a single lunr search index with different data sources,
// we use prefixes on the IDs in the index, which allows us to trace back
// from search results to the data sources.
// It's not pretty, and should likely be redone when and if we ever get time
// to consolidate the data into one bucket. On the other hand, different data
// in separate buckets also has its benefits.

// NOTE: must be kept in sync with definitions in `build-index-all.mjs`.
// TODO: figure out importing .mjs code into TypeScript code to de-duplicate.
export const addIdPrefixMutopia = (id: string): string => "m" + id;
export const addIdPrefixSession = (id: string): string => "s" + id;
export const addIdPrefixMisc = (id: string): string => "i" + id;

export const idPrefixIsMutopia = (id: string): boolean => id[0] === "m";
export const idPrefixIsSession = (id: string): boolean => id[0] === "s";
export const idPrefixIsMisc = (id: string): boolean => id[0] === "i";

export const removeIdPrefix = (id: string): string => id.slice(1);
