import {
  FilterSelect,
  mutopiaInstruments,
  mutopiaStyles,
  mutopiaComposersOptions,
  sessionMeters,
} from "./filters";
import { SearchResults } from "./results";
import { Pagination } from "./pagination";
import type { PageComponentProps } from "../../types/types";
import { iota, transformSearchQuery } from "../../utils/utils";

import styles from "./sheet-music.module.css";
import { Show, createSignal, onMount } from "solid-js";
import lunr from "lunr";

// @ts-ignore
import { mutopiaItems } from "./data-mutopia-items";
import { mutopiaIdsSorted } from "./data-mutopia-ids-sorted";

// @ts-ignore
import { sessionItems } from "./data-session-items";
import { sessionIdsSorted } from "./data-session-ids-sorted";

import { miscIdsSorted } from "./data-misc-ids-sorted";

// @ts-ignore
import { indexAll } from "./index-all";

import {
  mutopiaComposersMap,
  mutopiaInstrumentsMap,
  mutopiaStylesMap,
  sessionMetersMap,
} from "./filters";

import {
  addIdPrefixMisc,
  addIdPrefixMutopia,
  addIdPrefixSession,
  idPrefixIsMisc,
  idPrefixIsMutopia,
  idPrefixIsSession,
  removeIdPrefix,
} from "./utils";

type Collection = "all" | "mutopia" | "session" | "misc";

const indexAllLoaded = lunr.Index.load(indexAll);

const debounce = (callback: any, wait: number) => {
  let timeoutId: any = null;
  return (...args: any) => {
    clearTimeout(timeoutId);
    timeoutId = setTimeout(() => {
      callback(...args);
    }, wait);
  };
};

const parseSearchInput = (queryInput: string) => {
  return (
    queryInput
      // Remove leading and trailing spaces.
      .trim()
      // Remove any wildcard characters.
      .replace(/\*/g, "")
      .toLowerCase()
      // Split by whitespaces which removes all whitespace.
      .split(/\s+/)
      // Remove single letters but keep single digits (0-9).
      .filter((term) => term.length > 1 || !isNaN(parseInt(term)))
      // Re-assemble into a string so computed can easily do an equality check.
      .join(" ")
  );
};

export default function SheetMusicComponent(props: PageComponentProps) {
  const resultsPerPage = 10;

  // PRIMARY STATE

  const [collection, setCollection] = createSignal<Collection>("all");

  const [searchInput, setSearchInput] = createSignal("");
  const [searchQuery, setSearchQuery] = createSignal("");

  const [pageNumber, setPageNumber] = createSignal(1);

  const [filterInstrument, setFilterInstrument] =
    createSignal("any-instrument");
  const [filterComposer, setFilterComposer] = createSignal("any-composer");
  const [filterStyle, setFilterStyle] = createSignal("any-style");
  const [filterSessionMeter, setFilterSessionMeter] = createSignal("any-meter");

  const [mounted, setMounted] = createSignal(false);

  // DERIVED STATE

  const resultIdsAll = () => {
    const query = searchQuery();
    const queryTerms = query ? query.split(" ") : [];

    if (queryTerms.length === 0) {
      // All results (browse mode).
      return [
        ...mutopiaIdsSorted.map(addIdPrefixMutopia),
        ...sessionIdsSorted.map(addIdPrefixSession),
        ...miscIdsSorted.map(addIdPrefixMisc),
      ];
    }

    const wildcardOptions = {
      wildcard: lunr.Query.wildcard.LEADING | lunr.Query.wildcard.TRAILING,
    };

    // Do an "all term" search that uses "or" logic.
    // The search includes both full terms (with fuzzy matching for longer
    // terms), and wildcard terms.
    const allTermResultIds: string[] = indexAllLoaded
      .query((q) => {
        queryTerms.forEach((queryTerm) => {
          q.term(queryTerm, { editDistance: queryTerm.length > 5 ? 1 : 0 });
          q.term(queryTerm, wildcardOptions);
        });
      })
      .map((item) => item.ref);

    if (queryTerms.length === 1) {
      // If there's only one search term we are already done.
      return allTermResultIds;
    }

    // Do a "single term" search for each term.
    const singleTermResultIdSets: Set<string>[] = queryTerms.map(
      (queryTerm) => {
        const results = indexAllLoaded.query((q) => {
          q.term(queryTerm, { editDistance: queryTerm.length > 5 ? 1 : 0 });
          q.term(queryTerm, wildcardOptions);
        });
        return new Set(results.map((item) => item.ref));
      }
    );

    // Return the intersection ("and" logic) of each single term search,
    // in the same order as the all term search results.
    const intersectionIds: string[] = allTermResultIds.filter((item) => {
      return singleTermResultIdSets.every((set) => set.has(item));
    });

    return intersectionIds;
  };

  const resultIdsMutopia = () => {
    const ids1 = resultIdsAll().filter(idPrefixIsMutopia);

    const instrumentLabel = mutopiaInstrumentsMap.get(filterInstrument());
    const ids2 =
      !instrumentLabel || filterInstrument() === "any-instrument"
        ? ids1
        : ids1.filter((id) =>
            mutopiaItems[removeIdPrefix(id)][3].includes(instrumentLabel)
          );

    const styleLabel = mutopiaStylesMap.get(filterStyle());
    const ids3 =
      !styleLabel || filterStyle() === "any-style"
        ? ids2
        : ids2.filter(
            (id) => mutopiaItems[removeIdPrefix(id)][0] === styleLabel
          );

    const composerMutopiaId = mutopiaComposersMap.get(filterComposer());
    const ids4 =
      !composerMutopiaId || filterComposer() === "any-composer"
        ? ids3
        : ids3.filter(
            (id) => mutopiaItems[removeIdPrefix(id)][1] === composerMutopiaId
          );
    return ids4;
  };

  const resultIdsSession = () => {
    const ids = resultIdsAll().filter(idPrefixIsSession);
    const meterLabel = sessionMetersMap.get(filterSessionMeter());

    return !meterLabel || filterSessionMeter() === "any-meter"
      ? ids
      : ids.filter((id) => sessionItems[removeIdPrefix(id)][1] === meterLabel);
  };

  const resultIdsMisc = () => {
    const ids = resultIdsAll().filter(idPrefixIsMisc);
    return ids;
  };

  const resultIdsForCurrentCollection = () => {
    const currentCollection = collection();

    return currentCollection === "all"
      ? resultIdsAll()
      : currentCollection === "mutopia"
      ? resultIdsMutopia()
      : currentCollection === "session"
      ? resultIdsSession()
      : currentCollection === "misc"
      ? resultIdsMisc()
      : // Should never happen.
        [];
  };

  const resultIdsToShow = () => {
    const to = pageNumber() * resultsPerPage;
    const from = to - resultsPerPage;
    const ids = resultIdsForCurrentCollection().slice(from, to);
    return ids;
  };

  const pagesCount = () => {
    const totalResults = resultIdsForCurrentCollection().length;
    const pagesCount = Math.ceil(totalResults / resultsPerPage);
    return pagesCount;
  };

  const paginationNumbers = () => {
    const current = pageNumber();
    const count = pagesCount();

    const numbers =
      current < 7 // near start?
        ? iota(count < 10 ? count : 10, 1) // near start.
        : count - 5 < current // near end?
        ? iota(10, count - 9) // near end.
        : iota(10, current - 5); // in middle.

    return numbers;
  };

  const currentCollectionName = () => {
    const current = collection();
    return current === "mutopia"
      ? "the Mutopia Project"
      : current === "session"
      ? "the Session"
      : "";
  };

  const currentCollectionUrl = () => {
    const current = collection();
    return current === "mutopia"
      ? "http://www.mutopiaproject.org/"
      : current === "session"
      ? "https://thesession.org/"
      : "";
  };

  const urlParamsFromState = () => {
    const params = new URLSearchParams();

    const q = transformSearchQuery(searchQuery());
    if (q) {
      params.set("q", q);
    }

    const currentCollection = collection();

    if (currentCollection !== "all") {
      params.set("collection", currentCollection);
    }

    if (currentCollection === "mutopia") {
      const style = filterStyle();
      const instrument = filterInstrument();
      const composer = filterComposer();
      if (instrument && instrument !== "any-instrument") {
        params.set("instrument", instrument);
      }
      if (style && style !== "any-style") {
        params.set("style", style);
      }
      if (composer && composer !== "any-composer") {
        params.set("composer", composer);
      }
    }

    if (currentCollection === "session") {
      const meter = filterSessionMeter();
      if (meter && meter !== "any-meter") {
        params.set("meter", meter);
      }
    }

    const page = pageNumber();
    if (page && page > 1) {
      params.set("page", String(page));
    }

    return params;
  };

  // EFFECTS / SETUP

  const setStateFromParams = (params: URLSearchParams) => {
    const q = params?.get("q");
    setSearchInput(q || "");
    setSearchQuery(q ? parseSearchInput(q) : "");

    // TODO: predicate functions to ensure valid values?

    const urlCollection = params.get("collection") || "all";
    setCollection(urlCollection as Collection);

    setFilterInstrument(
      (urlCollection === "mutopia" && params.get("instrument")) ||
        "any-instrument"
    );

    setFilterStyle(
      (urlCollection === "mutopia" && params.get("style")) || "any-style"
    );

    setFilterComposer(
      (urlCollection === "mutopia" && params.get("composer")) || "any-composer"
    );

    setFilterSessionMeter(
      (urlCollection === "session" && params.get("meter")) || "any-meter"
    );

    // The parsed page number could be NaN which is falsy.
    const urlPage = parseInt(params.get("page") || "1", 10);
    setPageNumber(urlPage && urlPage > 1 ? urlPage : 1);
  };

  /**
   * Hande popstate event that is fired when user clicks back/forward buttons.
   */
  const popstateListener = () => {
    setTimeout(() => {
      const params = new URLSearchParams(window.location.search);
      setStateFromParams(params);
    }, 0);
  };

  onMount(() => {
    window.addEventListener("popstate", popstateListener);

    const params = new URLSearchParams(window.location.search);
    setStateFromParams(params);

    setMounted(true);
  });

  // ACTIONS / EVENT HANDLERS

  /**
   * Utility to push a new URL onto browser history stack when params change.
   */
  const navigateOnParamChange = () => {
    const params = String(urlParamsFromState());

    const path = params.length
      ? `${window.location.pathname}?${params}`
      : window.location.pathname;

    history.pushState(null, "", path);
  };

  /**
   * Debounced setter for the search query so the user finishes typing before
   * the search is performed.
   */
  const setSearchQueryDebounced = debounce((input: string) => {
    const query = parseSearchInput(input);
    setPageNumber(1);
    setSearchQuery(query);
    navigateOnParamChange();
  }, 500);

  /**
   * Sets the content of the search input field, and starts or resets debounce.
   */
  const handleSearchChange = (query: string) => {
    setSearchInput(query);
    // Don't change the URL yet, wait for debounced function to do that.
    setSearchQueryDebounced(query);
  };

  const handleCollectionChange = (collection: Collection) => {
    // Reset all filters when the collection changes.
    setFilterInstrument("any-instrument");
    setFilterComposer("any-composer");
    setFilterStyle("any-style");
    setFilterSessionMeter("any-meter");
    setPageNumber(1);
    setCollection(collection);
    navigateOnParamChange();
  };

  return (
    <div class={styles.sheetMusicWrapper}>
      <div>
        <div class="field has-addons">
          <div class="control is-expanded">
            <input
              id="searchBox"
              class="input"
              type="text"
              placeholder="Search for sheet music"
              name="query"
              value={searchInput()}
              onInput={(e) => handleSearchChange(e.currentTarget.value)}
            />
          </div>
          <div class="control">
            <button
              class="button"
              id="search-button"
              value="Clear"
              onClick={() => handleSearchChange("")}
            >
              ✕ {/* &#x2715 */}
            </button>
          </div>
        </div>

        <div class={styles.collectionSelectMenuContainer}>
          <div class="select is-small">
            <select
              title="Filter by Collection"
              onInput={(event) => {
                event.preventDefault();
                handleCollectionChange(event.currentTarget.value as Collection);
              }}
            >
              <option value="all" selected={"all" === collection()}>{`All (${
                resultIdsAll().length
              })`}</option>
              <option
                value="mutopia"
                selected={"mutopia" === collection()}
              >{`Mutopia Project Collection (${
                resultIdsMutopia().length
              })`}</option>
              <option
                value="session"
                selected={"session" === collection()}
              >{`The Session Collection (${
                resultIdsSession().length
              })`}</option>
              <option
                value="misc"
                selected={"misc" === collection()}
              >{`Miscellaneous Collection (${resultIdsMisc().length})`}</option>
            </select>
          </div>
        </div>
      </div>

      <div class={styles.searchFilterUi}>
        <Show when={mounted()}>
          <Show when={collection() === "all"}>
            <div>
              Sheet music from various sources converted to{" "}
              {props.clairnoteName} with LilyPond. See{" "}
              <a href={props.urlDir + "about-sheet-music/"}>
                About Sheet Music
              </a>
              .
            </div>
            <div class={styles.collectionDescription} />
          </Show>

          <Show when={collection() === "mutopia"}>
            <div>
              <FilterSelect
                name="Filter by Instrument"
                title="Filter by Instrument"
                filterSetter={(value) => {
                  setPageNumber(1);
                  setFilterInstrument(value);
                  navigateOnParamChange();
                }}
                value={filterInstrument()}
                options={mutopiaInstruments}
              />
              <FilterSelect
                name="Filter by Style"
                title="Filter by Style"
                filterSetter={(value) => {
                  setPageNumber(1);
                  setFilterStyle(value);
                  navigateOnParamChange();
                }}
                value={filterStyle()}
                options={mutopiaStyles}
              />
              <FilterSelect
                name="Filter by Composer"
                title="Filter by Composer"
                filterSetter={(value) => {
                  setPageNumber(1);
                  setFilterComposer(value);
                  navigateOnParamChange();
                }}
                value={filterComposer()}
                options={mutopiaComposersOptions}
              />
            </div>
            <div class={styles.collectionDescription}>
              Sheet music from{" "}
              <a
                href={currentCollectionUrl()}
                target="_blank"
                rel="noopener noreferrer"
              >
                {currentCollectionName()}
              </a>
              , converted to {props.clairnoteName} with LilyPond. See{" "}
              <a href={props.urlDir + "about-sheet-music/"}>
                About Sheet Music
              </a>
              . Filter search results by instrument, style, and/or composer.
            </div>
          </Show>

          <Show when={collection() === "session"}>
            <div>
              <FilterSelect
                name="Filter by Meter"
                title="Filter by Meter"
                filterSetter={(value) => {
                  setPageNumber(1);
                  setFilterSessionMeter(value);
                  navigateOnParamChange();
                }}
                value={filterSessionMeter()}
                options={sessionMeters}
              />
            </div>
            <div class={styles.collectionDescription}>
              Sheet music from{" "}
              <a
                target="_blank"
                rel="noopener noreferrer"
                href={currentCollectionUrl()}
              >
                {currentCollectionName()}
              </a>
              , converted to {props.clairnoteName} with LilyPond. See{" "}
              <a href={props.urlDir + "about-sheet-music/"}>
                About Sheet Music
              </a>
              . Filter search results by meter.
            </div>
          </Show>

          <Show when={collection() === "misc"}>
            <div class={styles.collectionDescription}>
              Sheet music from various sources converted to{" "}
              {props.clairnoteName} with LilyPond. See{" "}
              <a href={props.urlDir + "about-sheet-music/"}>
                About Sheet Music
              </a>
              .
            </div>
          </Show>

          <div>
            {`${resultIdsForCurrentCollection().length} result${
              resultIdsForCurrentCollection().length === 1 ? "" : "s"
            }`}
            {pagesCount() > 1 && `, page ${pageNumber()} of ${pagesCount()}`}
          </div>
        </Show>
      </div>

      <Show
        when={mounted()}
        fallback={<div style={{ "font-style": "italic" }}>Loading...</div>}
      >
        <SearchResults
          resultIdsToShow={resultIdsToShow()}
          currentCollectionName={currentCollectionName()}
          clairnoteType={props.dn ? "dn" : "sn"}
        />
        <Show when={paginationNumbers().length > 1}>
          <Pagination
            navigateOnParamChange={navigateOnParamChange}
            pageNumber={pageNumber()}
            setPageNumber={setPageNumber}
            paginationNumbers={paginationNumbers()}
          />
        </Show>
      </Show>
    </div>
  );
}
