// This rule is going away soon.
// https://github.com/jsx-eslint/eslint-plugin-jsx-a11y/issues/398
/* eslint-disable jsx-a11y/no-onchange */

import { For } from "solid-js";
import styles from "./sheet-music.module.css";

type OptionItem = [string, string];

export const mutopiaInstruments: OptionItem[] = [
  ["any-instrument", "Any Instrument"],
  ["bass", "Bass"],
  ["basso-continuo", "Basso-continuo"],
  ["bassoon", "Bassoon"],
  ["brass-ensemble", "Brass-ensemble"],
  ["cello", "Cello"],
  ["choir", "Choir"],
  ["clarinet", "Clarinet"],
  ["clavier", "Clavier"],
  ["double-bass", "Double-bass"],
  ["ensemble", "Ensemble"],
  ["flute", "Flute"],
  ["guitar", "Guitar"],
  ["harpsichord", "Harpsichord"],
  ["horn", "Horn"],
  ["koto", "Koto"],
  ["lute", "Lute"],
  ["mandolin", "Mandolin"],
  ["oboe", "Oboe"],
  ["orchestra", "Orchestra"],
  ["organ", "Organ"],
  ["piano", "Piano"],
  ["recorder", "Recorder"],
  ["shakuhachi", "Shakuhachi"],
  ["shamisen", "Shamisen"],
  ["string-ensemble", "String-ensemble"],
  ["string-quartet", "String-quartet"],
  ["timpani", "Timpani"],
  ["trombone", "Trombone"],
  ["trumpet", "Trumpet"],
  ["vihuela", "Vihuela"],
  ["viola", "Viola"],
  ["violin", "Violin"],
  ["voice", "Voice"],
];

// Eventually if we update the data for the search result items to use the
// slugs and not the labels, then we won't need this map.
export const mutopiaInstrumentsMap = new Map(mutopiaInstruments);

export const mutopiaStyles: OptionItem[] = [
  ["any-style", "Any Style"],
  ["baroque", "Baroque"],
  ["classical", "Classical"],
  ["folk", "Folk"],
  ["hymn", "Hymn"],
  ["jazz", "Jazz"],
  ["march", "March"],
  ["modern", "Modern"],
  ["renaissance", "Renaissance"],
  ["romantic", "Romantic"],
  ["song", "Song"],
  ["technique", "Technique"],
];

// Eventually if we update the data for the search result items to use the
// slugs and not the labels, then we won't need this map.
export const mutopiaStylesMap = new Map(mutopiaStyles);

export const mutopiaComposers: OptionItem[] = [
  ["AnyComposer", "Any Composer"],
  ["AbtF", "F. Abt (1819–1885)"],
  ["AdamsS", "S. Adams (1844–1913)"],
  ["AguadoD", "D. Aguado (1784–1849)"],
  ["AllegriG", "G. Allegri (c.1582–1652)"],
  ["AndreJ", "J. André (1741–1799)"],
  ["Anonymous", "Anonymous"],
  ["ArbanJB", "J.-B. Arban (1825–1889)"],
  ["ArneT", "T. Arne (1710–1788)"],
  ["AscherJ", "J. Ascher (1829–1869)"],
  ["BachJS", "J. S. Bach (1685–1750)"],
  ["BanchieriA", "A. Banchieri (c.1567–1634)"],
  ["BarbellaE", "E. Barbella (1718–1777)"],
  ["BartokB", "B. Bartók (1881–1945)"],
  ["BeethovenLv", "L. V. Beethoven (1770–1827)"],
  ["BehrF", "F. Behr (1837–1898)"],
  ["BendaJA", "J. A. Benda (1722–1795)"],
  ["BenoitP", "P. Benoit (1834–1901)"],
  ["BishopHR", "H. R. Bishop (1786–1855)"],
  ["BlakeB", "B. Blake (1751–1827)"],
  ["BoismortierJBd", "J. B. de Boismortier (1689–1755)"],
  ["BourgeoisL", "L. Bourgeois (c.1510–1560)"],
  ["BrahmsJ", "J. Brahms (1833–1897)"],
  ["BreyC", "C. Brey (1954–)"],
  ["BrownCJ", "C. J. Brown (1947–)"],
  ["BruchM", "M. Bruch (1838–1920)"],
  ["BrucknerA", "A. Bruckner (1824–1896)"],
  ["BurgmullerJFF", "J. F. F. Burgmüller (1806–1874)"],
  ["BuxtehudeD", "D. Buxtehude (c.1637–1707)"],
  ["CarcassiM", "M. Carcassi (1792–1853)"],
  ["CareyH", "H. Carey (1687?–1743)"],
  ["CaudiosoD", "D. Caudioso (17??–?)"],
  ["CecereC", "C. Cecere (1706–1761)"],
  ["ChopinFF", "F. F. Chopin (1810–1849)"],
  ["Claribel", "Claribel [C. A. Barnard ] (1830–1869)"],
  ["ClementiM", "M. Clementi (1752–1832)"],
  ["CocchiG", "G. Cocchi (1712–1796)"],
  ["CzernyC", "C. Czerny (1791–1857)"],
  ["DandrieuJ", "J.-F. Dandrieu (1681–1738)"],
  ["DebussyC", "C. Debussy (1862–1918)"],
  ["DiabelliA", "A. Diabelli (1781–1858)"],
  ["DoaneWH", "W. H. Doane (1832–1915)"],
  ["EcclesH", "H. Eccles (1670–1742)"],
  ["EllorJ", "J. Ellor (1819–1899)"],
  ["FaureG", "G. Fauré (1845–1924)"],
  ["FieldJ", "J. Field (1782–1837)"],
  ["FischerJKF", "J. K. F. Fischer (1665–1746)"],
  ["FosterSC", "S. C. Foster (1826–1864)"],
  ["FuchsR", "R. Fuchs (1847–1927)"],
  ["GabelloneG", "G. Gabellone (1727–1796)"],
  ["GalileiV", "V. Galilei (1520–1591)"],
  ["GervasioGB", "G. B. Gervasio (c.1725–c.1785)"],
  ["GiulianiM", "M. Giuliani (1781–1829)"],
  ["GiulianoG", "G. Giuliano (c.1750)"],
  ["GloverCW", "C. W. Glover (1797–1868)"],
  ["GobbaertsL", "F. J. Gobbaerts (L. Streabbog) (1835-1886)"],
  ["GounodC", "C. Gounod (1818–1893)"],
  ["GriegE", "E. Grieg (1843–1907)"],
  ["GrignyNd", "N. de Grigny (1672–1703)"],
  ["GruberFX", "F. X. Gruber (1787–1863)"],
  ["HallRB", "R. B. Hall (1858–1907)"],
  ["HandelGF", "G. F. Handel (1685–1759)"],
  ["HanonCL", "C. L. Hanon (1819-1900)"],
  ["HaydnFJ", "F. J. Haydn (1732–1809)"],
  ["HeiseP", "P. Heise (1830–1879)"],
  ["HullahJ", "J. Hullah (1812–1884)"],
  ["HumperdinckE", "E. Humperdinck (1873–1916)"],
  ["HunterC", "C. Hunter (1876–1906)"],
  ["IlievGK", "G. K. Iliev (1977–)"],
  ["JolyD", "D. Joly (?–1879)"],
  ["JoplinS", "S. Joplin (1868–1917)"],
  ["KiallmarkGF", "G. F. Kiallmark (1804–1887)"],
  ["KuffnerJ", "J. Küffner (1776–1856)"],
  ["KuhlauF", "F. Kuhlau (1786–1832)"],
  ["KuhnauJ", "J. Kuhnau (1660–1722)"],
  ["LottiA", "A. Lotti (c.1667–1740)"],
  ["LullyJB", "J. B. Lully (1632–1687)"],
  ["LutherM", "M. Luther (1483–1546)"],
  ["Mendelssohn-BartholdyF", "F. Mendelssohn-Bartholdy (1809–1847)"],
  ["MertzJK", "J. K. Mertz (1806–1856)"],
  ["MethfesselA", "A. Methfessel (1785–1869)"],
  ["MilanL", "L. Milan (1536–1561)"],
  ["MooreT", "T. Moore (1779–1833)"],
  ["MorelandC", "C. Moreland (?–?)"],
  ["MozartWA", "W. A. Mozart (1756–1791)"],
  ["MuellerAE", "A. E. Müller (1767–1817)"],
  ["MuellerW", "W. Müller (1767–1835)"],
  ["MuffatG", "G. Muffat (1653–1704)"],
  ["NeumarkG", "G. Neumark (1621–1681)"],
  ["PachelbelJ", "J. Pachelbel (1653–1706)"],
  ["PaganiniN", "N. Paganini (1782–1840)"],
  ["PergolesiGB", "G. B. Pergolesi (1710–1736)"],
  ["PezeliusJ", "J. Pezelius (1639–1694)"],
  ["PleyelIJ", "I. J. Pleyel (1757–1831)"],
  ["PurcellH", "H. Purcell (1658–1695)"],
  ["RachmaninoffS", "S. Rachmaninoff (1873-1943)"],
  ["RameauJP", "J. P. Rameau (1683–1764)"],
  ["RegerM", "M. Reger (1873–1916)"],
  ["RollaA", "A. Rolla (1757–1841)"],
  ["RootGF", "G. F. Root (1792–1868)"],
  ["RougetdeLisleCJ", "C. J. Rouget de Lisle (1760–1836)"],
  ["SanzG", "G. Sanz (1979–)"],
  ["SatieE", "E. Satie (1866–1925)"],
  ["ScarlattiD", "D. Scarlatti (1685–1757)"],
  ["SchreckG", "G. Schreck (1849–1918)"],
  ["SchubertF", "F. Schubert (1797–1828)"],
  ["SchulzJAP", "J. A. P. Schulz (1747–1800)"],
  ["SchumannR", "R. Schumann (1810–1856)"],
  ["ScriabinA", "A. Scriabin (1872–1915)"],
  ["SilcherF", "F. Silcher (1789–1860)"],
  ["SmallwoodW", "W. Smallwood (1831–1897)"],
  ["SorF", "F. Sor (1778–1839)"],
  ["StainerJ", "J. Stainer (1840–1901)"],
  ["StraussJJ", "J. Strauss Jr. (1825–1899)"],
  ["SuterH", "H. Suter (1870–1926)"],
  ["TallisT", "T. Tallis (1681–1767)"],
  ["TarregaF", "F. Tarrega (1852–1909)"],
  ["TchaikovskyPI", "P. I. Tchaikovsky (1840–1893)"],
  ["TelemannGP", "G. P. Telemann (1584–1635)"],
  ["TitelouzeJ", "J. Titelouze (1563–1633)"],
  ["Traditional", "Traditional"],
  ["TurpinT", "T. Turpin (1582/83–1649)"],
  ["UgolinoV", "V. Ugolino (?–?)"],
  ["VidalPA", "P. A. Vidal (1863–1931)"],
  ["VivaldiA", "A. Vivaldi (1678–1741)"],
  ["VolkmannR", "R. Volkmann (1815–1883)"],
  ["WadeJF", "J. F. Wade (1711–1786)"],
  ["WeckmannM", "M. Weckmann (1621–1674)"],
  ["WeelkesT", "T. Weelkes (1576–1623)"],
  ["WernerH", "H. Werner (1800–1833)"],
  ["WorrallH", "H. Worrall (1825–1902)"],
];

/**
 * Converts `VidalPA` into `vidal-p-a` to make the Mutopia composer IDs
 * lowercase for more consistency in URL param formatting.
 */
const slugifyMutopiaComposer = (text: string): string => {
  const slug = text
    // Put a "-" right before every capital letter.
    .replace(/([A-Z])/g, "-$1")
    // Remove "-" from the start.
    .replace(/^-+/, "")
    // Remove "-" from the end (shouldn't really be necessary).
    // .replace(/-+$/, "")
    .toLowerCase();
  return slug;
};

// Eventually if we update the data for the search result items to use the
// slugs directly, then we won't need this transformation.
export const mutopiaComposersOptions: OptionItem[] = mutopiaComposers.map(
  ([key, label]) => [slugifyMutopiaComposer(key), label]
);

// Eventually if we update the data for the search result items to use the
// slugs directly, then we won't need this map.
export const mutopiaComposersMap = new Map(
  mutopiaComposers.map(([key]) => [slugifyMutopiaComposer(key), key])
);

export const sessionMeters: OptionItem[] = [
  ["any-meter", "Any Meter"],
  ["jig", "Jig"],
  ["reel", "Reel"],
  ["slip-jig", "Slip Jig"],
  ["hornpipe", "Hornpipe"],
  ["polka", "Polka"],
  ["slide", "Slide"],
  ["waltz", "Waltz"],
  ["barndance", "Barndance"],
  ["strathspey", "Strathspey"],
  ["three-two", "Three-Two"],
  ["mazurka", "Mazurka"],
];

// Eventually if we update the data for the search result items to use the
// slugs and not the labels, then we won't need this map.
export const sessionMetersMap = new Map(sessionMeters);

export const FilterSelect = (props: {
  name: string;
  title: string;
  filterSetter: (value: string) => void;
  value: string;
  options: OptionItem[];
}) => {
  return (
    <div class={styles.filterSelectWrapper}>
      <div class="select is-small">
        <select
          name={props.name}
          title={props.title}
          onInput={(event) => {
            event.preventDefault();
            props.filterSetter(event.target.value);
          }}
        >
          <For each={props.options}>
            {(opt) => {
              const [value, label] = opt;
              return (
                <option value={value} selected={value === props.value}>
                  {label}
                </option>
              );
            }}
          </For>
        </select>
      </div>
    </div>
  );
};
