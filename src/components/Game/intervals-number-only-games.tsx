import { For, type ParentComponent, type Setter } from "solid-js";
import { GameModalButton } from "./GameModal";

import utilStyles from "../../styles/util.module.css";
import avStyles from "../AudioVisualizer/audiovisualizer.module.css";
import gameStyles from "./game.module.css";
import { includes } from "../../utils/utils";

export const intervalsNumberOnlyGames = [
  {
    source: [3, 4, 12],
    name: "Level 1: 3rd, Octave",
  },
  {
    source: [1, 2, 3, 4, 12],
    name: "Level 2: 2nd",
  },
  {
    source: [1, 2, 3, 4, 5, 12],
    name: "Level 3: 4th",
  },
  {
    source: [1, 2, 3, 4, 5, 8, 9, 12],
    name: "Level 4: 6th",
  },
  {
    source: [1, 2, 3, 4, 5, 7, 8, 9, 12],
    name: "Level 5: 5th",
  },
  {
    source: [1, 2, 3, 4, 5, 7, 8, 9, 10, 11, 12],
    name: "Level 6: 7th",
  },
  // TODO: tritones?
  //   {
  //     source: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
  //     name: "Level 5: Tritone (All Intervals)",
  //   },
] as const;

export type IntervalsNumberOnlyGame =
  | (typeof intervalsNumberOnlyGames)[number]
  | {
      source: readonly number[];
      name: "Custom";
    };

export const IntervalsNumberOnlyGameSelector = (props: {
  game: IntervalsNumberOnlyGame;
  setGame: Setter<IntervalsNumberOnlyGame>;
  endGameClearStaves: () => void;
}) => {
  const gamesBySource = intervalsNumberOnlyGames.reduce<
    Map<string, IntervalsNumberOnlyGame>
  >((acc, game) => {
    acc.set(game.source.toSorted().toString(), game);
    return acc;
  }, new Map());

  const buttonClicked = (vals: number[]) => {
    props.setGame((prev) => {
      const set = new Set(prev.source);

      vals.forEach((val) => (set.has(val) ? set.delete(val) : set.add(val)));

      const newSource =
        set.size >= 3 ? [...set].toSorted() : prev.source.toSorted();

      const newGame = gamesBySource.get(newSource.toString());

      return (
        newGame ?? {
          source: newSource,
          name: "Custom",
        }
      );
    });
  };

  return (
    <GameModalButton
      buttonText={props.game.name}
      onClick={props.endGameClearStaves}
    >
      <div class="block">
        <h4 class="title is-4">{"Intervals: Number Only"}</h4>
      </div>
      <div class={`block ${gameStyles.radioColumn}`}>
        <For each={intervalsNumberOnlyGames} fallback={<span></span>}>
          {(item, index) => (
            <div class="control">
              <input
                type="radio"
                class="radio"
                id={`radio${index()}`}
                name="level"
                onClick={() => props.setGame(item)}
                checked={props.game.name === item.name}
              />
              <label class="radio" for={`radio${index()}`}>
                {item.name}
              </label>
            </div>
          )}
        </For>
      </div>
      <div class="block">
        <IntervalsNumberOnlyButtons
          oneTwoSemitones={{
            clickHandler: () => buttonClicked([1, 2]),
            checked: () => includes(props.game.source, 2),
          }}
          threeFourSemitones={{
            clickHandler: () => buttonClicked([3, 4]),
            checked: () => includes(props.game.source, 4),
          }}
          fiveSemitones={{
            clickHandler: () => buttonClicked([5]),
            checked: () => includes(props.game.source, 5),
          }}
          sevenSemitones={{
            clickHandler: () => buttonClicked([7]),
            checked: () => includes(props.game.source, 7),
          }}
          eightNineSemitones={{
            clickHandler: () => buttonClicked([8, 9]),
            checked: () => includes(props.game.source, 8),
          }}
          tenElevenSemitones={{
            clickHandler: () => buttonClicked([10, 11]),
            checked: () => includes(props.game.source, 10),
          }}
          twelveSemitones={{
            clickHandler: () => buttonClicked([12]),
            checked: () => includes(props.game.source, 12),
          }}
        />
      </div>
    </GameModalButton>
  );
};

export const IntervalsNumberOnlyInputButtons = (props: {
  intervalClicked: (intervals: number[]) => void;
  source: readonly number[];
}) => {
  return (
    <IntervalsNumberOnlyButtons
      oneTwoSemitones={{
        clickHandler: () => props.intervalClicked([1, 2]),
        disabled: () => !props.source.includes(1) && !props.source.includes(2),
      }}
      threeFourSemitones={{
        clickHandler: () => props.intervalClicked([3, 4]),
        disabled: () => !props.source.includes(3) && !props.source.includes(4),
      }}
      fiveSemitones={{
        // Add 6 for tritone support (augmented 4ths).
        clickHandler: () => props.intervalClicked([5]),
        disabled: () => !props.source.includes(5) && !props.source.includes(6),
      }}
      sevenSemitones={{
        // Add 6 for tritone support (diminished 5ths).
        clickHandler: () => props.intervalClicked([7]),
        disabled: () => !props.source.includes(6) && !props.source.includes(7),
      }}
      eightNineSemitones={{
        clickHandler: () => props.intervalClicked([8, 9]),
        disabled: () => !props.source.includes(8) && !props.source.includes(9),
      }}
      tenElevenSemitones={{
        clickHandler: () => props.intervalClicked([10, 11]),
        disabled: () =>
          !props.source.includes(10) && !props.source.includes(11),
      }}
      twelveSemitones={{
        clickHandler: () => props.intervalClicked([12]),
        disabled: () => !props.source.includes(12),
      }}
    />
  );
};

const IntervalNumberButton: ParentComponent<{
  title: string;
  clickHandler: () => void;
  disabled: boolean;
  checked?: boolean | undefined;
}> = (props) => {
  const classes = () =>
    [
      "button is-small",
      utilStyles.buttonMinHeight,
      avStyles.intervalNumberButton,
      utilStyles.marginBottomMinusOnePx,
      props.checked === undefined
        ? undefined
        : props.checked === false
        ? gameStyles.unchecked
        : gameStyles.checked,
    ]
      .filter((x) => x)
      .join(" ");

  return (
    <div class="control">
      <button
        title={props.title}
        onClick={props.clickHandler}
        class={classes()}
        disabled={props.disabled}
      >
        {props.children}
      </button>
    </div>
  );
};

type Button = {
  clickHandler: () => void;
  disabled?: () => boolean;
  checked?: () => boolean;
};

const IntervalsNumberOnlyButtons = (props: {
  oneTwoSemitones: Button;
  threeFourSemitones: Button;
  fiveSemitones: Button;
  sevenSemitones: Button;
  eightNineSemitones: Button;
  tenElevenSemitones: Button;
  twelveSemitones: Button;
}) => {
  return (
    <div class={"field has-addons has-addons-centered"}>
      <IntervalNumberButton
        title="1 or 2 semitones"
        clickHandler={props.oneTwoSemitones.clickHandler}
        disabled={props.oneTwoSemitones.disabled?.() ?? false}
        checked={props.oneTwoSemitones.checked?.()}
      >
        <span class={gameStyles.wideText}>Second</span>
        <span class={gameStyles.narrowText}>2nd</span>
      </IntervalNumberButton>
      <IntervalNumberButton
        title="3 or 4 semitones"
        clickHandler={props.threeFourSemitones.clickHandler}
        disabled={props.threeFourSemitones.disabled?.() ?? false}
        checked={props.threeFourSemitones.checked?.()}
      >
        <span class={gameStyles.wideText}>Third</span>
        <span class={gameStyles.narrowText}>3rd</span>
      </IntervalNumberButton>
      <IntervalNumberButton
        title="5 semitones (P4)"
        clickHandler={props.fiveSemitones.clickHandler}
        disabled={props.fiveSemitones.disabled?.() ?? false}
        checked={props.fiveSemitones.checked?.()}
      >
        <span class={gameStyles.wideText}>Fourth</span>
        <span class={gameStyles.narrowText}>4th</span>
      </IntervalNumberButton>
      <IntervalNumberButton
        title="7 semitones (P5)"
        clickHandler={props.sevenSemitones.clickHandler}
        disabled={props.sevenSemitones.disabled?.() ?? false}
        checked={props.sevenSemitones.checked?.()}
      >
        <span class={gameStyles.wideText}>Fifth</span>
        <span class={gameStyles.narrowText}>5th</span>
      </IntervalNumberButton>
      <IntervalNumberButton
        title="8 or 9 semitones"
        clickHandler={props.eightNineSemitones.clickHandler}
        disabled={props.eightNineSemitones.disabled?.() ?? false}
        checked={props.eightNineSemitones.checked?.()}
      >
        <span class={gameStyles.wideText}>Sixth</span>
        <span class={gameStyles.narrowText}>6th</span>
      </IntervalNumberButton>
      <IntervalNumberButton
        title="10 or 11 semitones"
        clickHandler={props.tenElevenSemitones.clickHandler}
        disabled={props.tenElevenSemitones.disabled?.() ?? false}
        checked={props.tenElevenSemitones.checked?.()}
      >
        <span class={gameStyles.wideText}>Seventh</span>
        <span class={gameStyles.narrowText}>7th</span>
      </IntervalNumberButton>
      <IntervalNumberButton
        title="12 semitones"
        clickHandler={props.twelveSemitones.clickHandler}
        disabled={props.twelveSemitones.disabled?.() ?? false}
        checked={props.twelveSemitones.checked?.()}
      >
        <span class={gameStyles.wideText}>Octave</span>
        <span class={gameStyles.narrowText}>Octave</span>
      </IntervalNumberButton>
    </div>
  );
};
