import { Keyboard } from "../AudioVisualizer/Keyboard";
import type { GameType } from "./Game";
import gameStyles from "./game.module.css";
import utilStyles from "../../styles/util.module.css";
import {
  Fretboard,
  guitarFretboardConfig,
  violinFretboardConfig,
} from "../AudioVisualizer/Fretboard";
import type {
  InstrumentNotePlayedAction,
  KeyboardRange,
} from "../AudioVisualizer/AudioVisualizerTypes";
import { Show } from "solid-js";
import type { ParentComponent } from "solid-js";

const TrumpetNoteButton: ParentComponent<{
  clickHandler: () => void;
}> = (props) => (
  <div class="control">
    <button
      onClick={props.clickHandler}
      class={`button ${utilStyles.buttonMinHeight} ${
        utilStyles.marginBottomMinusOnePx || ""
      }`}
    >
      <span class={gameStyles.trumpetButtonText}>{props.children}</span>
    </button>
  </div>
);

export const ControlsBottom = (props: {
  gameType: GameType;
  noteSource: number[];
  keyboardRange: KeyboardRange;
  instrumentNotePlayedAction: InstrumentNotePlayedAction;
  trumpetClicked: (valves: string) => void;
}) => {
  return (
    <>
      <Show when={props.gameType === "notesKeyboardTrad"}>
        <div>
          <Keyboard
            keyboardType={"keyboard-trad"}
            keyboardRange={props.keyboardRange}
            instrumentNotePlayedAction={props.instrumentNotePlayedAction}
            pageType={"game"}
            enabledNotes={props.noteSource}
          />
        </div>
      </Show>
      <Show when={props.gameType === "notesKeyboardSixSix"}>
        <div>
          <Keyboard
            keyboardType={"keyboard-six-six"}
            keyboardRange={props.keyboardRange}
            instrumentNotePlayedAction={props.instrumentNotePlayedAction}
            pageType={"game"}
            enabledNotes={props.noteSource}
          />
        </div>
      </Show>
      <Show when={props.gameType === "notesGuitar"}>
        <div>
          <Fretboard
            fretboardConfig={guitarFretboardConfig}
            instrumentNotePlayedAction={props.instrumentNotePlayedAction}
            enabledNotes={props.noteSource}
          />
        </div>
      </Show>
      <Show when={props.gameType === "notesViolin"}>
        <div>
          <Fretboard
            fretboardConfig={violinFretboardConfig}
            instrumentNotePlayedAction={props.instrumentNotePlayedAction}
            enabledNotes={props.noteSource}
          />
        </div>
      </Show>
      <Show when={props.gameType === "notesTrumpet"}>
        <div
          class={`field has-addons has-addons-centered ${utilStyles.flexWrapWrap}`}
        >
          <TrumpetNoteButton clickHandler={() => props.trumpetClicked("•••")}>
            •••
          </TrumpetNoteButton>
          <TrumpetNoteButton clickHandler={() => props.trumpetClicked("•◦•")}>
            •◦•
          </TrumpetNoteButton>
          <TrumpetNoteButton clickHandler={() => props.trumpetClicked("◦••")}>
            ◦••
          </TrumpetNoteButton>
          <TrumpetNoteButton clickHandler={() => props.trumpetClicked("••◦")}>
            ••◦
          </TrumpetNoteButton>
          <TrumpetNoteButton clickHandler={() => props.trumpetClicked("•◦◦")}>
            •◦◦
          </TrumpetNoteButton>
          <TrumpetNoteButton clickHandler={() => props.trumpetClicked("◦•◦")}>
            ◦•◦
          </TrumpetNoteButton>
          <TrumpetNoteButton clickHandler={() => props.trumpetClicked("◦◦◦")}>
            ◦◦◦
          </TrumpetNoteButton>
        </div>
      </Show>
    </>
  );
};
