import { Staff } from "../AudioVisualizer/staff";
import { ControlsBottom } from "./controls-bottom";
import {
  GameSelect,
  NotesGameSelect,
  NotesGuitarGameSelect,
  TargetsAtATimeSelect,
} from "./controls-top";

import { Show, createSignal } from "solid-js";
import { closestNumber, iota, randomInteger } from "../../utils/utils";
import type {
  INoteSeries,
  INoteSeriesCaption,
} from "../AudioVisualizer/AudioVisualizerTypes";
import { makePiano } from "../AudioVisualizer/audio-load";
import avStyles from "../AudioVisualizer/audiovisualizer.module.css";
import { getXCoordinates } from "../AudioVisualizer/store-functions";
import { VolumeSelect } from "../VolumeSelect";
import {
  generateIntervalTargets,
  generateNoteTargets,
  generateNoteTargetsForAllNotesGames,
  getNotesForIntervals,
} from "./game-state-functions";
import gameStyles from "./game.module.css";
import {
  IntervalsGameSelector,
  IntervalsInputButtons,
  intervalsGames,
  type IntervalsGame,
} from "./intervals-number-and-quality-games";
import {
  IntervalsNumberOnlyGameSelector,
  IntervalsNumberOnlyInputButtons,
  intervalsNumberOnlyGames,
  type IntervalsNumberOnlyGame,
} from "./intervals-number-only-games";

export type GameType =
  | "notesKeyboardTrad"
  | "notesKeyboardSixSix"
  | "notesGuitar"
  | "notesViolin"
  | "notesTrumpet"
  | "intervals"
  | "intervalsNumberOnly";

export type NotesGameType =
  | "oneOctaveNaturals"
  | "oneOctaveSharpsFlats"
  | "oneOctaveAll"
  | "twoOctavesNaturals"
  | "twoOctavesSharpsFlats"
  | "twoOctavesAll";

export type GameNoteSeries = Omit<INoteSeries, "xOffsets" | "delays">;

export type GameStatus = "on" | "off" | "onButAttemptsOff";

/**
 * Clairnote game component.
 */
export const Game = (props: { sn: boolean }) => {
  // CONSTANTS

  const clairnoteType = props.sn ? "sn" : "dn";

  // Render a two-octave keyboard. Hide the 2nd octave with CSS for small
  // viewport. Two octaves: C (40) to B (63). One octave: C (40) to B (51).
  const keyboardRange = {
    lowestNote: 40,
    highestNote: 63,
  };

  const noteCaptions = [
    "G# or Ab",
    "A",
    "A# or Bb",
    "B",
    "C",
    "C# or Db",
    "D",
    "D# or Eb",
    "E",
    "F",
    "F# or Gb",
    "G",
  ];

  const intervalCaptions = [
    "Unison",
    "Minor 2nd",
    "Major 2nd",
    "Minor 3rd",
    "Major 3rd",
    "Perfect 4th",
    "Tritone",
    "Perfect 5th",
    "Minor 6th",
    "Major 6th",
    "Minor 7th",
    "Major 7th",
    "Octave",
  ];

  const trumpetValvesToNotes = new Map([
    ["•••", [32, 39]], // e, b,
    ["•◦•", [33, 40]], // f, c
    ["◦••", [34, 41, 46, 58]],
    ["••◦", [35, 42, 47, 51, 59]], // g, d, g, b, g
    ["•◦◦", [36, 43, 48, 52, 55, 60]],
    ["◦•◦", [37, 44, 49, 53, 56, 61]], // a, e, a, e, a
    ["◦◦◦", [38, 45, 50, 54, 57, 62]],
  ]);

  /**
   * Return the closest note to the target note.
   */
  const noteFromTrumpetValves = (
    valves: string,
    noteTarget: number
  ): number | undefined => {
    const notes = trumpetValvesToNotes.get(valves);

    return notes?.reduce((closest, current) =>
      Math.abs(closest - noteTarget) < Math.abs(current - noteTarget)
        ? closest
        : current
    );
  };

  const trumpetNotesToValves = new Map([
    [32, "•••"], // e
    [33, "•◦•"], // f
    [34, "◦••"],
    [35, "••◦"], // g
    [36, "•◦◦"],
    [37, "◦•◦"], // a
    [38, "◦◦◦"], // b-flat
    [39, "•••"], // b
    [40, "•◦•"], // c
    [41, "◦••"],
    [42, "••◦"], // d
    [43, "•◦◦"],
    [44, "◦•◦"], // e
    [45, "◦◦◦"], // f
    [46, "◦••"],
    [47, "••◦"], // g
    [48, "•◦◦"],
    [49, "◦•◦"], // a
    [50, "◦◦◦"], // b-flat
    [51, "••◦"], // b
    [52, "•◦◦"], // c
    [53, "◦•◦"],
    [54, "◦◦◦"], // d
    [55, "•◦◦"],
    [56, "◦•◦"], // e
    [57, "◦◦◦"], // f
    [58, "◦••"],
    [59, "••◦"], // g
    [60, "•◦◦"],
    [61, "◦•◦"], // a
    [62, "◦◦◦"], // b-flat
    // "63", "◦◦◦" // b (unused)
  ]);

  const secondsPerGame = 60;
  const gameLeftMarginDefault = 4.3;
  const noteXOffsetDefault = 5.3;
  const correctCaptionColor = "green";
  const incorrectCaptionColor = "#E3701A";
  const piano = makePiano();

  // PRIMARY STATE

  const timers: {
    feedback: number | undefined;
    staff: number | undefined;
    targets: number | undefined;
    countdown: number | undefined;
  } = {
    feedback: undefined,
    staff: undefined,
    targets: undefined,
    countdown: undefined,
  };

  // UI game selection state
  const [gameType, setGameType] = createSignal<GameType>("notesKeyboardTrad");

  const [notesGameType, setNotesGameType] =
    createSignal<NotesGameType>("oneOctaveNaturals");

  const [intervalsGame, setIntervalsGame] = createSignal<IntervalsGame>(
    intervalsGames[0]
  );

  const [intervalsNumberOnlyGame, setIntervalsNumberOnlyGame] =
    createSignal<IntervalsNumberOnlyGame>(intervalsNumberOnlyGames[0]);

  // number of target notes or intervals at a time on the staff
  const [targetsAtATime, setTargetsAtATime] = createSignal(4);

  // UI-output visible current game state
  const [rightTally, setRightTally] = createSignal(0);
  const [wrongTally, setWrongTally] = createSignal(0);
  const [secondsLeft, setSecondsLeft] = createSignal(0);
  const [feedbackText, setFeedbackText] = createSignal("");

  // internal current game state
  const [gameStatus, setGameStatus] = createSignal<GameStatus>("off");
  const [currentTarget, setCurrentTarget] = createSignal(0);
  const [noteTargets, setNoteTargets] = createSignal<number[]>([]);
  const [intervalTargets, setIntervalTargets] = createSignal<number[]>([]);
  const [targetsOnStaff, setTargetsOnStaff] = createSignal(4);

  const [targetIndexesToDraw, setTargetIndexesToDraw] = createSignal<number[]>(
    []
  );
  const [attemptsToDraw, setAttemptsToDraw] = createSignal<number[][]>([]);
  const [attemptCaptions, setAttemptCaptions] = createSignal<
    INoteSeriesCaption[]
  >([]);

  const [volume, setVolume] = createSignal(0.35);
  const [mediaQueryList, setMediaQueryList] = createSignal<
    MediaQueryList | undefined
  >();

  // DERIVED STATE

  const targetsNoteSeries = (): GameNoteSeries => ({
    yPositions: noteTargets().map(
      // Fallback interval should never happen but keeps TypeScript happy.
      gameType() === "intervals" || gameType() === "intervalsNumberOnly"
        ? (x, i) => [x, x + (intervalTargets()[i] || 1)]
        : (x) => [x]
    ),
    captions: [],
  });

  const attemptsNoteSeries = (): GameNoteSeries => ({
    yPositions: attemptsToDraw(),
    captions: attemptCaptions(),
  });

  const attemptIndexesToDraw = () => iota(attemptsToDraw().length);

  const xCoordinates = () =>
    getXCoordinates(
      [0, 1, 2, 3, 4, 5, 6, 7, 8],
      Array(9).fill(noteXOffsetDefault),
      gameLeftMarginDefault
    );

  const totalTally = () => rightTally() - wrongTally();

  const noteSource = () => {
    const oneOctaveNaturals = [40, 42, 44, 45, 47, 49, 51];
    const oneOctaveSharpsFlats = [41, 43, 46, 48, 50];

    switch (gameType()) {
      case "intervals":
      // Fall through.
      case "intervalsNumberOnly":
        return oneOctaveNaturals;
      case "notesKeyboardTrad":
      // Fall through.
      case "notesKeyboardSixSix":
        const keyboardTwoOctavesNaturals = [
          40, 42, 44, 45, 47, 49, 51, 52, 54, 56, 57, 59, 61, 63,
        ];
        const keyboardTwoOctavesSharpsFlats = [
          41, 43, 46, 48, 50, 53, 55, 58, 60, 62,
        ];
        const keyboardNoteSets = {
          oneOctaveNaturals,
          oneOctaveSharpsFlats,
          oneOctaveAll: oneOctaveNaturals.concat(oneOctaveSharpsFlats).sort(),
          twoOctavesNaturals: keyboardTwoOctavesNaturals,
          twoOctavesSharpsFlats: keyboardTwoOctavesSharpsFlats,
          twoOctavesAll: keyboardTwoOctavesNaturals
            .concat(keyboardTwoOctavesSharpsFlats)
            .sort(),
        };
        return keyboardNoteSets[notesGameType()];
      case "notesGuitar":
        const guitarTwoOctavesNaturals = [
          32, 33, 35, 37, 39, 40, 42, 44, 45, 47, 49, 51, 52, 54, 56, 57, 59,
        ];
        const guitarTwoOctavesSharpsFlats = [
          34, 36, 38, 41, 43, 46, 48, 50, 53, 55, 58, 60,
        ];
        const guitarNoteSets = {
          oneOctaveNaturals,
          oneOctaveSharpsFlats,
          oneOctaveAll: oneOctaveNaturals.concat(oneOctaveSharpsFlats).sort(),
          twoOctavesNaturals: guitarTwoOctavesNaturals,
          twoOctavesSharpsFlats: guitarTwoOctavesSharpsFlats,
          twoOctavesAll: guitarTwoOctavesNaturals
            .concat(guitarTwoOctavesSharpsFlats)
            .sort(),
        };
        return guitarNoteSets[notesGameType()];
      case "notesViolin":
        const violinTwoOctavesNaturals = [
          35, 37, 39, 40, 42, 44, 45, 47, 49, 51, 52, 54, 56, 57, 59, 61,
        ];
        const violinTwoOctavesSharpsFlats = [
          36, 38, 41, 43, 46, 48, 50, 53, 55, 58, 60, 62,
        ];
        const violinNoteSets = {
          oneOctaveNaturals,
          oneOctaveSharpsFlats,
          oneOctaveAll: oneOctaveNaturals.concat(oneOctaveSharpsFlats).sort(),
          twoOctavesNaturals: violinTwoOctavesNaturals,
          twoOctavesSharpsFlats: violinTwoOctavesSharpsFlats,
          twoOctavesAll: violinTwoOctavesNaturals
            .concat(violinTwoOctavesSharpsFlats)
            .sort(),
        };
        return violinNoteSets[notesGameType()];
      case "notesTrumpet":
        const trumpetTwoOctavesNaturals = [
          32, 33, 35, 37, 39, 40, 42, 44, 45, 47, 49, 51, 52, 54, 56, 57,
        ];
        const trumpetTwoOctavesSharpsFlats = [
          34, 36, 38, 41, 43, 46, 48, 50, 53, 55,
        ];
        const trumpetNoteSets = {
          oneOctaveNaturals,
          oneOctaveSharpsFlats,
          oneOctaveAll: oneOctaveNaturals,
          twoOctavesNaturals: trumpetTwoOctavesNaturals,
          twoOctavesSharpsFlats: trumpetTwoOctavesSharpsFlats,
          twoOctavesAll: trumpetTwoOctavesNaturals
            .concat(trumpetTwoOctavesSharpsFlats)
            .sort(),
        };
        return trumpetNoteSets[notesGameType()];
    }
  };

  // ACTIONS

  const changeGameType = (type: GameType) => {
    endGameClearStaves();
    setGameType(type);
  };

  const changeNotesGameType = (type: NotesGameType) => {
    endGameClearStaves();
    setNotesGameType(type);
  };

  const startGame = () => {
    setGameStatus("on");
    setRightTally(0);
    setWrongTally(0);

    clearBothStaves();

    setCurrentTarget(0);
    setTargetsOnStaff(0);
    setSecondsLeft(secondsPerGame);

    // Generate note or interval targets.
    if (gameType() === "intervals") {
      setIntervalTargets(generateIntervalTargets(intervalsGame().source, 120));
      setNoteTargets(getNotesForIntervals(noteSource(), intervalTargets()));
      //
    } else if (gameType() === "intervalsNumberOnly") {
      setIntervalTargets(
        generateIntervalTargets(intervalsNumberOnlyGame().source, 120)
      );
      setNoteTargets(getNotesForIntervals(noteSource(), intervalTargets()));
      //
    } else if (notesGameType() === "oneOctaveAll") {
      setNoteTargets(generateNoteTargetsForAllNotesGames(noteSource(), 1, 120));
      //
    } else if (notesGameType() === "twoOctavesAll") {
      setNoteTargets(generateNoteTargetsForAllNotesGames(noteSource(), 2, 120));
      //
    } else {
      setNoteTargets(generateNoteTargets(noteSource(), 120));
    }

    const countItDown = () => {
      setSecondsLeft(secondsLeft() - 1);
      if (secondsLeft() === 0) {
        endGame();
        setFeedbackText(`Game Over — Score: ${totalTally()}`);
      }
    };

    const startGamePart3 = () => {
      setFeedbackText(" ");
      showTargets();
      timers.countdown = window.setInterval(countItDown, 1000);
    };

    const startGamePart2 = () => {
      setFeedbackText("Go!");
      timers.feedback = window.setTimeout(startGamePart3, 500);
    };

    setFeedbackText("Ready...");

    timers.feedback = window.setTimeout(startGamePart2, 1000);
  };

  const endGame = () => {
    Object.values(timers).forEach((timerId) => clearTimeout(timerId));
    setGameStatus("off");
    setSecondsLeft(0);
    setFeedbackText(" ");
  };

  const endGameClearStaves = () => {
    // Stop button clicked or different game selected.
    endGame();
    clearBothStaves();
    setRightTally(0);
    setWrongTally(0);
  };

  /**
   * When playing 2 octave keyboard games on small screens, we show only one
   * octave of keyboard. Then the note played does not have to match the
   * octave of the target note to be correct.  E.g. playing c1 is correct for
   * a c2 target.
   */
  const isInexactGame = () => {
    if (
      gameType() !== "notesKeyboardTrad" &&
      gameType() !== "notesKeyboardSixSix"
    ) {
      return false;
    }
    if (
      notesGameType() === "oneOctaveAll" ||
      notesGameType() === "oneOctaveNaturals" ||
      notesGameType() === "oneOctaveSharpsFlats"
    ) {
      return false;
    }
    if (!mediaQueryList()) {
      // Only access `window` in a function like this or `onMount` because it
      // doesn't exist ahead of time (e.g. during static site generation).
      setMediaQueryList(window.matchMedia("(min-width: 425px)"));
    }
    const isLargeView = Boolean(mediaQueryList()?.matches);
    return !isLargeView;
  };

  /**
   * When playing an inexact game, we find the note that's closest to the
   * target note and use that as the note the player clicked, displaying it on
   * the staff.
   */
  const getAdjustedNote = (targetNote: number, note: number) => {
    if (!isInexactGame()) {
      return note;
    }
    const notePlus12 = note + 12;
    const noteDiff = Math.abs(note - targetNote);
    const notePlus12Diff = Math.abs(notePlus12 - targetNote);

    if (noteDiff < notePlus12Diff) {
      return note;
    }
    return notePlus12;
  };

  const instrumentNotePlayedAction = (note: number) => {
    if (gameStatus() !== "on") {
      // if game is not fully on, let them play music
      piano.play(note);
      return;
    }

    const targetNote = noteTargets()[currentTarget()];
    const exactlyCorrect = note === targetNote;

    const inexactlyCorrect =
      !exactlyCorrect && isInexactGame() && note + 12 === targetNote;

    const correct = exactlyCorrect || inexactlyCorrect;

    const message = correct
      ? "Yes! " + noteCaptions[note % 12]
      : "Try again...";

    // Target note should never be undefined, fallback is just for TS.
    const adjustedNote = getAdjustedNote(targetNote ?? note, note);

    // Could be used to give hints, instead of just try again.
    // note < targetNote ? "Too low..." : "Too high...";

    handleAttempt(correct, adjustedNote, message, undefined);
  };

  /**
   * In diatonic interval game a button may correspond to two intervals (like
   * major and minor 3rd). So we pass an array of intervals to allow passing
   * both, either of which might be correct.
   */
  const intervalClicked = (intervals: number[]) => {
    const index = randomInteger(intervals.length);
    const interval = intervals[index];
    // Check for interval is for TypeScript.
    if (gameStatus() !== "on" && interval !== undefined) {
      // if game is not fully on, let them play some intervals
      const c = 40;
      const topNote = c + interval;
      piano.play(c);
      piano.play(topNote);
      return;
    }

    const noteTarget = noteTargets()[currentTarget()];
    const intervalTarget = intervalTargets()[currentTarget()];

    // Checks and fallbacks are for TypeScript, should never be needed.
    if (noteTarget && intervalTarget) {
      const isCorrect = intervals.includes(intervalTarget);

      const message = isCorrect
        ? `Yes! ${intervalCaptions[intervalTarget] || ""}`
        : "Try again...";

      // Could be used to give hints, instead of just try again.
      // interval < intervalTarget ? "Too small..." : "Too large...";

      handleAttempt(
        isCorrect,
        noteTarget,
        message,
        isCorrect ? intervalTarget : closestNumber(intervals, intervalTarget)
      );
    }
  };

  const trumpetClicked = (valves: string) => {
    if (gameStatus() !== "on") {
      // if game is not on, let them play the trumpet
      const note = noteFromTrumpetValves(valves, 42);
      if (note) {
        piano.play(note);
      }
      return;
    }
    const noteTarget = noteTargets()[currentTarget()];
    const targetValves = noteTarget && trumpetNotesToValves.get(noteTarget);
    const correct = valves === targetValves;

    // Checks and fallbacks are just for TypeScript and should not happen.
    if (correct && noteTarget) {
      const noteCaption = noteCaptions[noteTarget % 12] || "";
      const message = `Yes! ${noteCaption}  ${valves}`;

      handleAttempt(correct, noteTarget, message);
    } else if (noteTarget) {
      const wrongNote = noteFromTrumpetValves(valves, noteTarget);
      // Could be used to give hints.
      // const message = wrongNote < noteTarget ? "Too low..." : "Too high...";
      if (wrongNote) {
        handleAttempt(correct, wrongNote, "Try again...");
      }
    }
  };

  const handleAttempt = (
    correct: boolean,
    note: number,
    message: string,
    interval?: number
  ) => {
    const notesArray = interval ? [note, note + interval] : [note];

    const caption = {
      text: message,
      fill: correct ? correctCaptionColor : incorrectCaptionColor,
      fontWeight: "bold",
    };

    // These indexes are positions on the staff.
    const attemptIndex = attemptsToDraw().length - 1;

    // Fallback is for TypeScript, should never happen.
    const targetIndexToDraw = targetIndexesToDraw()[0] || 0;

    const targetIndex = currentTarget() - targetIndexToDraw;

    const firstAttempt = attemptIndex < targetIndex;

    if (firstAttempt) {
      setAttemptsToDraw([...attemptsToDraw(), notesArray]);
      setAttemptCaptions([...attemptCaptions(), caption]);
    } else {
      const copiedAttemps = [...attemptsToDraw()];
      const copiedCaptions = [...attemptCaptions()];

      copiedAttemps[attemptIndex] = notesArray;
      copiedCaptions[attemptIndex] = caption;

      setAttemptsToDraw(copiedAttemps);
      setAttemptCaptions(copiedCaptions);
    }

    if (interval) {
      piano.play(note);
      piano.play(note + interval);
    } else {
      piano.play(note);
    }

    if (correct) {
      setRightTally(rightTally() + 1);
      setCurrentTarget(currentTarget() + 1);
      // reload the target staff if needed
      if (attemptsToDraw().length >= targetsOnStaff()) {
        // Briefly turn game off so no attempts are registered while reloading
        // the targets on the staff.
        setGameStatus("onButAttemptsOff");
        timers.staff = window.setTimeout(clearBothStaves, 600);

        timers.targets = window.setTimeout(() => {
          setGameStatus("on");
          showTargets();
        }, 850);
      } else {
        // clear the last note caption
        const lastCaption = attemptCaptions().length - 1;

        timers.feedback = window.setTimeout(() => {
          const copy = [...attemptCaptions()];
          copy[lastCaption] = { text: "" };
          setAttemptCaptions(copy);
        }, 800);
      }
    } else {
      // incorrect
      setWrongTally(wrongTally() + 1);
    }
  };

  const clearBothStaves = () => {
    setTargetIndexesToDraw([]);
    setAttemptsToDraw([]);
    setAttemptCaptions([]);
  };

  const showTargets = () => {
    setTargetsOnStaff(targetsAtATime());
    const indexes = iota(targetsAtATime(), currentTarget());
    setTargetIndexesToDraw(indexes);
  };

  const setVolumeAction = (volume: number) => {
    piano.volume(volume);
    setVolume(volume);
  };

  return (
    <div class={`${avStyles.audiovisualizer} ${gameStyles.gameWrapper}`}>
      <div class={gameStyles.controlsTop}>
        <div
          class={gameStyles.scoreBoard}
          title={`${rightTally()} correct, ${wrongTally()} incorrect`}
        >
          {secondsLeft()} Seconds &nbsp; {totalTally()} Points
        </div>
        <div class={avStyles.buttonOrSelectSpacer}>
          <button
            class={"button is-small"}
            onClick={() =>
              gameStatus() === "off" ? startGame() : endGameClearStaves()
            }
          >
            {gameStatus() === "off" ? "Start" : "Stop"}
          </button>
        </div>
        <div class={avStyles.buttonOrSelectSpacer}>
          <GameSelect gameType={gameType()} changeGameType={changeGameType} />
        </div>
        <Show
          when={
            gameType() === "notesKeyboardTrad" ||
            gameType() === "notesKeyboardSixSix" ||
            gameType() === "notesTrumpet"
          }
        >
          <div class={avStyles.buttonOrSelectSpacer}>
            <NotesGameSelect
              notesGameType={notesGameType()}
              changeNotesGameType={changeNotesGameType}
            />
          </div>
        </Show>
        <Show
          when={gameType() === "notesGuitar" || gameType() === "notesViolin"}
        >
          <div class={avStyles.buttonOrSelectSpacer}>
            <NotesGuitarGameSelect
              notesGameType={notesGameType()}
              changeNotesGameType={changeNotesGameType}
            />
          </div>
        </Show>
        <Show when={gameType() === "intervals"}>
          <div class={avStyles.buttonOrSelectSpacer}>
            <IntervalsGameSelector
              game={intervalsGame()}
              setGame={setIntervalsGame}
              endGameClearStaves={endGameClearStaves}
            />
          </div>
        </Show>
        <Show when={gameType() === "intervalsNumberOnly"}>
          <div class={avStyles.buttonOrSelectSpacer}>
            <IntervalsNumberOnlyGameSelector
              game={intervalsNumberOnlyGame()}
              setGame={setIntervalsNumberOnlyGame}
              endGameClearStaves={endGameClearStaves}
            />
          </div>
        </Show>
        <div class={avStyles.buttonOrSelectSpacer}>
          <TargetsAtATimeSelect
            targetsAtATime={targetsAtATime()}
            setTargetsAtATime={setTargetsAtATime}
          />
        </div>
        <div class={avStyles.buttonOrSelectSpacer}>
          <VolumeSelect volume={volume()} setVolume={setVolumeAction} />
        </div>
      </div>

      <Staff
        staffLinesY={[1.6747, 3.1747, 6.1747, 7.6747]}
        staffLinesX={0.3347}
        noteSeries={targetsNoteSeries()}
        noteIndexesToDraw={targetIndexesToDraw()}
        xCoordinates={xCoordinates()}
        // The ratio of width/height should be the same as the ratio of
        // width/height in the viewBox (the last two values).
        width="149.34mm"
        height="39.185mm"
        viewBox="0 0 49.990275 13.116840"
        // Below are the values before the height was increased slightly so
        // that the low E note would appear fully for guitar games.
        // height="37.185mm"
        // viewBox="0 0 49.990275 12.44715"
        clairnoteType={clairnoteType}
        notePlaying={undefined}
      />
      <Staff
        staffLinesY={[1.6747, 3.1747, 6.1747, 7.6747]}
        staffLinesX={0.3347}
        noteSeries={attemptsNoteSeries()}
        noteIndexesToDraw={attemptIndexesToDraw()}
        xCoordinates={xCoordinates()}
        width="149.34mm"
        height="49.58mm"
        viewBox="0 0 49.990275 16.5962"
        clairnoteType={clairnoteType}
        notePlaying={undefined}
      />
      <div class={gameStyles.feedback}>{feedbackText()}</div>
      <ControlsBottom
        gameType={gameType()}
        noteSource={noteSource()}
        keyboardRange={keyboardRange}
        instrumentNotePlayedAction={instrumentNotePlayedAction}
        trumpetClicked={trumpetClicked}
      />
      <Show when={gameType() === "intervals"}>
        <IntervalsInputButtons
          source={intervalsGame().source}
          intervalClicked={intervalClicked}
        />
      </Show>
      <Show when={gameType() === "intervalsNumberOnly"}>
        <IntervalsNumberOnlyInputButtons
          source={intervalsNumberOnlyGame().source}
          intervalClicked={intervalClicked}
        />
      </Show>
    </div>
  );
};
