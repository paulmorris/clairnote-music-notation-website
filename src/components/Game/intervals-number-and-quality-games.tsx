import { For, type ParentComponent, type Setter } from "solid-js";
import { GameModalButton } from "./GameModal";

import utilStyles from "../../styles/util.module.css";
import avStyles from "../AudioVisualizer/audiovisualizer.module.css";
import gameStyles from "./game.module.css";
import { includes } from "../../utils/utils";

export const intervalsGames = [
  {
    source: [2, 4, 12],
    name: "Level 1: Major 3rd, Major 2nd, Octave",
  },
  {
    source: [1, 2, 4, 12],
    name: "Level 2: Minor 2nd",
  },
  {
    source: [1, 2, 3, 4, 12],
    name: "Level 3: Minor 3rd",
  },
  {
    source: [1, 2, 3, 4, 5, 12],
    name: "Level 4: Perfect 4th",
  },
  {
    source: [1, 2, 3, 4, 5, 8, 12],
    name: "Level 5: Minor 6th",
  },
  {
    source: [1, 2, 3, 4, 5, 7, 8, 12],
    name: "Level 6: Perfect 5th",
  },
  {
    source: [1, 2, 3, 4, 5, 7, 8, 9, 12],
    name: "Level 7: Major 6th",
  },
  {
    source: [1, 2, 3, 4, 5, 7, 8, 9, 11, 12],
    name: "Level 8: Major 7th",
  },
  {
    source: [1, 2, 3, 4, 5, 7, 8, 9, 10, 11, 12],
    name: "Level 9: Minor 7th",
  },
  {
    source: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
    name: "Level 10: Tritone",
  },
  {
    source: [2, 4, 6, 8, 10, 12],
    name: "Even Number of Semitones",
  },
  {
    source: [1, 3, 5, 7, 9, 11],
    name: "Odd Number of Semitones",
  },
] as const;

export type IntervalsGame =
  | (typeof intervalsGames)[number]
  | {
      source: readonly number[];
      name: "Custom";
    };

export const IntervalsGameSelector = (props: {
  game: IntervalsGame;
  setGame: Setter<IntervalsGame>;
  endGameClearStaves: () => void;
}) => {
  const gamesBySource = intervalsGames.reduce<Map<string, IntervalsGame>>(
    (acc, game) => {
      acc.set(game.source.toSorted().toString(), game);
      return acc;
    },
    new Map()
  );

  const buttonClicked = (val: number) => {
    props.setGame((prev) => {
      const set = new Set(prev.source);

      set.has(val) ? set.delete(val) : set.add(val);

      const newSource =
        set.size >= 3 ? [...set].toSorted() : prev.source.toSorted();

      const newGame = gamesBySource.get(newSource.toString());

      return (
        newGame ?? {
          source: newSource,
          name: "Custom",
        }
      );
    });
  };

  return (
    <GameModalButton
      buttonText={props.game.name}
      onClick={props.endGameClearStaves}
    >
      <div class="block">
        <h4 class="title is-4">{"Intervals: Number and Quality"}</h4>
      </div>
      <div class={`block ${gameStyles.radioColumn}`}>
        <For each={intervalsGames} fallback={<span></span>}>
          {(item, index) => (
            <div class="control">
              <input
                type="radio"
                class="radio"
                id={`radio${index()}`}
                name="level"
                onClick={() => props.setGame(item)}
                checked={props.game.name === item.name}
              />
              <label class="radio" for={`radio${index()}`}>
                {item.name}
              </label>
            </div>
          )}
        </For>
      </div>
      <div class="block">
        <IntervalsButtons
          oneSemitones={{
            clickHandler: () => buttonClicked(1),
            checked: () => includes(props.game.source, 1),
          }}
          twoSemitones={{
            clickHandler: () => buttonClicked(2),
            checked: () => includes(props.game.source, 2),
          }}
          threeSemitones={{
            clickHandler: () => buttonClicked(3),
            checked: () => includes(props.game.source, 3),
          }}
          fourSemitones={{
            clickHandler: () => buttonClicked(4),
            checked: () => includes(props.game.source, 4),
          }}
          fiveSemitones={{
            clickHandler: () => buttonClicked(5),
            checked: () => includes(props.game.source, 5),
          }}
          sixSemitones={{
            clickHandler: () => buttonClicked(6),
            checked: () => includes(props.game.source, 6),
          }}
          sevenSemitones={{
            clickHandler: () => buttonClicked(7),
            checked: () => includes(props.game.source, 7),
          }}
          eightSemitones={{
            clickHandler: () => buttonClicked(8),
            checked: () => includes(props.game.source, 8),
          }}
          nineSemitones={{
            clickHandler: () => buttonClicked(9),
            checked: () => includes(props.game.source, 9),
          }}
          tenSemitones={{
            clickHandler: () => buttonClicked(10),
            checked: () => includes(props.game.source, 10),
          }}
          elevenSemitones={{
            clickHandler: () => buttonClicked(11),
            checked: () => includes(props.game.source, 11),
          }}
          twelveSemitones={{
            clickHandler: () => buttonClicked(12),
            checked: () => includes(props.game.source, 12),
          }}
        />
      </div>
    </GameModalButton>
  );
};

export const IntervalsInputButtons = (props: {
  intervalClicked: (intervals: number[]) => void;
  source: readonly number[];
}) => {
  return (
    <IntervalsButtons
      oneSemitones={{
        clickHandler: () => props.intervalClicked([1]),
        disabled: () => !props.source.includes(1),
      }}
      twoSemitones={{
        clickHandler: () => props.intervalClicked([2]),
        disabled: () => !props.source.includes(2),
      }}
      threeSemitones={{
        clickHandler: () => props.intervalClicked([3]),
        disabled: () => !props.source.includes(3),
      }}
      fourSemitones={{
        clickHandler: () => props.intervalClicked([4]),
        disabled: () => !props.source.includes(4),
      }}
      fiveSemitones={{
        clickHandler: () => props.intervalClicked([5]),
        disabled: () => !props.source.includes(5),
      }}
      sixSemitones={{
        clickHandler: () => props.intervalClicked([6]),
        disabled: () => !props.source.includes(6),
      }}
      sevenSemitones={{
        clickHandler: () => props.intervalClicked([7]),
        disabled: () => !props.source.includes(7),
      }}
      eightSemitones={{
        clickHandler: () => props.intervalClicked([8]),
        disabled: () => !props.source.includes(8),
      }}
      nineSemitones={{
        clickHandler: () => props.intervalClicked([9]),
        disabled: () => !props.source.includes(9),
      }}
      tenSemitones={{
        clickHandler: () => props.intervalClicked([10]),
        disabled: () => !props.source.includes(10),
      }}
      elevenSemitones={{
        clickHandler: () => props.intervalClicked([11]),
        disabled: () => !props.source.includes(11),
      }}
      twelveSemitones={{
        clickHandler: () => props.intervalClicked([12]),
        disabled: () => !props.source.includes(12),
      }}
    />
  );
};

const IntervalNumberAndQualityButton: ParentComponent<{
  clickHandler: () => void;
  title: string;
  disabled: boolean;
  checked?: boolean | undefined;
}> = (props) => {
  const classes = () =>
    [
      "button is-small",
      utilStyles.buttonMinHeight,
      avStyles.chromaticIntervalButton,
      props.checked === undefined
        ? undefined
        : props.checked === false
        ? gameStyles.unchecked
        : gameStyles.checked,
    ]
      .filter((x) => x)
      .join(" ");

  return (
    <div class="control">
      <button
        title={props.title}
        onClick={props.clickHandler}
        class={classes()}
        disabled={props.disabled}
      >
        {props.children}
      </button>
    </div>
  );
};

type Button = {
  clickHandler: () => void;
  disabled?: () => boolean;
  checked?: () => boolean;
};

const IntervalsButtons = (props: {
  oneSemitones: Button;
  twoSemitones: Button;
  threeSemitones: Button;
  fourSemitones: Button;
  fiveSemitones: Button;
  sixSemitones: Button;
  sevenSemitones: Button;
  eightSemitones: Button;
  nineSemitones: Button;
  tenSemitones: Button;
  elevenSemitones: Button;
  twelveSemitones: Button;
}) => {
  return (
    <div>
      {/* Odd semitone intervals row. */}
      <div class={avStyles.chromaticIntervalButtonsTopRow}>
        <div class={"field has-addons has-addons-centered"}>
          <IntervalNumberAndQualityButton
            title="1 semitone"
            clickHandler={props.oneSemitones.clickHandler}
            disabled={props.oneSemitones.disabled?.() ?? false}
            checked={props.oneSemitones.checked?.()}
          >
            <span class={gameStyles.wideText}>Minor 2nd</span>
            <span class={gameStyles.narrowText}>m2</span>
          </IntervalNumberAndQualityButton>
          <IntervalNumberAndQualityButton
            title="3 semitones"
            clickHandler={props.threeSemitones.clickHandler}
            disabled={props.threeSemitones.disabled?.() ?? false}
            checked={props.threeSemitones.checked?.()}
          >
            <span class={gameStyles.wideText}>Minor 3rd</span>
            <span class={gameStyles.narrowText}>m3</span>
          </IntervalNumberAndQualityButton>
          <IntervalNumberAndQualityButton
            title="5 semitones"
            clickHandler={props.fiveSemitones.clickHandler}
            disabled={props.fiveSemitones.disabled?.() ?? false}
            checked={props.fiveSemitones.checked?.()}
          >
            <span class={gameStyles.wideText}>Perfect 4th</span>
            <span class={gameStyles.narrowText}>P4</span>
          </IntervalNumberAndQualityButton>
          <IntervalNumberAndQualityButton
            title="7 semitones"
            clickHandler={props.sevenSemitones.clickHandler}
            disabled={props.sevenSemitones.disabled?.() ?? false}
            checked={props.sevenSemitones.checked?.()}
          >
            <span class={gameStyles.wideText}>Perfect 5th</span>
            <span class={gameStyles.narrowText}>P5</span>
          </IntervalNumberAndQualityButton>
          <IntervalNumberAndQualityButton
            title="9 semitones"
            clickHandler={props.nineSemitones.clickHandler}
            disabled={props.nineSemitones.disabled?.() ?? false}
            checked={props.nineSemitones.checked?.()}
          >
            <span class={gameStyles.wideText}>Major 6th</span>
            <span class={gameStyles.narrowText}>M6</span>
          </IntervalNumberAndQualityButton>
          <IntervalNumberAndQualityButton
            title="11 semitones"
            clickHandler={props.elevenSemitones.clickHandler}
            disabled={props.elevenSemitones.disabled?.() ?? false}
            checked={props.elevenSemitones.checked?.()}
          >
            <span class={gameStyles.wideText}>Major 7th</span>
            <span class={gameStyles.narrowText}>M7</span>
          </IntervalNumberAndQualityButton>
        </div>
      </div>
      {/* Even semitone intervals row. */}
      <div class={avStyles.chromaticIntervalButtonsBottomRow}>
        <div class={"field has-addons has-addons-centered"}>
          <IntervalNumberAndQualityButton
            title="2 semitones"
            clickHandler={props.twoSemitones.clickHandler}
            disabled={props.twoSemitones.disabled?.() ?? false}
            checked={props.twoSemitones.checked?.()}
          >
            <span class={gameStyles.wideText}>Major 2nd</span>
            <span class={gameStyles.narrowText}>M2</span>
          </IntervalNumberAndQualityButton>
          <IntervalNumberAndQualityButton
            title="4 semitones"
            clickHandler={props.fourSemitones.clickHandler}
            disabled={props.fourSemitones.disabled?.() ?? false}
            checked={props.fourSemitones.checked?.()}
          >
            <span class={gameStyles.wideText}>Major 3rd</span>
            <span class={gameStyles.narrowText}>M3</span>
          </IntervalNumberAndQualityButton>
          <IntervalNumberAndQualityButton
            title="6 semitones"
            clickHandler={props.sixSemitones.clickHandler}
            disabled={props.sixSemitones.disabled?.() ?? false}
            checked={props.sixSemitones.checked?.()}
          >
            <span class={gameStyles.wideText}>Tritone</span>
            <span class={gameStyles.narrowText}>TT</span>
          </IntervalNumberAndQualityButton>
          <IntervalNumberAndQualityButton
            title="8 semitones"
            clickHandler={props.eightSemitones.clickHandler}
            disabled={props.eightSemitones.disabled?.() ?? false}
            checked={props.eightSemitones.checked?.()}
          >
            <span class={gameStyles.wideText}>Minor 6th</span>
            <span class={gameStyles.narrowText}>m6</span>
          </IntervalNumberAndQualityButton>
          <IntervalNumberAndQualityButton
            title="10 semitones"
            clickHandler={props.tenSemitones.clickHandler}
            disabled={props.tenSemitones.disabled?.() ?? false}
            checked={props.tenSemitones.checked?.()}
          >
            <span class={gameStyles.wideText}>Minor 7th</span>
            <span class={gameStyles.narrowText}>m7</span>
          </IntervalNumberAndQualityButton>
          <IntervalNumberAndQualityButton
            title="12 semitones"
            clickHandler={props.twelveSemitones.clickHandler}
            disabled={props.twelveSemitones.disabled?.() ?? false}
            checked={props.twelveSemitones.checked?.()}
          >
            <span class={gameStyles.wideText}>Octave</span>
            <span class={gameStyles.narrowText}>P8</span>
          </IntervalNumberAndQualityButton>
        </div>
      </div>
    </div>
  );
};
