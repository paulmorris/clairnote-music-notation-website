import { randomInteger } from "../../utils/utils";

// use with Array.map to transpose up an octave
const upAnOctave = (num: number) => {
  return num + 12;
};

/**
 * Takes a key and returns a somewhat randomized next key. The possible key
 * changes limit us to the ones that contain 3 or more new notes that were not
 * in the previous key.
 *
 * @param key - The previous key.
 * @returns The next key.
 */
const getNextKey = (key: number) => {
  // Previously this was: [4, 6, 9, 11]
  const keyChanges = [
    // E.g. from C-major (0) to:
    6, // F#/Gb-major (6 sharps, 6 flats)
    11, // B-major (5 sharps)
    4, // E-major (4 sharps)
    9, // A-major (3 sharps)
    3, // Eb-major (3 flats)
    8, // Ab-major (4 flats)
    1, // Db-major (5 flats)
  ];
  // Fallback is for TypeScript and should never occur.
  const keyChange = keyChanges[randomInteger(keyChanges.length)] || 0;

  return (key + keyChange) % 12;
};

/**
 * Transposes an array of note numbers by a "new key" number, and returns a new
 * array of numbers. We use some modulo math by effectively shifting the series,
 * so it's as if it started at 0.
 *
 * The notes passed in should only be oneOctaveNaturals or twoOctaveNaturals.
 */
const transposeToNewKey = (
  notes: number[],
  newKey: number,
  lowestPossibleNote: number,
  highestPossibleNote: number
) => {
  const sizeOfNoteRange = highestPossibleNote - lowestPossibleNote + 1;

  // octaveRange is typically 1 or 2, so moduloNumber is 12 or 24.
  const octaveRange = Math.ceil(sizeOfNoteRange / 12);
  const moduloNumber = octaveRange * 12;

  const lowestNote = Math.min(...notes);

  return notes.map((note) => {
    // The note shifted, as if the lowest note was 0.
    const shiftedNote = note - lowestNote;
    const transposedNote = shiftedNote + newKey;
    // If the note is outside of the note range it gets "wrapped" back into it.
    const wrappedNote = transposedNote % moduloNumber;
    const unshiftedNote = wrappedNote + lowestNote;

    // If the transposed note is outside the range of notes, move it back in
    // by transposing it up or down an octave.
    if (unshiftedNote > highestPossibleNote) {
      return unshiftedNote - 12;
    }
    if (unshiftedNote < lowestPossibleNote) {
      return unshiftedNote + 12;
    }
    return unshiftedNote;
  });
};

// A function for testing, should be moved into proper tests eventually
/*
anyRepeats: function(ary) {
    var i;
    for (i = 3; i < ary.length; i += 1) {
        if (ary[i] === ary[i-1] ||
            ary[i] === ary[i-2] ||
            ary[i] === ary[i-3]) {
            return true;
        }
    }
    return false;
};
*/

/**
 * The order of the targets (notes or intervals) is randomized using a
 * recursive function. We avoid repeated targets, and actually make it so that
 * the closest the same target can be to itself is some N (`lastNTargetsCount`)
 * targets between them. We have to stop when there are only N targets left in
 * the set to avoid chance of infinite recursion if the only targets left to
 * choose from happen to be the same as the last N targets loaded.
 */
const pickRandomTarget = (
  targetSource: number[],
  lastNTargets: number[]
): number => {
  // Pick a value randomly and return its index if it is not in lastNTargets,
  // else recurse and try again.
  const index = randomInteger(targetSource.length);
  const picked = targetSource[index];
  // Check for truthy picked is for TypeScript, should not be needed at runtime.
  if (picked && !lastNTargets.includes(picked)) {
    return index;
  }
  return pickRandomTarget(targetSource, lastNTargets);
};

const shuffleTargets = (
  targetSource: number[],
  lastNTargets: number[],
  lastNTargetsCount: number
) => {
  const oldTargets = targetSource.slice();
  const len = oldTargets.length - lastNTargetsCount;
  const newTargets = lastNTargets.slice();

  // Randomize the order of targets.
  for (let i = 0; i < len; i += 1) {
    // pick a semi-random target from oldTargets, add it to newTargets,
    // remove it from oldTargets.
    const index = pickRandomTarget(
      oldTargets,
      newTargets.slice(-lastNTargetsCount)
    );
    const target = oldTargets[index];
    // Check is for TypeScript, should not ever fail.
    if (target) {
      newTargets.push(target);
      oldTargets.splice(index, 1);
    }
  }

  // for testing purposes...
  // if (anyRepeats(newTargets)) { return shuffleTargets(); }

  // Don't return the first `lastNTargetsCount` items; they are already in the
  // queue.
  return newTargets.slice(lastNTargetsCount);
};

const doubleArray = <T>(array: readonly T[]) =>
  array.slice().concat(array.slice());

// const getRandomKey = () => Math.floor(Math.random() * 12);

const getMoreTargets = (
  targetSource: readonly number[],
  lastNTargets: number[],
  lastNTargetsCount: number
) => {
  return shuffleTargets(
    doubleArray(targetSource),
    lastNTargets,
    lastNTargetsCount
  );
};

/**
 * Given the length of the target source array, return the number to use for
 * lastNTargetsCount (the smallest number of notes possible between two
 * identical targets).
 *
 * 0 for source lengths: 0, 1, 2
 * 1 for source lengths: 3, 4, 5
 * 2 for source lengths: 6, 7, 8
 * 3 for source lengths: 9 and above
 */
const calculateLastNTargetsCount = (targetSourceLength: number) => {
  return Math.min(3, Math.floor(targetSourceLength / 3));
};

const filterToNaturalNotes = (notes: number[]) => {
  const naturalsModuloTwelve = new Set([4, 6, 8, 9, 11, 1, 3]);
  return notes.filter((note) => naturalsModuloTwelve.has(note % 12));
};

export const generateNoteTargetsForAllNotesGames = (
  noteSource: number[],
  octaves: 1 | 2,
  total: number
) => {
  const result: number[] = [];
  const lastNTargetsCount = calculateLastNTargetsCount(noteSource.length);

  // Always start with C major as the key. Previously it was a random key.
  // let key = getRandomKey();
  let key = 0;

  // Assumptions: the notes in noteSource are a continuous chromatic series
  // -- no missing notes.
  // (Changing key doesn't make sense unless it is an "all notes" game.)
  const noteSourceNaturals = filterToNaturalNotes(noteSource);

  const lowestPossibleNote = Math.min(...noteSource);
  const highestPossibleNote = Math.max(...noteSource);

  while (result.length < total) {
    const transposedNotes = transposeToNewKey(
      noteSourceNaturals,
      key,
      lowestPossibleNote,
      highestPossibleNote
    );

    const lastNTargets = result.slice(-lastNTargetsCount);

    const targets = getMoreTargets(
      transposedNotes,
      lastNTargets,
      lastNTargetsCount
    );

    // For two octaves reduce length so we aren't in the same key too long.
    const nextTargets = octaves === 2 ? targets.slice(0, 14) : targets;

    key = getNextKey(key);
    Array.prototype.push.apply(result, nextTargets);
  }
  return result;
};

export const generateNoteTargets = (noteSource: number[], total: number) => {
  const result: number[] = [];
  const lastNTargetsCount = calculateLastNTargetsCount(noteSource.length);

  while (result.length < total) {
    const lastNTargets = result.slice(-lastNTargetsCount);
    const targets = getMoreTargets(noteSource, lastNTargets, lastNTargetsCount);
    Array.prototype.push.apply(result, targets);
  }
  return result;
};

export const generateIntervalTargets = (
  intervalSource: readonly number[],
  total: number
) => {
  const lastNTargetsCount = calculateLastNTargetsCount(intervalSource.length);
  const result: number[] = [];

  while (result.length < total) {
    const lastNTargets = result.slice(lastNTargetsCount);
    const newAdds = getMoreTargets(
      intervalSource,
      lastNTargets,
      lastNTargetsCount
    );

    Array.prototype.push.apply(result, newAdds);
  }
  return result;
};

/**
 * @see getNotesForIntervals
 *
 * @param interval - The interval.
 * @param lowNotes - The array to search for the lower note.
 * @param highNotes - The array to search for the higher note.
 * @returns A note.
 */
const getNoteForInterval = (
  interval: number,
  lowNotes: number[],
  highNotes: number[]
): number => {
  const randomInt = randomInteger(lowNotes.length);
  const lowNote = lowNotes[randomInt];

  if (lowNote !== undefined && !highNotes.includes(lowNote + interval)) {
    return lowNote;
  }

  lowNotes.splice(randomInt, 1);

  if (lowNotes.length === 0) {
    // This should never happen, all intervals should always be
    // possible between some pair of notes from lowNotes and highNotes.
    // Return lowNote as a reasonable fallback.
    // The additional fallback is for TypeScript and doubly shouldn't happen.
    return lowNote || 0;
  }
  return getNoteForInterval(interval, lowNotes, highNotes);
};

/**
 * Returns an array of the lower notes for each interval in `intervals`, and
 * guarantees that the low notes and high notes for the intervals both exist in
 * the noteSource notes, as transposed to a given key. The key that is
 * "in effect" changes every N intervals.
 *
 * @param noteSource - The pattern of notes, typically a diatonic scale pattern.
 *                     e.g. [40, 42, 44, 45, 47, 49, 51]
 * @param intervals - The intervals for which to find notes. e.g. for all
 *                    intervals: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
 * @returns An array of notes that are the lower notes for each given interval.
 */
export const getNotesForIntervals = (
  noteSource: number[],
  intervals: number[]
): number[] => {
  // Always start with C major as the key.
  // Previously we started with and stayed in a random key.
  // let key = getRandomKey();
  let key = 0;

  const result: number[] = [];

  const lowestPossibleNote = Math.min(...noteSource);
  const highestPossibleNote = Math.max(...noteSource);

  while (result.length < intervals.length) {
    const lowNotes = transposeToNewKey(
      noteSource,
      key,
      lowestPossibleNote,
      highestPossibleNote
    );
    const highNotes = lowNotes.slice().concat(lowNotes.map(upAnOctave));

    // This is the number of interval targets in a given key before the key
    // changes.
    const intervalsPerKey = 12;
    const start = result.length;
    const nextStart = start + intervalsPerKey;

    const notes = intervals
      .slice(start, nextStart)
      .map((interval) =>
        getNoteForInterval(interval, lowNotes.slice(), highNotes.slice())
      );

    Array.prototype.push.apply(result, notes);

    key = getNextKey(key);
  }

  return result;
};
