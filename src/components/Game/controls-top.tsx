// This rule is going away soon.
// https://github.com/jsx-eslint/eslint-plugin-jsx-a11y/issues/398
/* eslint-disable jsx-a11y/no-onchange */

import type { GameType, NotesGameType } from "./Game";
import { makeSelectOptions } from "../../utils/utils";
import type { SelectOption } from "../../types/types";
import gameStyles from "./game.module.css";

export function GameSelect(props: {
  gameType: GameType;
  changeGameType: (type: GameType) => void;
}) {
  const options: SelectOption<GameType>[] = [
    { value: "notesKeyboardTrad", label: "Notes: Traditional Keyboard" },
    { value: "notesKeyboardSixSix", label: "Notes: Janko-style Keyboard" },
    { value: "notesGuitar", label: "Notes: Guitar" },
    { value: "notesViolin", label: "Notes: Violin or Mandolin" },
    { value: "notesTrumpet", label: "Notes: Trumpet" },
    { value: "intervals", label: "Intervals: Number and Quality" },
    { value: "intervalsNumberOnly", label: "Intervals: Number Only" },
  ];
  return (
    <div class="select is-small">
      <select
        name="Select which notes or intervals to practice."
        title="Select which notes or intervals to practice."
        onChange={(event) => {
          props.changeGameType(event.target.value as GameType);
        }}
      >
        {makeSelectOptions(options, props.gameType)}
      </select>
    </div>
  );
}

export function NotesGameSelect(props: {
  notesGameType: NotesGameType;
  changeNotesGameType: (type: NotesGameType) => void;
}) {
  const options: SelectOption<NotesGameType>[] = [
    { value: "oneOctaveNaturals", label: "1 Octave: Naturals" },
    { value: "oneOctaveSharpsFlats", label: "1 Octave: Sharps & Flats" },
    { value: "oneOctaveAll", label: "1 Octave: All" },
    { value: "twoOctavesNaturals", label: "2 Octaves: Naturals" },
    { value: "twoOctavesSharpsFlats", label: "2 Octaves: Sharps & Flats" },
    { value: "twoOctavesAll", label: "2 Octaves: All" },
  ];

  return (
    <div class="select is-small">
      <select
        name="Select which notes to practice."
        title="Select which notes to practice."
        onChange={(event) => {
          props.changeNotesGameType(event.target.value as NotesGameType);
        }}
        class={gameStyles.gameTypeSelect}
      >
        {makeSelectOptions(options, props.notesGameType)}
      </select>
    </div>
  );
}

/**
 * Also used with violin and mandolin games.
 */
export function NotesGuitarGameSelect(props: {
  notesGameType: NotesGameType;
  changeNotesGameType: (type: NotesGameType) => void;
}) {
  // TODO: Rework things to make notes game types more flexible (per instrument).
  const options: SelectOption<NotesGameType>[] = [
    { value: "oneOctaveNaturals", label: "1 Octave: Naturals" },
    { value: "oneOctaveSharpsFlats", label: "1 Octave: Sharps & Flats" },
    { value: "oneOctaveAll", label: "1 Octave: All Notes" },
    { value: "twoOctavesNaturals", label: "All Naturals" },
    { value: "twoOctavesSharpsFlats", label: "All Sharps & Flats" },
    { value: "twoOctavesAll", label: "All Notes" },
  ];
  return (
    <div class="select is-small">
      <select
        name="Select which notes to practice."
        title="Select which notes to practice."
        onChange={(event) => {
          props.changeNotesGameType(event.target.value as NotesGameType);
        }}
        class={gameStyles.gameTypeSelect}
      >
        {makeSelectOptions(options, props.notesGameType)}
      </select>
    </div>
  );
}

export function TargetsAtATimeSelect(props: {
  targetsAtATime: number;
  setTargetsAtATime: (targets: number) => void;
}) {
  const options: SelectOption<number>[] = [
    { value: 1, label: "1 at a Time" },
    { value: 2, label: "2 at a Time" },
    { value: 4, label: "4 at a Time" },
    { value: 8, label: "8 at a Time" },
  ];

  return (
    <div class="select is-small">
      <select
        name="How many notes/intervals on the staff at once?"
        title="How many notes/intervals on the staff at once?"
        onChange={(event) =>
          props.setTargetsAtATime(parseInt(event.target.value))
        }
      >
        {makeSelectOptions(options, props.targetsAtATime)}
      </select>
    </div>
  );
}
