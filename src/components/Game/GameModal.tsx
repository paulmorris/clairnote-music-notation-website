import { createSignal, type ParentComponent } from "solid-js";

export const GameModalButton: ParentComponent<{
  buttonText: string;
  onClick?: () => void;
}> = (props) => {
  const [isModalVisible, setIsModalVisible] = createSignal(false);

  const toggleModal = () => setIsModalVisible(!isModalVisible());

  return (
    <>
      <button
        class="button is-small"
        onClick={() => {
          props.onClick?.();
          toggleModal();
        }}
      >
        {props.buttonText}
      </button>

      <div class={`modal ${isModalVisible() ? "is-active" : ""}`}>
        <div class="modal-background" onClick={toggleModal}></div>
        <div class="modal-content" style={{ "font-size": "16px" }}>
          <div class="box">{props.children}</div>
        </div>
        <button
          class="modal-close is-large"
          aria-label="close"
          onClick={toggleModal}
        ></button>
      </div>
    </>
  );
};
