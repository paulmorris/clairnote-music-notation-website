import { makeSelectOptions } from "../utils/utils";
import type { SelectOption } from "../types/types";

export const VolumeSelect = (props: {
  volume: number;
  setVolume: (volume: number) => void;
  className?: string;
}) => {
  const volumeOptions: SelectOption<number>[] = [
    { value: 1.0, label: "Volume 4" },
    { value: 0.65, label: "Volume 3" },
    { value: 0.35, label: "Volume 2" },
    { value: 0.1, label: "Volume 1" },
    { value: 0, label: "No Sound" },
  ];

  return (
    <div class={"select is-small"}>
      <select
        class={props.className}
        name="Volume Selector"
        title="Volume"
        onInput={(event) => props.setVolume(parseFloat(event.target.value))}
      >
        {makeSelectOptions(volumeOptions, props.volume)}
      </select>
    </div>
  );
};
