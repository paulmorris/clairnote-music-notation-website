import type {
  ModeType,
  INoteSeries,
  Root,
  ScaleType,
  DiatonicIntervalType,
  ChromaticIntervalType,
  IntervalConfig,
} from "./AudioVisualizerTypes";

const defaultNoteDelay = 500;
const defaultIntervalDelay = 800;

const sharpFlatCaptions = [
  "C",
  "C#/Db",
  "D",
  "D#/Eb",
  "E",
  "F",
  "F#/Gb",
  "G",
  "G#/Ab",
  "A",
  "A#/Bb",
  "B",
];

const sharpCaptions = [
  "C",
  "C#",
  "D",
  "D#",
  "E",
  "F",
  "F#",
  "G",
  "G#",
  "A",
  "A#",
  "B",
];

const flatCaptions = [
  "C",
  "Db",
  "D",
  "Eb",
  "E",
  "F",
  "Gb",
  "G",
  "Ab",
  "A",
  "Bb",
  "B",
];

const intervalCaptions = [
  "Unison",
  "Minor/2nd",
  "Major/2nd",
  "Minor/3rd",
  "Major/3rd",
  "4th",
  "Tritone",
  "5th",
  "Minor/6th",
  "Major/6th",
  "Minor/7th",
  "Major/7th",
  "Octave",
];

const noteSeriesYPositions = {
  major: [40, 42, 44, 45, 47, 49, 51, 52, 51, 49, 47, 45, 44, 42, 40],
  naturalMinor: [40, 42, 43, 45, 47, 48, 50, 52, 50, 48, 47, 45, 43, 42, 40],
  harmonicMinor: [40, 42, 43, 45, 47, 48, 51, 52, 51, 48, 47, 45, 43, 42, 40],
  melodicMinor: [40, 42, 43, 45, 47, 49, 51, 52, 50, 48, 47, 45, 43, 42, 40],
  blues: [40, 43, 45, 46, 47, 50, 52, 50, 47, 46, 45, 43, 40],
  chromatic: [40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51],
  wholeTone: [40, 42, 44, 46, 48, 50, 41, 43, 45, 47, 49, 51],
  fourGroups: [40, 44, 48, 42, 46, 50, 41, 45, 49, 43, 47, 51],

  ionian: [40, 42, 44, 45, 47, 49, 51, 52, 51, 49, 47, 45, 44, 42, 40],
  dorian: [40, 42, 43, 45, 47, 49, 50, 52, 50, 49, 47, 45, 43, 42, 40],
  phrygian: [40, 41, 43, 45, 47, 48, 50, 52, 50, 48, 47, 45, 43, 41, 40],
  lydian: [40, 42, 44, 46, 47, 49, 51, 52, 51, 49, 47, 46, 44, 42, 40],
  mixolydian: [40, 42, 44, 45, 47, 49, 50, 52, 50, 49, 47, 45, 44, 42, 40],
  aeolian: [40, 42, 43, 45, 47, 48, 50, 52, 50, 48, 47, 45, 43, 42, 40],
  locrian: [40, 41, 43, 45, 46, 48, 50, 52, 50, 48, 46, 45, 43, 41, 40],
};

// diatonic scale degrees
const diatonicIntervalLookup: Record<DiatonicIntervalType, number> = {
  seconds: 1,
  thirds: 2,
  fourths: 3,
  fifths: 4,
  sixths: 5,
  sevenths: 6,
  octaves: 7,
};

// chromatic semitones
const chromaticIntervalLookup: Record<ChromaticIntervalType, number> = {
  minorSeconds: 1,
  majorSeconds: 2,
  minorThirds: 3,
  majorThirds: 4,
  perfectFourths: 5,
  tritones: 6,
  perfectFifths: 7,
  minorSixths: 8,
  majorSixths: 9,
  minorSevenths: 10,
  majorSevenths: 11,
  octaves: 12,
};

// For converting between diatonic and chromatic interval types.
export const diatonicToChromaticInterval: Record<
  DiatonicIntervalType,
  ChromaticIntervalType
> = {
  seconds: "majorSeconds",
  thirds: "majorThirds",
  fourths: "perfectFourths",
  fifths: "perfectFifths",
  sixths: "majorSixths",
  sevenths: "majorSevenths",
  octaves: "octaves",
};

export const chromaticToDiatonicInterval: Record<
  ChromaticIntervalType,
  DiatonicIntervalType
> = {
  minorSeconds: "seconds",
  majorSeconds: "seconds",
  minorThirds: "thirds",
  majorThirds: "thirds",
  perfectFourths: "fourths",
  tritones: "fifths",
  perfectFifths: "fifths",
  minorSixths: "sixths",
  majorSixths: "sixths",
  minorSevenths: "sevenths",
  majorSevenths: "sevenths",
  octaves: "octaves",
};

/**
 * Converts an interval config object from diatonic to chromatic or vice-versa.
 * Used when the interval scale changes from diatonic to chromatic or
 * vice-versa, and so the interval type has to change,
 * e.g. "majorThirds" <--> "thirds".
 */
export function convertIntervalConfig(config: IntervalConfig): IntervalConfig {
  return config.flavor === "diatonic"
    ? {
        flavor: "chromatic",
        type: diatonicToChromaticInterval[config.type],
      }
    : {
        flavor: "diatonic",
        type: chromaticToDiatonicInterval[config.type],
      };
}

function makeDiatonicIntervals(
  yPositions: number[][],
  intervalType: DiatonicIntervalType
) {
  // Should not be needed, but TypeScript wants to know for sure.
  const isNumberNotUndefined = (n: number | undefined): n is number =>
    n !== undefined;

  const positions = yPositions.map((n) => n[0]).filter(isNumberNotUndefined);

  const scale = positions.slice(0, 7).filter(isNumberNotUndefined);

  const threeOctaveScale = scale.concat(
    scale.map((n) => n + 12),
    scale.map((n) => n + 24)
  );

  const intervalScaleDegree = diatonicIntervalLookup[intervalType];

  const intervalSeries = positions.map((pos) => {
    const scaleIndex = threeOctaveScale.findIndex((n) => n === pos);
    const intervalIndex = scaleIndex + intervalScaleDegree;
    const thing = threeOctaveScale[intervalIndex];

    // The 0 fallback shouldn't happen and is for TypeScript.
    return thing !== undefined ? thing - pos : 0;
  });
  return intervalSeries;
}

const makeChromaticIntervals = (
  yPositions: number[][],
  intervalType: ChromaticIntervalType
): number[] => {
  return Array(yPositions.length).fill(chromaticIntervalLookup[intervalType]);
};

const getModeShift = (type: ScaleType | ModeType) => {
  const lookup = new Map([
    ["naturalMinor", 9],
    ["harmonicMinor", 9],
    ["melodicMinor", 9],
    ["dorian", 2],
    ["phrygian", 4],
    ["lydian", 5],
    ["mixolydian", 7],
    ["aeolian", 9],
    ["locrian", 11],
  ]);
  return lookup.get(type) || 0;
};

const rootNoteYOffsets = {
  c: 0,
  cs: 1,
  d: 2,
  ds: 3,
  e: 4,
  f: 5,
  fs: 6,
  g: 7,
  gs: 8,
  a: 9,
  as: 10,
  b: 11,
};

const makeYPositions = (type: ScaleType | ModeType, root: Root) =>
  noteSeriesYPositions[type].map((pos) => [pos + rootNoteYOffsets[root]]);

const getNoteXOffset = (type?: ScaleType | ModeType) => {
  const lookup = new Map([
    // This was the previous chromatic number when it had one more note in it.
    // (It used to go C to C instead of C to B to match whole tone etc.)
    // ["chromatic", 4.9],
    ["chromatic", 5.3],
    ["wholeTone", 5.3],
    ["fourGroups", 5.3],
  ]);
  return (type && lookup.get(type)) || 4.25;
};

const makeXOffsets = (length: number, offset: number): number[] =>
  Array(length).fill(offset);

export const getXCoordinates = (
  indexesToDraw: number[],
  offsets: number[],
  leftMargin: number
) => {
  return indexesToDraw.slice(1).reduce(
    (result, index, step) => {
      const offset = offsets[index];
      const previousCoordinate = result[step];

      // Fallback should never happen, just here for TypeScript.
      return previousCoordinate && offset
        ? result.concat([previousCoordinate + offset])
        : result;
    },
    [leftMargin]
  );
};

function getCaptionLookup(seriesType: ScaleType | ModeType, rootIndex: number) {
  switch (seriesType) {
    case "chromatic":
    case "wholeTone":
    case "blues":
      return sharpFlatCaptions;
    default:
      break;
  }

  // normalize mode/root to major/ionian/root as if it were a major key
  // e.g. 7 Lydian --> 2 Ionian/Major
  const asMajor = (rootIndex - getModeShift(seriesType) + 12) % 12;

  // numbers correspond to sharp major keys: G D A E B F#
  // if asMajor is one of these numbers --> sharps
  const isSharps = [7, 2, 9, 4, 11, 6].indexOf(asMajor) !== -1;

  // TODO?: 6 sharps or flats (n === 6)
  // F needs to be E# (for sharps), else you get both F and F#
  // ? or B needs to be Cb (for flats)

  return isSharps ? sharpCaptions : flatCaptions;
}

function makeCaptions(seriesType: ScaleType | ModeType, root: Root) {
  // determine note captions: sharps, flats, or both, depending on key etc.
  const rootIndex = rootNoteYOffsets[root],
    captionLookup = getCaptionLookup(seriesType, rootIndex),
    rootFirstLookup = [
      ...captionLookup.slice(rootIndex),
      ...captionLookup.slice(0, rootIndex),
    ];
  return noteSeriesYPositions[seriesType].map((y) => ({
    // Fallback is for TypeScript.
    text: rootFirstLookup[(y - 40) % 12] || "",
  }));
}

const makeNoteDelays = (count: number) => Array(count).fill(defaultNoteDelay);

const makeIntervalDelays = (count: number) =>
  Array(count).fill(defaultIntervalDelay);

export function makeNoteSeries(
  type: ScaleType | ModeType,
  root: Root,
  intervalConfig?: IntervalConfig
): INoteSeries {
  const yPositions = makeYPositions(type, root);

  const intervals = !intervalConfig
    ? false
    : intervalConfig.flavor === "diatonic"
    ? makeDiatonicIntervals(yPositions, intervalConfig.type)
    : makeChromaticIntervals(yPositions, intervalConfig.type);

  const captions = intervals
    ? intervals.map((n) => ({
        // Fallback is for TypeScript
        text: intervalCaptions[n] || "",
        fontSize: "0.9",
      }))
    : makeCaptions(type, root);

  if (intervals) {
    yPositions.forEach((ypos, i) => {
      const ypos0 = ypos[0];
      const intervalsi = intervals[i];
      // Should always be defined, just here for TypeScript.
      if (ypos0 && intervalsi) {
        ypos.push(ypos0 + intervalsi);
      }
    });
  }
  return {
    yPositions: yPositions,
    xOffsets: makeXOffsets(yPositions.length, getNoteXOffset(type)),
    captions: captions,
    delays: intervalConfig
      ? makeIntervalDelays(yPositions.length)
      : makeNoteDelays(yPositions.length),
  };
}

export function makeInstrumentNoteSeries(
  yPositions: number[],
  delays: number[] | false
): INoteSeries {
  return {
    yPositions: yPositions.map((ypos) => [ypos]),
    xOffsets: makeXOffsets(yPositions.length, getNoteXOffset()),
    captions: yPositions.map((n) => ({
      // Fallback is for TypeScript
      text: sharpFlatCaptions[(n + 8) % 12] || "",
    })),
    // If no delays are supplied, use default even rhythm delays,
    // otherwise add a default delay for the final note.
    delays: delays
      ? delays.concat([defaultNoteDelay])
      : makeNoteDelays(yPositions.length),
  };
}
