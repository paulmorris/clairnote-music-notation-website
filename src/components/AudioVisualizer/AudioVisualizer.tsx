import type {
  AudioStatus,
  AvrInputMode,
  ChromaticIntervalType,
  DiatonicIntervalType,
  InstrumentNotePlayedAction,
  IntervalConfig,
  IntervalFlavor,
  IntervalScaleType,
  KeyboardRange,
  ModeType,
  PlaybackRhythm,
  Root,
  ScaleType,
} from "./AudioVisualizerTypes";
import { Staff } from "./staff";
import { iota } from "../../utils/utils";

import avStyles from "./audiovisualizer.module.css";
import { makePiano } from "./audio-load";
import { createSignal } from "solid-js";
import {
  convertIntervalConfig,
  getXCoordinates,
  makeInstrumentNoteSeries,
  makeNoteSeries,
} from "./store-functions";
import { TopButtonRow } from "./TopButtonRow";
import { SecondaryControls } from "./SecondaryControls";

// TODO: use these in index.js
/* const staffLinesY = [1.6747, 3.1747, 6.1747, 7.6747]*/
/* const staffLinesX = 0.3347*/

const maxNotesOnStaff = 15;
const leftMargin = 3.1347;

export const AudioVisualizer = (props: {
  sn: boolean;
  inputMode?: AvrInputMode;
  hideNoteSeriesMenu?: boolean;
}) => {
  // PRIMARY STATE

  const [audioStatus, setAudioStatus] = createSignal<AudioStatus>("loading");
  const [volume, setVolume] = createSignal(0.35);
  const [notePlaying, setNotePlaying] = createSignal<number | undefined>(
    undefined
  );

  const [inputMode, setInputMode] = createSignal<AvrInputMode>(
    props.inputMode || "scales"
  );

  const [scaleType, setScaleType] = createSignal<ScaleType>("major");
  const [scaleRoot, setScaleRoot] = createSignal<Root>("c");

  const [modeType, setModeType] = createSignal<ModeType>("ionian");
  const [modeRoot, setModeRoot] = createSignal<Root>("c");

  const [intervalScaleType, setIntervalScaleType] =
    createSignal<IntervalScaleType>("fourGroups");

  const [intervalConfig, setIntervalConfig] = createSignal<IntervalConfig>({
    flavor: "chromatic",
    type: "majorThirds",
  });

  const [instrumentNotes, setInstrumentNotes] = createSignal<number[]>([]);
  const [instrumentDelays, setInstrumentDelays] = createSignal<number[]>([]);

  const [instrumentTimestamp, setInstrumentTimestamp] = createSignal<
    number | undefined
  >(undefined);

  const [playbackRhythm, setPlaybackRhythm] =
    createSignal<PlaybackRhythm>("even");

  let playbackTimerId: number | undefined;
  const setPlaybackTimerId = (id: number | undefined) => (playbackTimerId = id);

  const piano = makePiano(() => setAudioStatus("stopped"));

  // DERIVED STATE

  const scaleNoteSeries = () => makeNoteSeries(scaleType(), scaleRoot());
  const modeNoteSeries = () => makeNoteSeries(modeType(), modeRoot());

  const intervalNoteSeries = () =>
    makeNoteSeries(intervalScaleType(), "c", intervalConfig());

  const instrumentNoteSeries = () => {
    const delays = playbackRhythm() === "even" ? false : instrumentDelays();
    return makeInstrumentNoteSeries(instrumentNotes(), delays);
  };

  const currentNoteSeries = () => {
    switch (inputMode()) {
      case "keyboard-trad":
      // Fall through.
      case "keyboard-six-six":
      // Fall through.
      case "guitar":
      // Fall through.
      case "violin":
        return instrumentNoteSeries();
      case "scales":
        return scaleNoteSeries();
      case "intervals":
        return intervalNoteSeries();
      case "modes":
        return modeNoteSeries();
    }
  };

  const noteIndexesToDraw = () => {
    // With instrument entry we may only show part of a series, so we calculate
    // which part, the indexes.
    const seriesLength = currentNoteSeries().yPositions.length;
    const note = notePlaying();
    const status = audioStatus();

    let firstIndex = 0;

    // Order matters, after the first case the rest are more notes than fit
    // on the staff via instrument entry.
    if (seriesLength <= maxNotesOnStaff) {
      firstIndex = 0;
    } else if (status === "stopped") {
      // When not playing audio, show last notes in the series.
      firstIndex = seriesLength - maxNotesOnStaff;
    } else if (status === "playing") {
      if (note === undefined) {
        // Show first notes in series when about to start playing.
        firstIndex = 0;
      } else {
        // When a note is playing.
        firstIndex = note >= maxNotesOnStaff ? note - (maxNotesOnStaff - 1) : 0;
      }
    }

    const notesOnStaff =
      seriesLength < maxNotesOnStaff ? seriesLength : maxNotesOnStaff;

    return iota(notesOnStaff, firstIndex);
  };

  const xCoordinates = () =>
    getXCoordinates(
      noteIndexesToDraw(),
      currentNoteSeries().xOffsets,
      leftMargin
    );

  const keyboardRange: KeyboardRange = {
    // Just over 3 octaves from E (32) to F (69).
    lowestNote: 32,
    highestNote: 69,
  };

  // ACTIONS

  const stopPlayback = () => {
    if (audioStatus() === "playing") {
      window.clearTimeout(playbackTimerId);
      setAudioStatus("stopped");
      setNotePlaying(undefined);
    }
  };

  const setVolumeAction = (volume: number) => {
    piano.volume(volume);
    setVolume(volume);
  };
  const setInputModeAction = (inputMode: AvrInputMode) => {
    stopPlayback();
    setInputMode(inputMode);
  };
  const setScaleTypeAction = (type: ScaleType) => {
    stopPlayback();
    setScaleType(type);
  };
  const setScaleRootAction = (root: Root) => {
    stopPlayback();
    setScaleRoot(root);
  };
  const setModeTypeAction = (type: ModeType) => {
    stopPlayback();
    setModeType(type);
  };
  const setModeRootAction = (root: Root) => {
    stopPlayback();
    setModeRoot(root);
  };
  const setIntervalConfigChromatic = (type: ChromaticIntervalType) => {
    stopPlayback();
    setIntervalConfig({ flavor: "chromatic", type });
  };
  const setIntervalConfigDiatonic = (type: DiatonicIntervalType) => {
    stopPlayback();
    setIntervalConfig({ flavor: "diatonic", type });
  };
  const setIntervalScaleAction = (
    scaleType: IntervalScaleType,
    flavor: IntervalFlavor
  ) => {
    stopPlayback();
    const flavorChanged = intervalConfig().flavor !== flavor;

    if (flavorChanged) {
      setIntervalConfig(convertIntervalConfig(intervalConfig()));
    }

    setIntervalScaleType(scaleType);
  };

  const instrumentNotePlayedAction: InstrumentNotePlayedAction = (
    note: number
  ) => {
    stopPlayback();
    const stamp = Date.now();
    piano.play(note);
    setInstrumentNotes([...instrumentNotes(), note]);

    const previousStamp = instrumentTimestamp();
    if (previousStamp) {
      const rawDelay = stamp - previousStamp;
      const delay = rawDelay > 2000 ? 2000 : rawDelay;
      setInstrumentDelays([...instrumentDelays(), delay]);
    }
    setInstrumentTimestamp(stamp);
  };

  const clearStaff = () => {
    stopPlayback();
    setInstrumentNotes([]);
    setInstrumentDelays([]);
    setInstrumentTimestamp(undefined);
  };

  return (
    <div class={avStyles.audiovisualizer}>
      <Staff
        staffLinesY={[1.6747, 3.1747, 6.1747, 7.6747]}
        staffLinesX={0.3347}
        noteSeries={currentNoteSeries()}
        noteIndexesToDraw={noteIndexesToDraw()}
        xCoordinates={xCoordinates()}
        notePlaying={notePlaying()}
        width="199.12mm"
        height="67mm"
        viewBox="0 0 66.6537 16.5962"
        clairnoteType={props.sn ? "sn" : "dn"}
      />
      <TopButtonRow
        audioStatus={audioStatus()}
        inputMode={inputMode()}
        setInputMode={setInputModeAction}
        stopPlayback={stopPlayback}
        hideNoteSeriesMenu={props.hideNoteSeriesMenu}
        currentNoteSeries={currentNoteSeries()}
        setAudioStatus={setAudioStatus}
        setPlaybackTimerId={setPlaybackTimerId}
        piano={piano}
        setNotePlaying={setNotePlaying}
        volume={volume()}
        setVolume={setVolumeAction}
      />
      <SecondaryControls
        inputMode={inputMode()}
        scaleType={scaleType()}
        setScaleType={setScaleTypeAction}
        modeType={modeType()}
        setModeType={setModeTypeAction}
        scaleRoot={scaleRoot()}
        setScaleRoot={setScaleRootAction}
        modeRoot={modeRoot()}
        setModeRoot={setModeRootAction}
        intervalScaleType={intervalScaleType()}
        setIntervalScaleAction={setIntervalScaleAction}
        intervalConfig={intervalConfig()}
        setIntervalConfigChromatic={setIntervalConfigChromatic}
        setIntervalConfigDiatonic={setIntervalConfigDiatonic}
        keyboardRange={keyboardRange}
        instrumentNotePlayedAction={instrumentNotePlayedAction}
        playbackRhythm={playbackRhythm()}
        setPlaybackRhythm={setPlaybackRhythm}
        clearStaff={clearStaff}
      />
    </div>
  );
};
