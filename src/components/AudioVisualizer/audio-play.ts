import type { AudioStatus, INoteSeries } from "./AudioVisualizerTypes";
import type { Piano } from "./audio-load";

// Keep playing a series of notes or intervals. As long as there are more notes
// to play the function keeps scheduling itself to be called again at a given
// delay to correctly time the notes.

const playNext = (
  index: number,
  series: INoteSeries,
  piano: Piano,
  setNotePlaying: (note: number) => void,
  setPlaybackTimerId: (id: number) => void,
  stopPlayback: () => void
) => {
  const timestamp = Date.now();
  const notes = series.yPositions[index];

  for (const n of notes || []) {
    piano.play(n);
  }
  setNotePlaying(index);

  const otherDelay = series.delays[index];

  // otherDelay should always be defined, but TS says it might not be.
  const delay = otherDelay ? otherDelay - (Date.now() - timestamp) : 500;

  if (index < series.yPositions.length - 1) {
    setPlaybackTimerId(
      window.setTimeout(
        playNext,
        delay,
        // arguments
        index + 1,
        series,
        piano,
        setNotePlaying,
        setPlaybackTimerId,
        stopPlayback
      )
    );
  } else {
    setPlaybackTimerId(window.setTimeout(() => stopPlayback(), delay));
  }
};

export const startPlayback = (
  setAudioStatus: (status: AudioStatus) => void,
  currentNoteSeries: INoteSeries,
  piano: Piano,
  setNotePlaying: (note: number) => void,
  setPlaybackTimerId: (id: number) => void,
  stopPlayback: () => void
) => {
  const series = currentNoteSeries;
  if (series.yPositions.length > 0) {
    setAudioStatus("playing");
    setPlaybackTimerId(
      window.setTimeout(
        playNext,
        500,
        // arguments
        0,
        series,
        piano,
        setNotePlaying,
        setPlaybackTimerId,
        stopPlayback
      )
    );
  }
};
