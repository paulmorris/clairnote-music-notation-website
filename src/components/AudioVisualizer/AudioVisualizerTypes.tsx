export interface KeyboardRange {
  lowestNote: number;
  highestNote: number;
}

export type IKeyboardType = "Traditional" | "Janko";

export type InstrumentNotePlayedAction = (note: number) => void;

export type IntervalScaleType =
  | "major"
  | "naturalMinor"
  | "wholeTone"
  | "chromatic"
  | "fourGroups";

export type IntervalFlavor = "diatonic" | "chromatic";

export type DiatonicIntervalType =
  | "seconds"
  | "thirds"
  | "fourths"
  | "fifths"
  | "sixths"
  | "sevenths"
  | "octaves";

export type ChromaticIntervalType =
  | "minorSeconds"
  | "majorSeconds"
  | "minorThirds"
  | "majorThirds"
  | "perfectFourths"
  | "tritones"
  | "perfectFifths"
  | "minorSixths"
  | "majorSixths"
  | "minorSevenths"
  | "majorSevenths"
  | "octaves";

export type ScaleType =
  | "major"
  | "naturalMinor"
  | "harmonicMinor"
  | "melodicMinor"
  | "blues"
  | "chromatic"
  | "wholeTone"
  | "fourGroups";

export type ModeType =
  | "ionian"
  | "dorian"
  | "phrygian"
  | "lydian"
  | "mixolydian"
  | "aeolian"
  | "locrian";

export type Root =
  | "c"
  | "d"
  | "e"
  | "fs"
  | "gs"
  | "as"
  | "cs"
  | "ds"
  | "f"
  | "g"
  | "a"
  | "b";

export interface INoteSeriesCaption {
  text: string;
  fontSize?: string;
  fill?: string;
  fontWeight?: string;
}

export interface INoteSeries {
  yPositions: number[][];
  captions: INoteSeriesCaption[];
  xOffsets: number[];
  delays: number[];
}

export type IntervalConfig =
  | { flavor: "diatonic"; type: DiatonicIntervalType }
  | { flavor: "chromatic"; type: ChromaticIntervalType };

export type AvrInputMode =
  | "keyboard-trad"
  | "keyboard-six-six"
  | "guitar"
  | "violin"
  | "scales"
  | "intervals"
  | "modes";

export type AudioStatus = "loading" | "stopped" | "playing";

export type PlaybackRhythm = "even" | "asPlayed";
