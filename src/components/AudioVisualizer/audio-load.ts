import { Howl } from "howler";

export interface Piano {
  play: (note: number) => number;
  volume: (volume: number) => number | Howl;
}

type Sprite = {
  [key: string]: [number, number];
};

const DEFAULT_VOLUME = 0.35;

let howl: Howl;

/**
 * Use a single "Howl" object (for efficiency reasons), but use it from our own
 * "Piano" objects so that we can have separate volume settings for
 * AudioVisualizer and Game.
 */
export const makePiano = (loadHandler?: () => void): Piano => {
  if (!howl) {
    const makeHowl = () => {
      /* create an object like this to define the audio sprite:
      {
        '32': [   0, 1500],  // E
        '33': [2000, 1500],  // F
        '34': [4000, 1500],  // F#
        ~~~~~~~~~~~~~~~~~~~~~~~~~~
        '68': [72000, 1500], // E
        '69': [74000, 1500]  // F
      }
      */

      const sprite: Sprite = {};
      let key = 32;

      while (key <= 69) {
        sprite[key] = [(key - 32) * 2000, 1500];
        key += 1;
      }

      return new Howl({
        src: [
          // audio sprite urls
          "/audio/sprites-3octaves-2secs-mono.mp3",
          "/audio/sprites-3octaves-2secs-mono.ogg",
        ],
        volume: DEFAULT_VOLUME,
        sprite,
      });
    };

    howl = makeHowl();
  }

  let savedVolume = DEFAULT_VOLUME;

  if (loadHandler) {
    const state = howl.state();
    if (state === "loaded") {
      loadHandler();
    } else if (state === "loading") {
      howl.once("load", loadHandler);
    }
  }

  return {
    play(note: number) {
      if (howl.volume() !== savedVolume) {
        howl.volume(savedVolume);
      }
      // If you pass a number to `play` it interprets it as an ID, not as a
      // sprite object key (which is what we want and get by passing a string).
      return howl.play(note.toString());
    },

    volume(volume: number) {
      savedVolume = volume;
      return howl.volume(volume);
    },
  };
};
