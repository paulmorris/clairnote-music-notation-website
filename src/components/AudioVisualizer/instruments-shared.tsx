import { makeSelectOptions } from "../../utils/utils";
import type { PlaybackRhythm } from "./AudioVisualizerTypes";
import type { SelectOption } from "../../types/types";

import avStyles from "./audiovisualizer.module.css";

export const PlaybackRhythmSelect = (props: {
  playbackRhythm: PlaybackRhythm;
  setPlaybackRhythm: (playbackRhythm: PlaybackRhythm) => void;
}) => {
  const playbackRhythmOptions: SelectOption<PlaybackRhythm>[] = [
    { value: "even", label: "Rhythm: Even" },
    { value: "asPlayed", label: "Rhythm: As Played" },
  ];

  return (
    <div class={avStyles.buttonOrSelectSpacer}>
      <div class="select is-small">
        <select
          name="Playback Rhythm"
          title="Playback Rhythm"
          onChange={(event) => {
            const value = event.currentTarget.value;
            props.setPlaybackRhythm(value as PlaybackRhythm);
          }}
        >
          {makeSelectOptions(playbackRhythmOptions, props.playbackRhythm)}
        </select>
      </div>
    </div>
  );
};

export const ClearStaffButton = (props: { clearStaff: () => void }) => {
  return (
    <div class={avStyles.buttonOrSelectSpacer}>
      <button class={"button is-small"} onClick={() => props.clearStaff()}>
        Clear Staff
      </button>
    </div>
  );
};
