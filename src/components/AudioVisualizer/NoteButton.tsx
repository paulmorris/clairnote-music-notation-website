import styles from "./note-button.module.css";
import { For, Show } from "solid-js";

export const NoteButton = (props: {
  instrumentNotePlayedAction: (note: number) => void;
  label: string;
  note: number;
  type: "white" | "black" | "spacer";
  className?: string;
  disabled?: boolean;
}) => {
  const typeClass = () => {
    return props.type === "white"
      ? styles.whiteNoteButton
      : props.type === "black"
      ? styles.blackNoteButton
      : props.type === "spacer"
      ? styles.spacerNoteButton
      : "";
  };

  const classes = () =>
    `${styles.noteButton} ${typeClass()} ${props.className}`;

  return (
    // Fallback is an invisible spacer button (identified by having no label).
    <Show when={props.label.length > 0} fallback={<div class={classes()} />}>
      <button
        onClick={() => props.instrumentNotePlayedAction(props.note)}
        class={classes()}
        disabled={props.disabled ?? false}
      >
        <Show when={props.label.length === 1}>
          <div class={styles.noteButtonLabel}>{props.label}</div>
        </Show>

        <Show when={props.label.length > 1}>
          <For each={props.label.split("/")}>
            {(textPart, index) => {
              const labelTwoClass =
                index() === 0 ? "" : styles.noteButtonLabelTwo;

              return (
                <div class={`${styles.noteButtonLabel} ${labelTwoClass}`}>
                  {textPart}
                </div>
              );
            }}
          </For>
        </Show>
      </button>
    </Show>
  );
};
