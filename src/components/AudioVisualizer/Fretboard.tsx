import { getNoteCaption } from "./audiovisualizer-utils";
import { NoteButton } from "./NoteButton";

import styles from "./instruments.module.css";
import { For } from "solid-js";
import type { InstrumentNotePlayedAction } from "./AudioVisualizerTypes";

type FretboardConfig = number[][];

export const guitarFretboardConfig: FretboardConfig = [
  [32, 37, 42, 47, 51, 56],
  [33, 38, 43, 48, 52, 57],
  [34, 39, 44, 49, 53, 58],
  [35, 40, 45, 50, 54, 59],
  [36, 41, 46, 51, 55, 60],
];

export const violinFretboardConfig: FretboardConfig = [
  [35, 42, 49, 56],
  [36, 43, 50, 57],
  [37, 44, 51, 58],
  [38, 45, 52, 59],
  [39, 46, 53, 60],
  [40, 47, 54, 61],
  [41, 48, 55, 62],
];

export const Fretboard = (props: {
  fretboardConfig: FretboardConfig;
  instrumentNotePlayedAction: InstrumentNotePlayedAction;
  enabledNotes?: number[];
}) => {
  return (
    <div class={styles.fretboardContainer}>
      <For each={props.fretboardConfig}>
        {(row, index) => (
          <div>
            <For each={row}>
              {(note) => (
                <NoteButton
                  instrumentNotePlayedAction={props.instrumentNotePlayedAction}
                  label={getNoteCaption(note)}
                  note={note}
                  type={index() === 0 ? "black" : "white"}
                  className={
                    index() === 0 ? styles.fretboardOpenStringButton || "" : ""
                  }
                  disabled={
                    props.enabledNotes
                      ? !props.enabledNotes.includes(note)
                      : false
                  }
                />
              )}
            </For>
          </div>
        )}
      </For>
    </div>
  );
};
