import type {
  InstrumentNotePlayedAction,
  KeyboardRange,
} from "./AudioVisualizerTypes";
import { range } from "../../utils/utils";
import { NoteButton } from "./NoteButton";
import { getNoteCaption } from "./audiovisualizer-utils";

import styles from "./instruments.module.css";
import noteButtonStyles from "./note-button.module.css";
import { For, createMemo } from "solid-js";

type KeyboardType = "keyboard-trad" | "keyboard-six-six";

type NoteButton = {
  label: string;
  note: number;
  type: "white" | "black" | "spacer";
};

type PageType = "avr" | "game";

const makeSpacerNoteButton = (note: number): NoteButton => ({
  label: "",
  note: note + 0.5,
  type: "spacer",
});

const makeNoteButtons = (
  lowestNote: number,
  highestNote: number,
  isTradKeyboard: boolean
): NoteButton[] => {
  const notes = range(lowestNote, highestNote);

  const noteButtons = notes.reduce<NoteButton[]>((result, note, index) => {
    const label = getNoteCaption(note);
    const type = label.length === 1 ? "white" : "black";

    result.push({ label, note, type });

    if (
      isTradKeyboard &&
      (label === "E" || label === "B") &&
      index !== notes.length - 1
    ) {
      // Add a spacer.
      result.push(makeSpacerNoteButton(note));
    }

    return result;
  }, []);

  return noteButtons;
};

const isEven = (_: NoteButton, i: number) => i % 2 === 0;
const isOdd = (_: NoteButton, i: number) => i % 2 !== 0;

const makeJankoRows = (noteButtons: NoteButton[]) => {
  const topRow = noteButtons.filter(isEven).concat([makeSpacerNoteButton(69)]);
  const bottomRow = noteButtons.filter(isOdd);
  return { topRow, bottomRow };
};

const makeTradRows = (noteButtons: NoteButton[]) => {
  const topRow = noteButtons.filter(isOdd);
  const bottomRow = noteButtons.filter(isEven);
  return { topRow, bottomRow };
};

const makeAudioVisualizerNoteClass = (note: number) => {
  return note >= 32 && note <= 39.5
    ? noteButtonStyles.avrNoteButtonOctaveThree
    : note >= 53 && note <= 64
    ? noteButtonStyles.avrNoteButtonOctaveTwo
    : note >= 65
    ? noteButtonStyles.avrNoteButtonOctaveThree
    : "";
};

const makeGameNoteClass = (note: number) => {
  // The `note <= 64` is needed to exclude janko keyboard spacer key.
  return note >= 51.5 && note <= 64
    ? noteButtonStyles.gameNoteButtonOctaveTwo
    : "";
};

export const Keyboard = (props: {
  keyboardType: KeyboardType;
  keyboardRange: KeyboardRange;
  instrumentNotePlayedAction: InstrumentNotePlayedAction;
  pageType: PageType;
  enabledNotes?: number[];
}) => {
  const isTradKeyboard = () => props.keyboardType === "keyboard-trad";

  const noteButtons = createMemo(() =>
    makeNoteButtons(
      props.keyboardRange.lowestNote,
      props.keyboardRange.highestNote,
      isTradKeyboard()
    )
  );

  const makeRows = createMemo(() =>
    isTradKeyboard()
      ? makeTradRows(noteButtons())
      : makeJankoRows(noteButtons())
  );

  const renderNoteButton = (noteButton: NoteButton) => (
    <NoteButton
      instrumentNotePlayedAction={props.instrumentNotePlayedAction}
      label={noteButton.label}
      note={noteButton.note}
      type={noteButton.type}
      className={
        props.pageType === "avr"
          ? makeAudioVisualizerNoteClass(noteButton.note) || ""
          : props.pageType === "game"
          ? makeGameNoteClass(noteButton.note) || ""
          : ""
      }
      disabled={
        props.enabledNotes
          ? !props.enabledNotes.includes(noteButton.note)
          : false
      }
    />
  );

  return (
    <>
      <div class={styles.keyboardRowsContainer}>
        <div>
          <For each={makeRows().topRow}>{renderNoteButton}</For>
        </div>
        <div>
          <For each={makeRows().bottomRow}>{renderNoteButton}</For>
        </div>
      </div>
    </>
  );
};
