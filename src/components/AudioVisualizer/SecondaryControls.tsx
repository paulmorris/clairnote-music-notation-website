import {
  Fretboard,
  guitarFretboardConfig,
  violinFretboardConfig,
} from "./Fretboard";
import { Keyboard } from "./Keyboard";
import avStyles from "./audiovisualizer.module.css";
import utilStyles from "../../styles/util.module.css";
import { ClearStaffButton, PlaybackRhythmSelect } from "./instruments-shared";
import type {
  AvrInputMode,
  ChromaticIntervalType,
  DiatonicIntervalType,
  InstrumentNotePlayedAction,
  IntervalConfig,
  IntervalFlavor,
  IntervalScaleType,
  KeyboardRange,
  ModeType,
  PlaybackRhythm,
  Root,
  ScaleType,
} from "./AudioVisualizerTypes";
import { Show } from "solid-js";
import type { ParentComponent } from "solid-js";

const ScaleTypeButton: ParentComponent<{
  clickHandler: () => void;
  pressed: boolean;
}> = (props) => (
  <div class={`control ${utilStyles.marginBottomMinusOnePx}`}>
    <button
      onClick={props.clickHandler}
      class={`button is-small ${utilStyles.buttonMinHeight} ${
        avStyles.scaleTypeButton
      } ${props.pressed ? avStyles.buttonPressed : ""}`}
    >
      {props.children}
    </button>
  </div>
);

const ScaleRootButton: ParentComponent<{
  clickHandler: () => void;
  pressed: boolean;
}> = (props) => (
  <div class="control">
    <button
      onClick={props.clickHandler}
      class={`button is-small ${utilStyles.buttonMinHeight} ${
        avStyles.scaleRootButton
      } ${props.pressed ? avStyles.buttonPressed : ""}`}
    >
      {props.children}
    </button>
  </div>
);

const IntervalScaleButton: ParentComponent<{
  clickHandler: () => void;
  pressed: boolean;
}> = (props) => (
  <div class="control">
    <button
      onClick={props.clickHandler}
      class={`button is-small ${utilStyles.buttonMinHeight} ${
        props.pressed ? avStyles.buttonPressed : ""
      }`}
    >
      {props.children}
    </button>
  </div>
);

const ChromaticIntervalButton: ParentComponent<{
  clickHandler: () => void;
  pressed: boolean;
  title: string;
}> = (props) => (
  <div class="control">
    <button
      title={props.title}
      onClick={props.clickHandler}
      class={`button is-small ${utilStyles.buttonMinHeight} ${
        avStyles.chromaticIntervalButton
      } ${props.pressed ? avStyles.buttonPressed : ""}`}
    >
      {props.children}
    </button>
  </div>
);

const DiatonicIntervalButton: ParentComponent<{
  clickHandler: () => void;
  pressed: boolean;
  title: string;
}> = (props) => (
  <div class="control">
    <button
      title={props.title}
      onClick={props.clickHandler}
      class={`button is-small ${utilStyles.buttonMinHeight} ${
        avStyles.diatonicIntervalButton
      } ${props.pressed ? avStyles.buttonPressed : ""}`}
    >
      {props.children}
    </button>
  </div>
);

const BelowKeyboardControls = (props: {
  playbackRhythm: PlaybackRhythm;
  setPlaybackRhythm: (playbackRhythm: PlaybackRhythm) => void;
  clearStaff: () => void;
}) => (
  <div>
    <PlaybackRhythmSelect
      playbackRhythm={props.playbackRhythm}
      setPlaybackRhythm={props.setPlaybackRhythm}
    />
    <ClearStaffButton clearStaff={props.clearStaff} />
  </div>
);

export const SecondaryControls = (props: {
  inputMode: AvrInputMode;
  scaleType: ScaleType;
  setScaleType: (type: ScaleType) => void;
  modeType: ModeType;
  setModeType: (type: ModeType) => void;
  scaleRoot: Root;
  setScaleRoot: (root: Root) => void;
  modeRoot: Root;
  setModeRoot: (root: Root) => void;
  intervalScaleType: IntervalScaleType;
  setIntervalScaleAction: (
    scaleType: IntervalScaleType,
    flavor: IntervalFlavor
  ) => void;
  intervalConfig: IntervalConfig;
  setIntervalConfigChromatic: (type: ChromaticIntervalType) => void;
  setIntervalConfigDiatonic: (type: DiatonicIntervalType) => void;
  keyboardRange: KeyboardRange;
  instrumentNotePlayedAction: InstrumentNotePlayedAction;
  playbackRhythm: PlaybackRhythm;
  setPlaybackRhythm: (playbackRhythm: PlaybackRhythm) => void;
  clearStaff: () => void;
}) => {
  const makeSetRoot = () =>
    props.inputMode === "modes" ? props.setModeRoot : props.setScaleRoot;

  const rootButtonPressed = (root: Root) =>
    props.inputMode === "modes"
      ? props.modeRoot === root
      : props.scaleRoot === root;

  return (
    <>
      <Show when={props.inputMode === "scales" || props.inputMode === "modes"}>
        <div class={avStyles.subcontrols}>
          <Show when={props.inputMode === "scales"}>
            <div class={avStyles.scalesButtonRow}>
              <div
                class={`field has-addons has-addons-centered ${utilStyles.flexWrapWrap}`}
              >
                <ScaleTypeButton
                  clickHandler={() => props.setScaleType("major")}
                  pressed={props.scaleType === "major"}
                >
                  Major
                </ScaleTypeButton>
                <ScaleTypeButton
                  clickHandler={() => props.setScaleType("naturalMinor")}
                  pressed={props.scaleType === "naturalMinor"}
                >
                  Natural Minor
                </ScaleTypeButton>
                <ScaleTypeButton
                  clickHandler={() => props.setScaleType("harmonicMinor")}
                  pressed={props.scaleType === "harmonicMinor"}
                >
                  Harmonic Minor
                </ScaleTypeButton>
                <ScaleTypeButton
                  clickHandler={() => props.setScaleType("melodicMinor")}
                  pressed={props.scaleType === "melodicMinor"}
                >
                  Melodic Minor
                </ScaleTypeButton>
                <ScaleTypeButton
                  clickHandler={() => props.setScaleType("blues")}
                  pressed={props.scaleType === "blues"}
                >
                  Blues
                </ScaleTypeButton>
                <ScaleTypeButton
                  clickHandler={() => props.setScaleType("chromatic")}
                  pressed={props.scaleType === "chromatic"}
                >
                  Chromatic
                </ScaleTypeButton>
                <ScaleTypeButton
                  clickHandler={() => props.setScaleType("wholeTone")}
                  pressed={props.scaleType === "wholeTone"}
                >
                  Whole Tone
                </ScaleTypeButton>
              </div>
            </div>
          </Show>

          <Show when={props.inputMode === "modes"}>
            <div class={avStyles.modesButtonRow}>
              <div
                class={`field has-addons has-addons-centered ${utilStyles.flexWrapWrap}`}
              >
                <ScaleTypeButton
                  clickHandler={() => props.setModeType("ionian")}
                  pressed={props.modeType === "ionian"}
                >
                  Ionian
                </ScaleTypeButton>
                <ScaleTypeButton
                  clickHandler={() => props.setModeType("dorian")}
                  pressed={props.modeType === "dorian"}
                >
                  Dorian
                </ScaleTypeButton>
                <ScaleTypeButton
                  clickHandler={() => props.setModeType("phrygian")}
                  pressed={props.modeType === "phrygian"}
                >
                  Phrygian
                </ScaleTypeButton>
                <ScaleTypeButton
                  clickHandler={() => props.setModeType("lydian")}
                  pressed={props.modeType === "lydian"}
                >
                  Lydian
                </ScaleTypeButton>
                <ScaleTypeButton
                  clickHandler={() => props.setModeType("mixolydian")}
                  pressed={props.modeType === "mixolydian"}
                >
                  Mixolydian
                </ScaleTypeButton>
                <ScaleTypeButton
                  clickHandler={() => props.setModeType("aeolian")}
                  pressed={props.modeType === "aeolian"}
                >
                  Aeolian
                </ScaleTypeButton>
                <ScaleTypeButton
                  clickHandler={() => props.setModeType("locrian")}
                  pressed={props.modeType === "locrian"}
                >
                  Locrian
                </ScaleTypeButton>
              </div>
            </div>
          </Show>

          <div class={avStyles.scaleRootButtonsTopRow}>
            <div class={"field has-addons has-addons-centered"}>
              <ScaleRootButton
                clickHandler={() => makeSetRoot()("c")}
                pressed={rootButtonPressed("c")}
              >
                C
              </ScaleRootButton>
              <ScaleRootButton
                clickHandler={() => makeSetRoot()("d")}
                pressed={rootButtonPressed("d")}
              >
                D
              </ScaleRootButton>
              <ScaleRootButton
                clickHandler={() => makeSetRoot()("e")}
                pressed={rootButtonPressed("e")}
              >
                E
              </ScaleRootButton>
              <ScaleRootButton
                clickHandler={() => makeSetRoot()("fs")}
                pressed={rootButtonPressed("fs")}
              >
                F#/Gb
              </ScaleRootButton>
              <ScaleRootButton
                clickHandler={() => makeSetRoot()("gs")}
                pressed={rootButtonPressed("gs")}
              >
                G#/Ab
              </ScaleRootButton>
              <ScaleRootButton
                clickHandler={() => makeSetRoot()("as")}
                pressed={rootButtonPressed("as")}
              >
                A#/Bb
              </ScaleRootButton>
            </div>
          </div>

          <div class={avStyles.scaleRootButtonsBottomRow}>
            <div class={"field has-addons has-addons-centered"}>
              <ScaleRootButton
                clickHandler={() => makeSetRoot()("cs")}
                pressed={rootButtonPressed("cs")}
              >
                C#/Db
              </ScaleRootButton>
              <ScaleRootButton
                clickHandler={() => makeSetRoot()("ds")}
                pressed={rootButtonPressed("ds")}
              >
                D#/Eb
              </ScaleRootButton>
              <ScaleRootButton
                clickHandler={() => makeSetRoot()("f")}
                pressed={rootButtonPressed("f")}
              >
                F
              </ScaleRootButton>
              <ScaleRootButton
                clickHandler={() => makeSetRoot()("g")}
                pressed={rootButtonPressed("g")}
              >
                G
              </ScaleRootButton>
              <ScaleRootButton
                clickHandler={() => makeSetRoot()("a")}
                pressed={rootButtonPressed("a")}
              >
                A
              </ScaleRootButton>
              <ScaleRootButton
                clickHandler={() => makeSetRoot()("b")}
                pressed={rootButtonPressed("b")}
              >
                B
              </ScaleRootButton>
            </div>
          </div>
        </div>
      </Show>

      <Show when={props.inputMode === "keyboard-trad"}>
        <div class={avStyles.subcontrols}>
          <Keyboard
            keyboardType={"keyboard-trad"}
            keyboardRange={props.keyboardRange}
            instrumentNotePlayedAction={props.instrumentNotePlayedAction}
            pageType={"avr"}
          />
          <BelowKeyboardControls
            playbackRhythm={props.playbackRhythm}
            setPlaybackRhythm={props.setPlaybackRhythm}
            clearStaff={props.clearStaff}
          />
        </div>
      </Show>

      <Show when={props.inputMode === "keyboard-six-six"}>
        <div class={avStyles.subcontrols}>
          <Keyboard
            keyboardType={"keyboard-six-six"}
            keyboardRange={props.keyboardRange}
            instrumentNotePlayedAction={props.instrumentNotePlayedAction}
            pageType={"avr"}
          />
          <BelowKeyboardControls
            playbackRhythm={props.playbackRhythm}
            setPlaybackRhythm={props.setPlaybackRhythm}
            clearStaff={props.clearStaff}
          />
        </div>
      </Show>

      <Show when={props.inputMode === "guitar"}>
        <div class={avStyles.subcontrols}>
          <Fretboard
            fretboardConfig={guitarFretboardConfig}
            instrumentNotePlayedAction={props.instrumentNotePlayedAction}
          />
          <div>
            <PlaybackRhythmSelect
              playbackRhythm={props.playbackRhythm}
              setPlaybackRhythm={props.setPlaybackRhythm}
            />
            <ClearStaffButton clearStaff={props.clearStaff} />
          </div>
        </div>
      </Show>

      <Show when={props.inputMode === "violin"}>
        <div class={avStyles.subcontrols}>
          <Fretboard
            fretboardConfig={violinFretboardConfig}
            instrumentNotePlayedAction={props.instrumentNotePlayedAction}
          />
          <div>
            <PlaybackRhythmSelect
              playbackRhythm={props.playbackRhythm}
              setPlaybackRhythm={props.setPlaybackRhythm}
            />
            <ClearStaffButton clearStaff={props.clearStaff} />
          </div>
        </div>
      </Show>

      <Show when={props.inputMode === "intervals"}>
        <div class={avStyles.subcontrols}>
          <div class={avStyles.intervalScaleButtonRow}>
            <div class={"field has-addons has-addons-centered"}>
              <IntervalScaleButton
                clickHandler={() =>
                  props.setIntervalScaleAction("fourGroups", "chromatic")
                }
                pressed={props.intervalScaleType === "fourGroups"}
              >
                <span>
                  <span class={avStyles.wideText}>Four </span>Groups
                </span>
              </IntervalScaleButton>
              <IntervalScaleButton
                clickHandler={() =>
                  props.setIntervalScaleAction("wholeTone", "chromatic")
                }
                pressed={props.intervalScaleType === "wholeTone"}
              >
                <span>
                  Whole Tone <span class={avStyles.wideText}>Scales</span>
                </span>
              </IntervalScaleButton>
              <IntervalScaleButton
                clickHandler={() =>
                  props.setIntervalScaleAction("chromatic", "chromatic")
                }
                pressed={props.intervalScaleType === "chromatic"}
              >
                <span>
                  Chromatic <span class={avStyles.wideText}>Scale</span>
                </span>
              </IntervalScaleButton>
              <IntervalScaleButton
                clickHandler={() =>
                  props.setIntervalScaleAction("major", "diatonic")
                }
                pressed={props.intervalScaleType === "major"}
              >
                <span>
                  C Major <span class={avStyles.wideText}>Scale</span>
                </span>
              </IntervalScaleButton>
              <IntervalScaleButton
                clickHandler={() =>
                  props.setIntervalScaleAction("naturalMinor", "diatonic")
                }
                pressed={props.intervalScaleType === "naturalMinor"}
              >
                <span>
                  C Minor <span class={avStyles.wideText}>Scale</span>
                </span>
              </IntervalScaleButton>
            </div>
          </div>

          <Show when={props.intervalConfig.flavor === "chromatic"}>
            <div class={avStyles.chromaticIntervalButtonsTopRow}>
              <div class={"field has-addons has-addons-centered"}>
                <ChromaticIntervalButton
                  title="1 semitone"
                  clickHandler={() =>
                    props.setIntervalConfigChromatic("minorSeconds")
                  }
                  pressed={props.intervalConfig.type === "minorSeconds"}
                >
                  <span class={avStyles.wideText}>Minor 2nds</span>
                  <span class={avStyles.narrowText}>m2</span>
                </ChromaticIntervalButton>
                <ChromaticIntervalButton
                  title="3 semitones"
                  clickHandler={() =>
                    props.setIntervalConfigChromatic("minorThirds")
                  }
                  pressed={props.intervalConfig.type === "minorThirds"}
                >
                  <span class={avStyles.wideText}>Minor 3rds</span>
                  <span class={avStyles.narrowText}>m3</span>
                </ChromaticIntervalButton>
                <ChromaticIntervalButton
                  title="5 semitones"
                  clickHandler={() =>
                    props.setIntervalConfigChromatic("perfectFourths")
                  }
                  pressed={props.intervalConfig.type === "perfectFourths"}
                >
                  <span class={avStyles.wideText}>4ths</span>
                  <span class={avStyles.narrowText}>P4</span>
                </ChromaticIntervalButton>
                <ChromaticIntervalButton
                  title="7 semitones"
                  clickHandler={() =>
                    props.setIntervalConfigChromatic("perfectFifths")
                  }
                  pressed={props.intervalConfig.type === "perfectFifths"}
                >
                  <span class={avStyles.wideText}>5ths</span>
                  <span class={avStyles.narrowText}>P5</span>
                </ChromaticIntervalButton>
                <ChromaticIntervalButton
                  title="9 semitones"
                  clickHandler={() =>
                    props.setIntervalConfigChromatic("majorSixths")
                  }
                  pressed={props.intervalConfig.type === "majorSixths"}
                >
                  <span class={avStyles.wideText}>Major 6ths</span>
                  <span class={avStyles.narrowText}>M6</span>
                </ChromaticIntervalButton>
                <ChromaticIntervalButton
                  title="11 semitones"
                  clickHandler={() =>
                    props.setIntervalConfigChromatic("majorSevenths")
                  }
                  pressed={props.intervalConfig.type === "majorSevenths"}
                >
                  <span class={avStyles.wideText}>Major 7ths</span>
                  <span class={avStyles.narrowText}>M7</span>
                </ChromaticIntervalButton>
              </div>
            </div>

            <div class={avStyles.chromaticIntervalButtonsBottomRow}>
              <div class={"field has-addons has-addons-centered"}>
                <ChromaticIntervalButton
                  title="2 semitones"
                  clickHandler={() =>
                    props.setIntervalConfigChromatic("majorSeconds")
                  }
                  pressed={props.intervalConfig.type === "majorSeconds"}
                >
                  <span class={avStyles.wideText}>Major 2nds</span>
                  <span class={avStyles.narrowText}>M2</span>
                </ChromaticIntervalButton>
                <ChromaticIntervalButton
                  title="4 semitones"
                  clickHandler={() =>
                    props.setIntervalConfigChromatic("majorThirds")
                  }
                  pressed={props.intervalConfig.type === "majorThirds"}
                >
                  <span class={avStyles.wideText}>Major 3rds</span>
                  <span class={avStyles.narrowText}>M3</span>
                </ChromaticIntervalButton>
                <ChromaticIntervalButton
                  title="6 semitones"
                  clickHandler={() =>
                    props.setIntervalConfigChromatic("tritones")
                  }
                  pressed={props.intervalConfig.type === "tritones"}
                >
                  <span class={avStyles.wideText}>Tritones</span>
                  <span class={avStyles.narrowText}>TT</span>
                </ChromaticIntervalButton>
                <ChromaticIntervalButton
                  title="8 semitones"
                  clickHandler={() =>
                    props.setIntervalConfigChromatic("minorSixths")
                  }
                  pressed={props.intervalConfig.type === "minorSixths"}
                >
                  <span class={avStyles.wideText}>Minor 6ths</span>
                  <span class={avStyles.narrowText}>m6</span>
                </ChromaticIntervalButton>
                <ChromaticIntervalButton
                  title="10 semitones"
                  clickHandler={() =>
                    props.setIntervalConfigChromatic("minorSevenths")
                  }
                  pressed={props.intervalConfig.type === "minorSevenths"}
                >
                  <span class={avStyles.wideText}>Minor 7ths</span>
                  <span class={avStyles.narrowText}>m7</span>
                </ChromaticIntervalButton>
                <ChromaticIntervalButton
                  title="12 semitones"
                  clickHandler={() =>
                    props.setIntervalConfigChromatic("octaves")
                  }
                  pressed={props.intervalConfig.type === "octaves"}
                >
                  <span class={avStyles.wideText}>Octaves</span>
                  <span class={avStyles.narrowText}>P8</span>
                </ChromaticIntervalButton>
              </div>
            </div>
          </Show>

          <Show when={props.intervalConfig.flavor === "diatonic"}>
            <div>
              <div class={"field has-addons has-addons-centered"}>
                <DiatonicIntervalButton
                  title="1 or 2 semitones"
                  clickHandler={() =>
                    props.setIntervalConfigDiatonic("seconds")
                  }
                  pressed={props.intervalConfig.type === "seconds"}
                >
                  2nds
                </DiatonicIntervalButton>
                <DiatonicIntervalButton
                  title="3 or 4 semitones"
                  clickHandler={() => props.setIntervalConfigDiatonic("thirds")}
                  pressed={props.intervalConfig.type === "thirds"}
                >
                  3rds
                </DiatonicIntervalButton>
                <DiatonicIntervalButton
                  title="5 (or 6) semitones"
                  clickHandler={() =>
                    props.setIntervalConfigDiatonic("fourths")
                  }
                  pressed={props.intervalConfig.type === "fourths"}
                >
                  4ths
                </DiatonicIntervalButton>
                <DiatonicIntervalButton
                  title="7 (or 6) semitones"
                  clickHandler={() => props.setIntervalConfigDiatonic("fifths")}
                  pressed={props.intervalConfig.type === "fifths"}
                >
                  5ths
                </DiatonicIntervalButton>
                <DiatonicIntervalButton
                  title="8 or 9 semitones"
                  clickHandler={() => props.setIntervalConfigDiatonic("sixths")}
                  pressed={props.intervalConfig.type === "sixths"}
                >
                  6ths
                </DiatonicIntervalButton>
                <DiatonicIntervalButton
                  title="10 or 11 semitones"
                  clickHandler={() =>
                    props.setIntervalConfigDiatonic("sevenths")
                  }
                  pressed={props.intervalConfig.type === "sevenths"}
                >
                  7ths
                </DiatonicIntervalButton>
                <DiatonicIntervalButton
                  title="12 semitones"
                  clickHandler={() =>
                    props.setIntervalConfigDiatonic("octaves")
                  }
                  pressed={props.intervalConfig.type === "octaves"}
                >
                  Octaves
                </DiatonicIntervalButton>
              </div>
            </div>
          </Show>
        </div>
      </Show>
    </>
  );
};
