import { Show } from "solid-js";
import type { SelectOption } from "../../types/types";
import { makeSelectOptions } from "../../utils/utils";
import { VolumeSelect } from "../VolumeSelect";
import type {
  AudioStatus,
  AvrInputMode,
  INoteSeries,
} from "./AudioVisualizerTypes";
import { startPlayback } from "./audio-play";
import avStyles from "./audiovisualizer.module.css";
import type { Piano } from "./audio-load";

export const TopButtonRow = (props: {
  audioStatus: AudioStatus;
  inputMode: AvrInputMode;
  setInputMode: (inputMode: AvrInputMode) => void;
  stopPlayback: () => void;
  hideNoteSeriesMenu?: boolean | undefined;
  currentNoteSeries: INoteSeries;
  setAudioStatus: (status: AudioStatus) => void;
  setPlaybackTimerId: (id: number) => void;
  piano: Piano;
  setNotePlaying: (note: number) => void;
  volume: number;
  setVolume: (volume: number) => void;
}) => {
  const inputModeOptions: SelectOption<AvrInputMode>[] = [
    { label: "Traditional Keyboard", value: "keyboard-trad" },
    { label: "Janko-style Keyboard", value: "keyboard-six-six" },
    { label: "Guitar", value: "guitar" },
    { label: "Violin or Mandolin", value: "violin" },
    { label: "Scales", value: "scales" },
    { label: "Intervals", value: "intervals" },
    { label: "Modes", value: "modes" },
  ];

  return (
    <div class={`${avStyles.topButtonRow}`}>
      <Show when={!props.hideNoteSeriesMenu}>
        <div class={avStyles.buttonOrSelectSpacer}>
          <div class={"select is-small"}>
            <select
              title="Show scales, intervals, melodies, or modes"
              onChange={(event) => {
                const value = event.target.value;
                props.setInputMode(value as AvrInputMode);
              }}
            >
              {makeSelectOptions(inputModeOptions, props.inputMode)}
            </select>
          </div>
        </div>
      </Show>

      <div class={avStyles.buttonOrSelectSpacer}>
        <button
          onClick={() => {
            if (props.audioStatus === "stopped") {
              startPlayback(
                props.setAudioStatus,
                props.currentNoteSeries,
                props.piano,
                props.setNotePlaying,
                props.setPlaybackTimerId,
                props.stopPlayback
              );
            } else {
              props.stopPlayback();
            }
          }}
          class={`button is-small ${
            props.audioStatus === "loading" ? "is-loading" : ""
          } ${avStyles.playAudioButton}`}
        >
          {props.audioStatus === "stopped" ? "Play Audio" : "Stop Audio"}
        </button>
      </div>

      {props.audioStatus !== "loading" && (
        <div class={avStyles.buttonOrSelectSpacer}>
          <VolumeSelect volume={props.volume} setVolume={props.setVolume} />
        </div>
      )}
    </div>
  );
};
