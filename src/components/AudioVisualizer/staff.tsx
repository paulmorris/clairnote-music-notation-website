import { iota } from "../../utils/utils";

import styles from "./audiovisualizer.module.css";
import type { INoteSeries, INoteSeriesCaption } from "./AudioVisualizerTypes";
import type { GameNoteSeries } from "../Game/Game";
import { For, Show } from "solid-js";

const playingNoteColor = "#E3701A";

const getLedgerYPositions = function ({
  yPosition,
  clairnoteType,
}: {
  yPosition: number;
  clairnoteType: "sn" | "dn";
}) {
  // takes a vertical note position and returns an array of
  // vertical ledger positions for that note
  // staff lines are: 44 48 (52) 56 60
  let ledgers: number[] = [];
  if (50 <= yPosition && yPosition <= 54) {
    ledgers.push(52);
  } else if (yPosition < 43 || yPosition > 61) {
    const line = yPosition < 43 ? 44 : 60,
      difference = yPosition - line,
      distance = Math.abs(difference),
      direction = difference < 0 ? -1 : 1,
      ledgerCount = Math.ceil((distance - 1) / 4);
    ledgers = iota(ledgerCount, 4, 4).map((l) => line + l * direction);
  }

  if (clairnoteType === "sn" && yPosition % 4 === 2) {
    // If the note is D, F#, or Bb, we add a ledger for the note.
    // (positions 30, 34, 38, 42, 46, 50, 54, 58, 62, 66, 70, 74)
    ledgers.push(yPosition);
  }
  return ledgers;
};

const hollowNotePath =
  "M1.0265 -0.4999C1.2152 -0.4618 1.3372 -0.3515 1.3549 -0.2025C1.3692 -0.0828 1.3124 0.0767 1.2094 0.2063C1.0774 0.3724 0.8892 0.4721 0.6459 0.5049C0.5851 0.5131 0.5489 0.5134 0.4433 0.5068C0.3289 0.4996 0.3088 0.4966 0.2606 0.4798C0.1181 0.4300 0.0248 0.3354 0.0042 0.2196C-0.0154 0.1092 0.0352 -0.0503 0.1321 -0.1828C0.2682 -0.3692 0.4766 -0.4818 0.7307 -0.5063C0.8236 -0.5152 0.9655 -0.5122 1.0265 -0.4999zM0.8920 -0.3272C0.8019 -0.3059 0.6744 -0.2509 0.5172 -0.1656C0.1997 0.0069 0.1097 0.0977 0.1513 0.2039C0.1688 0.2487 0.1917 0.2776 0.2265 0.2986C0.3097 0.3488 0.4220 0.3375 0.6348 0.2572C0.9775 0.1280 1.2160 -0.0339 1.2240 -0.1427C1.2287 -0.2070 1.1849 -0.2813 1.1220 -0.3156C1.0837 -0.3365 1.0773 -0.3379 1.0151 -0.3393C0.9699 -0.3403 0.9308 -0.3364 0.8920 -0.3272z";

const solidNotePath =
  "M1.0162 -0.5005C1.1901 -0.4580 1.3000 -0.3601 1.3267 -0.2237C1.3473 -0.1188 1.3091 0.0324 1.2347 0.1410C1.1037 0.3322 0.8787 0.4644 0.6176 0.5036C0.5244 0.5176 0.3659 0.5121 0.2884 0.4921C-0.0297 0.4098 -0.0964 0.0918 0.1475 -0.1799C0.2984 -0.3479 0.5439 -0.4756 0.7758 -0.5066C0.8400 -0.5151 0.9696 -0.5119 1.0162 -0.5005z";

const makeNoteCaption = ({
  caption: { text, fontSize, fill, fontWeight },
  xCoordinate,
  yCoordinate,
}: {
  caption: INoteSeriesCaption;
  xCoordinate: number;
  yCoordinate: number;
}) => (
  <text
    color="rgb(50%, 50%, 50%)"
    font-family="sans-serif"
    font-size={fontSize || "1.1"}
    text-anchor="middle"
    fill={fill || "currentColor"}
    font-weight={fontWeight || "normal"}
    transform={"translate(" + xCoordinate + "," + yCoordinate + ")"}
  >
    <For each={text.split("/")}>
      {(captionLine, index) => (
        <tspan x="0" dy={index() === 0 ? "0" : "1.2em"}>
          {captionLine}
        </tspan>
      )}
    </For>
  </text>
);

const makeLedger = ({ x, y }: XY) => (
  // 24.1747 - 0.0740 = 24.1007
  <rect
    width="2.5313"
    height="0.1479"
    ry="0.0740"
    fill="currentColor"
    transform={"translate(" + x + "," + (y * -0.375 + 24.1007) + ")"}
  />
);

const makeNotehead = (note: INote) => (
  <path
    stroke-width={0.0001}
    stroke-linejoin="round"
    stroke-linecap="round"
    d={note.solid ? solidNotePath : hollowNotePath}
    transform={"translate(" + note.xCoordinate + "," + note.yCoordinate + ")"}
  />
);

const getAllLedgerPositions = (notes: INote[]): XY[] => {
  const ledgersIn = notes
    .map((note) => note.ledgerPositions)
    .reduce((result, lps) => result.concat(...lps), []);

  const dedup: Record<number, Set<number>> = {};
  ledgersIn.forEach(({ x, y }) => {
    if (dedup[x] === undefined) {
      dedup[x] = new Set();
    }
    dedup[x]?.add(y);
  });

  const flattened: XY[] = [];
  Object.entries(dedup).forEach(([x, ys]) => {
    ys.forEach((y) => {
      flattened.push({ x: parseFloat(x), y });
    });
  });
  return flattened;
};

const yPositionToCoordinate = (yp: number) => yp * -0.375 + 24.1747;

interface XY {
  x: number;
  y: number;
}

interface INote {
  interval: number;
  solid: boolean;
  yCoordinate: number;
  yPosition: number;
  clairnoteType: "sn" | "dn";
  leftOfStem: boolean;
  xCoordinate: number;
  ledgerPositions: XY[];
}

// yPositions (array of integers) indicates the vertical position of the notes,
// xCoordinate (number) the SVG coordinate for the note's horizontal position,
// caption (string) text for the caption
//
const NoteColumn = (props: {
  yPositions: number[];
  xCoordinate: number;
  caption: INoteSeriesCaption | undefined;
  isPlaying: boolean;
  clairnoteType: "sn" | "dn";
}) => {
  /* const hasInterval = yPositions.length > 1,*/
  const lowestYPosn = Math.min(...props.yPositions);
  const highestYPosn = Math.max(...props.yPositions);
  const biggestInterval = highestYPosn - lowestYPosn;
  const stemUp = (highestYPosn + lowestYPosn) / 2 <= 51;

  const sortedYPositions = props.yPositions
    .slice()
    .sort(stemUp ? (a, b) => a - b : (a, b) => b - a);

  const partialNotes = sortedYPositions.map(
    (
      yPos,
      i,
      yPosns
    ): Omit<INote, "leftOfStem" | "xCoordinate" | "ledgerPositions"> => {
      return {
        // ` || 0 ` to guard against undefined object, shouldn't happen but
        // for TS's sake.
        interval: Math.abs(i === 0 ? 0 : yPos - (yPosns[i - 1] || 0)),
        solid: props.clairnoteType === "sn" ? true : yPos % 2 === 0,
        yCoordinate: yPositionToCoordinate(yPos),
        yPosition: yPos,
        clairnoteType: props.clairnoteType,
      };
    }
  );

  const stemRightOffset = partialNotes[0]?.solid ? 1.2375 : 1.2624;

  const has2ndInterval = partialNotes.reduce(
    (result, note) => result || note.interval === 1 || note.interval === 2,
    false
  );

  const leftOfStemValues: boolean[] = [];

  const notes: INote[] = partialNotes.map((note, index) => {
    const leftOfStem =
      index === 0 || note.interval > 2 ? stemUp : !leftOfStemValues[index - 1];

    leftOfStemValues.push(leftOfStem);

    const finalXCoordinate = has2ndInterval
      ? leftOfStem
        ? props.xCoordinate
        : props.xCoordinate + stemRightOffset
      : props.xCoordinate;

    const ledgerX = finalXCoordinate - 0.5995;

    const ledgerPositions = getLedgerYPositions(note).map((lp) => ({
      x: ledgerX,
      y: lp,
    }));

    return {
      ...note,
      leftOfStem,
      xCoordinate: finalXCoordinate,
      ledgerPositions,
    };
  });

  const ledgers = getAllLedgerPositions(notes);

  const stemExtension = 0.375 * biggestInterval;

  const stemX =
    props.xCoordinate + (stemUp || has2ndInterval ? stemRightOffset : 0);

  const stemY =
    yPositionToCoordinate(lowestYPosn) - stemExtension - (stemUp ? 3.5 : 0);

  const stemHeight = 3.3133 + stemExtension;

  return (
    <g>
      <For each={ledgers}>{makeLedger}</For>
      <g style={{ fill: props.isPlaying ? playingNoteColor : "#000" }}>
        <rect
          width="0.0948"
          height={stemHeight}
          ry="0.0235"
          transform={"translate(" + stemX + "," + stemY + ")"}
        />
        <For each={notes}>{makeNotehead}</For>
      </g>
      {props.caption &&
        makeNoteCaption({
          caption: props.caption,
          xCoordinate: props.xCoordinate + stemRightOffset / 2,
          yCoordinate: 14.1748,
        })}
    </g>
  );
};

const NoteSeries = (
  props: Pick<
    StaffProps,
    | "noteSeries"
    | "noteIndexesToDraw"
    | "xCoordinates"
    | "notePlaying"
    | "clairnoteType"
  >
) => {
  return (
    <g>
      {/* Using <For> doesn't work here. */}
      {props.noteIndexesToDraw.map((index, step) => (
        <NoteColumn
          yPositions={props.noteSeries.yPositions[index] || []}
          xCoordinate={props.xCoordinates[step] || 0}
          caption={props.noteSeries.captions[index]}
          isPlaying={index === props.notePlaying}
          clairnoteType={props.clairnoteType}
        />
      ))}
    </g>
  );
};

interface StaffProps {
  staffLinesY: number[];
  staffLinesX: number;
  noteSeries: INoteSeries | GameNoteSeries;
  noteIndexesToDraw: number[];
  xCoordinates: number[];
  notePlaying: number | undefined;
  width: string;
  height: string;
  viewBox: string;
  clairnoteType: "sn" | "dn";
}

export const Staff = (props: StaffProps) => {
  return (
    <svg
      class={styles.svgStaff}
      xmlns="http://www.w3.org/2000/svg"
      version="1.2"
      width={props.width}
      height={props.height}
      viewBox={props.viewBox}
    >
      <For each={props.staffLinesY}>
        {(y) => (
          <line
            stroke-linejoin="round"
            stroke-linecap="round"
            stroke-width="0.0729"
            stroke="currentColor"
            x1={0.0365 + props.staffLinesX}
            y1={y}
            x2={66.0207 + props.staffLinesX}
            y2={y}
          />
        )}
      </For>
      <Show when={props.noteIndexesToDraw.length > 0}>
        <NoteSeries
          noteSeries={props.noteSeries}
          noteIndexesToDraw={props.noteIndexesToDraw}
          xCoordinates={props.xCoordinates}
          notePlaying={props.notePlaying}
          clairnoteType={props.clairnoteType}
        />
      </Show>
    </svg>
  );
};
