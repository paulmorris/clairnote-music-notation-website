\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\language "english"

#(set-global-staff-size 34)
\pointAndClickOff

\header{
  tagline = ""
}

\paper {
  page-breaking = #ly:one-line-auto-height-breaking
  top-system-spacing = 0
  top-margin = 1
  bottom-margin = 1
  left-margin = 1
  right-margin = 1
}

wholeToneScales = \relative f {
  \cadenzaOn
  \hide Rest
  % c'8 cis d dis e f fis g gis a ais b c
  % c'8[ cis d dis] e[ f fis g] gis[ a ais b] c4
  % c'4 cs d ds  e f fs g gs a as b c
  c'4 d e f g a b c
  s8
  cs,4 ds fs gs as
  % \bar "|" r32
  % df,4 ef \omit Accidental f g  a b
}

myFlat = \markup { \translate #(cons 0.1 0.25) \flat }
mySharp = \markup { \translate #(cons 0 0.55)  \sharp }

fsize = -6
spc = #2.5

text = \lyricmode {
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "C"}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "D"}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "E"}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "F"}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "G"}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "A"}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "B"}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "C"}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  \center-column {\line {"C" \mySharp } \translate #(cons 0 -0.5) \line{"D" \myFlat }}}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  \center-column {\line {"D" \mySharp } \translate #(cons 0 -0.5) \line{"E" \myFlat }}}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  \center-column {\line {"F" \mySharp } \translate #(cons 0 -0.5) \line{"G" \myFlat }}}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  \center-column {\line {"G" \mySharp } \translate #(cons 0 -0.5) \line{"A" \myFlat }}}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  \center-column {\line {"A" \mySharp } \translate #(cons 0 -0.5) \line{"B" \myFlat }}}
}

\layout {
  \context {
    \Score
    \remove "System_start_delimiter_engraver"
    % \override StaffGrouper.staff-staff-spacing.padding = #100
    % \override StaffGrouper.staff-staff-spacing.basic-distance = #100
  }
  \context {
    \Lyrics
    %\override VerticalAxisGroup.nonstaff-relatedstaff-spacing.basic-distance = #5
  }
  \context {
    \Staff
    \override VerticalAxisGroup
    .default-staff-staff-spacing.basic-distance = #18
  }
}

\score {
  <<
    \new Staff
    \with {
      \remove "Accidental_engraver"
      \remove "Key_engraver"
      \remove "Time_signature_engraver"
      \remove "Clef_engraver"
      % \remove "Bar_engraver"
      % \cnNoteheadStyle "funksol"
    }{
      \new Voice = "one" {
        \override Score.SpacingSpanner
        .common-shortest-duration = #(ly:make-moment 1 12 )
        
        \override TextScript #'extra-offset = #'(0 . 0)
        s4^\markup \with-color #grey { \fontsize #-6 \sans ""}
        \wholeToneScales
      }
    }
    \new Lyrics {
      \override VerticalAxisGroup
      .nonstaff-relatedstaff-spacing.basic-distance = #7.0
      \lyricsto "one"
      \text
    }
    \new TradStaff \with {
      \remove "Time_signature_engraver"
      \remove "Clef_engraver"
    }{
      \new Voice = "two" {
        \override Score.SpacingSpanner
        .common-shortest-duration = #(ly:make-moment 1 12 )
        
        \override TextScript #'extra-offset = #'(0 . 0)
        s4^\markup \with-color #grey { \fontsize #-6 \sans ""}
        \wholeToneScales
      }
    }
    %{
    \new Lyrics {
      \override VerticalAxisGroup.nonstaff-relatedstaff-spacing.basic-distance = #4.5
      \lyricsto "two"
      \tradText
    }
    %}
  >>
}