\version "2.22.0"
% clairnote-type = sn
\include "clairnote.ly"
\language "english"

#(set-global-staff-size 34)
\pointAndClickOff

\header{
  tagline = ""
}

\paper {
  page-breaking = #ly:one-line-auto-height-breaking
  top-system-spacing = 0
  top-margin = 1
  bottom-margin = 1
  left-margin = 1
  right-margin = 1
}

fsize = #-6
spc = #3.5
clr = #green
wsclr = #(x11-color 'SeaGreen)
hsclr = #(x11-color 'RoyalBlue)
whln = #1.2
hhln = #1.5

cMajorCaptioned = {
  \relative f {
    \cadenzaOn
    % s16

    %c'4
    % s8_\markup \with-color #clr \sans \fontsize #fsize { \lower #spc \halign #hln  \center-column {"Whole" "Step" }}
    % d _\markup \with-color #wsclr \sans \fontsize #fsize { \lower #spc \halign #whln  \center-column {"Whole" "Step" }}
    e' % _\markup \with-color #wsclr \sans \fontsize #fsize { \lower #spc  \halign #whln \center-column {"Whole" "Step" }}
    f _\markup \with-color #hsclr \sans \fontsize #fsize { \lower #spc \halign #hhln  \center-column {"Half" "Step" }}
    fs _\markup \with-color #hsclr \sans \fontsize #fsize { \lower #spc \halign #hhln  \center-column {"Half" "Step" }}
    g _\markup \with-color #hsclr \sans \fontsize #fsize { \lower #spc \halign #hhln  \center-column {"Half" "Step" }}
    gs _\markup \with-color #hsclr \sans \fontsize #fsize { \lower #spc \halign #hhln  \center-column {"Half" "Step" }}
    % c _\markup \with-color #hsclr \sans \fontsize #fsize { \lower #spc \halign #hhln  \center-column {"Half" "Step" }}
    % <c' c'>8[ <d d'> <e e'> <f f'>]  <g g'>[  <a a'> <b b'> <c c'>]
    % s16
  }
}

myFlat = \markup { \translate #(cons 0.1 0.25) \flat }
mySharp = \markup { \translate #(cons 0 0.55)  \sharp }

fsize = -6
spc = #2.5

text = \lyricmode {
  % \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "C"}
  % \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "D"}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "E"}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "F"}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  {"F" \mySharp }}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "G"}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  {"G" \mySharp }}
  % \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "C"}
}

cMajor = {
  \relative f {
    \cadenzaOn
    % s16

    % c'4
    % s8_\markup \with-color #clr \sans \fontsize #fsize { \lower #spc \halign #1  \center-column {"Whole" "Step" }}
    % d
    e'
    f
    fs
    g
    gs
    % a
    % b
    % c
    % <c' c'>8[ <d d'> <e e'> <f f'>]  <g g'>[  <a a'> <b b'> <c c'>]
    % s16
  }
}


\layout {
  \context {
    \Score
    \remove "System_start_delimiter_engraver"
    % \override StaffGrouper.staff-staff-spacing.padding = #100
    % \override StaffGrouper.staff-staff-spacing.basic-distance = #100
  }
  \context {
    \Lyrics
    %\override VerticalAxisGroup.nonstaff-relatedstaff-spacing.basic-distance = #5
  }
  \context {
    \Staff
    \override VerticalAxisGroup
    .default-staff-staff-spacing.basic-distance = #13.5
  }
}

\score {
  <<
    \new Staff
    \with {
      % \remove "Accidental_engraver"
      \remove "Key_engraver"
      \remove "Time_signature_engraver"
      \remove "Clef_engraver"
      % \remove "Bar_engraver"
      % \cnNoteheadStyle "funksol"
    }{
      \new Voice = "one" {
        \override Score.SpacingSpanner
        .common-shortest-duration = #(ly:make-moment 1 20 )

        \override TextScript #'extra-offset = #'(0 . 0)
        s4^\markup \with-color #grey {
          \fontsize #-6 \sans ""
        }
        \cMajorCaptioned
      }
    }

    \new TradStaff \with {
      \remove "Time_signature_engraver"
      \remove "Clef_engraver"
    }{
      \new Voice = "two" {
        \override Score.SpacingSpanner
        .common-shortest-duration = #(ly:make-moment 1 20 )

        \override TextScript #'extra-offset = #'(0 . 0)
        s4^\markup \with-color #grey {
          \fontsize #-6 \sans ""
        }
        \cMajor
      }
    }

    \new Lyrics \with {
      \override VerticalAxisGroup
      .nonstaff-relatedstaff-spacing.basic-distance = #4.5
    } {
      \lyricsto "two" \text
    }

  >>
}