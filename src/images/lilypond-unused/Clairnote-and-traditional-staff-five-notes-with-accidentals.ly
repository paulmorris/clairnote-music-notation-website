\version "2.19.52"
\include "clairnote.ly"
\language "english"
#(set-default-paper-size "letter")

#(set-global-staff-size 34)

% for properly scaled svg images for clairnote.org
% #(set-global-staff-size 34)
% fsize = -6
% \pointAndClickOff

\header {
  tagline = ##f
}

#(set! paper-alist (cons '("my size" . (cons
                                        (* 7.8 in) ; width
                                        (* 1.9 in) ; height 1.5 with captions
                                        )) paper-alist))

\paper {
  #(set-paper-size "my size")
  % indent = 0
  % ragged-bottom = ##t
  % page-count = #1
  % page-breaking = #ly:one-page-breaking
  top-margin = 0 \cm
  bottom-margin = 0 \cm
  left-margin = 0.2 \cm
  right-margin = 0.2 \cm
  top-system-spacing = ##f
}


myFlat = \markup { \translate #(cons 0.1 0.25) \flat }
mySharp = \markup { \translate #(cons 0 0.55)  \sharp }

fsize = -6
spc = #1
wsclr = #(x11-color 'SeaGreen)
hsclr = #(x11-color 'RoyalBlue)

text = \lyricmode {
  \markup \with-color #wsclr \sans \fontsize #fsize { \lower #spc \center-column {"Major" "Third"}}
  \markup \with-color #hsclr \sans \fontsize #fsize { \lower #spc \center-column {"Minor" "Third"}}

  \markup \with-color #hsclr \sans \fontsize #fsize { \lower #spc \center-column {"Minor" "Third"}}
  \markup \with-color #wsclr \sans \fontsize #fsize { \lower #spc \center-column {"Major" "Third"}}

  \markup \with-color #wsclr \sans \fontsize #fsize { \lower #spc \center-column {"Major" "Triad"}}
  \markup \with-color #hsclr \sans \fontsize #fsize { \lower #spc \center-column {"Minor" "Triad"}}

  %{
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "G"}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  \center-column {\line {"G" \mySharp }}}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "A"}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  \center-column {\line {"A" \myFlat } }}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  \center-column {\line {"C" \mySharp }}}
  %}
}


text = \lyricmode {
  %   \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "C"}
  %   \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "D"}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "E"}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "F"}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  {"F" \mySharp }} % \center-column {\line {"F" \mySharp } \translate #(cons 0 -0.5) \line{"G" \myFlat}}}

  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "G"}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  {"G" \mySharp }} % \center-column {\line {"G" \mySharp } \translate #(cons 0 -0.5) \line{"A" \myFlat}}}


  %   \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "A"}
  %   \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "B"}
  %   \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "C"}
  %   \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  \center-column {\line {"C" \mySharp } \translate #(cons 0 -0.5) \line{"D" \myFlat }}}
  %   \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  \center-column {\line {"D" \mySharp } \translate #(cons 0 -0.5) \line{"E" \myFlat }}}
  %   \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  \center-column {\line {"F" \mySharp } \translate #(cons 0 -0.5) \line{"G" \myFlat }}}
  %   \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  \center-column {\line {"A" \mySharp } \translate #(cons 0 -0.5) \line{"B" \myFlat }}}
}

notes = \relative c' {
  e f fs g gs
}

fsize = #-6
spc = #3
clr = #green
wsclr = #(x11-color 'SeaGreen)
hsclr = #(x11-color 'RoyalBlue)
whln = #1.2
hhln = #1.5

notesCaptioned =
\relative f' {
  % s16

  % c'4
  % s8_\markup \with-color #clr \sans \fontsize #fsize { \lower #spc \halign #hln  \center-column {"Whole" "Step" }}
  % d _\markup \with-color #wsclr \sans \fontsize #fsize { \lower #spc \halign #whln  \center-column {"Whole" "Step" }}
  e4 %_\markup \with-color #wsclr \sans \fontsize #fsize { \lower #spc  \halign #whln \center-column {"Whole" "Step" }}
  f ^\markup \with-color #hsclr \sans \fontsize #fsize { \raise #spc \halign #hhln  \center-column {"Half" "Step" }}
  fs^\markup \with-color #hsclr \sans \fontsize #fsize { \raise #spc \halign #hhln  \center-column {"Half" "Step" }} 
  g ^\markup \with-color #hsclr \sans \fontsize #fsize { \raise #spc \halign #hhln  \center-column {"Half" "Step" }}
  gs ^\markup \with-color #hsclr \sans \fontsize #fsize { \raise #spc \halign #hhln  \center-column {"Half" "Step" }}
  % b _\markup \with-color #wsclr \sans \fontsize #fsize { \lower #spc \halign #whln  \center-column {"Whole" "Step" }}
  % c _\markup \with-color #hsclr \sans \fontsize #fsize { \lower #spc \halign #hhln  \center-column {"Half" "Step" }}
  % <c' c'>8[ <d d'> <e e'> <f f'>]  <g g'>[  <a a'> <b b'> <c c'>]
  % s16
}


\layout {
  % ragged-right = ##t
  % indent = #0
  % line-width = #380
  \context {
    \Score
    proportionalNotationDuration = #(ly:make-moment 1 8)
    % \override SpacingSpanner #'uniform-stretching = ##t
    % \override Score.SpacingSpanner #'strict-note-spacing = ##t
    \remove "System_start_delimiter_engraver"
    \override BarNumber #'break-visibility = #'#(#f #f #f)
    explicitKeySignatureVisibility = #end-of-line-invisible % #'#(#f #t #t)
    printKeyCancellation = ##f
    \override KeyCancellation #'break-visibility = #all-invisible % #'#(#f #f #f)

  }
  \context {
    \Staff
    \override VerticalAxisGroup.default-staff-staff-spacing.basic-distance = #11
    % \staffSize #1.1
    % \remove "Accidental_engraver"
    \remove "Clef_engraver"
    \remove "Time_signature_engraver"
    % \remove "Accidental_engraver"
    % \remove "Key_engraver"
    \remove "Bar_engraver"
  }
  \context {
    \TradStaff
    % \staffSize #1.1
    % \remove "Accidental_engraver"
    \remove "Clef_engraver"
    \remove "Time_signature_engraver"
    % \remove "Accidental_engraver"
    % \remove "Key_engraver"
    \remove "Bar_engraver"
  }
}

\markup {
  \justify-line {

    \center-column {
      \score {
        <<
          \new TradStaff {
            \new Voice = "b" {
              % \key d \major
              % <>^\markup \with-color #grey { \fontsize #-6 \sans "Key of D Major, Chromatic Notes"}
              \cadenzaOn
              \omit Rest
              r8
              \notesCaptioned
              % r16
            }
          }
          \new Lyrics \lyricsto "b" \text
        >>
      }

      %{
      \vspace #1.12
      \with-color #grey  \sans \fontsize #smallsize
      \center-column {"Traditional" "Half Notes and Quarter Notes"}
      %}

      % \vspace #0.3
      % \with-color #grey  \sans \fontsize #-1 "Treble Clef"
    }

%{
    \center-column {
      
      % \with-color #grey \large "—>"
      

      \with-color #grey
      \combine
      \arrow-head #X #RIGHT ##t
      \filled-box #'(-3.0 . -0.7) #'(-0.12 . 0.12) #1
      
      % \with-color #grey \combine
      % \arrow-head #X #RIGHT ##t
      % \draw-line #'(-3.5 . 0)
    }
%}

    \center-column {
      \score {
        <<
          \new Staff
          {
            \new Voice = "one" {
              % \key d \major
              \cadenzaOn
              \omit Rest
              r8
              \notesCaptioned
              % r16
            }
          }
          \new Lyrics \lyricsto "one" \text
        >>
      }
      %{
      \vspace #0.8
      \with-color #grey  \sans \fontsize #smallsize
      \center-column {"Clairnote" "Half Notes and Quarter Notes"}
      %}

      % \vspace #0.3
      % \with-color #grey  \sans \fontsize #-1 "Treble Clef"
    }
    %{
    \center-column {
      \with-color #grey \large "="
    }

    \center-column {
      \score {
        \new Staff
        {
          \new Voice = "three" {
            \omit Rest r8
            gs'2
          }
        }
      }
      \vspace #0.8
      \with-color #grey  \sans \fontsize #smallsize
      \center-column {"Clairnote" "Half Note"}
      % \vspace #0.3
      % \with-color #grey  \sans \fontsize #-1 "Treble Clef"
    }
    %}
  }
}