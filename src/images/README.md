# How to create images for the Clairnote site:

## In LilyPond Files

Use this:

```
  #(set-global-staff-size 34) % for properly scaled svg images
  \pointAndClickOff
  fsize = -6
  text = \lyricmode {
    \markup \with-color #grey \sans \fontsize #fsize {"A-flat"}
  }
```

Use markup with settings like this:

```
<>-\markup \sans \with-color #grey \fontsize #-4 \pad-markup #1 "Caption"
```

`\with-color #grey` is the same as %50 grey in Inkscape

`\fontsize #-5` is used for the size of caption on the top left of staves.

Deprecated: use the following to skip this scaling step in Inkscape:
`\magnifyStaff #1.75`

## Run LilyPond to Generate SVG files

save .ly files in `src/images/lilypond/`

Make sure there are 'svg-dn' and 'svg-sn' directories for svg files to go in.

Run the following command:

```
npm run build-images
```

or alternatively:

```
npm run build-sn-images
npm run build-dn-images
```

Make sure `clairnote.ly` and `clairnote-dn.ly` are available for including.
For example, they could be placed in:
`~/lilypond/usr/share/lilypond/current/ly`

### One file at a time

In this directory:
cd src/images/lilypond/

lilypond -dno-point-and-click -dbackend=svg -o '../svg-sn' lilypond-file.ly

lilypond -dno-point-and-click -dbackend=svg -o '../svg-dn' -dinclude-settings='clairnote-dn.ly' lilypond-file.ly

### The old non-npm way

Or alternatively, in the `src/images/lilypond/` directory,
run the following bulk command for generating SVG images.
Make sure there are 'svg-dn' and 'svg-sn' directories for svg files to go in.

```
find \*.ly -maxdepth 1 -exec ~/bin/lilypond -dno-point-and-click -dbackend=svg -dinclude-settings="clairnote-dn.ly" -o "../svg-dn" {} \;

find \*.ly -maxdepth 1 -exec ~/bin/lilypond -dno-point-and-click -dbackend=svg -o "../svg-sn" {} \;
```

(swap out lilypond-stable instead of lilypond for the stable version)

## Batch running convert-ly

```
npm run convert-ly-images
```

```
find \*.ly -maxdepth 1 -exec ~/bin/convert-ly {} \;
```

## The old Inkscape way

Export as SVG using Frescobaldi
resolution 300
$lilypond -dno-point-and-click -ddelete-intermediate-files $include -dbackend=svg $filename

Edit in Inkscape.

Scale to 175% size. (if not already scaled with \magnifyStaff)

Change font color of captions? 50% or 60% grey in Inkscape

Make any additional spacing adjustments.

Resize canvas using "document properties," use 5.00 on each side.

Save As... Inkscape SVG.
Save a Copy... as PNG and SVG for use on the site.
