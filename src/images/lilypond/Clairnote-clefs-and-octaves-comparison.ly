\version "2.19.52"
% clairnote-type = "sn"
\include "clairnote.ly"
\language "english"
#(set-default-paper-size "letter")

#(set-global-staff-size 34)

% for properly scaled svg images for clairnote.org
% #(set-global-staff-size 34)
% fsize = -6
% \pointAndClickOff

\header {
  tagline = ##f
}

#(set! paper-alist (cons '("my size" . (cons
                                        (* 6.2 in) ; width
                                        (* 3.15 in) ; height 1.5 with captions
                                        )) paper-alist))

\paper {
  #(set-paper-size "my size")
  % indent = 0
  % ragged-bottom = ##t
  % page-count = #1
  % page-breaking = #ly:one-page-breaking
  top-margin = 0 \cm
  bottom-margin = 0 \cm
  left-margin = 0.2 \cm
  right-margin = 0.2 \cm
  top-system-spacing = ##f
}

\layout {
  % ragged-right = ##t
  % indent = #0
  % line-width = #380
  \context {
    \Score
    proportionalNotationDuration = #(ly:make-moment 1 8)
    % \override SpacingSpanner #'uniform-stretching = ##t
    % \override Score.SpacingSpanner #'strict-note-spacing = ##t
    % \remove "System_start_delimiter_engraver"
    \omit SystemStartBrace
    \override BarNumber #'break-visibility = #'#(#f #f #f)
    explicitKeySignatureVisibility = #end-of-line-invisible % #'#(#f #t #t)
    printKeyCancellation = ##f
    \override KeyCancellation #'break-visibility = #all-invisible % #'#(#f #f #f)

  }
  \context {
    \Staff
    \override VerticalAxisGroup.default-staff-staff-spacing.basic-distance = #11
    % \staffSize #1.1
    % \remove "Accidental_engraver"
    % \remove "Clef_engraver"
    \remove "Time_signature_engraver"
    % \remove "Accidental_engraver"
    % \remove "Key_engraver"
    \remove "Bar_engraver"
  }
  \context {
    \TradStaff
    % \staffSize #1.1
    % \remove "Accidental_engraver"
    % \remove "Clef_engraver"
    \remove "Time_signature_engraver"
    % \remove "Accidental_engraver"
    % \remove "Key_engraver"
    \remove "Bar_engraver"
  }
}

upper = \relative c' {
  % <c c'> <d d'>
  <e e'> <f f'> <g g'>
}

lower = \relative c, {
  % <c, c'> <d d'>
  <e e'> <f f'> <g g'>
}

fsize = -6
spc = 1

text = \lyricmode {
  \markup \with-color #grey \sans \fontsize #fsize { \lower  #spc  \center-column {"E" "E" "E" "E"}}
  \markup \with-color #grey \sans \fontsize #fsize { \lower  #spc  \center-column {"F" "F" "F" "F"}}
  \markup \with-color #grey \sans \fontsize #fsize { \lower  #spc  \center-column {"G" "G" "G" "G"}}

}

\markup {
  \justify-line {

    \center-column {
      \score {
        <<
          \new PianoStaff
          <<
            \new TradStaff = upper
            \with {
              % \remove "Accidental_engraver"
              % \remove "Key_engraver"
              % \staffSize #-2
              \omit Rest
            }
            {
              % r8
              % <>^\markup \with-color #grey { \fontsize #-6 \sans "Treble and Bass Clefs"}
              \upper
            }
            \new TradStaff = "lower"
            \with {
              % \remove "Accidental_engraver"
              % \remove "Key_engraver"
              % \staffSize #-2
              \omit Rest
            }
            \new Voice = "low"
            {
              \clef bass
              % r8
              \lower
            }
          >>
          \new Lyrics \lyricsto "low" \text
        >>
        %{
        <<
          \new TradStaff {
            \new Voice = "b" {
              \key d \major
              % <>^\markup \with-color #grey { \fontsize #-6 \sans "Key of D Major, Chromatic Notes"}
              \cadenzaOn
              \omit Rest
              % r16
              \notes
              % r16
            }
          }
          \new Lyrics \lyricsto "b" \text
        >>
        %}
      }

      %{
      \vspace #1.12
      \with-color #grey  \sans \fontsize #smallsize
      \center-column {"Traditional" "Half Notes and Quarter Notes"}
      %}

      % \vspace #0.3
      % \with-color #grey  \sans \fontsize #-1 "Treble Clef"
    }

    \center-column {
      %{
      \with-color #grey \large "—>"
      %}
      \raise #4.6
      \with-color #grey
      \combine
      \arrow-head #X #RIGHT ##t
      \filled-box #'(-3.0 . -0.7) #'(-0.12 . 0.12) #1

      %{
      \with-color #grey \combine
      \arrow-head #X #RIGHT ##t
      \draw-line #'(-3.5 . 0)
      %}

    }

    \center-column {
      \score {
        <<
          \new PianoStaff
          <<
            \new Staff = upper
            \with {
              % \remove "Accidental_engraver"
              % \remove "Key_engraver"
              % \staffSize #-2
              \omit Rest
            }
            {
              % r8
              \upper
            }
            \new Staff = lower
            \with {
              % \remove "Accidental_engraver"
              % \remove "Key_engraver"
              % \staffSize #-2
              \omit Rest
            }
            \new Voice = "low" {
              \clef bass
              % r8
              \lower
            }
          >>
          \new Lyrics \lyricsto "low" \text
        >>
        %{
        <<
          \new Staff
          {
            \new Voice = "one" {
              \key d \major
              \cadenzaOn
              \omit Rest
              % r16
              \notes
              % r16
            }
          }
          \new Lyrics \lyricsto "one" \text
        >>
        %}
      }
      %{
      \vspace #0.8
      \with-color #grey  \sans \fontsize #smallsize
      \center-column {"Clairnote" "Half Notes and Quarter Notes"}
      %}

      % \vspace #0.3
      % \with-color #grey  \sans \fontsize #-1 "Treble Clef"
    }
    %{
    \center-column {
      \with-color #grey \large "="
    }

    \center-column {
      \score {
        \new Staff
        {
          \new Voice = "three" {
            \omit Rest r8
            gs'2
          }
        }
      }
      \vspace #0.8
      \with-color #grey  \sans \fontsize #smallsize
      \center-column {"Clairnote" "Half Note"}
      % \vspace #0.3
      % \with-color #grey  \sans \fontsize #-1 "Treble Clef"
    }
    %}
  }
}