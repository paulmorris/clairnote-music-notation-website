\version "2.19.52"
\include "clairnote.ly"
\language "english"
#(set-default-paper-size "letter")

#(set-global-staff-size 34)

% for properly scaled svg images for clairnote.org
% #(set-global-staff-size 34)
% fsize = -6
\pointAndClickOff

\header {
  tagline = ##f
}

#(set! paper-alist (cons '("my size" . (cons
                                        (* 18.3 cm) ; width 21.5
                                        (* 4.5 cm) ; height
                                        )) paper-alist))

\paper {
  #(set-paper-size "my size")
  % indent = 0
  % ragged-bottom = ##t
  % page-count = #1
  % page-breaking = #ly:one-line-auto-height-breaking
  % page-breaking = #ly:one-page-breaking
  top-margin = 0
  bottom-margin = 0
  left-margin = 1
  right-margin = 1
  top-system-spacing = ##f
}

\layout {
  % ragged-right = ##t
  % indent = #0
  % line-width = #380
  \context {
    \Score
    proportionalNotationDuration = #(ly:make-moment 1 7)
    % \override SpacingSpanner #'uniform-stretching = ##t
    % \override Score.SpacingSpanner #'strict-note-spacing = ##t
    \remove "System_start_delimiter_engraver"
    \override BarNumber #'break-visibility = #'#(#f #f #f)
    explicitKeySignatureVisibility = #end-of-line-invisible % #'#(#f #t #t)
    printKeyCancellation = ##f
    \override KeyCancellation #'break-visibility = #all-invisible % #'#(#f #f #f)

  }
  \context {
    \Staff
    \override VerticalAxisGroup.default-staff-staff-spacing.basic-distance = #11
    % \staffSize #1.1
    % \remove "Accidental_engraver"
    \remove "Clef_engraver"
    \remove "Time_signature_engraver"
    \remove "Accidental_engraver"
    \remove "Key_engraver"
  }
  \context {
    \TradStaff
    % \staffSize #1.1
    % \remove "Accidental_engraver"
    \remove "Clef_engraver"
    \remove "Time_signature_engraver"
    \remove "Accidental_engraver"
    \remove "Key_engraver"
  }
}

myFlat = \markup { \translate #(cons 0.1 0.25) \flat }
mySharp = \markup { \translate #(cons 0 0.55)  \sharp }

fsize = -6
spc = #2.5

textBlacks = \lyricmode {
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "C"}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "D"}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "E"}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  \center-column {\line {"F" \mySharp } \translate #(cons 0 -0.5) \line{"G" \myFlat }}}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  \center-column {\line {"G" \mySharp } \translate #(cons 0 -0.5) \line{"A" \myFlat }}}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  \center-column {\line {"A" \mySharp } \translate #(cons 0 -0.5) \line{"B" \myFlat }}}
}

textWhites = \lyricmode {
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  \center-column {\line {"C" \mySharp } \translate #(cons 0 -0.5) \line{"D" \myFlat }}}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  \center-column {\line {"D" \mySharp } \translate #(cons 0 -0.5) \line{"E" \myFlat }}}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "F"}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "G"}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "A"}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "B"}

}

lineNotes = \relative {
  \cadenzaOn
  s8 c'4 d e fs gs as
}

spaceNotes = \relative {
  \cadenzaOn
  s8 cs'4 ds f g a b
}

\markup {
  \justify-line {

    \center-column {
      \score {
        <<
          \new Staff {
            \new Voice = "b" {
              \lineNotes
            }
          }
          \new Lyrics \lyricsto "b" \textBlacks
        >>
      }
    }

    \center-column {
      \score {
        <<
          \new Staff \with {
            \remove "Bar_engraver"
          }
          {
            \new Voice = "one" {
              \spaceNotes
            }
          }
          \new Lyrics \lyricsto "one" \textWhites
        >>
      }
    }

  }
}