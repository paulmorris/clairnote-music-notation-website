\version "2.19.52"

% \include "ignatzek-jazz-chords.ily"
% \include "AccordsJazzDefs.ily"
% \include "LilyJAZZ.ily"
% clairnote-type = "sn"
\include "clairnote.ly"
\pointAndClickOff

#(set-global-staff-size 34)

\paper {
  system-system-spacing.basic-distance = #20 % 24
  markup-system-spacing.basic-distance = #22 % 22
  paper-width = 23.5 \cm
  % paper-height = 44 \cm
  top-margin = 0 \cm
  bottom-margin = 0 \cm
  right-margin = 0.5 \cm
  left-margin = 0.5 \cm
  page-breaking = #ly:one-page-breaking
}

\header	{
  tagline = ##f
  % poet = "Jean-Pierre"
  % composer = "lilyJAZZ chords"
}

accords = \chordmode {
  \key c \major
  \time 1/4
  c4
  cis4
  ces
  c:6
  c:6.9
  c:3.5.9
  \break s
  c:7+
  c:maj7.5-
  c:maj7.5+
  c:maj9
  c:maj11
  c:maj13
  c:7
  \break s
  c:9
  c:11
  c:13
  c:m
  c:m6
  c:m6.9
  c:m5.9
  \break s
  c:m7
  c:m7.11
  c:m7.13
  c:m9
  c:m11
  % \break s
  c:m13
  c:m7+
  \break s
  c:m9.7+
  c:m7.5-
  c:m9.5-
  c:m11.5-
  c:dim
  c:dim7
  \break s
  c:aug
  c:sus2
  c:sus4
  c:sus4.7
  c:sus4.7.9
  c:7.5-
  c:7.5+
  \break s
  c:9-
  c:9-.5-
  c:9-.5+
  c:9+
  c:9+.5-
  c:9+.5+
  % c:8
}

paroles = \lyricmode {
  "cis" "ces" "c:6" "c:6.9" "c:3.5.9" %{ "c:maj" %} "c:7+"
  "c:maj7.5-" "c:maj7.5+" "c:maj9" "c:maj11" "c:maj13" "c:7" "c:9" "c:11" "c:13"
  "c:m" "c:m6" "c:m6.9" "c:m5.9" "c:m7" "c:m7.11" "c:m7.13"
  "c:m9" "c:m11" "c:m13" "c:m7+" "c:m9.7+" "c:m7.5-" "c:m9.5-" "c:m11.5-"
  "c:dim"  "c:dim7" "c:aug"     "c:sus2" "c:sus4" "c:sus4.7" "c:sus4.7.9"
  "c:7.5-" "c:7.5+" "c:9-" "c:9-.5-" "c:9-.5+" "c:9+" "c:9+.5-" "c:9+.5+" "c:8"
}

\score {
  <<
    <<
      \new ChordNames \accords
      \new Staff % \new Voice = chant
      {
        % \jazzOn
        \accords
      }
      % \new Lyrics \lyricsto chant \paroles
    >>
    <<
      % \new ChordNames \accords
      \new TradStaff {
        %  \new Voice = chant
        {
          % \jazzOn
          \accords
        }
      }
      % \new Lyrics \lyricsto chant \paroles
    >>
  >>
  \layout {
    indent = #0
    \context {
      \Score
      \override Clef.break-visibility = #'#(#f #f #f)		% only one clef
      \override KeySignature.break-visibility = #'#(#f #f #f)	% only one key signature
      \override BarNumber.break-visibility = #'#(#f #f #f)
      \override LyricText.font-name = #"DejaVu Sans Condensed"
      \override LyricText.font-size = #1
      \hide TimeSignature
      \omit BarLine
      \override SpacingSpanner.common-shortest-duration = #(ly:make-moment 1 12 )
    }
    \context {
      \Staff
      % \remove "Time_signature_engraver"
      % \omit BarLine
      % \omit TimeSignature
    }
  }
  % \midi {}
}
