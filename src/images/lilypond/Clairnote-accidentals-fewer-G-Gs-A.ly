\version "2.19.63"
\language "english"
\include "clairnote.ly"

\header{ tagline = "" }

#(set-global-staff-size 34)

\pointAndClickOff

\paper {
  page-breaking = #ly:one-page-breaking
  last-bottom-spacing = 0
  paper-width = 12 \cm
  score-system-spacing.basic-distance = 11

  top-system-spacing = 0
  top-margin = 1
  bottom-margin = 1
  left-margin = 1
  right-margin = 1
}

music = \relative {
  \time 1/16
  s16
  % \bar ""
  \time 6/8
  % g'16 gis a aes g gis a aes g gis a aes g gis a aes
  g'16 gs a g gs a g gs a g gs a
  % g aes a aes g aes a aes g aes a aes g aes a aes
  \time 1/16
  s16  \bar ""
  % \break g
}

fsize = -6

text = \lyricmode {
  \markup \with-color #grey \sans \fontsize #fsize {"G Sharp"}
  \markup \with-color #grey \sans \fontsize #fsize {"A Flat"}
  \markup \with-color #grey \sans \fontsize #fsize {"A Sharp"}
  \markup \with-color #grey \sans \fontsize #fsize {"B Flat"}
  % \markup \with-color #grey \sans \fontsize #fsize {"B"}
  % \markup \with-color #grey \sans \fontsize #fsize {"C-sharp"}
  % \markup \with-color #grey \sans \fontsize #fsize {"D-sharp"}
  % \markup \with-color #grey \sans \fontsize #fsize {"E"}
}

\layout {
  % ragged-right = ##t
  indent = #0
  % line-width = #80
  \context {
    \Score
    proportionalNotationDuration = #(ly:make-moment 1 12)
    % \override SpacingSpanner #'uniform-stretching = ##t
    % \override Score.SpacingSpanner #'strict-note-spacing = ##t
    \remove "System_start_delimiter_engraver"
    \override BarNumber #'break-visibility = #all-invisible % #'#(#f #f #f)
    explicitKeySignatureVisibility = #end-of-line-invisible % #'#(#f #t #t)
    printKeyCancellation = ##f
    \override KeyCancellation #'break-visibility = #all-invisible % #'#(#f #f #f)
  }
}

\score {
  \new Staff \with {
    % \cnNoteheadStyle "lilypond"
    \remove "Clef_engraver"
    \remove "Time_signature_engraver"
    % \magnifyStaff #1.75
  } {
    \new Voice = "one" { \music }
  }
}
\score {
  \new TradStaff \with {
    % \magnifyStaff #0.95
    \remove "Clef_engraver"
    \remove "Time_signature_engraver"
  } {
    \new Voice = "two" { \music }
  }
  % \new Lyrics \lyricsto "two" \text

  % \midi { }
}
