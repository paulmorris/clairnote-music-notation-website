\version "2.19.49"
\include "clairnote.ly"

#(set-global-staff-size 34)
\pointAndClickOff

\header{
  tagline = ""
}

\paper {
  page-breaking = #ly:one-line-auto-height-breaking
  top-system-spacing = 0
  top-margin = 1
  bottom-margin = 1
  left-margin = 1
  right-margin = 1
}

chromaticScale = \relative f {
  \cadenzaOn
  \hide Rest
  % c'8 cis d dis e f fis g gis a ais b c
  % c'8[ cis d dis] e[ f fis g] gis[ a ais b] c4
  c'8[ cis d dis] e[ f fis g]
  % \bar "|" r32
  gis8[ a ais b] c4
}

myFlat = \markup { \translate #(cons 0.1 0.25) \flat }
mySharp = \markup { \translate #(cons 0 0.55)  \sharp }

fsize = -6
spc = #2.5

text = \lyricmode {
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "C"}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  \center-column {\line {"C" \mySharp } \translate #(cons 0 -0.5) \line{"D" \myFlat }}}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "D"}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  \center-column {\line {"D" \mySharp } \translate #(cons 0 -0.5) \line{"E" \myFlat }}}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "E"}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "F"}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  \center-column {\line {"F" \mySharp } \translate #(cons 0 -0.5) \line{"G" \myFlat }}}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "G"}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  \center-column {\line {"G" \mySharp } \translate #(cons 0 -0.5) \line{"A" \myFlat }}}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "A"}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  \center-column {\line {"A" \mySharp } \translate #(cons 0 -0.5) \line{"B" \myFlat }}}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "B"}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "C"}
}

\score {
  <<
\new Staff
\with {
  \remove "Accidental_engraver"
  \remove "Key_engraver"
  \remove "Time_signature_engraver"
  \remove "Clef_engraver"
  % \remove "Bar_engraver"
  % \cnNoteheadStyle "funksol"
}{
  \new Voice = "one" {
    \override Score.SpacingSpanner.common-shortest-duration = #(ly:make-moment 1 24 )
    \override TextScript #'extra-offset = #'(0 . 0)
    s4^\markup \with-color #grey { \fontsize #-6 \sans "Chromatic Scale"}
    \chromaticScale
  }
}
\new Lyrics \lyricsto "one" \text
>>
}