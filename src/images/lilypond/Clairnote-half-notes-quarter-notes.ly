\version "2.19.49"
\include "clairnote.ly"

#(set-global-staff-size 34)
\pointAndClickOff

\header{
  tagline = ""
}

\paper {
  page-breaking = #ly:one-page-breaking
  top-system-spacing = 0
  top-margin = 1
  bottom-margin = 1
  left-margin = 1
  right-margin = 1
  paper-width = 13.9 \cm
  score-markup-spacing = 0 \cm
  indent = 0
}

rhythm = \relative {
  % \cadenzaOn
  e'2 e4 e4 f2 f4 f4
}

fsize = -6
spc = 1

text = \lyricmode {
  \markup \with-color #grey \sans \fontsize #fsize { \lower  #spc  \center-column {"Half" "Note"}}
  \markup \with-color #grey \sans \fontsize #fsize { \lower  #spc  \center-column {"Quarter" "Note"}}
  \markup \with-color #grey \sans \fontsize #fsize { \lower  #spc  \center-column {"Quarter" "Note"}}
  \markup \with-color #grey \sans \fontsize #fsize { \lower  #spc  \center-column {"Half" "Note"}}
  \markup \with-color #grey \sans \fontsize #fsize { \lower  #spc  \center-column {"Quarter" "Note"}}
  \markup \with-color #grey \sans \fontsize #fsize { \lower  #spc  \center-column {"Quarter" "Note"}}
}

\score {
  <<
    \new Staff
    \with {
      \remove "Accidental_engraver"
      \remove "Key_engraver"
      % \remove "Time_signature_engraver"
      \remove "Clef_engraver"
      % \remove "Bar_engraver"
    }
    \new Voice = "one" {
      \override Score.SpacingSpanner.common-shortest-duration = #(ly:make-moment 1 20)
      % \once \override Score.NoteColumn.X-offset = #3
      % \override TextScript #'extra-offset = #'(0 . 0)
      % s4^\markup { \fontsize #-6 \with-color #grey \sans {Chromatic Scale}}
      \partial 8
      \omit Rest
      r8
      \transpose c c \rhythm
      % r8
    }
    % \new Lyrics \lyricsto "two" \wholeToneText
  >>
}

\markup \vspace #1

\score {
  <<
    \new TradStaff
    \with {
      \remove "Accidental_engraver"
      \remove "Key_engraver"
      % \remove "Time_signature_engraver"
      \remove "Clef_engraver"
      % \remove "Bar_engraver"
      \numericTimeSignature
    }
    \new Voice = "two" {
      \override Score.SpacingSpanner.common-shortest-duration = #(ly:make-moment 1 20)
      % \override TextScript #'extra-offset = #'(0 . 0)
      % s4^\markup { \fontsize #-6 \with-color #grey \sans {}}
      \partial 8
      \omit Rest
      r8
      \rhythm
    }
    \new Lyrics \lyricsto "two" \text
  >>
}
