\version "2.19.49"
\include "clairnote.ly"
\language "english"

#(set-global-staff-size 34)
\pointAndClickOff

\header{
  tagline = ""
}

\paper {
  page-breaking = #ly:one-line-auto-height-breaking
  top-system-spacing = 0
  top-margin = 1
  bottom-margin = 1
  left-margin = 1
  right-margin = 1
}

notes = \relative {
  \cadenzaOn
  % \hide Rest
  s4
  c'4 d e f g a b
}

myFlat = \markup { \translate #(cons 0.1 0.25) \flat }
mySharp = \markup { \translate #(cons 0 0.55)  \sharp }

fsize = -6
spc = #2.5

text = \lyricmode {
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "C"}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "D"}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "E"}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "F"}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "G"}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "A"}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "B"}
}

\score {
  <<
    \new Staff
    \with {
      \remove "Accidental_engraver"
      \remove "Key_engraver"
      \remove "Time_signature_engraver"
      \remove "Clef_engraver"
      % \remove "Bar_engraver"
      % \cnNoteheadStyle "funksol"
    }{
      \new Voice = "one" {
        \override Score.SpacingSpanner.common-shortest-duration = #(ly:make-moment 1 27 )
        % \override TextScript #'extra-offset = #'(0 . 0)
        s4^\markup \with-color #grey { \fontsize #-6 \sans ""}
        \notes
      }
    }
    \new Lyrics \lyricsto "one" \text
  >>
}