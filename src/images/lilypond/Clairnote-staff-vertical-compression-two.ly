\version "2.19.52"
\include "clairnote.ly"
\language "english"
#(set-default-paper-size "letter")

#(set-global-staff-size 34)

% for properly scaled svg images for clairnote.org
% #(set-global-staff-size 34)
% fsize = -6
% \pointAndClickOff

\header {
  tagline = ##f
}

#(set! paper-alist (cons '("my size" . (cons
                                        (* 17.8 cm) ; width
                                        (* 6.9 cm) ; height
                                        )) paper-alist))

\paper {
  #(set-paper-size "my size")
  % indent = 0
  % ragged-bottom = ##t
  % page-count = #1
  % page-breaking = #ly:one-page-breaking
  top-margin = 0
  bottom-margin = 0
  left-margin = 1
  right-margin = 1
  top-system-spacing = ##f
}

music = {
  \stemUp s8. <e' e''>4 <f' f''>
}

text = \lyricmode {
  \markup \with-color #grey  \sans \fontsize #-7 {" "}
}

textB = \markup \with-color #grey  \sans \fontsize #-5 {Middle C (C4)}

smallsize = #-4

\layout {
  % ragged-right = ##t
  % indent = #0
  % line-width = #380
  \context {
    \Score
    proportionalNotationDuration = #(ly:make-moment 1 8)
    % \override SpacingSpanner #'uniform-stretching = ##t
    % \override Score.SpacingSpanner #'strict-note-spacing = ##t
    \remove "System_start_delimiter_engraver"
    \override BarNumber #'break-visibility = #'#(#f #f #f)
    explicitKeySignatureVisibility = #end-of-line-invisible % #'#(#f #t #t)
    printKeyCancellation = ##f
    \override KeyCancellation #'break-visibility = #all-invisible % #'#(#f #f #f)

  }
  \context {
    \Staff
    \override VerticalAxisGroup.default-staff-staff-spacing.basic-distance = #11
    % \staffSize #1.1
    % \remove "Accidental_engraver"
    \remove "Clef_engraver"
    \remove "Time_signature_engraver"
    \remove "Accidental_engraver"
    \remove "Key_engraver"
  }
  \context {
    \TradStaff
    % \staffSize #1.1
    % \remove "Accidental_engraver"
    \remove "Clef_engraver"
    \remove "Time_signature_engraver"
    \remove "Accidental_engraver"
    \remove "Key_engraver"
  }
}

\markup {
  \justify-line {

    \center-column {
      \vspace #0.670
      \score {
        <<
          \new TradStaff {
            \new Voice = "b" {
              \music
            }
          }
          \new Lyrics \lyricsto "b" \text
        >>

      }
      \vspace #0.3
      \with-color #grey  \sans \fontsize #smallsize "Standard"
      \with-color #grey  \sans \fontsize #smallsize "Staff"
      \with-color #grey  \sans \fontsize #smallsize "7/7 (1.0)"
    }

    \center-column {
      \vspace #0.559
      \score {
        <<
          \new Staff \with {
            \cnStaffCompression #7/12
          }
          {
            \new Voice = "one" {
              \music
            }
          }
          \new Lyrics \lyricsto "one" \text
        >>

      }
      \vspace #0.41
      \with-color #grey  \sans \fontsize #smallsize "Ultra-Compressed"
      \with-color #grey  \sans \fontsize #smallsize "Chromatic Staff"
      \with-color #grey  \sans \fontsize #smallsize "(1.0)"
    }

    \center-column {
      \vspace #0.335
      \score {
        <<
          \new Staff \with {
            \cnStaffCompression #0.75
          }
          {
            \new Voice = "one" {
              \music
            }
          }
          \new Lyrics \lyricsto "one" \text
        >>

      }
      \vspace #0.635
      \with-color #grey  \sans \fontsize #smallsize "Clairnote"
      \with-color #grey  \sans \fontsize #smallsize "Staff"
      \with-color #grey  \sans \fontsize #smallsize "9/7 (1.285...)"
    }

    \center-column {
      \score {
        <<
          \new Staff \with {
            \cnStaffCompression #1
          } {
            \new Voice = "three" {
              \music
            }
          }
          \new Lyrics \lyricsto "three" \text
        >>
      }
      \vspace #0.965
      \with-color #grey  \sans \fontsize #smallsize "Uncompressed"
      \with-color #grey  \sans \fontsize #smallsize "Chromatic Staff"
      \with-color #grey  \sans \fontsize #smallsize "12/7 (1.714...)"
    }

  }
}