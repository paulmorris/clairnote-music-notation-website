\version "2.19.49"
\include "clairnote.ly"

#(set-global-staff-size 34)
\pointAndClickOff

\header{
  tagline = ""
}

\paper {
  page-breaking = #ly:one-page-breaking
  top-system-spacing = 0
  top-margin = 1
  bottom-margin = 1
  left-margin = 1
  right-margin = 1
  paper-width = 17.0 \cm
  score-markup-spacing = 0 \cm
  indent = 0
}

rhythm = \relative f {
  \cadenzaOn
  g'1 g2 g4 g8 g8[ g8] g16 g16[ g16]
}

fsize = -6
spc = 1

text = \lyricmode {
  \markup \with-color #grey \sans \fontsize #fsize { \lower  #spc  \center-column {"Whole" "Notes"}}
  \markup \with-color #grey \sans \fontsize #fsize { \lower  #spc  \center-column {"Half" "Notes"}}
  \markup \with-color #grey \sans \fontsize #fsize { \lower  #spc  \center-column {"Quarter" "Notes"}}
  \markup \with-color #grey \sans \fontsize #fsize { \lower  #spc  \center-column {"8th" "Notes"}}
  \markup \with-color #grey \sans \fontsize #fsize { \lower  #spc  \center-column {""}}
  \markup \with-color #grey \sans \fontsize #fsize { \lower  #spc  \center-column {""}}
  \markup \with-color #grey \sans \fontsize #fsize { \lower  #spc  \center-column {"16th" "Notes"}}
  \markup \with-color #grey \sans \fontsize #fsize { \lower  #spc  \center-column {""}}
}

\score {
  <<
    \new Staff
    \with {
      \remove "Accidental_engraver"
      \remove "Key_engraver"
      \remove "Time_signature_engraver"
      \remove "Clef_engraver"
      \remove "Bar_engraver"
    }
    \new Voice = "one" {
      \override Score.SpacingSpanner.common-shortest-duration = #(ly:make-moment 1 24)
      % \override TextScript #'extra-offset = #'(0 . 0)
      % s4^\markup { \fontsize #-6 \with-color #grey \sans {Chromatic Scale}}
      \omit Rest
      r8 \transpose c cis \rhythm r8
    }

    % \new Lyrics \lyricsto "one" \text
  >>
}

\markup \vspace #1

\score {
  <<
    \new Staff
    \with {
      \remove "Accidental_engraver"
      \remove "Key_engraver"
      \remove "Time_signature_engraver"
      \remove "Clef_engraver"
      \remove "Bar_engraver"
    }
    \new Voice = "one" {
      \override Score.SpacingSpanner.common-shortest-duration = #(ly:make-moment 1 24)
      % \once \override Score.NoteColumn.X-offset = #3
      % \override TextScript #'extra-offset = #'(0 . 0)
      % s4^\markup { \fontsize #-6 \with-color #grey \sans {Chromatic Scale}}
      \omit Rest
      r8
      \transpose c c \rhythm
      r8
    }
    % \new Lyrics \lyricsto "two" \wholeToneText
  >>
}

\markup \vspace #1

\score {
  <<
    \new TradStaff
    \with {
      \remove "Accidental_engraver"
      \remove "Key_engraver"
      \remove "Time_signature_engraver"
      \remove "Clef_engraver"
      \remove "Bar_engraver"
    }
    \new Voice = "two" {
      \override Score.SpacingSpanner.common-shortest-duration = #(ly:make-moment 1 24)
      % \override TextScript #'extra-offset = #'(0 . 0)
      % s4^\markup { \fontsize #-6 \with-color #grey \sans {}}
      \omit Rest
      r8 \rhythm r8
    }
    \new Lyrics \lyricsto "two" \text
  >>
}
