\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\language "english"

#(set-global-staff-size 34)
\pointAndClickOff

\header{
  tagline = ""
}

\paper {
  page-breaking = #ly:one-line-auto-height-breaking
  top-system-spacing = 0
  top-margin = 1
  bottom-margin = 1
  left-margin = 1
  right-margin = 1
}

chromaticScale = \relative f {
  \cadenzaOn
  c''' b bf a af g gf f e ef d df c
}

myFlat = \markup { \translate #(cons 0.1 0.25) \flat }
mySharp = \markup { \translate #(cons 0 0.55)  \sharp } 

fsize = -6
spc = #2

text = \lyricmode {
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "C"}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "B"}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  \line {"B" \myFlat }}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "A"}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  \line {"A" \myFlat }}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "G"}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  \line {"G" \myFlat }}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "F"}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "E"}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  \line {"E" \myFlat }}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "D"}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  \line {"D" \myFlat }}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "C"}
  % \markup \sans \fontsize #fsize {C}  
}

\score {
  <<
\new Staff
\with {
  % \remove "Accidental_engraver"
  \remove "Key_engraver"
  \remove "Time_signature_engraver"
  \remove "Clef_engraver"
  \remove "Bar_engraver"
  % \cnNoteheadStyle "funksol"
}{
  \new Voice = "one" {
    \override Score.SpacingSpanner.common-shortest-duration = #(ly:make-moment 1 16 )
    \override TextScript #'extra-offset = #'(0 . 0)
    s4^\markup \with-color #grey { \fontsize #-6 \sans "Chromatic Scale"}
    \chromaticScale
  }
}
\new Lyrics \lyricsto "one" \text
>>
}