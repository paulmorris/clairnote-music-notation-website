\version "2.19.63"
\language "english"
\include "clairnote.ly"

\header{ tagline = "" }

#(set-global-staff-size 34)

\pointAndClickOff

\paper {
  page-breaking = #ly:one-line-auto-height-breaking
  top-system-spacing = 0
  top-margin = 1
  bottom-margin = 1
  left-margin = 1
  right-margin = 1
}

music = \relative c' {
  \time 6/4
  s8 a'4 gss f gff
  \bar ""
}

fsize = -6

text = \lyricmode {
  \markup \with-color #grey \sans \fontsize #fsize {"A"}
  \markup \with-color #grey \sans \fontsize #fsize \center-column {
    \line {"G double"}
    \line {"sharp"}
  }
  \markup \with-color #grey \sans \fontsize #fsize {"F"}
  \markup \with-color #grey \sans \fontsize #fsize \center-column {
    \line {"G double"}
    \line {"flat"}
  }
  % \markup \with-color #grey \sans \fontsize #fsize {"B"}
  % \markup \with-color #grey \sans \fontsize #fsize {"C-sharp"}
  % \markup \with-color #grey \sans \fontsize #fsize {"D-sharp"}
  % \markup \with-color #grey \sans \fontsize #fsize {"E"}
}

\score {
  <<
    \new Staff \with {
      % \cnNoteheadStyle "lilypond"
      \remove "Clef_engraver"
      \remove "Time_signature_engraver"
      % \magnifyStaff #1.75
    } {
      \new Voice = "one" { \music }
    }
    \new TradStaff \with {
      % \magnifyStaff #0.95
      \remove "Clef_engraver"
      \remove "Time_signature_engraver"
    } {
      \new Voice = "two" { \music }
    }
    \new Lyrics \lyricsto "two" \text
  >>
  \layout {
    % ragged-right = ##t
    indent = #0
    % line-width = #80
    \context {
      \Score
      proportionalNotationDuration = #(ly:make-moment 1 16)
      % \override SpacingSpanner #'uniform-stretching = ##t
      % \override Score.SpacingSpanner #'strict-note-spacing = ##t
      \remove "System_start_delimiter_engraver"
      \override BarNumber.break-visibility = #all-invisible % #'#(#f #f #f)
      explicitKeySignatureVisibility = #end-of-line-invisible % #'#(#f #t #t)
      printKeyCancellation = ##f
      \override KeyCancellation.break-visibility = #all-invisible % #'#(#f #f #f)
    }
  }
  % \midi { }
}
