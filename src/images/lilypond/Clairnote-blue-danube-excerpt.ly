\version "2.19.49"
\include "clairnote.ly"

#(set-global-staff-size 30)
\pointAndClickOff

\header{
  tagline = ""
}

\paper {
  page-breaking = #ly:one-line-auto-height-breaking
  top-system-spacing = 0
  top-margin = 2
  bottom-margin = 2
  left-margin = 6
  right-margin = 1
}

upper = \relative {
  \clef treble
  \key c \major
  \time 3/4

  \partial 4
  c'4
  c4(_\mf e g)
  g2 <e' g>4
  <e g>2 <c e>4
  <c e>2 c,4
  c4( e g)
  g2 <f' g>4
  <f g>2 <b, f'>4
}

lower = \relative {
  \clef bass
  \key c \major
  \time 3/4

  \partial 4
  r4
  r2.
  c4_\p <e g> <e g>
  c4 <e g> <e g>
  c4 <e g> <e g>
  c4 <e g> <e g>
  d4 <f g> <f g>
  d4 <f g> <f g>
}

\score {
  \new PianoStaff
  <<
    % \set PianoStaff.instrumentName = #"Piano"
    \new Staff = upper
    \with {
      % \remove "Accidental_engraver"
      % \remove "Key_engraver"
      % \staffSize #-2
    }
    { \upper }
    \new Staff = lower
    \with {
      % \remove "Accidental_engraver"
      % \remove "Key_engraver"
      % \staffSize #-2
    }
    { \lower }
  >>

  \layout { }
  % \midi { }
}