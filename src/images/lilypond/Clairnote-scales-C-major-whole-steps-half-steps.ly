\version "2.19.49"
\include "clairnote.ly"
\language "english"

#(set-global-staff-size 34)
\pointAndClickOff

\header{
  tagline = ""
}

\paper {
  page-breaking = #ly:one-line-auto-height-breaking
  top-system-spacing = 0
  top-margin = 0.5
  bottom-margin = 0.5
  left-margin = 1
  right-margin = 1
}

%{
wholeToneScales = {
  \relative f {
    \partcombineChords
    \cadenzaOn
    % s16
    c'4 d e \stemUp fis \stemUp gis \stemUp ais
    cis, dis \stemUp f \stemUp g \stemUp a \stemUp b
    % s16
  }
}


cMajorScale = {
  \relative f {
    \partcombineChords
    \cadenzaOn
    % s16
    c'8 d e \stemUp f \stemUp g \stemUp a b c
    % s16
  }
}
%}

fsize = #-6
spc = #5.5
clr = #green
wsclr = #(x11-color 'SeaGreen)
hsclr = #(x11-color 'RoyalBlue)
hsmove = #2.1
wsmove = #1.6

cMinorOctaves = {
  \relative f {
    \cadenzaOn
    % s16

    c'4
    % s8_\markup \with-color #clr \sans \fontsize #fsize { \lower #spc \halign #1  \center-column {"Whole" "Step" }}
    d _\markup \with-color #wsclr \sans \fontsize #fsize { \lower #spc \halign #wsmove  \center-column {"Whole" "Step" }}
    e _\markup \with-color #wsclr \sans \fontsize #fsize { \lower #spc  \halign #wsmove \center-column {"Whole" "Step" }}
    f _\markup \with-color #hsclr \sans \fontsize #fsize { \lower #spc \halign #hsmove  \center-column {"Half" "Step" }}
    g _\markup \with-color #wsclr \sans \fontsize #fsize { \lower #spc \halign #wsmove  \center-column {"Whole" "Step" }}
    a _\markup \with-color #wsclr \sans \fontsize #fsize { \lower #spc \halign #wsmove  \center-column {"Whole" "Step" }}
    b _\markup \with-color #wsclr \sans \fontsize #fsize { \lower #spc \halign #wsmove  \center-column {"Whole" "Step" }}
    c _\markup \with-color #hsclr \sans \fontsize #fsize { \lower #spc \halign #hsmove  \center-column {"Half" "Step" }}
    % <c' c'>8[ <d d'> <e e'> <f f'>]  <g g'>[  <a a'> <b b'> <c c'>]
    % s16
  }
}

% myFlat = \markup { \translate #(cons 0.1 0.25) \flat }
% mySharp = \markup { \translate #(cons 0 0.55)  \sharp }


%{
text = \lyricmode {

  \markup \with-color #clr \sans \fontsize #fsize { \lower #spc \center-column {"Whole" "Step"}}
  \markup \with-color #clr \sans \fontsize #fsize { \lower #spc \center-column {"D"}}
  \markup \with-color #clr \sans \fontsize #fsize { \lower #spc \center-column {"E"}}
  \markup \with-color #clr \sans \fontsize #fsize { \lower #spc \center-column {"F"}}
  \markup \with-color #clr \sans \fontsize #fsize { \lower #spc \center-column {"G"}}
  \markup \with-color #clr \sans \fontsize #fsize { \lower #spc \center-column {"A"}}
  \markup \with-color #clr \sans \fontsize #fsize { \lower #spc \center-column {"B"}}
  \markup \with-color #clr \sans \fontsize #fsize { \lower #spc \center-column {"C"}}

  % \markup \with-color #clr \sans \fontsize #fsize { \lower #spc C}
  % \markup \with-color #clr \sans \fontsize #fsize { \lower #spc \center-column {\line {"D" \mySharp } \translate #(cons 0 -0.5) \line{"E" \myFlat }}}
}
%}
%{
\new StaffClairnote
\with {
  \remove "Accidental_engraver"
  \remove "Key_engraver"
  \remove "Time_signature_engraver"
  \remove "Clef_engraver"
  \remove "Bar_engraver"
} {
  \override Score.SpacingSpanner.common-shortest-duration = #(ly:make-moment 1 12 )
  \override TextScript #'extra-offset = #'(0 . 0)
  s1^\markup { \with-color #clr  \fontsize #-5 \sans {Octaves}}
  \partcombine \cMajorScale \transpose c c' \cMajorScale
}
%}


\score {
  <<
    \new Staff
    \with {
      \remove "Accidental_engraver"
      \remove "Key_engraver"
      \remove "Time_signature_engraver"
      \remove "Clef_engraver"
      \remove "Bar_engraver"
    }{
      \new Voice = "one" {
        \override Score.SpacingSpanner.common-shortest-duration = #(ly:make-moment 1 32)
        % \override TextScript #'extra-offset = #'(0 . 2)
        s4^\markup \with-color #grey { \fontsize #-6 \sans "C Major Scale"}
        \cMinorOctaves
      }
    }
    % \new Lyrics \lyricsto "one" \text
  >>
}