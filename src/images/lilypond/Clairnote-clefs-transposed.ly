\version "2.19.52"
\include "clairnote.ly"
\language "english"
#(set-default-paper-size "letter")

#(set-global-staff-size 34)

% for properly scaled svg images for clairnote.org
% #(set-global-staff-size 34)
% fsize = -6
% \pointAndClickOff

\header {
  tagline = ##f
}

#(set! paper-alist (cons '("my size" . (cons
                                        (* 10.0 in) ; width
                                        (* 3.4 in) ; height
                                        )) paper-alist))

\paper {
  #(set-paper-size "my size")
  % indent = 0
  % ragged-bottom = ##t
  % page-count = #1
  % page-breaking = #ly:one-page-breaking
  top-margin = 0.5 \cm
  bottom-margin = 0.5 \cm
  left-margin = 0.5 \cm
  right-margin = 0.5 \cm
  top-system-spacing = ##f
}


text = \lyricmode {
  \markup \with-color #grey  \sans \fontsize #-7 {" "}
}

textB = \markup \with-color #grey  \sans \fontsize #-5 {Middle C (C4)}

smallsize = #-2

\layout {
  % ragged-right = ##t
  % indent = #0
  % line-width = #380
  \context {
    \Score
    proportionalNotationDuration = #(ly:make-moment 1 8)
    % \override SpacingSpanner #'uniform-stretching = ##t
    % \override Score.SpacingSpanner #'strict-note-spacing = ##t
    \remove "System_start_delimiter_engraver"
    \override BarNumber #'break-visibility = #'#(#f #f #f)
    explicitKeySignatureVisibility = #end-of-line-invisible % #'#(#f #t #t)
    printKeyCancellation = ##f
    \override KeyCancellation #'break-visibility = #all-invisible % #'#(#f #f #f)

  }
  \context {
    \Staff
    \override VerticalAxisGroup.default-staff-staff-spacing.basic-distance = #11
    % \staffSize #1.1
    % \remove "Accidental_engraver"
    % \remove "Clef_engraver"
    \remove "Time_signature_engraver"
    \remove "Accidental_engraver"
    \remove "Key_engraver"
  }
  \context {
    \TradStaff
    % \staffSize #1.1
    % \remove "Accidental_engraver"
    % \remove "Clef_engraver"
    \remove "Time_signature_engraver"
    \remove "Accidental_engraver"
    \remove "Key_engraver"
  }
}

\markup {
  \justify-line {

    \center-column {
      \score {
        <<
          \new Staff
          {
            \new Voice = "a" {
              \clef "bass_15"
              e,,4
            }
          }
          \new TradStaff {
            \new Voice = "b" {
              \clef "bass_15"
              e,,4
            }
          }
          \new Lyrics \lyricsto "b" \text
        >>

      }
      \vspace #1
      \with-color #grey  \sans \fontsize #smallsize "E1"
      % \vspace #0.3
      % \with-color #grey  \sans \fontsize #-1 "Treble Clef"
    }

    \center-column {
      \score {
        <<
          \new Staff
          {
            \new Voice = "one" {
              \clef "bass_8"
              e,4
            }
          }
          \new TradStaff {
            \new Voice = "two" {
              \clef "bass_8"
              e,4
            }
          }
          \new Lyrics \lyricsto "two" \text
        >>

      }
      \vspace #1
      \with-color #grey  \sans \fontsize #smallsize "E2"
      % \vspace #0.3
      % \with-color #grey  \sans \fontsize #-1 "Treble Clef"
    }

    \center-column {
      \score {
        <<
          \new Staff
          {
            \new Voice = "three" {
              \clef bass
              e4
            }
          }
          \new TradStaff {
            \new Voice = "four" {
              \clef bass
              e4
            }
          }
          \new Lyrics \lyricsto "four" \text
        >>
      }
      \vspace #1
      \with-color #grey  \sans \fontsize #smallsize "E3"
      % \vspace #0.3
      % \with-color #grey  \sans \fontsize #-1 "Treble Clef"
    }

    \center-column {
      \score {
        <<
          \new Staff
          {
            \new Voice = "five" {
              \clef alto
              e'4
            }
          }
          \new TradStaff {
            \new Voice = "six" {
              \clef alto
              e'4
            }
          }
          \new Lyrics \lyricsto "six" \text
        >>
      }
      \vspace #1
      \with-color #grey  \sans \fontsize #smallsize "E4"
      % \vspace #0.3
      % \with-color #grey  \sans \fontsize #-1 "Treble Clef"
    }

    \center-column {
      \score {
        <<
          \new Staff
          {
            \new Voice = "seven" {
              \clef treble
              e''4
            }
          }
          \new TradStaff {
            \new Voice = "eight" {
              \clef treble
              e''4
            }
          }
          \new Lyrics \lyricsto "eight" \text
        >>
      }
      \vspace #1
      \with-color #grey  \sans \fontsize #smallsize "E5"
      % \vspace #0.3
      % \with-color #grey  \sans \fontsize #-1 "Treble Clef"
    }

    \center-column {
      \score {
        <<
          \new Staff
          {
            \new Voice = "nine" {
              \clef "treble^8"
              e'''4
            }
          }
          \new TradStaff {
            \new Voice = "ten" {
              \clef "treble^8"
              e'''4
            }
          }
          \new Lyrics \lyricsto "ten" \text
        >>
      }
      \vspace #1
      \with-color #grey  \sans \fontsize #smallsize "E6"
      % \vspace #0.3
      % \with-color #grey  \sans \fontsize #-1 "Treble Clef"
    }

    \center-column {
      \score {
        <<
          \new Staff
          {
            \new Voice = "d" {
              \clef "treble^15"
              e''''4
            }
          }
          \new TradStaff {
            \new Voice = "e" {
              \clef "treble^15"
              e''''4
            }
          }
          \new Lyrics \lyricsto "e" \text
        >>
      }
      \vspace #1
      \with-color #grey  \sans \fontsize #smallsize "E7"
      % \vspace #0.3
      % \with-color #grey  \sans \fontsize #-1 "Treble Clef"
    }

  }
}