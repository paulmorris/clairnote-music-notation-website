\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"
\language "english" 

#(set-global-staff-size 34)
\pointAndClickOff

\header{
  tagline = ""
}

\paper {
  page-breaking = #ly:one-line-auto-height-breaking
  top-system-spacing = 0
  top-margin = 1
  bottom-margin = 1
  left-margin = 1
  right-margin = 1
}

cMajorOctaves = {
  \relative f {
    \cadenzaOn
    % s16
    <c' c'>4 <d d'> <e e'> <f f'>  <g g'>  <a a'> <b b'> <c c'>
    % s16
  }
}

% myFlat = \markup { \translate #(cons 0.1 0.25) \flat }
% mySharp = \markup { \translate #(cons 0 0.55)  \sharp }

fsize = #-6
spc = #2.5
clr = #grey

text = \lyricmode {
  \markup \with-color #clr \sans \fontsize #fsize { \lower #spc \center-column {\line {"C"} \translate #(cons 0 -0.5) \line {"C"}}}
  \markup \with-color #clr \sans \fontsize #fsize { \lower #spc \center-column {\line {"D"} \translate #(cons 0 -0.5) \line {"D"}}}
  \markup \with-color #clr \sans \fontsize #fsize { \lower #spc \center-column {\line {"E"} \translate #(cons 0 -0.5) \line {"E"}}}
  \markup \with-color #clr \sans \fontsize #fsize { \lower #spc \center-column {\line {"F"} \translate #(cons 0 -0.5) \line {"F"}}}
  \markup \with-color #clr \sans \fontsize #fsize { \lower #spc \center-column {\line {"G"} \translate #(cons 0 -0.5) \line {"G"}}}
  \markup \with-color #clr \sans \fontsize #fsize { \lower #spc \center-column {\line {"A"} \translate #(cons 0 -0.5) \line {"A"}}}
  \markup \with-color #clr \sans \fontsize #fsize { \lower #spc \center-column {\line {"B"} \translate #(cons 0 -0.5) \line {"B"}}}
  \markup \with-color #clr \sans \fontsize #fsize { \lower #spc \center-column {\line {"C"} \translate #(cons 0 -0.5) \line {"C"}}}
  % \markup \with-color #clr \sans \fontsize #fsize { \lower #spc C}
  % \markup \with-color #clr \sans \fontsize #fsize { \lower #spc \center-column {\line {"D" \mySharp } \translate #(cons 0 -0.5) \line{"E" \myFlat }}}
}

\layout {
  \context {
    \Score
    \remove "System_start_delimiter_engraver"
    % \override StaffGrouper.staff-staff-spacing.padding = #100
    % \override StaffGrouper.staff-staff-spacing.basic-distance = #100
  }
  \context {
    \Lyrics
    %\override VerticalAxisGroup.nonstaff-relatedstaff-spacing.basic-distance = #5
  }
  \context {
    \Staff
    \override VerticalAxisGroup
    .default-staff-staff-spacing.basic-distance = #18
  }
}

\score {
  <<
    \new Staff
    \with {
      \remove "Accidental_engraver"
      \remove "Key_engraver"
      \remove "Time_signature_engraver"
      \remove "Clef_engraver"
      % \remove "Bar_engraver"
      % \cnNoteheadStyle "funksol"
    }{
      \new Voice = "one" {
        \override Score.SpacingSpanner
        .common-shortest-duration = #(ly:make-moment 1 24 )

        % \override TextScript #'extra-offset = #'(0 . 2)

        s4^\markup \with-color #grey {
          \fontsize #-6 \sans "" % C Major Scale, Octaves "C Major Scale, C# Pentatonic Scale"
        }
        \cMajorOctaves
      }
    }

    \new Lyrics {
      \override VerticalAxisGroup
      .nonstaff-relatedstaff-spacing.basic-distance = #7.0
      \lyricsto "one"
      \text
    }

    \new TradStaff \with {
      \remove "Time_signature_engraver"
      \remove "Clef_engraver"
    }{
      \new Voice = "two" {
        \override Score.SpacingSpanner
        .common-shortest-duration = #(ly:make-moment 1 24 )

        \override TextScript #'extra-offset = #'(0 . 0)

        s4^\markup \with-color #grey {
          \fontsize #-6 \sans ""
        }
        \cMajorOctaves
      }
    }
    %{
    \new Lyrics {
      \override VerticalAxisGroup.nonstaff-relatedstaff-spacing.basic-distance = #4.5
      \lyricsto "two"
      \tradText
    }
    %}
  >>
}