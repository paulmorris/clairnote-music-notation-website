\version "2.19.49"
\include "clairnote.ly"
\language "english"

#(set-global-staff-size 26)
\pointAndClickOff

\header{
  tagline = ""
}

#(set! paper-alist (cons '("my size" . (cons
                                        (* 17.0 cm) ; width 21.5
                                        (* 19.75 cm) ; height
                                        )) paper-alist))

\paper {
  #(set-paper-size "my size")
  % page-breaking = #ly:one-line-auto-height-breaking
  % page-breaking = #ly:one-page-breaking
  % paper-width = 22 \cm
  % paper-height = 22 \cm
  top-system-spacing = 0
  top-margin = 0.5
  bottom-margin = 0
  left-margin = 1
  right-margin = 1
}

\layout {
  \context {
    \Staff
    \remove "Accidental_engraver"
    \remove "Key_engraver"
    \remove "Time_signature_engraver"
    \remove "Clef_engraver"
    \remove "Bar_engraver"
  }
  \context {
    \Score
    \override SpacingSpanner.common-shortest-duration = #(ly:make-moment 1 8)
  }
}

fsize = #-4
spc = #4.5
clr = #green
wsclr = #(x11-color 'SeaGreen)
hsclr = #(x11-color 'RoyalBlue)
hsmove = #2.5
wsmove = #2


makeScaleStaff =
#(define-music-function (label pitch spc) (string? ly:pitch? number?)
   #{
     \cadenzaOn
     s4^\markup \with-color #grey \fontsize #-5 \sans #label
     \transpose c $pitch {
       c
       d _\markup \with-color #wsclr \sans \fontsize #fsize { \lower #spc \halign #wsmove  "W" }
       e _\markup \with-color #wsclr \sans \fontsize #fsize { \lower #spc \halign #wsmove  "W" }
       f _\markup \with-color #hsclr \sans \fontsize #fsize { \lower #spc \halign #hsmove  "H" }
       g _\markup \with-color #wsclr \sans \fontsize #fsize { \lower #spc \halign #wsmove  "W" }
       a _\markup \with-color #wsclr \sans \fontsize #fsize { \lower #spc \halign #wsmove  "W" }
       b _\markup \with-color #wsclr \sans \fontsize #fsize { \lower #spc \halign #wsmove  "W" }
       c'_\markup \with-color #hsclr \sans \fontsize #fsize { \lower #spc \halign #hsmove  "H" }
     }
   #})




% myFlat = \markup { \translate #(cons 0.1 0.25) \flat }
% mySharp = \markup { \translate #(cons 0 0.55)  \sharp }

vs = #1

\markup {
  \justify-line {

    \center-column {
      \score { \makeScaleStaff "C Major" c' #4.5 } \vspace #vs
      \score { \makeScaleStaff "D Major" d' #4 } \vspace #vs
      \score { \makeScaleStaff "E Major" e' #3 } \vspace #vs
      \score { \makeScaleStaff "G Flat Major" gf' #3 } \vspace #vs
      \score { \makeScaleStaff "A Flat Major" af' #3 } \vspace #vs
      \score { \makeScaleStaff "B Flat Major" bf' #3 }
    }

    \center-column {
      \score { \makeScaleStaff "D Flat Major" df' #4.5 } \vspace #vs
      \score { \makeScaleStaff "E Flat Major" ef' #4 } \vspace #vs
      \score { \makeScaleStaff "F Major" f' #3 } \vspace #vs
      \score { \makeScaleStaff "G Major" g' #3 } \vspace #vs
      \score { \makeScaleStaff "A Major" a' #3 } \vspace #vs
      \score { \makeScaleStaff "B Major" b' #3 }
    }

  }
}