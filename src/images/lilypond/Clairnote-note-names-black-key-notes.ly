\version "2.19.49"
\include "clairnote.ly"
\language "english"

#(set-global-staff-size 34)
\pointAndClickOff

\header{
  tagline = ""
}

\paper {
  page-breaking = #ly:one-line-auto-height-breaking
  top-system-spacing = 0
  top-margin = 1
  bottom-margin = 1
  left-margin = 1
  right-margin = 1
}

notes = \relative {
  \cadenzaOn
  % \hide Rest
  s4
  cs'4 ds fs gs as
}

myFlat = \markup { \translate #(cons 0.1 0.25) \flat }
mySharp = \markup { \translate #(cons 0 0.55)  \sharp }

fsize = -6
spc = #2.5

text = \lyricmode {
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  \center-column {\line {"C Sharp" } \translate #(cons 0 -0.5) \line{"D Flat" }}}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  \center-column {\line {"D Sharp" } \translate #(cons 0 -0.5) \line{"E Flat" }}}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  \center-column {\line {"F Sharp" } \translate #(cons 0 -0.5) \line{"G Flat" }}}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  \center-column {\line {"G Sharp" } \translate #(cons 0 -0.5) \line{"A Flat" }}}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  \center-column {\line {"A Sharp" } \translate #(cons 0 -0.5) \line{"B Flat" }}}
  % \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "B"}
}

\score {
  <<
    \new Staff
    \with {
      \remove "Accidental_engraver"
      \remove "Key_engraver"
      \remove "Time_signature_engraver"
      \remove "Clef_engraver"
      % \remove "Bar_engraver"
      % \cnNoteheadStyle "funksol"
    }{
      \new Voice = "one" {
        \override Score.SpacingSpanner.common-shortest-duration = #(ly:make-moment 1 96 )
        % \override TextScript #'extra-offset = #'(0 . 0)
        s4^\markup \with-color #grey { \fontsize #-6 \sans ""} % \"Black Key\" Notes
        \notes
      }
    }
    \new Lyrics \lyricsto "one" \text
  >>
}