\version "2.19.49"
\include "clairnote.ly"
\language "english"

#(set-global-staff-size 26)
\pointAndClickOff

\header{
  tagline = ""
}

#(set! paper-alist (cons '("my size" . (cons
                                        (* 13.3 cm) ; width 21.5
                                        (* 19.75 cm) ; height
                                        )) paper-alist))

\paper {
  #(set-paper-size "my size")
  % page-breaking = #ly:one-line-auto-height-breaking
  % page-breaking = #ly:one-page-breaking
  % paper-width = 22 \cm
  % paper-height = 22 \cm
  top-system-spacing = 0
  top-margin = 0.5
  bottom-margin = 0
  left-margin = 1
  right-margin = 1
}

\layout {
  \context {
    \Staff
    \remove "Accidental_engraver"
    \remove "Key_engraver"
    \remove "Time_signature_engraver"
    \remove "Clef_engraver"
    \remove "Bar_engraver"
  }
  \context {
    \Score
    \override SpacingSpanner.common-shortest-duration = #(ly:make-moment 1 8)
  }
}

fsize = #-4
spc = #4.5
clr = #green
wsclr = #(x11-color 'SeaGreen)
hsclr = #(x11-color 'RoyalBlue)
hsmove = #2.5
wsmove = #2
mthirdmove = #1.2
mthirdclr = #(x11-color 'MediumPurple)

makeScaleStaff =
#(define-music-function (label pitch spc) (string? ly:pitch? number?)
   #{
     \cadenzaOn
     s4^\markup \with-color #grey \fontsize #-5 \sans #label
     \transpose c $pitch {
       c
       ef _\markup \with-color #mthirdclr \sans \fontsize #fsize { \lower #spc \halign #mthirdmove  "m3" }
       f _\markup \with-color #wsclr \sans \fontsize #fsize { \lower #spc \halign #wsmove  "W" }
       g _\markup \with-color #wsclr \sans \fontsize #fsize { \lower #spc \halign #wsmove  "W" }
       bf _\markup \with-color #mthirdclr \sans \fontsize #fsize { \lower #spc \halign #mthirdmove  "m3" }
       c' _\markup \with-color #wsclr \sans \fontsize #fsize { \lower #spc \halign #wsmove  "W" }

     }
   #})




% myFlat = \markup { \translate #(cons 0.1 0.25) \flat }
% mySharp = \markup { \translate #(cons 0 0.55)  \sharp }

vs = #1

\markup {
  \justify-line {

    \center-column {
      \score { \makeScaleStaff "C Minor Pentatonic" c' #4.5 } \vspace #vs
      \score { \makeScaleStaff "D Minor Pentatonic" d' #4 } \vspace #vs
      \score { \makeScaleStaff "E Minor Pentatonic" e' #3 } \vspace #vs
      \score { \makeScaleStaff "F Sharp Minor Pentatonic" gf' #3 } \vspace #vs
      \score { \makeScaleStaff "G Sharp Minor Pentatonic" af' #3 } \vspace #vs
      \score { \makeScaleStaff "B Flat Minor Pentatonic" bf' #3 }
    }

    \center-column {
      \score { \makeScaleStaff "C Sharp Minor Pentatonic" df' #4.5 } \vspace #vs
      \score { \makeScaleStaff "E Flat Minor Pentatonic" ef' #4 } \vspace #vs
      \score { \makeScaleStaff "F Minor Pentatonic" f' #3 } \vspace #vs
      \score { \makeScaleStaff "G Minor Pentatonic" g' #3 } \vspace #vs
      \score { \makeScaleStaff "A Minor Pentatonic" a' #3 } \vspace #vs
      \score { \makeScaleStaff "B Minor Pentatonic" b' #3 }
    }

  }
}