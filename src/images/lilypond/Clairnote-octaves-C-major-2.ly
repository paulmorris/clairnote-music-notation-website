\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"

#(set-global-staff-size 34)
\pointAndClickOff

\header{
  tagline = ""
}

\paper {
  page-breaking = #ly:one-line-auto-height-breaking
  top-system-spacing = 0
  top-margin = 6
  bottom-margin = 1
  left-margin = 1
  right-margin = 1
}

%{
wholeToneScales = {
  \relative f {
    \partcombineChords
    \cadenzaOn
    % s16
    c'4 d e \stemUp fis \stemUp gis \stemUp ais
    cis, dis \stemUp f \stemUp g \stemUp a \stemUp b
    % s16
  }
}


cMajorScale = {
  \relative f {
    \partcombineChords
    \cadenzaOn
    % s16
    c'8 d e \stemUp f \stemUp g \stemUp a b c
    % s16
  }
}
%}

cMajorOctaves = {
  \relative f {
    \cadenzaOn
    % s16
    <c' c'>8[ <d d'> <e e'> <f f'>]  <g g'>[  <a a'> <b b'> <c c'>]
    % s16
  }
}

% myFlat = \markup { \translate #(cons 0.1 0.25) \flat }
% mySharp = \markup { \translate #(cons 0 0.55)  \sharp }

fsize = #-6
spc = #3
clr = #grey

text = \lyricmode {
  \markup \with-color #clr \sans \fontsize #fsize { \lower #spc \center-column {\line {"C"} \translate #(cons 0 -0.5) \line {"C"}}}
  \markup \with-color #clr \sans \fontsize #fsize { \lower #spc \center-column {\line {"D"} \translate #(cons 0 -0.5) \line {"D"}}}
  \markup \with-color #clr \sans \fontsize #fsize { \lower #spc \center-column {\line {"E"} \translate #(cons 0 -0.5) \line {"E"}}}
  \markup \with-color #clr \sans \fontsize #fsize { \lower #spc \center-column {\line {"F"} \translate #(cons 0 -0.5) \line {"F"}}}
  \markup \with-color #clr \sans \fontsize #fsize { \lower #spc \center-column {\line {"G"} \translate #(cons 0 -0.5) \line {"G"}}}
  \markup \with-color #clr \sans \fontsize #fsize { \lower #spc \center-column {\line {"A"} \translate #(cons 0 -0.5) \line {"A"}}}
  \markup \with-color #clr \sans \fontsize #fsize { \lower #spc \center-column {\line {"B"} \translate #(cons 0 -0.5) \line {"B"}}}
  \markup \with-color #clr \sans \fontsize #fsize { \lower #spc \center-column {\line {"C"} \translate #(cons 0 -0.5) \line {"C"}}}
  % \markup \with-color #clr \sans \fontsize #fsize { \lower #spc C}
  % \markup \with-color #clr \sans \fontsize #fsize { \lower #spc \center-column {\line {"D" \mySharp } \translate #(cons 0 -0.5) \line{"E" \myFlat }}}
}
%{
\new StaffClairnote
\with {
  \remove "Accidental_engraver"
  \remove "Key_engraver"
  \remove "Time_signature_engraver"
  \remove "Clef_engraver"
  \remove "Bar_engraver"
} {
  \override Score.SpacingSpanner.common-shortest-duration = #(ly:make-moment 1 12 )
  \override TextScript #'extra-offset = #'(0 . 0)
  s1^\markup { \with-color #clr  \fontsize #-5 \sans {Octaves}}
  \partcombine \cMajorScale \transpose c c' \cMajorScale
}
%}

\score {
  <<
    \new Staff
    \with {
      \remove "Accidental_engraver"
      \remove "Key_engraver"
      \remove "Time_signature_engraver"
      \remove "Clef_engraver"
      \remove "Bar_engraver"
    }{
      \new Voice = "one" {
        \override Score.SpacingSpanner.common-shortest-duration = #(ly:make-moment 1 32)
        \override TextScript #'extra-offset = #'(0 . 2)
        s4^\markup \with-color #clr { \fontsize #-6 \sans "C Major Scale, Octaves"}
        \cMajorOctaves
      }
    }
    \new Lyrics \lyricsto "one" \text
  >>
}