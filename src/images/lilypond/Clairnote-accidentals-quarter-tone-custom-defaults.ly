\version "2.24.0"
\language "english"
\include "clairnote.ly"

\header{ tagline = "" }

#(set-global-staff-size 34)

\pointAndClickOff

\paper {
  page-breaking = #ly:one-line-auto-height-breaking
  top-system-spacing = 0
  top-margin = 1
  bottom-margin = 1
  left-margin = 1
  right-margin = 1
}

music = \relative f' {
  \time 6/4
  \set Staff.extraNatural = ##f
  s8
  gqs4 
  gtqs
  gqf
  gtqf 
  % g4 
  \bar ""
}

fsize = -6
 
text = \lyricmode {
  \markup \with-color #grey \sans \fontsize #fsize { \center-column {
    \line { "G quarter-" }
    \line { "tone sharp" }
  }}
  \markup \with-color #grey \sans \fontsize #fsize { \center-column {
    \line {"G three-"}
    \line {"quarter-"}
    \line {"tone sharp"}
  }}
  \markup \with-color #grey \sans \fontsize #fsize { \center-column {
    \line { "G quarter-" }
    \line { "tone flat" }
  }}
  \markup \with-color #grey \sans \fontsize #fsize { \center-column {
    \line {"G three-"}
    \line {"quarter-"}
    \line {"tone flat"}
  }}
}

\score {
  <<
    \new Staff \with {
      % \cnQuarterToneSharpGlyph "accidentals.natural.arrowup"
      % \cnQuarterToneFlatGlyph "accidentals.flat.arrowup"
      \remove "Clef_engraver" 
      \remove "Time_signature_engraver"
    } {
      \new Voice = "two" { \music }
    }
    \new Lyrics \lyricsto "two" \text
  >>
  \layout {
    % ragged-right = ##t
    indent = #0
    % line-width = #80
    \context {
      \Score
      proportionalNotationDuration = #(ly:make-moment 1 16)
      % \override SpacingSpanner #'uniform-stretching = ##t
      % \override Score.SpacingSpanner #'strict-note-spacing = ##t
      \remove "System_start_delimiter_engraver"
      \override BarNumber.break-visibility = #all-invisible % #'#(#f #f #f)
      explicitKeySignatureVisibility = #end-of-line-invisible % #'#(#f #t #t)
      printKeyCancellation = ##f
      \override KeyCancellation.break-visibility = #all-invisible % #'#(#f #f #f)
    }
  }
  % \midi { }
}
