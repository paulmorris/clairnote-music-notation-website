\version "2.19.49"
\include "clairnote.ly"
\language "english"

#(set-global-staff-size 34)
\pointAndClickOff

\header{
  tagline = ""
}

\paper {
  page-breaking = #ly:one-page-breaking
  top-system-spacing = 0
  top-margin = 1
  bottom-margin = 1
  left-margin = 1
  right-margin = 1
  indent = 0
  score-markup-spacing = 0
}

chromaticScale = \relative f {
  \key d \major
  \cadenzaOn
  \hide Rest
  % c'8 cis d dis e f fis g gis a ais b c
  % c'8[ cis d dis] e[ f fis g] gis[ a ais b] c4
  d'4 ds e f fs g
  % \bar "|" r32
  d' cs c b bf a af
  % gs a as b c cs d
}

myFlat = \markup { \translate #(cons 0.1 0.25) \flat }
mySharp = \markup { \translate #(cons 0 0.55)  \sharp }

fsize = -6
spc = #3.5

text = \lyricmode {
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "D"}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  \center-column {\line {"D" \mySharp }}}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "E"}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "F"}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  \center-column {\line {"F" \mySharp } }}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "G"}

  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "D"}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  \center-column {\line {"C" \mySharp } }}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "C"}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "B"}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  \center-column {\line {"B" \myFlat } }}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "A"}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  \center-column {\line {"A" \myFlat } }}


  %{
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  \center-column {\line {"G" \mySharp } }}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "A"}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  \center-column {\line {"A" \mySharp } }}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "B"}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "C"}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  \center-column {\line {"C" \mySharp } }}
  \markup \with-color #grey \sans \fontsize #fsize { \lower #spc  "D"}
  %}
}



\score {
  <<
    \new TradStaff \with {
      \remove "Time_signature_engraver"
      \remove "Clef_engraver"
    } {
      \new Voice = "one" {
        \override Score.SpacingSpanner.common-shortest-duration = #(ly:make-moment 1 24 )
        \override TextScript #'extra-offset = #'(0 . 0)
        s4^\markup \with-color #grey { \fontsize #-6 \sans "Key of D Major, Chromatic Notes"}
        \chromaticScale
      }
    }
  >>
}

\markup \vspace #0.6

\score {
  <<
    \new Staff
    \with {
      \remove "Time_signature_engraver"
      \remove "Clef_engraver"
      % \remove "Bar_engraver"
      % \cnNoteheadStyle "funksol"
    }{
      \new Voice = "one" {
        \override Score.SpacingSpanner.common-shortest-duration = #(ly:make-moment 1 24 )
        \override TextScript #'extra-offset = #'(0 . 0)
        % s4^\markup \with-color #grey { \fontsize #-6 \sans "D Major Key, Chromatic Scale"}
        \chromaticScale
      }
    }
    \new Lyrics \lyricsto "one" \text
  >>
}