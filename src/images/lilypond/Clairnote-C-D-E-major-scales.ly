\version "2.19.32"
\include "clairnote.ly"

#(set-global-staff-size 34)
\pointAndClickOff

\header{
  tagline = ""
}

\paper {
  page-breaking = #ly:one-line-auto-height-breaking
  top-system-spacing = 0
  top-margin = 6
  bottom-margin = 1
  left-margin = 1
  right-margin = 1
}

%{
wholeToneScales = {
  \relative f {
    \partcombineChords
    \cadenzaOn
    % s16
    c'4 d e \stemUp fis \stemUp gis \stemUp ais
    cis, dis \stemUp f \stemUp g \stemUp a \stemUp b
    % s16
  }
}

cMajorScale = {
  \relative f {
    \partcombineChords
    \cadenzaOn
    % s16
    c'8 d e \stemUp f \stemUp g \stemUp a b c
    % s16
  }
}

cMajorOctaves = {
  \relative f {
    \cadenzaOn
    % s16
    <c' c'>8[ <d d'>] <e e'>[ <f f'>]  <g g'>[  <a a'>] <b b'>[ <c c'>]
    % s16
  }
}
%}

% myFlat = \markup { \translate #(cons 0.1 0.25) \flat }
% mySharp = \markup { \translate #(cons 0 0.55)  \sharp }

fsize = #-6
spc = #5
clr = #grey

scales = {
  \relative f {
    \cadenzaOn
    % s16
    c'4 _ \markup \with-color #clr \sans \fontsize #fsize { \lower #spc "C Major Scale"}
    d e f g a b c

    d, _ \markup \with-color #clr \sans \fontsize #fsize { \lower #spc "D Major Scale"}
    e fis g a b cis d

    e, _ \markup \with-color #clr \sans \fontsize #fsize { \lower #spc "E Major Scale"}
    fis gis a b cis dis e

    %{
    c, _ \markup \with-color #clr \sans \fontsize #fsize { \lower #spc "C Minor Scale"}
    d ees f g aes bes c

    d, _ \markup \with-color #clr \sans \fontsize #fsize { \lower #spc "D Minor Scale"}
    e f g a bes c d
    %}
    % a, b c d e f g a

    s16
  }
}

\score {
  <<
    \new Staff
    \with {
      \remove "Accidental_engraver"
      \remove "Key_engraver"
      \remove "Time_signature_engraver"
      \remove "Clef_engraver"
      \remove "Bar_engraver"
    }{
      \new Voice = "one" {
        \override Score.SpacingSpanner.common-shortest-duration = #(ly:make-moment 1 4 )
        % \override TextScript #'extra-offset = #'(0 . 2)
        s4^\markup \with-color #clr { \fontsize #-6 \sans {}}
        \scales
      }
    }
  >>
}
