\version "2.24.0"
\include "clairnote.ly"
\language "english"

#(set-global-staff-size 34)
\pointAndClickOff

\header{
  tagline = ""
}

\paper {
  page-breaking = #ly:one-line-auto-height-breaking
  top-system-spacing = 0
  top-margin = 0.5
  bottom-margin = 0.5
  left-margin = 1
  right-margin = 1
}

% global modifications
\layout {
  \context {
    \Score
    \override SpacingSpanner.common-shortest-duration = #(ly:make-moment 1 12 )
    % \override TextScript.extra-offset = #'(0 . 0)
  }
  \context {
    \Staff
    \remove "Accidental_engraver"
    \remove "Key_engraver"
    \remove "Time_signature_engraver"
    \remove "Clef_engraver"
    \remove "Bar_engraver"
    % \override Stem.direction = 1
  }
}

expand =
#(define-music-function (parser location mus label)
   (ly:music? string?)
   #{
     s4^\markup { \with-color #grey  \fontsize #-6 \sans { #label }}
     $mus
     \transpose c d $mus
     \transpose c e $mus
     \transpose c fs $mus
     \transpose c gs $mus
     \transpose c as $mus
     \transpose c cs $mus
     \transpose c ds $mus
     \transpose c f $mus
     \transpose c g $mus
     \transpose c a $mus
     \transpose c b $mus
   #})

\expand c' "Unisons (0)"

