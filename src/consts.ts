// Place any global data in this file.

import type { SnOrDnProps } from "./types/types";

export const SITE_TITLE = "Clairnote Music Notation";

export const SITE_DESCRIPTION =
  "An alternative music notation system that makes music easier to read and understand.";

export const dnOnlyPages = [
  "/notetrainer-application/",
  "/handwritten-sheet-music/",
  "/software-musescore/",
];

export const snProps: SnOrDnProps = Object.freeze({
  sn: true,
  dn: false,
  clairnoteName: "Clairnote SN",
  otherClairnoteName: "Clairnote DN",
  urlDir: "/",
  otherUrlDir: "/dn/",
  lineOrSolid: "line",
  spaceOrHollow: "space",
});

export const dnProps: SnOrDnProps = Object.freeze({
  sn: false,
  dn: true,
  clairnoteName: "Clairnote DN",
  otherClairnoteName: "Clairnote SN",
  urlDir: "/dn/",
  otherUrlDir: "/",
  lineOrSolid: "solid",
  spaceOrHollow: "hollow",
});
