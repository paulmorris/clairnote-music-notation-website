---
title: "Now Accepting All Clefs"
link: https://clairnote.org/blog/2014/05/now-accepting-clefs/
author: paulmorris
special_byline: ""
description:
post_id: 2529
date: 2014/05/20
created_gmt: 2014/05/20
comment_status: closed
slug: now-accepting-clefs
draft: false
post_type: post
tags: ["LilyPond"]
---

This is another post in a [series](/blog/2014/04/repeat-signs/) of [posts](/blog/2014/05/staff-new-staffclairnote/) about recent improvements to the <del>clairnote-code.ly</del> clairnote.ly file. This one is about clef improvements, namely better support for additional clefs, octave clefs / transposed clefs, and clefs on traditional staves.

**Additional Clefs**

Previously the only clefs that were supported by the clairnote.ly file were the more common clefs: treble, bass, and alto (and their aliases the G clef, F clef, and C clef).  Now all the clefs that LilyPond supports are handled correctly.  (Well, not [mensural clefs](http://lilypond.org/doc/v2.16/Documentation/notation/typesetting-mensural-music#mensural-clefs).  LilyPond supports a lot of clefs!)  This includes obscure clefs you may never have heard of such as: tenor, french, soprano, mezzosoprano, baritone, varbaritone, subbass, and percussion.

But why support all these different clefs?  Do they even make sense in Clairnote?  Not really, because in Clairnote clef signs only indicate the octave range of the staff and do not change the notes of the lines and spaces like traditional clefs do.  So there's no need for that many clefs.  (See [Clefs](/dn/clefs/) page.)  What happens is that the clairnote.ly file effectively converts each of these clefs into either a treble, bass, or alto clef, depending on which one is closest in terms of its range of notes on the staff.  (Technically, the settings for each of these clefs is redefined so that they are identical to the settings of either the treble, bass, or alto clefs.)

Here's an image showing all of these clefs in traditional notation, and their corresponding clefs in Clairnote:

![Illustration of all clefs in traditional and Clairnote music notation](/images/Clairnote-all-clefs.svg)

But why wouldn't you just enter a treble, bass, or alto clef to start with, if that's what you end up with on the Clairnote staff?  Generally that's exactly what you would do, but what if you are converting an existing LilyPond file that uses one of these other clefs?  Now you don't have to do anything special to that LilyPond file.   You can just leave it as it is, with the music in any clef, as entered for traditional notation, and it will be correctly converted to Clairnote automatically.

**Octave Clefs / Transposed Clefs**

What about [octave clefs](https://en.wikipedia.org/wiki/Clef#Octave_clefs) that are transposed up or down one or more octaves?  These are now handled correctly too, so you can convert an existing LilyPond file without having to worry about them.  The amount of the transposition is converted from seven notes per octave to twelve notes per octave so that it works correctly on the Clairnote staff.  So if a file contains `\clef "treble^8"` (for a treble clef transposed up an octave) it is effectively converted to `\clef "treble^13"`. Following the conventions of traditional notation, a little "13"  or "25" will appear above or below the clef indicating how it has been transposed.

If you would like to leave traditional notation behind completely you can just use "13" and "25" directly (for example: `\clef "treble^13"` or `\clef "bass_25"`).  These two numbers are treated as exceptions and are simply "passed through" without being converted. (Note that this will prevent the music from being output correctly to traditional notation.)

Here is an image illustrating some octave clefs (transposed clefs) in traditional notation and in Clairnote:

![Transposed clefs in traditional music notation and Clairnote music notation](/images/Clairnote-clefs-transposed.svg)

**Clefs on Traditional Staves**

One of the biggest improvements is in the way that clefs are handled in files that produce both Clairnote staves and traditional staves.  Previously you had to have two copies of the music stored in two variables in the file, one for the Clairnote staff and one for the traditional staff.  The Clairnote copy would come first, then the command `#(set-traditional-clefs)`, and then the traditional copy of the music.  The `#(set-traditional-clefs)` command resets the clef settings to their traditional values so that the music that follows it will have the correct clef settings for the traditional staff.  Needless to say, this was not very convenient.

Now you only need one copy of the music, stored in a single variable, that gets displayed on both kinds of staff.  Just use the following for the traditional staff that tells LilyPond to use the traditional clef settings, and the notes will appear in the correct position on the staff:

    \new StaffTrad \clefsTrad {
      % music goes here...
    }

“\clefsTrad” is a LilyPond music function that processes the music between the curly brackets that follow it, changing the clef settings in the music from the Clairnote defaults back to the traditional settings.  This new approach follows the principle of [separation of presentation and content](https://en.wikipedia.org/wiki/Separation_of_presentation_and_content) and is much more convenient.  [Cue clefs](https://en.wikipedia.org/wiki/Musical_cue) and octave clefs (transposed clefs) are also converted to traditional settings and work as expected.

[September 17, 2015 – Update: `\clefsTrad` is no longer needed. See [this post](/blog/2015/09/8va-8vb-and-clefs/).]

There is one caveat.  Basically only treble, bass, and alto clefs are supported on a `StaffTrad` staff. The clairnote.ly file changes the clef settings for use with Clairnote staves. This means effectively converting all clefs to treble, bass, or alto clefs. So if a clef was not one of these three clefs to start with, it will also be converted to one of these clefs on the traditional staff.  (There is technically no way to know what clef it was to start with, at the point where the settings are converted back to traditional settings.)  The music will not be unplayable; it will just be in a different clef than the input indicated. The old `#(set-traditional-clefs)` command is still available and offers a way around this limitation in rare cases where it is needed.

All of these improvements to how clefs are handled are included in the most recent version of the clairnote.ly file that is always available from the [Software](/dn/software/) page.  These improvements focus on improving support for clef input and leave clef output relatively untouched.  I am still considering introducing some kind of novel clef symbols that would help prevent any confusion between the way clefs function in Clairnote (just indicating a staff's octave register) and in traditional notation (also changing the positions of notes on the staff, see [Clefs](/dn/clefs/)).

Update: the "clairnote-code.ly" file was renamed "clairnote.ly" on May 15, 2017.
