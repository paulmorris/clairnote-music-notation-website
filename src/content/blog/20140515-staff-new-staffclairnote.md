---
title: "Staff is the New StaffClairnote"
link: https://clairnote.org/blog/2014/05/staff-new-staffclairnote/
author: paulmorris
special_byline: ""
description:
post_id: 2499
date: 2014/05/15
created_gmt: 2014/05/15
comment_status: closed
slug: staff-new-staffclairnote
draft: false
post_type: post
tags: ["LilyPond"]
---

One of the more significant recent changes to the <del>clairnote-code.ly</del> clairnote.ly file is that now when you `\include "clairnote.ly"` in a LilyPond file all staves will automatically become Clairnote staves by default. You no longer have to enter `\new StaffClairnote` instead of `\new Staff` to get a Clairnote staff. It also means that you don't have to explicitly enter a staff command at all. If you just enter `{ c4 d e f }` the notes will automatically appear on a Clairnote staff.

This approach is a lot more convenient and will simplify the process of converting existing files since it only requires one simple step. It also just makes sense because the only reason to include the clairnote.ly file is because you want Clairnote output. I would have set it up this way from the start had I known how to do it at the time.

(And if we needed yet another reason for doing it this way... including the clairnote.ly file already changes the clef settings for use with Clairnote staves which makes them incompatible with traditional staves.  So previously if you just included that file the default output was left somewhere in between the two kinds of staff, unless you took the additional step of explicitly specifying that you wanted a Clairnote staff or changing the clef settings back to traditional settings.  Now things are much simpler and more straightforward.)

If you want to have traditional staves in the same file with Clairnote staves, say for comparison, you would now use `\new StaffTrad` which produces a traditional staff.  Here's a summary of the changes:

\new StaffClairnote  =>  \new Staff  
\new Staff  =>  \new StaffTrad

In other words Staff is the new StaffClairnote, and StaffTrad is the artist formerly known as Staff.  This is all done with LilyPond and custom staff contexts.  See this LSR snippet on [Defining a Custom Staff Context](http://lsr.di.unimi.it/LSR/Item?id=882).

The [Software: LilyPond](/dn/software/) page has been revised to reflect these changes, and you can download the latest clairnote.ly file from that page.  There's more to come about "under-the-hood" improvements to the clef code.

Update: the "clairnote-code.ly" file was renamed "clairnote.ly" on May 15, 2017.
