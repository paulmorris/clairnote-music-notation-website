---
title: "A Sight-Singing Anthology in Clairnote"
link: https://clairnote.org/blog/2017/08/sight-singing-anthology-clairnote/
author: paulmorris
special_byline: ""
description:
post_id: 3601
date: 2017/08/16
created_gmt: 2017/08/16
comment_status: closed
slug: sight-singing-anthology-clairnote
draft: false
post_type: post
tags: ["Sheet Music"]
---

Recently, through the LilyPond user listserv, I was pleased to discover _Eyes and Ears_, an anthology of melodies for practicing sight-singing that is available for free download, under a Creative Commons license, with source files in LilyPond format.  With some work I was able to convert the book into Clairnote music notation, and you can now download it from the [More Sheet Music page](/dn/sheet-music/).

## _Eyes and Ears_: an Anthology for Practicing Sight-Singing

While it's always good to have more sheet music available in Clairnote, it's especially nice to have resources for learning to read and make music.  For pianists, we already have the [Introduction to the Art of Playing on the Piano Forte by Muzio Clementi](/blog/2016/12/muzio-clementis-introduction-art-piano-forte/), a lesson book that I converted earlier this year, and now we have something to offer singers too.

The _[Eyes and Ears](http://www.lightandmatter.com/sight/sight.html)_ anthology by Benjamin Crowell is basically a sight-singing textbook, weighing in at over 100 pages and containing over 400 melodies, organized into thematic chapters and sections (e.g. Rhythm, Minor Keys, The Leap of a Fifth Within the Tonic Triad, etc.), and progressing from easier to more difficult material.  It was originally published in 2004 and has been used at a number of colleges and universities. The melodies come from a wide range of cultures and styles, from classical composers to popular and folk sources.  What a wonderful resource to have freely available!  Many thanks to Ben Crowell for his work putting it together and for publishing it under a Creative Commons license.

Having this anthology available in Clairnote provides wonderful material for singers to try out the notation system.  Some have argued that a notation system like Clairnote, that represents twelve notes per octave on the staff, is more suited for playing instruments like a guitar or piano, which also clearly present twelve notes per octave, than for singing.  The idea is that singers have diatonic scales 'in their heads' and not a chromatic scale and so a staff that represents a diatonic scale is more appropriate or intuitive for singing than Clairnote's chromatic staff.

On the other hand, it should be helpful for singers to be able to see and become more aware of intervals such as half steps and whole steps, major and minor thirds, etc. (and even more so with music that involves accidentals or changes keys).  These basic melodic and harmonic elements of music (the differences between intervals) are more readily apparent on the Clairnote staff than the traditional staff.

For example, Crowell recommends identifying "do," the tonic note, as a first-step when sight singing a melody.  The patterns of solid and hollow notes on the Clairnote staff make this easier to do, as they reveal the interval pattern of the current key or scale. For a melody in a major key, you could look for the half steps which would lead you to two possibilities for the tonic note. The tonic would be the higher note in one of those half steps (assuming no accidentals). A similar approach would work for minor keys. (And, well, Clairnote's key signatures also directly indicate the tonic note of the key, unlike traditional key signatures.)

Additionally, no one thinks twice about singing pentatonic music (5 notes per octave) on a diatonic staff (7 notes per octave), so why should there be a problem singing diatonic music (7 notes per octave) on a chromatic staff (12 notes per octave)?  In both cases some of the notes on the staff are not used in the music, but the notes that are part of the music remain clear and the relationships between them become even clearer.

At any rate, having an anthology of melodies expressly for practicing sight-singing makes it possible to explore these questions more empirically through actual experience reading music.

## Conversion to Clairnote

Converting sheet music to Clairnote is usually a simple matter of inserting `\include "clairnote.ly"` into a LilyPond file, but it was more involved for this sizeable collection in book form with two tables of contents and two indexes.  The original was produced using a number of software tools in addition to LilyPond, including LaTeX, Perl, flex, bison, and make files.  I had some moderate success getting the original source code to produce a Clairnote version.  The music was converted and displayed just fine but ultimately there were a number of other issues that I wasn't able to resolve (spacing and otherwise), since I was unfamiliar with the software involved.

It seemed that the quickest and simplest way forward was to re-implement it using only LilyPond, without the other dependencies.  I wrote a Python script to modify the LilyPond source files so their meta-data and musical content were made available in LilyPond/Scheme variables.  Then I wrote another script to extract the rest of the content from the main .tex file (e.g. chapter and section headings, the order of the melodies, textual content, etc.) and produce a LilyPond file from it.  (Hooray for regular expressions!)

Much of the work was fairly tedious and detail-oriented, like fixing the accent marks in the table of contents and index, or displaying sharp and flat signs for the keys listed in the table of contents.  A more interesting technical challenge was implementing the index (by title and composer) and the second "brief" table of contents.  Fortunately, LilyPond has Scheme code for automatically creating a table of contents that I was able to copy and modify to do the job (sorting the entries alphabetically for the index).

## Rhythmic Staves

An issue I had to solve was that the first chapter on rhythm uses one-line [rhythmic staves](http://lilypond.org/doc/v2.18/Documentation/notation/displaying-rhythms#showing-melody-rhythms) to show only the rhythm of a melody.  Fortunately it was fairly straightforward to add support for this kind of staff to the [clairnote.ly](/dn/software/) file. By adapting the existing Clairnote code it was easy to customize  the `RhythmicStaff` context (and add a `TradRhythmicStaff` context for creating traditional rhythmic staves).  Now it is easy to produce one-line Clairnote rhythmic staves (e.g. with [double stems to indicate half notes](/dn/rhythm-notation/)). For example, this LilyPond file:

    \version "2.19.63"
    \include "clairnote.ly"
    \new RhythmicStaff {
      c4. 8 2
      8. 16 8. 16 4 r4
      c1
    }

Will produce this output:

![Clairnote rhythm staff example](/images/Clairnote-rhythm-staff-example.svg)

As compared with the same rhythm on a traditional rhythmic staff (without the `\include "clairnote.ly"` line):

![Traditional rhythm staff example](/images/Clairnote-rhythm-staff-example-trad.svg)

## Differences

As described on its copyright page, the Clairnote version differs from the original in some respects.  For example, one has to consult the table of contents to identify a melody and its composer rather than having footnotes on each page.  (LilyPond can do footnotes, and I gave it a try but they took up a lot of space, being stacked vertically, whereas in the original they are arranged horizontally, in-line, which is not supported by LilyPond's footnotes.)  On the more positive side, the index entries include the number of the melody in addition to the page number, and in the PDF file the index and table of contents entries are clickable links that take you to a particular melody.

All in all I think the Clairnote version of _Eyes and Ears_ is worthy of the original. For the curious, the source code is available in a [repository](https://github.com/PaulMorris/eyes-and-ears-clairnote) on Github. Thanks again to Benjamin Crowell for his excellent anthology, and I hope my readers will enjoy practicing sight-reading with these melodies in Clairnote!
