---
title: "Repeat Signs"
link: https://clairnote.org/blog/2014/04/repeat-signs/
author: paulmorris
special_byline: ""
description:
post_id: 2429
date: 2014/04/30
created_gmt: 2014/04/30
comment_status: closed
slug: repeat-signs
draft: false
post_type: post
tags: ["LilyPond"]
---

The first image below shows how LilyPond positions the two dots in repeat signs (by default) for a custom four-line staff like Clairnote's. They look a little like bolts attaching the bar lines to the staff.  That's no discredit to LilyPond since this is a non-standard staff after all. But fortunately, we can now enjoy better positioning of these dots (as shown in the second image), thanks to recent improvements to the <del>clairnote-code.ly</del> clairnote.ly file that allows LilyPond to create sheet music in Clairnote music notation. Repeat signs now look much more like they do in traditional notation (the third image), making things easier for anyone learning both systems or switching from one to the other.

![repeat-dots-Clairnote-old](/images/repeat-dots-Clairnote-old.svg)
![repeat-dots-Clairnote-new](/images/repeat-dots-Clairnote-new.svg)
![repeat-dots-traditional](/images/repeat-dots-traditional.svg)

You can see these new repeat dots in the examples of [sheet music](/dn/sheet-music/) in Clairnote.  The most recent clairnote.ly file is available (as always) from the [Software: LilyPond](/dn/software/) page. This is yet another example of how almost anything can be tweaked and customized with LilyPond, given enough patience.  Thanks to Jan Braunstein and his attention to detail for initiating the work on this.  (It has been sitting on the back burner for awhile). He was interested in it for his [Chromatic Lyre Notation](http://www.delos.cz/en/Chromatic_Notation.html), an alternative notation system that is similar to Clairnote in many respects.

While working on the positioning of dots in repeat signs it looked like I might need to copy some code from LilyPond, which is free software licensed under the [GNU General Public License (GPL)](http://www.gnu.org/licenses/gpl.html).  In the end I didn't need to use the code from LilyPond after all, but I have gone ahead and licensed the clairnote.ly file under the GPL so it falls under the same license as LilyPond.

The repeat signs are only one of several recent improvements that I have made to the clairnote-code.ly file.  Some of them are "under-the-hood" changes (such as using closures rather than global variables) as I continue to learn more about programming in Scheme.  Others I will cover in future posts.

Update: the "clairnote-code.ly" file was renamed "clairnote.ly" on May 15, 2017.
