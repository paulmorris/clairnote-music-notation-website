---
title: "Sheet Music: 84 New Works, 4 Christmas Carols"
link: https://clairnote.org/blog/2015/12/sheet-music-84-new-works-4-christmas-carols/
author: paulmorris
special_byline: ""
description:
post_id: 3233
date: 2015/12/12
created_gmt: 2015/12/12
comment_status: closed
slug: sheet-music-84-new-works-4-christmas-carols
draft: false
post_type: post
tags: ["Sheet Music"]
---

84 new pieces of sheet music in Clairnote music notation are now available for download from the Clairnote website, bringing the total we offer to over 500.  Four of these are Christmas carols, just in time for the holiday season.

I'm pleased to report that 11 of these new additions were contributed by Willem Feltkamp who uses Clairnote.  These can be found on the [More Sheet Music](/dn/sheet-music/) page.  Many thanks to him for helping to make more music available in Clairnote!

There are four Christmas carols, great for singing harmonies over the holiday season:

- Joy to the World
- Deck the Halls
- Stille Nacht / Silent Night
- We Wish You a Merry Christmas

And seven more traditional works:

- Apples in Winter
- The Cobbler
- Golden Keyboard
- The Hag’s Purse
- The Irish Washerwoman
- Last Lesson
- Morrison’s Jig for Harp

Beyond these 11 directly contributed works, there are 73 new additions to the [Clairnote Sheet Music Library](/dn/sheet-music/), bringing the total available there up to 535.  These 73 additions are listed at the end of this post.  They have been automatically converted to Clairnote from the traditional [LilyPond](/dn/software/) files in the [Mutopia Project](http://www.mutopiaproject.org/)'s collection.  As with all the other works in the library they are either (1) new additions to the Mutopia Project or (2) older files that have been upgraded to the most recent version of LilyPond, since the last conversion [back in June](/blog/2015/06/sheet-music-library-71-new-works/).  The process for doing these automatic conversions keeps improving incrementally, and the next one should be 3-4 months from now.

Eventually I would like to implement a more flexible system that would bring all of this sheet music into the [Sheet Music Library](/dn/sheet-music/) so it could be accessed in one place. For now, given time constraints and the way the library is set up to automatically track the Mutopia Project, the directly contributed works will remain available on the [More Sheet Music](/dn/sheet-music/) page.

On a final note, Clairnote was introduced just over two years ago, and it has been a good first two years!  I hope to publish a post soon that looks back over the past year and discusses some of the recent improvements to the <del>`clairnote-code.ly`</del> `clairnote.ly` file.

Here is the list of the 73 new additions to the library, including lots of works for guitar and piano:

D. Aguado (1784–1849) | 8 Petite Pièces for Guitar, No. 1 | Guitar

D. Aguado (1784–1849) | 8 Petite Pièces for Guitar, No. 2 | Guitar

D. Aguado (1784–1849) | 8 Petite Pièces for Guitar, No. 3 | Guitar

D. Aguado (1784–1849) | 8 Petite Pièces for Guitar, No. 4 | Guitar

D. Aguado (1784–1849) | 8 Petite Pièces for Guitar, No. 5 | Guitar

D. Aguado (1784–1849) | 8 Petite Pièces for Guitar, No. 6 | Guitar

D. Aguado (1784–1849) | 8 Petite Pièces for Guitar, No. 7 | Guitar

D. Aguado (1784–1849) | 8 Petite Pièces for Guitar, No. 8 | Guitar

T. Arne (1710–1788) | Hymn of Eve (Uxbridge) | Voice (SATB)

J. S. Bach (1685–1750) | Das Wohltemperierte Clavier I, Praeludium XX | Harpsichord, Piano

J. S. Bach (1685–1750) | Das Wohltemperierte Clavier I, Praeludium 24 | Harpsichord, Piano

A. Banchieri (c.1567–1634) | SUONATA PRIMA, FVGA PLAGALE From Primo Registro of the Organo Suonarino | Organ

A. Banchieri (c.1567–1634) | SUONATA SECONDA, FVGA TRIPLICATA From Primo Registro of the Organo Suonarino | Organ

A. Banchieri (c.1567–1634) | SUONATA TERZA, FVGA GRAVE From Primo Registro of the Organo Suonarino | Organ

A. Banchieri (c.1567–1634) | SUONATA QUARTA, FVGA CROMATICA From Primo Registro of the Organo Suonarino | Organ

A. Banchieri (c.1567–1634) | SUONATA QUINTA, FVGA HARMONICA From Primo Registro of the Organo Suonarino | Organ

A. Banchieri (c.1567–1634) | SUONATA SESTA, FVGA TRIPLICATA From Primo Registro of the Organo Suonarino | Organ

L. V. Beethoven (1770–1827) | Overture to Egmont | Orchestra: Flutes, Oboe, Clarinet, Bassoon, Horns, Trumpets, Timpani, Violins, Viola, ‘Cello, Bass

C. J. Brown (1947–) | Wedding Music | Trumpet and organ, or flute and piano

A. Bruckner (1824–1896) | Perger Präludium | Organ

D. Buxtehude (c.1637–1707) | Toccata | Organ

M. Carcassi (1792–1853) | Etude 1 | Guitar

M. Carcassi (1792–1853) | Etude 3 | Guitar

M. Carcassi (1792–1853) | Etude 15 | Guitar

C. Czerny (1791–1857) | 160 Eight-Measure Exercises, No. 1 | Piano

C. Czerny (1791–1857) | 160 Eight-Measure Exercises, No. 2 | Piano

C. Czerny (1791–1857) | 160 Eight-Measure Exercises, No. 3 | Piano

C. Czerny (1791–1857) | 160 Eight-Measure Exercises, No. 5 | Piano

C. Czerny (1791–1857) | 160 Eight-Measure Exercises, No. 6 | Piano

C. Czerny (1791–1857) | 160 Eight-Measure Exercises, No. 7 | Piano

C. Czerny (1791–1857) | 160 Eight-Measure Exercises, No. 8 | Piano

C. Czerny (1791–1857) | 160 Eight-Measure Exercises, No. 9 | Piano

C. Czerny (1791–1857) | 160 Eight-Measure Exercises, No. 10 | Piano

C. Czerny (1791–1857) | 160 Eight-Measure Exercises, No. 11 | Piano

C. Czerny (1791–1857) | 160 Eight-Measure Exercises, No. 12 | Piano

C. Czerny (1791–1857) | 160 Eight-Measure Exercises, No. 13 | Piano

C. Czerny (1791–1857) | 160 Eight-Measure Exercises, No. 14 | Piano

C. Czerny (1791–1857) | 160 Eight-Measure Exercises, No. 15 | Piano

C. Czerny (1791–1857) | 160 Eight-Measure Exercises, No. 16 | Piano

C. Czerny (1791–1857) | 160 Eight-Measure Exercises, No. 17 | Piano

C. Czerny (1791–1857) | 160 Eight-Measure Exercises, No. 19 | Piano

H. Eccles (1670–1742) | Sonata in G minor | Violin

G. Fauré (1845–1924) | Les Djinns | Voice and Piano

M. Giuliani (1781–1829) | 16 Studies for the Guitar: No. 9 | Guitar

M. Giuliani (1781–1829) | 16 Studies for the Guitar: No. 10 | Guitar

M. Giuliani (1781–1829) | 16 Studies for the Guitar: No. 11 | Guitar

M. Giuliani (1781–1829) | 16 Studies for the Guitar: No. 12 | Guitar

M. Giuliani (1781–1829) | 16 Studies for the Guitar: No. 13 | Guitar

M. Giuliani (1781–1829) | 16 Studies for the Guitar: No. 14 | Guitar

M. Giuliani (1781–1829) | 16 Studies for the Guitar: No. 15 | Guitar

M. Giuliani (1781–1829) | 16 Studies for the Guitar: No. 16 | Guitar

C. L. Hanon (1819-1900) | The Virtuoso Pianist (Part I) | Piano

W. A. Mozart (1756–1791) | Sonate Opus KV 331 – Rondo Alla Turca | Piano

W. A. Mozart (1756–1791) | Fantasy in D minor | Piano

W. A. Mozart (1756–1791) | Ein Musikalischer Spass (A Musical Joke) Mv 1 | Orchestra: Horns, Violin, Viola, ‘Cello

J. Pachelbel (1653–1706) | Canon per 3 Violini e Basso | 3 Violins, Cello

S. Rachmaninoff (1873-1943) | Prelude Op. 23, No. 8 | Piano

E. Satie (1866–1925) | Celle qui parle trop | Piano

E. Satie (1866–1925) | Danse maigre | Piano

E. Satie (1866–1925) | Españaña | Piano

E. Satie (1866–1925) | Tyrolienne turque | Piano

E. Satie (1866–1925) | Gnossienne No. 1 | Piano

E. Satie (1866–1925) | Sa Taille | Piano

E. Satie (1866–1925) | Ses Jambes | Piano

E. Satie (1866–1925) | Son Binocle | Piano

F. Schubert (1797–1828) | Impromptu in Ges-dur | Piano

F. Sor (1778–1839) | Divertissements pour la guitare, n°1 Andante | Guitar

F. Sor (1778–1839) | Divertissements pour la guitare, n°2 Waltz | Guitar

F. Tarrega (1852–1909) | Claro de Luna (Beethoven’s Moonlight Sonata) | Guitar

P. I. Tchaikovsky (1840–1893) | Morning Prayer | Piano

P. I. Tchaikovsky (1840–1893) | Old French Song | Piano

J. Titelouze (1563–1633) | Ad Cœnam (2e verset) | Organ

H. Worrall (1825–1902) | Spanish Fandango | Guitar

Update: the “clairnote-code.ly” file was renamed “clairnote.ly” on May 15, 2017.
