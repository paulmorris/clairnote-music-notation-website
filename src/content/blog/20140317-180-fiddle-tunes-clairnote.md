---
title: "180 Fiddle Tunes in Clairnote"
link: https://clairnote.org/blog/2014/03/180-fiddle-tunes-clairnote/
author: paulmorris
special_byline: ""
description:
post_id: 2404
date: 2014/03/17
created_gmt: 2014/03/17
comment_status: closed
slug: 180-fiddle-tunes-clairnote
draft: false
post_type: post
tags: ["Sheet Music"]
---

Saint Patrick's day is a lucky day... to announce a new collection of fiddle tunes in Clairnote, many of them of Irish derivation.  So raise your glass and rosin up your bow because this collection contains not 10, not 50, but 180 tunes (you read that right!) in Clairnote music notation.  And all 85 pages of music are available in one PDF file for free on the [Sheet Music](/dn/sheet-music/) page.

![Cover page of "180 Fiddle Tunes in Clairnote Music Notation"](/images/180-fiddle-tunes-in-Clairnote-cover-page.png)

Most of the tunes are jigs and reels of the American (USA), English, French-Canadian, and Irish folk traditions that would be appropriate for contra dancing or square dancing.  There are also a few marches, rags, and waltzes.  Some of the tunes are more contemporary in their authorship. They were originally assembled and engraved with LilyPond in the aughts of the 21st century for [Your Friends and Neighbors](http://canaaninstitute.org/mn/mus_sound.html), an open band / folk music collective in Ithaca, New York.  I recently helped them out by updating the whole collection from LilyPond 1.8 to 1.18.  This upgrade improved the quality of the music engraving and made it easier to produce single PDF files of the whole collection in different versions: treble, bass, and alto clefs, and transposed for B-flat and F instruments.  It also made it possible to easily convert the collection to Clairnote notation.

If nothing else, by sheer volume alone this is the most significant addition to the sheet music available in Clairnote.  So check it out and enjoy the music!
