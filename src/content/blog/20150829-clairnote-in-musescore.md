---
title: "Clairnote in MuseScore"
link: https://clairnote.org/blog/2015/08/clairnote-in-musescore/
author: paulmorris
special_byline: ""
description:
post_id: 3121
date: 2015/08/29
created_gmt: 2015/08/29
comment_status: closed
slug: clairnote-in-musescore
draft: false
post_type: post
tags: ["MuseScore"]
---

Clairnote in MuseScore? Too good to be true! And yet, as amazing as it seems, this post is to announce and acknowledge an effort to add support for Clairnote to MuseScore.  This project was undertaken by Craig Fisher and has actually been underway for over a year now.  Many thanks to him for all his great work on this!

The plan is to provide features that can support a range of alternative music notation systems, but Clairnote has become the central focus, the "flagship" system if you will.  While there is still more to do and the new features have not yet been added to the _official_ MuseScore release, the basic elements of Clairnote are now implemented.  Check out the new [Software: MuseScore](/software-musescore/) page for more details.

![musescore-clairnote-screenshot](/images/musescore-clairnote-screenshot.png)

![musescore-logo-whitebg-l](/images/musescore-logo-whitebg-l-300x69.png)

[MuseScore](https://musescore.org/) is a well-liked free and open source application for creating and editing sheet music.  While [LilyPond](/dn/software/) produces great results, no one would argue that it is the most user friendly software around.  So eventually having full support for Clairnote in MuseScore with its familiar GUI (graphical user interface) will be a big step forward.

