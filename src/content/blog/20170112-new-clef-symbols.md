---
title: "New Clef Symbols"
link: https://clairnote.org/blog/2017/01/new-clef-symbols/
author: paulmorris
special_byline: ""
description:
post_id: 3440
date: 2017/01/12
created_gmt: 2017/01/12
comment_status: closed
slug: new-clef-symbols
draft: false
post_type: post
tags: ["Music Notation", "LilyPond"]
---

In December I (finally) designed Clairnote's custom clef symbols and wrote the code to render them with LilyPond.  Take a look!

![Clef symbols in Clairnote and traditional music notation systems](/images/Clairnote-clefs.svg)

Notice that Clairnote's clefs resemble the traditional clef symbols and represent similar octave ranges.  The numbers explicitly indicate the octave register of the staff.  They follow the conventional octave numbering system (where middle C is the lowest note of octave four, and so is known as C4).

Previously Clairnote just used traditional clef symbols.  However, this was potentially confusing because they did not mean the same thing or work in the same way as in traditional notation.  Namely, they did not affect the meaning of the staff lines and spaces (which notes they represent) but simply indicated the octave register of the staff.

Having custom clef symbols that are visually distinct from the traditional symbols avoids any confusion.  It is clear that they are different from traditional clefs and do not work in the same way.  On the other hand because they are visually similar they are easy to learn and understand for those familiar with traditional clefs.  The octave register that they represent is similar in both systems, providing a helpful parallel.

See the [Clefs](/dn/clefs/) page for more discussion and illustrations.  I have updated the images and files on this site to show the new clef symbols.  The only exception are some files in the Sheet Music Library which still need an update. Download the latest <del>clairnote-code.ly</del> clairnote.ly file from the [Software: Lilypond](/dn/software/) page to render the new clef symbols with LilyPond.

## Next up: ledger lines, double stems, website improvements

It's a new year, and quite a bit has been going on at Clairnote central in recent months, despite a lag in blogging.  Future posts will cover recent developments related to the website, ledger lines, and double stems for half notes. Update: the "clairnote-code.ly" file was renamed "clairnote.ly" on May 15, 2017.
