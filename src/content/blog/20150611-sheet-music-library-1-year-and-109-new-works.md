---
title: "Sheet Music Library: 1 Year and 109 New Works"
link: https://clairnote.org/blog/2015/06/sheet-music-library-1-year-and-109-new-works/
author: paulmorris
special_byline: ""
description:
post_id: 2956
date: 2015/06/11
created_gmt: 2015/06/11
comment_status: closed
slug: sheet-music-library-1-year-and-109-new-works
draft: false
post_type: post
tags: ["Sheet Music"]
---

One year ago today the Clairnote sheet music library [arrived](/blog/2014/06/clairnote-sheet-music-library/), providing Clairnote versions of works from the [Mutopia Project](http://www.mutopiaproject.org/).  Over the past year the library has seen a number of improvements, and recently it gained an additional 109 pieces of sheet music.  This brings the total number of works up to 389! (*)

First a quick look back at some of the improvements.  The redesign of the Clairnote website last fall brought a more seamless integration of the library with the rest of the site (e.g. bringing the global site navigation menus to the library).  The "filtering" user interface that started out in a sidebar now appears in an overlay that stays out of your way when you're not using it.  And [MIDI.js](https://github.com/mudcube/MIDI.js), the javascript library that powers the MIDI playback, got an upgrade that fixed some bugs on the Chrome browser.

While all of these improvements are nice, what is most exciting is the new sheet music.  The 109 new additions are either works that have been added to the Mutopia collection over the past year, or works that were already in the collection that have been upgraded to the latest version of LilyPond (which makes it possible to convert them into Clairnote).  (There is an [ongoing effort](https://github.com/MutopiaProject/MutopiaProject/milestones) to update the oldest files in the Mutopia collection to the most recent version of LilyPond.)

The new additions are listed at the end of this post. They include a significant number of works for guitar by Carcassi, Giuliani, and Sor.  Under the heading of "Traditional" there is also a whole collection of traditional Japanese popular music originally published in 1895.  (Original source given on the Mutopia website: Nagai, Iwai and Obata, Kenhachiro, "Seiyo gakufu Nihon zokkyokushu", pub. Miki Shoten, Osaka, 1895. English title, "A Collection of Japanese Popular Music.")

These additions to the library were accompanied by some major improvements to the bulk conversion process for getting the music into Clairnote and onto the Clairnote website.  This will make these updates easier to do in the future without effectively starting over each time.  So with luck the next update will happen a lot sooner than a year from now.  (I'm thinking 3-4 times per year would be a good pace.)

Please check out the new additions to the [Clairnote Sheet Music Library](/dn/sheet-music/)! Here is a list of them, sorted by composer:



F. Abt (1819–1885) | When the Swallows Homeward Fly (Agathe) | Voice and Piano

S. Adams (1844–1913) | The Blue Alsatian Mountains | Voice and Piano

Anonymous | Easter Hymn | Voice (SATB)

J.-B. Arban (1825–1889) | Method for the Cornet. First Studies | Trumpet

J. Ascher (1829–1869) | Alice, Where Art Thou? | Voice and Piano

J. S. Bach (1685–1750) | Die Kunst der Fuge, Contrapunctus I | Harpsichord, Piano

J. S. Bach (1685–1750) | Christ Lag In Todesbanden | Choir (SATB)

J. S. Bach (1685–1750) | Gott, wie gross ist deine Güte | Piano

J. S. Bach (1685–1750) | Goldberg Variations – 22 | Harpsichord

J. S. Bach (1685–1750) | Goldberg Variations – 23 | Harpsichord

L. V. Beethoven (1770–1827) | Sonata No. 23 “Appassionata” | Piano

H. R. Bishop (1786–1855) | Home, Sweet Home | Voice and Piano

L. Bourgeois (c.1510–1560) | Old 100th | Voice (SATB)

J. Brahms (1833–1897) | Cancion de Cuna | Recorder, Guitar

M. Carcassi (1792–1853) | Caprice No. 1 | Guitar

M. Carcassi (1792–1853) | Caprice No. 2 | Guitar

M. Carcassi (1792–1853) | Caprice No. 3 | Guitar

M. Carcassi (1792–1853) | Caprice No. 4 | Guitar

M. Carcassi (1792–1853) | Caprice No. 5 | Guitar

M. Carcassi (1792–1853) | Caprice No. 6 | Guitar

M. Carcassi (1792–1853) | Etude 2 | Guitar

M. Carcassi (1792–1853) | Etude 4 | Guitar

M. Carcassi (1792–1853) | Etude 6 | Guitar

M. Carcassi (1792–1853) | Etude 10 | Guitar

M. Carcassi (1792–1853) | Etude 11 | Guitar

M. Carcassi (1792–1853) | Etude 12 | Guitar

M. Carcassi (1792–1853) | 25 Studies: No. 13 | Guitar

M. Carcassi (1792–1853) | 25 Studies: No. 14 | Guitar

M. Carcassi (1792–1853) | 25 Studies: No. 16 | Guitar

M. Carcassi (1792–1853) | 25 Studies: No. 17 | Guitar

M. Carcassi (1792–1853) | 25 Studies: No. 18 | Guitar

M. Carcassi (1792–1853) | 25 Studies: No. 19 | Guitar

M. Carcassi (1792–1853) | 25 Studies: No. 20 | Guitar

M. Carcassi (1792–1853) | 25 Studies: No. 21 | Guitar

H. Carey (1687?–1743) | Sally in Our Alley | Voice and Piano

F. F. Chopin (1810–1849) | Nocturne 8 in Db Major | Piano

F. F. Chopin (1810–1849) | Prelude: Op. 28, No. 20 | Piano

Claribel [C. A. Barnard ] (1830–1869) | I Cannot Sing the Old Songs | Voice, Piano

C. Debussy (1862–1918) | Deuxième Arabesque | Piano

M. Giuliani (1781–1829) | 24 Studies for the Guitar, No. 16 | Guitar

M. Giuliani (1781–1829) | 16 Studies for the Guitar | Guitar

M. Giuliani (1781–1829) | 16 Studies for the Guitar | Guitar

M. Giuliani (1781–1829) | 16 Studies for the Guitar | Guitar

M. Giuliani (1781–1829) | 16 Studies for the Guitar: 4 | Guitar

M. Giuliani (1781–1829) | 16 Studies for the Guitar: No. 5 | Guitar

M. Giuliani (1781–1829) | 16 Studies for the Guitar: No. 7 | Guitar

C. W. Glover (1797–1868) | Do They Think of Me At Home? | Voice and Piano

J. Hullah (1812–1884) | The Three Fishers | Voice and Piano

D. Joly (?–1879) | Tyrolienne | Guitar

S. Joplin (1868–1917) | Sugar Cane | Piano

G. F. Kiallmark (1804–1887) | The Old Oaken Bucket | Voice and Piano

J. Küffner (1776–1856) | Air varié | Guitar

T. Moore (1779–1833) | The Last Rose of Summer (Martha) | Voice and Piano

C. Moreland (?–?) | My Mother’s Old Red Shawl | Voice and Piano

W. A. Mozart (1756–1791) | Fugue fragment Anh. 41 (375g) | Piano

W. A. Mozart (1756–1791) | Marche funebre KV 453a | Piano

N. Paganini (1782–1840) | 24 Caprices for Solo Violin: 23 | Violin

N. Paganini (1782–1840) | 24 Caprices for Solo Violin: 24 | Violin

G. B. Pergolesi (1710–1736) | Glory to God in the highest | Voice (SATB) and Organ

S. Rachmaninoff (1873-1943) | Prelude Op. 23, No. 2 | Piano

S. Rachmaninoff (1873-1943) | Prelude Op. 23, No. 3 | Piano

S. Rachmaninoff (1873-1943) | Prelude Op. 23, No. 4 | Piano

J. P. Rameau (1683–1764) | Tambourin | Harpsichord, Piano

G. F. Root (1792–1868) | The Vacant Chair | Voice and Piano

G. F. Root (1792–1868) | There’s Music in the Air | Voice (SATB) and Piano

G. F. Root (1792–1868) | There’s Music in the Air | Voice and Piano

G. Sanz (1979–) | Preludio | Guitar

F. Silcher (1789–1860) | The Loreley | Voice and Piano

F. Sor (1778–1839) | Op. 35, Study No. 10 | Guitar

F. Sor (1778–1839) | Op. 35, Study No. 18 | Guitar

F. Sor (1778–1839) | Op. 35, Study No. 19 | Guitar

F. Sor (1778–1839) | Op. 35, Study No. 20 | Guitar

F. Sor (1778–1839) | Op. 35, Study No. 22 | Guitar

F. Sor (1778–1839) | Op. 35, Study No. 3 | Guitar

F. Sor (1778–1839) | Op. 35, Study No. 4 | Guitar

F. Sor (1778–1839) | Op. 35, Study No. 5 | Guitar

F. Sor (1778–1839) | Op. 35, Study No. 6 | Guitar

F. Sor (1778–1839) | Op. 35, Study No. 7 | Guitar

F. Sor (1778–1839) | Op. 35, Study No. 8 | Guitar

F. Sor (1778–1839) | Op. 35, Study No. 9 | Guitar

T. Tallis (1681–1767) | If Ye Love Me | Voice (SATB) and Piano

Traditional | Greensleeves | Voice, Guitar

Traditional | Gonbe ga Tanemaku | Shamisen

Traditional | Toka-Ebisu | Shamisen

Traditional | O-Edo-Nihonbashi | Shamisen

Traditional | Suiryo-Bushi | Shamisen

Traditional | Riukiu-Bushi | Shamisen

Traditional | Sedo no Danbatake | Shamisen

Traditional | Inshu-Inaba | Shamisen

Traditional | Kappore | Shakuhachi

Traditional | Yube-Yonda | Koto

Traditional | K Honen | Koto

Traditional | Sakura-Miyotote | Koto

Traditional | Waga-Koiwa | Koto

Traditional | Kosunoto | Koto

Traditional | Kayo-Kami | Koto

Traditional | Horete-Kayouni | Koto

Traditional | Doteo-Toruwa | Koto

Traditional | Otsue-Bushi | Koto

Traditional | Asaku-Tomo | Koto

Traditional | Iyo-Bushi | Koto

Traditional | Oki No Taisen | Koto

Traditional | Gosho no Oniwa | Koto

Traditional | Murasaki | Koto

Traditional | Fuku-Ju-So | Koto

Traditional | Ukiyo-Bushi | Koto

T. Turpin (1582/83–1649) | The St. Louis Rag | Piano

T. Turpin (1582/83–1649) | The Harlem Rag | Piano

J. F. Wade (1711–1786) | Adeste Fideles | Voice (SATB)

(*) Astute observers will be questioning my math skills, having noticed that the library launched with 300 works, and that 300 + 109 does not make 389. What happened to the missing 20 works? Two were removed from the library awhile ago because errors were found in the files. The other 18 remain more of a mystery. The new batch processes I’m using filtered them out for one reason or another. Fortunately I know which ones they are, and time permitting (ha!) I’ll try to look further into it and eventually get them back into the library.
