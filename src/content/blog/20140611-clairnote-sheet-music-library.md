---
title: "The Clairnote Sheet Music Library"
link: https://clairnote.org/blog/2014/06/clairnote-sheet-music-library/
author: paulmorris
special_byline: ""
description:
post_id: 2610
date: 2014/06/11
created_gmt: 2014/06/11
comment_status: closed
slug: clairnote-sheet-music-library
draft: false
post_type: post
tags: ["Clairnote Website", "LilyPond", "Sheet Music"]
---

Lets take it to the next level.

After several months in the pipeline I am pleased to announce a new [Sheet Music Library](/dn/sheet-music/) offering 300 (!!!) musical works in Clairnote music notation.  The sheet music is free to download in PDF format in both US Letter and A4 sizes.  MIDI and [LilyPond](/dn/software/) source files are also available.

None of this would be possible without the wonderful [Mutopia Project](http://www.mutopiaproject.org/) and its dedicated volunteer contributors.  The Mutopia Project offers the original traditional notation versions of these works (and many more), under [free culture](http://creativecommons.org/freeworks) licenses that allow copying and modification, including conversion to alternative notation systems like Clairnote.

And none of that would be possible without LilyPond and its dedicated community of contributors and supporters.  I am very grateful to be "standing on the shoulders" of these excellent projects.  It is inspiring to see what can be accomplished together through cooperation and sharing (as opposed to competition and proprietary ownership).  But back to the library...

**The Sheet Music**

Working on the collection of [180 Fiddle Tunes in Clairnote](/blog/2014/03/180-fiddle-tunes-clairnote/) a few months ago helped me see LilyPond's batch-processing potential.  Because LilyPond is a [command-line](https://en.wikipedia.org/wiki/Command-line_interface) program you can easily run it on every LilyPond file in a given directory with a single command, automatically producing PDF and MIDI files for each file.  This makes it relatively easy to convert a large chunk of traditional sheet music into Clairnote (much easier than with a GUI program).  So while putting this library together was a lot of work, it was less work than you might think.

Basically, after downloading the LilyPond files from the Mutopia Project site I did a bulk find-and-replace to add the <del>`\include "clairnote-code.ly"`</del> `\include "clairnote.ly"` command that allows LilyPond to render the music in Clairnote music notation.  I also made small edits to the footer text that contains the license and credit information -- to mention the conversion (lest anyone mistake the Mutopia Project as endorsing Clairnote).

I did this with all of the Mutopia files that were LilyPond version 2.14, 2.16, or 2.18, using the "convert-ly" command to automatically upgrade them all to 2.18.  I noted any errors that LilyPond logged when upgrading or rendering the files and left out any files that had errors.  Just for simplicity's sake I also left out any works that used more than one LilyPond file (bundled into .zip archives on the Mutopia site).  Time permitting, I would like to eventually move past the "low hanging fruit" and work on bringing more of these works into the library as well.

**User Interface**

With the sheet music in hand the next step was to come up with an interface for accessing the files.  With so many musical works there needed to be a good way to find what you were interested in.  To keep it relatively simple I built a single web page that lists each of the works with their composer and other relevant information or "meta data" (thanks again to the Mutopia Project) along with links to the PDF, MIDI, and LilyPond files, the piece's Mutopia page (with the original traditional notation files), and the relevant Creative Commons license.  Having a single page that contains all the works makes it easy to use your browser's "find" feature to search the library by keyword.

Next I added a sidebar that allows you to click checkboxes to filter which works are displayed, according to style (classical, baroque, romantic, ...), instrument (piano, violin, ...) and composer (G. F. Handel, S. Joplin, ...).  The filtering is done with Javascript (client side) so it is basically instantaneous.  I have some ideas for more improvements (such as better integration with the rest of the Clairnote site), but hopefully this approach will work (and scale) well enough.

**MIDI Playback**

With every piece having a midi file it would be a shame if there were not an easy way to play them and get a sense of what the music sounded like.  So the icing on the cake is that the library also features MIDI playback, right in your browser and without plugins.  This is thanks to [MIDI.js](https://github.com/mudcube/MIDI.js) and [jasmid](https://github.com/gasman/jasmid), a couple of Javascript libraries that work with the [Web Audio API](https://developer.mozilla.org/en-US/docs/Web/API/Web_Audio_API).  The MIDI playback is still a work in progress but it already works fairly well, with centralized play, pause, and stop controls.  For pieces with multiple midi files (in a .zip archive) there is currently no playback.  Another one for the to-do list.

Stepping back from all these implementation details, I'll conclude by saying that this is a big step for Clairnote.  One of the most significant obstacles facing any alternative notation system is the lack of sheet music available for that system.  There is still a long way to go to catch up with [Klavarscribo](https://en.wikipedia.org/wiki/Klavar) (with its estimated 200,000 musical works), but 300 feels like a really good start.

In any case it is a nice payoff for all my efforts learning to program in [Scheme](https://en.wikipedia.org/wiki/Scheme_%28programming_language%29) and coaxing LilyPond into engraving sheet music in Clairnote.  It's always good to enjoy the fruits of one's labor.  But wait... what about the music in the library?  Which 300 works are available?  Please [have a look!](/dn/sheet-music/)  Fans of J.S. Bach will be pleased.

Update: the "clairnote-code.ly" file was renamed "clairnote.ly" on May 15, 2017.

## Comments

**[Clinton Blackmore](#113 "2014-06-11 16:16:44"):** Nice work, Paul! That's awesome.

**[Paul Morris](#118 "2014-06-12 15:26:05"):** Thanks Clinton!

**[Crow John](#125 "2014-06-16 13:23:39"):** I've already started using them, and it is indeed a faster to read notation. I find now, instead of trying to read the notes, I'm more trying to figure out the fingering (if its not included). Thanks! Also, from now on I will be composing my music in Clairnote. I'm no great composer and I do it in my free-time but if I ever decide to put it online or actually publish it, a Clairnote version will be included.

**[Paul Morris](#137 "2014-06-22 18:56:57"):** Thanks Crow John! Glad to hear that you like and are using Clairnote.
