---
title: "New Microtonal Accidental Signs"
author: "paulmorris"
description: "Clairnote music notation now has a system of quarter tone accidental signs for notating microtonal music."
date: 2024/01/03
comment_status: open
slug: "new-microtonal-accidental-signs"
draft: false
post_type: post
tags: ["Music Notation", "LilyPond", "Clairnote Website"]
---

About two years ago
[a post on Mastodon](https://mastodon.social/@lis@mk.catgirlsfor.science/107547099109769076)
mentioned Clairnote music notation,
stating that Clairnote "seems cool but doesn't support microtones at all :blobcatsad:".
The post included a snippet of sheet music in Clairnote SN notation, as rendered by LilyPond, with quarter-tone
and three-quarter-tone accidental signs, that looked like this:

![Example showing previously unsupported quarter tone accidenals](/2024/01/quarter-tone-accidental-example-broken.svg)

On the positive side, LilyPond's default
[Stein-Zimmerman](https://www.w3.org/2019/03/smufl13/tables/stein-zimmermann-accidentals-24-edo.html)
quarter-tone and
three-quarter-tone accidental signs
did appear in front of the relevant notes.
However, the positions of the sharpened notes on
the staff were inconsistent with the positions of the flattened notes.
Also the three-quarter-tone accidental signs were misleading and nonsensical
given the way notes are positioned on the chromatic staff.
Finally, the microtonal accidentals were smaller than usual.

What a sad state of affairs!
At minimum Clairnote seeks to achieve parity with
traditional music notation. Anything that's possible in
traditional notation should also be possible in Clairnote.
So this (rather embarassing) lack of support for music that includes
quarter tone accidentals was never
by design but was mostly just the result of my not having
the time to work on it.

Today I am pleased to report that I have added quarter-tone and
three-quarter-tone accidental signs to Clairnote (as a notation
system) and I have also added support for them to the
`clairnote.ly` software (version `20231228`) that allows LilyPond to produce sheet music
in Clairnote notation. Here is that same musical snippet in today's
Clairnote SN:

![Example showing previously unsupported quarter tone accidenals](/2024/01/quarter-tone-accidental-example-fixed.svg)

Now the vertical positions of the notes on the staff are consistent and
coherent,
there are new three-quarter-tone sharps and flats that make sense on a
chromatic staff, and the accidental signs are their usual size.
To learn more about Clairnote's quarter tone accidental signs,
check out the new
[Microtonal Accidental Signs](/accidental-signs/#microtonal-accidental-signs)
section of the Accidental Signs page.

Since there is more than one set of glyphs that can be used for microtonal
accidental signs in traditional notation, I also made it possible to easily
customize which quarter-tone glyphs are used in Clairnote's accidental signs.
And for additional flexibility, I made it so that LilyPond's standard
mechanisms for customizing accidental glyphs would work with Clairnote
notation.
(Alas, they were not working previously.)
For more details see the new
[Custom Quarter Tone Accidental Signs](/software/#custom-quarter-tone-accidental-signs)
section on the Software: LilyPond
page.

While I was working on Clairnote's accidental signs I also fixed a gap in
functionality where cautionary accidental signs
(an accidental sign in parentheses)
were not working with Clairnote.
They now work the same way they do in traditional notation.
For example, if you add a `?` after a note's name in LilyPond input
(e.g. for English input: `gf?`, `gs?`, `g?`)
then a cautionary (parenthesized) accidental sign will appear before the note.

![Cautionary accidentals (parenthesized accidentals)](/2024/01/parenthesized-accidentals.svg)

I'm glad that these new quarter-tone and three-quarter-tone accidental signs
will make it possible to
notate a wider and more diverse range of music in Clairnote,
including, for example, traditional music of
[Persia and the Middle East](https://en.wikipedia.org/wiki/Quarter_tone#Music_of_the_Middle_East)
and various microtonal works by
[composers](<https://en.wikipedia.org/wiki/Microtone_(music)#In_the_West>)
from Europe and beyond.

Of course, there is more to microtonal music than quarter tone accidental signs &mdash; the world of microtonal music is vast and diverse.
While many microtonal works will now be able to be notated in Clairnote,
others may require different accidental signs or
other more specialized notational features not offered by Clairnote,
at least not "out of the box."
Fortunately, LilyPond's flexibility allows it to support various types of
microtonal music in various ways. (For example, see Graham Breed's
[Microtonality in LilyPond](http://x31eq.com/lilypond/)
webpage.)
Similarly with Clairnote's `clairnote.ly` software,
the goal is to provide that kind of flexibility,
while avoiding interfering with LilyPond's flexibility.
This approach will help make it possible to
notate various types of microtonal music in Clairnote notation.
There is likely more that could be done along these lines.
I am no expert in microtonal music and how it is notated,
so I welcome constructive criticism and feedback.

As always the most recent version of `clairnote.ly`
is available for download from the
[Software: LilyPond](/software/) page.
Until next time!
