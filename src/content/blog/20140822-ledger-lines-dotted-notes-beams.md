---
title: "Ledger Lines, Dotted Notes, Beams"
link: https://clairnote.org/blog/2014/08/ledger-lines-dotted-notes-beams/
author: paulmorris
special_byline: ""
description:
post_id: 2707
date: 2014/08/22
created_gmt: 2014/08/22
comment_status: closed
slug: ledger-lines-dotted-notes-beams
draft: false
post_type: post
tags: ["LilyPond"]
---

Recent work has lead to improved rendering of ledger lines, dotted notes, and beams in Clairnote music notation (by LilyPond).

Here's a quick summary (TL;DR):

- ledger lines are now a little wider for better readability
- the position of the dots on dotted notes was sometimes slightly wrong and is now always correct
- beams were too thin and this is now fixed

## Ledger Lines

![Ledger lines are now wider in Clairnote music notation](/images/ledger-line-width.svg)

Ledger lines in Clairnote are now slightly wider than they are in traditional music notation (as rendered by LilyPond).  This makes the position of notes a little easier to read when ledger lines are involved.  This only required a simple override of `LedgerLineSpanner.length-fraction` and `LedgerLineSpanner.minimum-length-fraction` in the custom staff context definition for Clairnote. They are now set to 0.45 and 0.35 respectively.  Thanks goes to the fine folks on the LilyPond mailing list for their help with understanding these properties (see in particular [this post](https://lists.gnu.org/archive/html/lilypond-user/2014-05/msg00598.html) and this [LSR snippet](http://lsr.di.unimi.it/LSR/Item?id=917)).

## Dotted Notes

![Position of dots on dotted notes in Clairnote music notation](/images/dotted-notes.svg)

As you would expect LilyPond automatically shifts the position of the dot upward on any dotted notes that fall on staff lines or ledger lines, so that they won't collide with the line. Fortunately, LilyPond takes into account custom staff line positions when making these adjustments, but unfortunately she does not take into account custom ledger line positions. (See issue [4040](https://code.google.com/p/lilypond/issues/detail?id=4040).)  With Clairnote this was causing dots to be shifted upward to avoid ledger lines that were not actually there.  (See the 3rd and 7th notes in the image above.)  Jan Braunstein first noticed this happening in his [Chromatic Lyre Notation](http://www.delos.cz/en/sheet_music/chromatic_lyre_notation.html), which is very similar to Clairnote. Working together we devised a workaround (using Scheme) to automatically correct the position of the dots everywhere it was needed. Of course eventually it would be better if LilyPond did this correctly "out of the box," but the relevant LilyPond code is in C++ which is beyond my current skill set.

## Beams

![Beam thickness in Clairnote music notation](/images/beams.svg)

I noticed that the beams (on beamed 8th notes, 16th notes, etc.) were thinner in Clairnote than in traditional music notation (as rendered by LilyPond).  It turns out that the [vertical compression of the Clairnote staff](/dn/staff/) was also affecting the thickness of the beams.  Beam thickness is partly based on the `StaffSymbol.staff-space` property, which is used to vertically compress the staff.  This assures that beams are still correctly proportioned on staves that have been scaled to larger or smaller sizes.  It was easy enough to adjust the `beam-thickness` property for beams in order to restore the correct thickness for Clairnote.  However, this was not enough.  When there was more than one beam (as with 16th notes, 32nd notes, etc.) the spacing between the beams was also being reduced along with the thickness of the beams.  So increasing the thickness of the beams without increasing their spacing lead to them merging into one really thick beam!  At first it wasn't obvious that it was the `length-fraction` property of beams that controls the spacing between them, but I eventually figured it out with some help from the LilyPond mailing list.  Now both beam thickness and the spacing between them is the same in Clairnote as in traditional notation (relative to the size of stems, noteheads, etc.).

Each of these changes can be seen in the sheet music on the Clairnote site.  It has all been updated using the new code.  The images on the site have not been updated as yet.  As always the latest code can be found in the most recent version of the <del>clairnote-code.ly</del> clairnote.ly file (see [Software](/dn/software/)).  The changes also include some "under-the-hood" improvements, refactoring (becoming ever more functional in coding style), and a few bug fixes.  For example, now forced accidentals always work.  (Before they would appear in cases like "fes e!" but not for cases like "cis cis!")

Overall, the fact that these are all relatively minor improvements is a good sign of the progress we've made!

Update: the "clairnote-code.ly" file was renamed "clairnote.ly" on May 15, 2017.
