---
title: 'Traditional Irish Music from "The Session" in Clairnote'
link: https://clairnote.org/blog/2017/07/traditional-irish-music-session-clairnote/
author: paulmorris
special_byline: ""
description:
post_id: 3539
date: 2017/07/14
created_gmt: 2017/07/14
comment_status: closed
slug: traditional-irish-music-session-clairnote
draft: false
post_type: post
tags: ["Sheet Music"]
---

[The Session](https://thesession.org) is an amazing website that focuses on traditional Irish music — reels, jigs, waltzes, etc.  In addition to listings of events, jam sessions, and recordings, it offers sheet music for literally thousands of traditional Irish tunes for free download.  Thanks to a recent conversion effort, over one thousand of them are now available in Clairnote music notation.

To be precise, there are 1,148 tunes in Clairnote. They make great material for learning to read Clairnote and for practicing sight reading skills. Moreover they're just great music and include many classic and well-loved tunes.

Most of the tunes have multiple "settings" — different versions or variations of the same tune.  The numbers identifying each setting are the same as those on the Session website.  (Some settings are missing because of errors when converting the files from abc to LilyPond format.)

To access the tunes, go to the Clairnote [sheet music library](/dn/sheet-music/). In the new menu to the left of the search box select "Collection: The Session". You can then search or browse through the tunes. The library is now organized into two collections of sheet music, the first from the [Mutopia Project](http://www.mutopiaproject.org/) and the second from the Session.

How did I do this batch-conversion?  The Session stores its music in the text-based [abc file format](http://abcnotation.com/) ("the defacto standard for folk and traditional music").  So after downloading the abc files I had to convert them to [LilyPond](/dn/software/) format and upgrade them to the most recent version of LilyPond to be able to render them in Clairnote music notation. Fortunately LilyPond comes with abc2ly, a conversion tool and convert-ly, a tool to upgrade LilyPond files.  As with the sheet music from the Mutopia Project, I automated the process (well, most of it) using a handful of [Python](https://www.python.org/) [scripts](https://github.com/PaulMorris/clairnote-sheet-music-library) that I wrote.

Unfortunately the abc2ly conversion script does not handle certain kinds of abc input. About one in five tune settings could not be successfully converted. I contributed a [small improvement](https://git.savannah.gnu.org/cgit/lilypond.git/commit/?id=3702169e4b17edbb4bff9331215eccaba6f2d478) so that it would support the R (rhythm / meter) and Z (transcription) meta-data fields used by the abc files from the Session. But it really deserves to be improved to better handle a wider range of abc input.

To download the files from the Session site I wrote a little web crawler script that would download a new file every two seconds, which worked well. But later I took a closer look at the Session site and found out that it has a very nice [read-only public API](https://thesession.org/api) that lets you download the whole collection of tunes as a single CSV or JSON file! So I'll use the API for any further conversion work I find the time to do. (The sheer volume of the whole collection, which is over 28,000 settings (!), would require significant changes to the way the sheet music library is implemented.) 
Thanks to my friends Adriel, Anja, Elias, and Nasreen for their help with various aspects of the scripts to do this conversion. Thanks and kudos to Jeremy Keith for creating and maintaining the Session site. It is really well done and a joy to use. And to my readers, I hope you enjoy these traditional Irish tunes in Clairnote music notation!
