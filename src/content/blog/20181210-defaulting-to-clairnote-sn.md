---
title: "Defaulting to Clairnote SN"
author: paulmorris
special_byline: ""
description: "Clairnote SN music notation is now the focus of the Clairnote website instead of Clairnote DN. Learn about the reasons and motivations for this change."
date: 2018/12/10
comment_status: open
slug: defaulting-to-clairnote-sn
draft: false
post_type: post
tags: ["Clairnote Website"]
---

Now that the Clairnote website's
[migration to GatsbyJS](/blog/2018/11/clairnote-website-and-gatsbyjs/)
is complete, I have changed the site to present Clairnote SN first,
by default, rather than Clairnote DN.
Although this is a significant shift in emphasis,
both variants are still fully represented and supported,
and all it takes is one click to switch from one to the other.

The reasons for making Clairnote SN the default variant
are the same reasons that someone might prefer it that I laid out
in my post
[Introducing Clairnote SN](/blog/2018/04/introducing-clairnote-sn/).

First, Clairnote SN is likely to be more interesting and appealing at first glance
for those visiting the site for the first time
who have no prior exposure to alternative notation systems with a chromatic staff.
It provides a gentler introduction to this approach to music notation,
because it is more immediately familiar,
and offers more continuity with the traditional system
by not changing the meaning of hollow and solid noteheads.

Second, Clairnote SN's continuity with traditional notation
makes it easier to learn Clairnote SN along with the traditional system.
Being able to read traditional music notation
is a useful skill and often necessary,
even if you prefer an alternative system.
So it makes sense to prioritize this "use case"
and default to the variant of Clairnote that makes it easier to take
this bilingual approach of learning both systems.

The third reason is not as easy to express,
so I'll dwell on it a little more.
Aside from Clairnote SN's continuity
with the traditional system,
some may find that when reading music,
they prefer for hollow and solid notes to symbolize duration
rather than pitch
(namely the [6-6 pitch pattern](/6-6-pitch-pattern/)).
It may feel more satisfying in a sort of "aesthetic" or "semantic" sense.
For example, I was sure, logically and theoretically,
that having a highly visible 6-6 pitch pattern
was a really good thing (for identifying notes and intervals).
However, having played music in Clairnote DN and SN,
I often find myself feeling
that the difference between hollow and solid noteheads
may be too strong a signal
for what it signifies in Clairnote DN (the 6-6 pitch pattern),
again, in some sort of "aesthetic" or "semantic" register.

Or to put it in terms of the other side of the coin,
musically, half notes and longer are often "landing" notes,
where a melody slows down or pauses,
whereas quarter notes and shorter are usually "movement" notes,
where a melody is more "in motion".
And there's something nice about the way this musical difference
is apparent in the clear symbolic difference between hollow and solid notes.
It can make it easier to see the contour or shape of a melody in terms
of it's timing or rhythm.
Similarly there's something nice about having a series of eighth
notes all be solid since they tend to have similar musical "weight"
or role.
I suppose this is the notion that it makes sense for similar things
to look similar, and for different things to look different.
And for any two notes, their durations may be a more significant factor
in whether they are felt or understood to be similar or different,
than whether they fall in one or the other whole tone scale
in the 6-6 pitch pattern.

This is not the whole story... but in any case,
when hollow and solid notes are used for the 6-6 pitch pattern,
it does give a different feel or aesthetic to reading the music
&mdash; as compared with the more subtle approach of having the
(ledger-) lines and spaces of the staff represent the 6-6 pitch pattern.

Of course, different people will have different takes on this,
but I currently find myself preferring Clairnote SN.
I've come to favor it over the last year or so,
through reading mostly melodic pieces,
some in Clairnote SN, some in Clairnote DN,
and others in traditional music notation.
Which brings me to a fourth reason to default to Clairnote SN,
which is that it is now the variant of Clairnote that I use
(when I can)
and so I'd prefer to put more emphasis on it going forward,
while still supporting Clairnote DN,
with its particular advantages, disadvantages, and aesthetic,
for those who prefer it.

Previously the DN variant was simply called "Clairnote",
but now it is called "Clairnote DN" on the site,
which will help to avoid confusion.
Also, I have adjusted the site's color scheme to help signal this change.
It used to be forest green for Clairnote SN
and blue for Clairnote DN.
The colors have now shifted along the color wheel
so it's a blue-green teal for Clairnote SN
and purple for Clairnote DN.
Switching between the two variants is easier than before
with a new "SN->DN" link in the main navigation menu.

For consitency,
when using LilyPond to make sheet music,
the newest version of the "clairnote.ly" file (20181125)
also defaults to Clairnote SN rather than Clairnote DN.
See [Software: LilyPond](/dn/software/) for how to specify Clairnote DN output.
[LilyBin + Clairnote](https://lilybin.clairnote.org/) also now defaults to Clairnote SN.
(It uses the latest "clairnote.ly" file.)

For more about Clairnote SN and DN,
including illustrations,
see the [Introducing Clairnote SN](/blog/2018/04/introducing-clairnote-sn/)
blog post and the [Clairnote SN and Clairnote DN](/clairnote-dn-clairnote-sn/) page.
