---
title: "Sheet Music: 92 New Works"
link: https://clairnote.org/blog/2017/02/sheet-music-92-new-works/
author: paulmorris
special_byline: ""
description:
post_id: 3345
date: 2017/02/02
created_gmt: 2017/02/02
comment_status: closed
slug: sheet-music-92-new-works
draft: false
post_type: post
tags: ["Sheet Music"]
---

I'm pleased to announce that 67 new works were added to the [Clairnote Sheet Music Library](/dn/sheet-music/) last month.  There were also 25 additions in April 2016.  That's a combined 92 additions in the past year, bringing the total in the library to 627.

As with the other pieces in the library, these new additions were automatically converted to Clairnote from traditional [LilyPond](/dn/software/) files in the [Mutopia Project](http://www.mutopiaproject.org/)‘s collection.  Basically the library tracks the Mutopia Project.  When new pieces are added there (or updated to LilyPond 2.18 or higher) they are later converted and added to the Clairnote library.  (That is, unless there are problems with the automatic conversion to Clairnote, which typically only affects a small number of them.) So, as always, many thanks to those volunteer contributors to the Mutopia Project for their work typesetting these pieces with LilyPond!

**Here are the 67 new additions from January 2017.  The 25 from April 2016 are listed below.**

D. Aguado | Les Favorites, No. 9 | Guitar

G. Allegri | Miserere mei, Deus | SSATB choir, SSAB Soli

Anonymous | Leoni | Voice (SATB)

Anonymous | Winchester New | Voice (SATB)

J. S. Bach | Concerto in G minor | Violin, Viola, Basso Continuo, String Ensemble

J. S. Bach | Ach, was soll ich Sünder machen | Harpsichord, Piano

J. S. Bach | Als der gütige Gott vollenden wollt’ sein Wort | Voice (SATB)

J. S. Bach | Trio Sonata V in C major BWV 529 | Organ

J. S. Bach | In Dulci Jubilo | Organ

J. S. Bach | Das Wohltemperierte Clavier I, Fuga XVII | Harpsichord, Piano

J. S. Bach | Preludio con Fuga | Harpsichord, Piano

B. Bartók | Romanian Folk Dance No.2 – Peasant Costume | Harpsichord, Piano

L. V. Beethoven | Sonata No. 17 (2nd Movement: Adagio) | Piano

L. V. Beethoven | Piano Concerto No. 3 in C Minor (1st Movement: Allegro con brio) | Piano and Orchestra

F. Behr | In May | Harpsichord, Piano

F. F. Chopin | Ballade number 4 | Piano

F. F. Chopin | Prélude 3 | Piano

F. F. Chopin | Prélude 5 | Piano

F. F. Chopin | Nocturne No. 19 in E minor | Piano

M. Clementi | Sonatina | Piano

C. Czerny | 50 Melodische Übungsstücke, No. 1 | Piano

C. Czerny | 50 Melodische Übungsstücke, No. 2 | Piano

C. Czerny | 50 Melodische Übungsstücke, No. 3 | Piano

C. Czerny | 50 Melodische Übungsstücke, No. 4 | Piano

C. Czerny | 50 Melodische Übungsstücke, No. 5 | Piano

C. Czerny | 50 Melodische Übungsstücke, No. 6 | Piano

C. Czerny | 50 Melodische Übungsstücke, No. 7 | Piano

C. Czerny | 50 Melodische Übungsstücke, No. 8 | Piano

C. Czerny | 50 Melodische Übungsstücke, No. 9 | Piano

W. H. Doane | Near the Cross | Voice and Piano

J. Field | Nocturne | Piano

M. Giuliani | Etudes, No. 10 | Guitar

M. Giuliani | Divertissements, No. 1 | Guitar

M. Giuliani | Divertissements, No. 2 | Guitar

M. Giuliani | Divertissements, No. 3 | Guitar

M. Giuliani | Divertissements, No. 4 | Guitar

M. Giuliani | Divertissements, No. 5 | Guitar

R. B. Hall | Tenth Regiment March | Concert Band: Flutes, Oboe, Bassoon, Clarinets, Bass Clarinet, Alto Sax, Tenor Sax, Baritone Sax, Trumpets, Cornets, Horns, Trombones, Euphonium, Basses, Drums, Bells, Timpani

F. Mendelssohn-Bartholdy | Aus tiefer Not schrei’ ich zu dir | SATB, Organ

F. Mendelssohn-Bartholdy | Herr, nun lässest du deinen Diener in Frieden fahren | SATB, Organ

W. A. Mozart | Menuett N°2 fûr das Pianoforte Kôch Verz N°2 | Piano

E. Satie | Enfantillages Pittoresques | Piano

E. Satie | Gnossienne No. 2 | Piano

E. Satie | Gnossienne No. 3 | Piano

W. Smallwood | The Harebell | Piano

F. Sor | Divertissements pour la guitare, n°3 Allegretto | Guitar

F. Sor | Grande Sonate, Op. 22 | Guitar

F. Sor | Six Petites Pièces, No. 1 | Guitar

F. Sor | Six Petites Pièces, No. 2 | Guitar

F. Sor | Six Petites Pièces, No. 3 | Guitar

F. Sor | Six Petites Pièces, No. 4 | Guitar

F. Sor | Six Petites Pièces, No. 5 | Guitar

F. Sor | 12 Etudes, No. 1 | Guitar

F. Sor | 12 Etudes, No. 10 | Guitar

F. Sor | 12 Etudes, No. 11 | Guitar

F. Sor | 12 Etudes, No. 12 | Guitar

F. Sor | 12 Etudes, No. 2 | Guitar

F. Sor | 12 Etudes, No. 3 | Guitar

F. Sor | 12 Etudes, No. 4 | Guitar

F. Sor | 12 Etudes, No. 5 | Guitar

F. Sor | 12 Etudes, No. 6 | Guitar

F. Sor | 12 Etudes, No. 7 | Guitar

F. Sor | 12 Etudes, No. 8 | Guitar

F. Sor | 12 Etudes, No. 9 | Guitar

G. P. Telemann | Concerto in G major for four violins | Violin

G. P. Telemann | Concerto in D major for four violins | Violin

G. P. Telemann | Sonata in C major for four violins | Violin

**And here are the 25 new additions from April 2016.**

D. Aguado | Allegro in G Major | Guitar

D. Aguado | Les Favorites | Guitar

D. Aguado | Les Favorites, No. 2 | Guitar

D. Aguado | Les Favorites, No. 3 | Guitar

D. Aguado | Les Favorites, No. 4 | Guitar

D. Aguado | Les Favorites, No. 5 | Guitar

D. Aguado | Les Favorites, No. 6 | Guitar

D. Aguado | Les Favorites, No. 7 | Guitar

D. Aguado | Les Favorites, No. 8 | Guitar

D. Aguado | Les Favorites, No. 10 | Guitar

J. S. Bach | Die Kunst der Fuge, Contrapunctus XI | String Ensemble

J. S. Bach | Ermentre dich, mein schwacher Geist | Piano

J. S. Bach | Vater unser im Himmelreich | Organ

L. V. Beethoven | Sonata No. 14 “Moonlight” (1st Movement: Adagio sostenuto) | 2 Guitars

M. Giuliani | Sonate Brilliant | Guitar

F. J. Gobbaerts (L. Streabbog) | Les étoiles d’or, No. 1 Valse | Piano

P. Heise | Gravsang af Shakespeare’s Cymbeline | Male choir TTBB

S. Joplin | The Entertainer | Piano

D. Scarlatti | Keyboard Sonata in G Major | Piano

D. Scarlatti | Keyboard Sonata in D Major | Piano

F. Sor | Opus 29, No. 13 | Guitar

F. Sor | Op. 35, Study No. 23 | Guitar

F. Sor | Op. 35, Study No. 24 | Guitar

F. Tarrega | Adelita | 2 Guitars

F. Tarrega | Lágrima | 2 Guitars
