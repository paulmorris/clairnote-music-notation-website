---
title: "Introducing Clairnote Music Notation"
link: https://clairnote.org/blog/2013/12/introducing-clairnote-music-notation/
author: paulmorris
special_byline: ""
description:
post_id: 1
date: 2013/12/07
created_gmt: 2013/12/07
comment_status: closed
slug: introducing-clairnote-music-notation
draft: false
post_type: post
attachments:
tags: ["Music Notation"]
---

I am pleased to introduce a new alternative notation system called Clairnote.  Like many of these kinds of systems Clairnote is not completely new, but is rather a variation on a theme.  Most notably it shares a line pattern and hollow/solid notehead pattern with [Beyreuther's Untitled system](http://musicnotation.org/system/untitled-by-johannes-beyreuther/) from 1959.  In a future post I will discuss the relationship between Clairnote and other systems, but for now I want to just briefly discuss the "vertical compression" of the Clairnote staff which is one of its more notable and/or distinguishing features.

One of the main (maybe only?) disadvantages of alternative notation systems with a chromatic staff is that they take up more vertical space on the page since they put twelve notes per octave on the staff rather than just seven.  The distance between any two given notes becomes greater which can make intervals somewhat harder to read and recognize, especially larger intervals.

Some notation systems like [Twinline](http://musicnotation.org/system/twinline-notation-by-thomas-reed/) and [TwinNote](http://musicnotation.org/system/twinnote-by-paul-morris/) attempt to address this by introducing different notehead shapes that allow for a more vertically compressed chromatic staff.  Clairnote achieves a similar degree of vertical compression by simply allowing vertically adjacent notes to overlap to a greater extent than on a traditional staff.  The hollow/solid notehead pattern helps by making it easier to differentiate vertically adjacent notes.  This approach has also been used by John Keller in his [Express Stave](http://musicnotation.org/system/express-stave-by-john-keller/) system starting in 2005.

I will discuss the details of this approach to vertical compression in a future post (with illustrations), but for now suffice it to say that notes an octave apart on a Clairnote staff are only 1.2 times as far apart as they are on a traditional staff.[1]  This is much less than 1.714... (12 divided by 7) which is the case on a "standard" uncompressed chromatic staff.

There is more to come about all of this, so please follow the Clairnote blog by [email](/blog/follow-by-email/) or [RSS feed](/rss.xml) if you are interested!

[1] Edited: originally it was "1.15 times as far apart" before March 2014.
