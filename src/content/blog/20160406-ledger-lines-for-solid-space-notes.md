---
title: "Ledger Lines for Solid Space Notes"
link: https://clairnote.org/blog/2016/04/ledger-lines-for-solid-space-notes/
author: paulmorris
special_byline: ""
description:
post_id: 3348
date: 2016/04/06
created_gmt: 2016/04/06
comment_status: closed
slug: ledger-lines-for-solid-space-notes
draft: false
post_type: post
tags: ["Music Notation", "LilyPond"]
---

In Clairnote the notes D, F-sharp/G-flat, and A-sharp/B-flat are all solid notes that fall right between staff lines or ledger lines.  So what should the ledger line pattern for these notes be?

Previously the ledger lines for these notes looked like this:

![Old ledger line pattern in Clairnote music notation](/images/ledger-extra-example-old.svg)

Namely, when these notes were not between two staff lines (which was more than 2/3rds of the time) they would float above or below the nearest line or ledger line.  This worked well enough, but since semantically these notes are positioned _between_ two lines, the new default pattern for ledger lines looks like this:

![New ledger line pattern in Clairnote music notation](/images/ledger-extra-example-new.svg)

Now these notes always appear _between_ two lines or ledger lines.  This makes them a little easier to identify by reducing the sense of ambiguity that can arise when they are just floating out there beyond the nearest line.  (Of course, all of the other notes are positioned either on a line or adjacent to a line (touching the line), making each note's position clear to see.)

Another benefit is that the C ledger line in the middle of the staff will appear more often, and that helps to make the interval relationships between notes that span this ledger line easier to see, by effectively making the invisible C line a bit more visible.

Fortunately LilyPond is very customizable and already had a property that made this change easy to do.  All it took was


    \override StaffSymbol.ledger-extra = 2

(see [staff-symbol-interface](http://lilypond.org/doc/v2.19/Documentation/internals/staff_002dsymbol_002dinterface)).  However, there was [a bug](https://sourceforge.net/p/testlilyissues/issues/4726/) in LilyPond that was causing ledger lines to appear on top of staff lines when ledger-extra was set to more than 0.  This bug had prevented me from making this the default in the past, but recently I was able to fix it.  The fix landed in LilyPond 2.19.36.

If you prefer the previous ledger line pattern/behavior you can easily get it back by adding this code near the top of your LilyPond file(s):


    \layout {
      \context {
        \Staff
        \override StaffSymbol.ledger-extra = 1
      }
    }

And that concludes this post, the latest in a series documenting recent changes to the <del>clairnote-code.ly</del> clairnote.ly file (see [Software:LilyPond](/dn/software/)).  One more significant improvement remains and will be covered in a future post.

Update: the "clairnote-code.ly" file was renamed "clairnote.ly" on May 15, 2017.

## Comments

**[Doug Keislar](#822 "2016-04-06 17:49:34"):** Hi Paul, I have *much* less experience with Clairnote than you do, but offhand the new ledger lines just seem to me like additional clutter. I'd rather see a feature where the C ledger line either gets omitted (possibly) or else is distinguished from the other ledger lines by having the other ledger lines be longer, in order to make it easier to see the usual staff pattern (i.e., the E and G# lines) when there are at least two ledger lines. I think that would make it easier to read notes off the main staff.

**[Paul Morris](#823 "2016-04-06 18:30:36"):** Hi Doug, Thanks for your comment! I guess what counts as additional clutter is in the eye of the beholder. I'm happy to let these kinds of details be worked out over time depending on what works best for those using Clairnote. At least now both possibilities are options with LilyPond. On the idea of having the C ledger line omitted or distinguished, I think it's a good one. As you probably recall from [my post]( https://groups.google.com/forum/#!topic/musicnotation/jR5TuJRY93k) to the MNP forum back at the end of January, I've started working on implementing this for LilyPond, so it should just be a matter of time. Stay tuned for future developments on that... 
