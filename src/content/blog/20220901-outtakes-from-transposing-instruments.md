---
title: "Outtakes from Transposing Instruments and Clairnote Music Notation"
author: "paulmorris"
description: ""
date: 2022/09/01
comment_status: open
slug: outtakes-transposing-instruments-and-clairnote-music-notation
draft: true
post_type: post
tags: [Music Notation]
---

NO

What makes an instrument a transposing instrument is the way music is conventionally written for that instrument, namely how the _music_ is transposed.

(So despite the name "transposing instruments" it is not that the instruments are transpos*ing*, it is that the music for them has been transpos*ed*.)

The name "transposing instrument" can be a little misleading because it's not really the instrument that does the transposing.
The transposing happens in the way the music for these instruments is written.

---

NO

In such an instrument family, there are a number of instruments that are pitched differently.
For example, alto, soprano, tenor, and baritone saxophones each have a different register or range of notes that they can produce.
For example, an alto saxophone has a higher range than a soprano saxophone.
(They may also have a slightly different timbre or tone, for example a flugel horn, a trumpet, and a cornet.)

By transposing the music for each of these instruments, the fingerings for each note remain consistent across each of the instruments.
A "C" on the staff is always played with the same fingering, regardless of the instrument.
That makes it easy for musicians to read and play different instruments in the same family.

For example, a middle C on the staff is played with no valves depressed on a trumpet, whether it's a standard B-flat trumpet, a piccolo trumpet, etc.

---

MAYBE

A musician that plays more than one instrument in a given instrument family clearly benefits from the "transposed music" convention.
On the other hand, a musician that only ever plays one instrument does not benefit from it, and in fact suffers from it because they cannot play music in concert pitch that is not transposed.

---

NO

Transposed music does seem justified for provide "ease of switching instruments" for musicians that play more than one instrument in a given family of transposing instruments.

The first is "ease of switching instruments" within a given family of instruments.

This "ease of switching instruments" rationale makes a lot of sense.
At least, it makes a lot of sense for musicians that actually do play more than one instrument in a given instrument family.
If you only ever play one instrument in your instrument family, then you never see the benefit.

So not writing music in concert key makes sense in order to keep fingering to note patterns consistent for all instruments in a given family.

---

But how then do you decide how far to transpose the music for a given instrument?
Is it arbitrary or is there some logic behind it?

---

END HERE

## How much is music transposed for transposing instruments and why?

Beyond the reasons for transposing the music for transposing instruments lies the question of how much the music is transposed.  
Why is it transposed in the way it is and not some other way?
Is it arbitrary?
An E on the staff could represent a B-flat just as well as a C on the staff.

There is actually a logic to it.
Most transposing instruments have what might be called a "home" note or key.
This note or key is typically simpler or easier to play than other notes or keys.
The home note for a B-flat trumpet is B-flat.
When you play the trumpet without any valves pressed the note that is produced is usually a B-flat.

It is no coincidence that the "home" note of a transposing instrument is typically represented as a C on the staff.

## Transposed Music in Clairnote Music Notation?

Even if we grant that it makes sense to transpose music to provide the benefit of easily switching between instruments within the same family, we still have

This brings us to another related factor which is that most transposing instruments have what we might call a "home" note or key, that is simpler to play than other notes or keys.
So the "home" note for a B-flat trumpet is "B-flat".
When you play the trumpet without any valves pressed the note that comes out is a "B-flat".
It's like the "default" note of the instrument, and the B-flat major scale is thus kind of a default scale or key.

It is no coincidence that the "home" note of a transposing instrument is represented as a "C" on the staff.
And the same goes for the "home" major key which is represented as C major on the staff.
C major is the "home" key of traditional music notation.
So it is "obvious" that the "C" is chosen as the home note and key to use for the home note/key for any given transposing instrument.  
This is so obvious and given that it is not even remarked upon in many discussions of transposing instruments.
Of course it is "C".
If for no other reason, this makes sense pedagogically.
The easiest key to play should be the easiest key to read (no sharps or flats).
That helps beginner students by easing their path during the difficult first steps of learning to read and play an instrument.

But what if you had a notation system where every key was equally easy to read?
Where one key was not easier than all the others?

A system with a chromatic staff, like Clairnote?

Would that provide new possibilities?  
Or would we continue to write music transposed for this class of instruments?

A proposal

Of course, we could keep doing it the same way as with traditional music notation.
However, I would like to propose another possibility.
For some instrument families, a particular instrument tends to be the one that most students start with.
For example, the B-flat trumpet.
Many musicians may play only or primarily that one instrument for their whole life.

So what if music for B-flat trumpet was written in concert pitch, so that B-flat on the instrument was B-flat on the staff?  
Then music for other trumpet family instruments could be transposed so that "B-flat" was always the "home" note in that instrument family.
That way you would still have consistent fingering to note mapping across all instruments in a given family,
but on the primary or most basic instrument in that family, the music would be written in concert pitch.

That way on that instrument you could play any music in concert pitch without the need to transpose it.
If you only ever played that one instrument, you would never have to deal with the complications of transposed music.

Actually you could do this with traditional music notation.  
What's so hard about reading in B-flat major?

- "ease of switching instruments" it allows you to play different instruments in the same family while associating the same fingering with the same notes on the staff
- "home note / home key" some notes and keys are easier to play on a given instrument, or to put it differently most transposing instruments have a "home" note and key that are the easiest to play.

## Clairnote music notation and transposing instruments

So when a B-flat trumpet player reads a C on the staff they will typically think of the note as a C and yet the note that they play and hear will actually be a B-flat.

For example, typically when trumpet players read a C on the staff they play a note that they think of as a C on their instrument, but what they actually play and hear is a B-flat.

For example, the standard "B-flat trumpet" (or "trumpet in B-flat") is a transposing instrument.
By convention the music written for a B-flat trumpet is transposed up a major second (two semitones) from concert pitch.
So when a B-flat trumpet player reads a C on the staff they will typically think of the note as a C and yet the note that they play and hear will actually be a B-flat.

A saxophone player may usually play a soprano saxophone, but sometimes may also play an alto, tenor, or baritone saxophone.
Each type of saxophone has a different register or range of notes that it can produce, but they are all played with the same fingering patterns.
By transposing the music for each of these instruments, the fingering for each note on the staff remains the same for all of the instruments.

It may not make as much sense musical traditions where the musician's role is structured differently, for example jazz or folk traditions

For example, music for soprano saxophones could be written in concert pitch, and then transposed for alto saxophones, etc.

## Note range

Another aspect is to get the range of the instrument to align with the register of the staff, but B-flat is only one staff position away from C on the traditional staff, and with a staff like the Clairnote staff this is less of a concern because the staff has a wider two octave range and each octave has the same staff pattern, etc.)
