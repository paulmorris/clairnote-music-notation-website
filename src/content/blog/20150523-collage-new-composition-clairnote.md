---
title: '"Collage" – a New Composition in Clairnote'
link: https://clairnote.org/blog/2015/05/collage-new-composition-clairnote/
author: paulmorris
special_byline: ""
description:
post_id: 2913
date: 2015/05/23
created_gmt: 2015/05/23
comment_status: closed
slug: collage-new-composition-clairnote
draft: false
post_type: post
---

A talented young music student named Samuel Lam has composed a new piece for the piano in Clairnote.  This is a "first" for Clairnote!  The piece is titled "Collage" and was inspired in part by Clairnote's "chromatic staff" approach to music notation.  As Samuel put it, "I tried to incorporate lots of different intervals and my writing was very chromatic."  Check it out in these PDF files that were produced with [LilyPond](/dn/software/):

  * [Collage-LamS-Clairnote.pdf](/files/Collage-LamS-Clairnote.pdf)
  * [Collage-LamS-Clairnote-no-accidental-signs.pdf](/files/Collage-LamS-Clairnote-no-accidental-signs.pdf)
  * [Collage-LamS-Traditional.pdf](/files/Collage-LamS-Traditional.pdf)

The first two files are in Clairnote and the third is in traditional notation for comparison.  In the second Clairnote's accidental signs have been omitted for a less dense visual appearance.  Omitting them might be an appealing option for (1) a highly chromatic score like this with many accidental signs and/or (2a) for an instrument like the piano where multiple notes are played at once and (2b) there is no difference in intonation between enharmonically equivalent notes.  (See [Accidental Signs](/dn/accidental-signs/).)

When comparing the PDF files notice that the line breaks were automatically determined by LilyPond to achieve optimal spacing with each version, so they are not uniformly laid out on the page.

You can also listen to the music as you read it in the visual score.  The recording below is a performance of the piece by the composer at a recent recital.  How about that piano technique?!

<audio src="/audio/Collage-LamS-recital.mp3" controls>
  Your browser doesn't support HTML5 audio. Here is a
  <a href="/audio/Collage-LamS-recital.mp3">link to the MP3 file</a>
  instead.
</audio>

I can't resist making a couple of comparisons between the Clairnote and traditional versions.  One thing that stands out is that the Clairnote versions take up less horizontal space because of the accidental signs that are narrower and less frequent (or simply absent).  For example, take a look at measure twenty:

![min6ths-Traditional](/images/min6ths-Traditional.png)

![min6ths-Clairnote](/images/min6ths-Clairnote.png)

![min6ths-Clairnote-no-accidentals](/images/min6ths-Clairnote-no-accidentals.png)

In addition to the difference in horizontal spacing, notice how much easier it is to read this passage in Clairnote, and to recognize it as a consistent series of chromatically ascending minor sixths. (See [Intervals](/dn/intervals/).)

Here is an excerpt from measure thirty-four and thirty-five.  Notice how in the Clairnote versions it is easy to see that the first three notes in the left hand (B, D, G) are the same three notes as those in the right hand (D, B, G), just in a different order.  This is not as readily apparent on the traditional staff where the staff lines represent different notes in the bass clef and treble clef.  The Clairnote staff cycles with the octave so any given note is easy to recognize in any octave.  (See [Staff](/dn/staff/).)

![bar43-44-Collage-Traditional](/images/bar43-44-Collage-Traditional.png)

![bar43-44-Collage-Clairnote](/images/bar43-44-Collage-Clairnote.png)

Ok, that's enough in the way of comparisons for one post.  Here are the LilyPond files that produced the PDFs:

  * [Collage-LamS-Clairnote.ly](/files/Collage-LamS-Clairnote.ly)
  * [Collage-LamS-Clairnote-no-accidental-signs.ly](/files/Collage-LamS-Clairnote-no-accidental-signs.ly)
  * [Collage-LamS-Traditional.ly](/files/Collage-LamS-Traditional.ly)
  
On a closing note, this piece has a wide range and employs "8va" and "8vb" signs to transpose the staff up or down an octave.  This brought to light a bug in the <del>clairnote-code.ly</del> clairnote.ly file (the one used for rendering music in Clairnote with LilyPond).  The notes in the 8va and 8vb sections were not being transposed accurately, leaving the notes at the wrong staff positions.  This was quickly fixed, which lead to some further improvements to the code.  ...but more about that in a future post.

A big thanks to Samuel Lam for composing this piece in Clairnote (the first ever to my knowledge) and for allowing me to share it here on the blog!

Update: the "clairnote-code.ly" file was renamed "clairnote.ly" on May 15, 2017.
