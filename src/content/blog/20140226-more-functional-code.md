---
title: "More Functional Code"
link: https://clairnote.org/blog/2014/02/more-functional-code/
author: paulmorris
special_byline: ""
description:
post_id: 2380
date: 2014/02/26
created_gmt: 2014/02/26
comment_status: closed
slug: more-functional-code
draft: false
post_type: post
tags: ["LilyPond"]
---

In LilyPond news, now that I have learned something about [functional programming](https://en.wikipedia.org/wiki/Functional_programming), I have been rewriting large swaths of the scheme code in the <del>clairnote-code.ly</del> clairnote.ly file in a more functional style and making general improvements along the way. This primarily includes the code for accidentals, key signatures, and putting notes on the correct side of the stem (for smaller intervals like major seconds). To give some sense of scope, the file was previously just over 1030 lines of code and is now only about 815. These are all "under-the-hood" improvements found in the current clairnote.ly file available on the [Software:LilyPond](/dn/software/) page.

Update: the "clairnote-code.ly" file was renamed "clairnote.ly" on May 15, 2017.
