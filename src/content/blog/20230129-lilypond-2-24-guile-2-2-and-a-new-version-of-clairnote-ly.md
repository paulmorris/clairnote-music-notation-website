---
title: "LilyPond 2.24, Guile 2.2, and a new version of clairnote.ly"
author: "paulmorris"
description: "A new version of the software for creating Clairnote music notation with LilyPond (clairnote.ly) is now available. It works with LilyPond 2.24 and Guile 2.2."
date: 2023/01/29
comment_status: open
slug: lilypond-2-24-guile-2-2-and-a-new-version-of-clairnote-ly
draft: false
post_type: post
tags: [LilyPond]
---

![First two lines of a LilyPond file with \version and \include](/2023/01/version-2-24-include-clairnote-ly.png)

_"Everything changes and nothing stands still."_ — [Heraclitus](https://en.wikiquote.org/wiki/Heraclitus)

A new version of the "clairnote.ly" file is now available for download from the [Software](http://localhost:8000/software/) page.
Make sure to use this new version with the recently released LilyPond 2.24.

The last month of 2022 brought two big music notation software releases: [MuseScore 4](https://musescore.org/en/4.0) and [LilyPond 2.24](https://lilypond.org/download.html).
While the new MuseScore is quite impressive, LilyPond is still the only game in town when it comes to creating Clairnote sheet music.
At least MuseScore can export MusicXML files that can be converted into LilyPond files. (See these blog posts by Andrés Megías for more about that: [one](https://clairnote.org/blog/2021/11/creating-clairnote-sheet-music-musescore-frescobaldi-and-lilypond/), [two](https://clairnote.org/blog/2022/03/converting-standard-sheet-music-in-a-pdf-file-into-clairnote-notation/), [three](https://clairnote.org/blog/2022/07/creating-clairnote-sheet-music-burgmullers-tarantella/), [four](https://clairnote.org/blog/2022/08/creating-clairnote-sheet-music-for-guitar-tarregas-adelita/)).

The newest LilyPond brings some major updates of its own (see the [full list of changes](https://lilypond.org/doc/v2.24/Documentation/changes/index.html)).
One of them is an entirely new build system that makes it possible to offer 64-bit builds for Windows and macOS.
Installing and uninstalling is now simpler as well.
To quote the changes page: "Also, all packages are made available as simple archives that can be extracted to any 'installation' location. To uninstall, simply delete that directory."

Another big change that made LilyPond's new build system possible was upgrading the version of [Guile](https://www.gnu.org/software/guile/) that LilyPond uses, from 1.8 to 2.2.
Guile is an implementation of the Scheme programming language and LilyPond is written in a combination of C++ and Guile/Scheme.
LilyPond is designed to be highly extensible and allows users to write Guile/Scheme code to extend LilyPond's behavior and customize its output.
This powerful extensibility is what makes it possible to create Clairnote sheet music using LilyPond, and indeed if you open up the "clairnote.ly" file you will see that it contains primarily Guile/Scheme code along with a little bit of regular LilyPond text input.

So about a month ago when I learned that LilyPond was making the switch to Guile 2.2, I was curious to see if any changes to the "clairnote.ly" file would be needed.
I was a little worried since there were some potential issues mentioned on the LilyPond changes page, and I had read that it had been quite a bit of work to get LilyPond to work with the newer versions of Guile. (The releases of Guile since 1.8 have involved quite significant and impressive changes, see the release announcments for [2.0](https://www.gnu.org/software/guile/news/gnu-guile-200-released.html), [2.2](https://www.gnu.org/software/guile/news/gnu-guile-220-released.html), and [3.0](https://www.gnu.org/software/guile/news/gnu-guile-300-released.html)).

And sure enough when I installed LilyPond 2.24 and tried it with the "clairnote.ly" file there was an error that prevented the creation of Clairnote sheet music.
However, after some debugging I found that the error was not related to the switch to Guile 2.2 but just the result of a change in LilyPond's code that fixed a bug (this one: [Base accidental rules on global timing, not measure position](https://gitlab.com/lilypond/lilypond/-/commit/2151499a7ca37a8138cea630be96da5daea88159)).

In short, LilyPond lets you select one of several pre-defined [accidental styles](https://lilypond.org/doc/v2.24/Documentation/notation/displaying-pitches#automatic-accidentals) that determine when accidental signs will be shown (since there is more than one convention for when to show accidental signs).
The "clairnote.ly" file basically hooks into this feature by setting up a custom accidental style that makes sense for Clairnote music notation, where fewer accidental signs are needed than with traditional notation (see [Accidental Signs](https://clairnote.org/accidental-signs/)).

And starting with LilyPond 2.23.4 (a development release) some of LilyPond's code related to accidental styles changed so that it expected to call a function that took just three arguments instead of four.
That missing fourth argument was then causing the error in the "clairnote.ly" code.
So I just needed to update that code accordingly to follow this change and keep it in sync with LilyPond's code.
Luckily [the fix](https://gitlab.com/paulmorris/lilypond-clairnote/-/commit/0810529f7b86aca64e919d70328f6bd84ba492e3) was not that difficult.
(Ideally I would be testing against the development releases of LilyPond to catch such issues sooner, since some people prefer to use the development versions.)

Once that one error was fixed there were no other changes needed, so there were actually no changes required by the switch to Guile 2.2.
While I was at it I refactored some of the code to make it easier to understand and work with.
It is always an interesting experience to read code that you wrote years ago.

That's all for this post. As I mentioned at the top this newest version of "clairnote.ly" (version 20230104) is now available from the [Software](https://clairnote.org/software/) page.
When you upgrade to LilyPond 2.24 you will also need to upgrade your copy of the "clairnote.ly" file. Then take a moment of gratitude for all the work that goes into creating and maintaining extensible software like LilyPond that empowers users and opens up new possibilities.
