---
title: "Happy First Birthday Clairnote!"
link: https://clairnote.org/blog/2014/12/happy-first-birthday-clairnote/
author: paulmorris
special_byline: ""
description:
post_id: 2768
date: 2014/12/07
created_gmt: 2014/12/07
comment_status: closed
slug: happy-first-birthday-clairnote
draft: false
post_type: post
tags: ["Untagged"]
---

One year ago today Clairnote music notation was [introduced](/blog/2013/12/introducing-clairnote-music-notation/) to the world.  Looking back it's been quite a productive first year (thanks in part to building on previous work with TwinNote music notation).  Take a moment to savor some of the highlights as they were chronicled in the Clairnote blog.

- [180 Fiddle Tunes in Clairnote](/blog/2014/03/180-fiddle-tunes-clairnote/)
- [Clairnote Sheet Music Library](/blog/2014/06/clairnote-sheet-music-library/) offering 300 works in Clairnote
- Numerous improvements to the Clairnote code for LilyPond (e.g. [Clairnote staves by default](/blog/2014/05/staff-new-staffclairnote/), [per-staff values](/blog/2014/05/custom-staff-properties/), [improved repeat signs](/blog/2014/04/repeat-signs/), [clefs](/blog/2014/05/now-accepting-clefs/), [etc](/blog/2014/08/ledger-lines-dotted-notes-beams/).)

There has even been a refresh of the website's design a couple months ago that I haven't had the time to blog about yet (stay tuned).

Happy Birthday Clairnote, here's to many more good years ahead...

               )
              (.)
              .|.
              l8J
              | |
          _.--| |--._
       .-';  ;`-'& ; `&.
      & &  ;  &   ; ;   \
      \      ;    &   &_/
       F"""---...---"""J
       | | | | | | | | |
       J | | | | | | | F
        `---.|.|.|.---'

Incidentally, I just noticed that a birthday of 12/7 is particularly appropriate (for a notation system with a chromatic staff that represents twelve notes per octave instead of the traditional seven). Thanks to <http://chris.com/ascii/> for the cake.
