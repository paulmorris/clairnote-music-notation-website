---
title: "Introducing Clairnote SN"
link: https://clairnote.org/blog/2018/04/introducing-clairnote-sn/
author: paulmorris
special_byline: ""
description: Learn about "Clairnote SN" a new alternative music notation system that is easier to learn along with traditional music notation.
post_id: 3839
date: 2018/04/05
created_gmt: 2018/04/05
comment_status: open
slug: introducing-clairnote-sn
draft: false
post_type: post
tags: ["Music Notation", "Clairnote Website"]
---

Introducing Clairnote SN — a variant of Clairnote that doesn't use hollow and solid noteheads for pitch. The "SN" is for "standard noteheads". If Clairnote SN were a beverage it might be called "Clairnote Lite", but a picture is worth a thousand words.

![](/images/Clairnote-DN-and-Clairnote-SN.svg)

As you can see, Clairnote SN uses the traditional rhythm/duration system without any modifications, whereas "regular" Clairnote (a.k.a. "Clairnote DN" for "dual noteheads") departs from that system by giving half notes double stems.

## So, what is the point of Clairnote SN?

First, and most importantly, it is easier to learn Clairnote SN along with traditional notation, because it embraces the traditional rhythm system, using solid and hollow noteheads to indicate duration. There is less friction or cognitive dissonance when moving between these systems, and that makes it easier to use Clairnote SN "bilingually" as a second language/notation system, the way someone might learn to read tablature or shape note notation in addition to standard notation.

Second, Clairnote SN may appeal to different (or more?) musicians than Clairnote DN, particularly on first glance, since it is less of a departure from the familiar and doesn't require learning new meanings for existing symbols (solid/hollow noteheads). More musicians might be willing to give it a try.

Finally, some may find it more "aesthetically," "conceptually," or "symbolically" satisfying. For example, when all notes of the same duration are visually similar, then a run of eighth notes would all be solid and not switch back and forth between solid and hollow noteheads. On this point reasonable people may disagree. The power of the solid/hollow distinction is that it is so visibly prominent. It works quite well when used to help visually distinguish the pitches of notes, as in Clairnote DN. But which notes are solid and hollow is not particularly musically significant from a listener's standpoint. When you hear a scale you don't hear a dramatic difference between the notes that are solid and those that are hollow. Of course they encode differences in interval relationships but they all just sound like different notes in the scale. (Does the listener's standpoint matter for the performer's task of reading and playing? Maybe or maybe not...) So on the one hand the strong signal it sends is a good thing, and on the other, the signal it sends may be stronger than it needs to be, in a way that detracts from the music reading process, at least for some readers.

In any case, Clairnote SN provides the same primary benefits as Clairnote DN (when compared with the traditional system), so both approaches are worth exploring, especially since one or the other variant will appeal to different people with different preferences and/or priorities.

## 6-6 Pitch Pattern

Clairnote SN still has a 6-6 pitch pattern, but instead of alternating hollow and solid noteheads it has alternating line and space notes. This is also a point of similarity with the traditional system where the notes on the staff also alternate regularly between lines and spaces. Clairnote SN uses ledger lines to provide this alternating line/space pattern, as you can see in this illustration:

![Clairnote SN 6-6 Whole Tone Scale](/images/Clairnote-SN-66-whole-tone-scale.svg)

This makes interval patterns apparent, but in a more subtle way than solid and hollow notes. For example, notes a major third apart are either both line notes or both space notes. Notes a minor third apart are always a line note and a space note.

![](/images/Clairnote-SN-intervals-major-3rds.svg)

![](/images/Clairnote-SN-intervals-minor-3rds.svg)

## Ledger Line Pattern

Working out the best ledger line pattern was an interesting challenge. I considered the traditional system's approach where ledger lines simply accumulate as you go past the last line of the staff. But that ends up being a lot of ledger lines to visually take in on a chromatic staff, and doesn't cohere with the major-third spacing of the staff lines.

So one goal was to have the ledger pattern repeat/extend/align with the staff lines and their major third spacing (as is the case in Clairnote DN). Another goal was to minimize how much readers would have to rely on distinguishing minute differences between physical distances between ledger lines in order to identify notes — in particular, the difference between lines a major third apart and lines a major second apart.

With these goals in mind, what I came up with was similar to [Clairnote DN's ledger pattern](/blog/2016/04/ledger-lines-for-solid-space-notes/), where the notes D, F#/Gb, and A#/Bb are always _between_ two lines or ledger lines. In Clairnote SN these notes also appear on a ledger line — one always situated between two other lines.

![](/images/Clairnote-SN-ledger-pattern.svg)

With this pattern some ledger lines repeat/extend/are aligned with the staff lines and their major third spacing, and other ledger lines always appear between either staff lines or the ledger lines aligned with the staff lines. Outside the staff, the "between" ledger lines appear less often, for only one out of four notes, while the ledger lines aligned with the staff lines appear more often, for basically four out of four notes.

The subtle difference between these two types of ledger lines helps with orientation and with distinguishing notes and intervals. It also means that musicians don't have to rely as much on discerning the distances between ledger lines, which could get problematic with hand-written notation. For example, when a note falls on a ledger line outside the staff it is either between two (ledger) lines or it isn't (e.g. when it's a major second or a major third past the last staff line, respectively).

This probably deserves more visual illustrations, but this explanation at least sheds light on the thinking behind Clairnote SN's ledger pattern.

## A Clairnote variant for different musicians and different use cases

I've been experimenting with Clairnote SN since October 2017. Although the difference between line and space notes is not as dramatic as between hollow and solid notes, I can still distinguish notes and intervals without any difficulty. It helps that diatonic music/scales have more whole steps than half steps.

![](/images/Clairnote-SN-scales-C-major-whole-steps-half-steps.svg)

![](/images/Clairnote-SN-scales-C-minor-whole-steps-half-steps.svg)

Over the years, some of the people I've talked with about Clairnote really resist the idea of using hollow/solid notes for pitch. Maybe it is just too unfamiliar for (some of) those who are used to the traditional system. I haven't had any noticeable difficulty when switching between reading Clairnote (DN) and the traditional system, but I'm sympathetic to those who would like to be able to move back and forth between them with minimal friction or cognitive dissonance.

At this point I have expanded the Clairnote website and related resources to cover both Clairnote (DN) and Clairnote SN more or less equally. That includes offering all of the same sheet music for both variants, and allowing the "clairnote.ly" file (that extends LilyPond) to optionally produce Clairnote SN sheet music. (You just add a line with `clairnote-type = sn` before the line with `\include "clairnote.ly"`. This is documented on the [LilyPond](/software/) page for Clairnote SN.) On the website, there are now basically two parallel sites, one for each variant. You can switch between them from almost any page by using the link in the “Clairnote [SN] Music Notation” menu in the main navigation bar. In the process, I also spruced up the site’s home page, including adding a fancy animated GIF showing all the key signatures in Clairnote and traditional music notation. All of this was quite a bit of work, so it’s good to have it wrapped up.

Now there are two great Clairnote variants to choose from, depending on your preferences and priorities. See also the [Clairnote DN and Clairnote SN](/dn/clairnote-dn-clairnote-sn) page for more discussion, and don’t hesitate to [let me know](/about/#contact) what you think about Clairnote SN.

## Comments

**[Doug Keislar](#1099 "2018-04-06 23:07:10"):** Great post, Paul; very clearly presented, as usual! You've grappled with the classic dilemma between introducing an optimal a system as possible, on the one hand, and retaining old features (to ease the transition for people familiar with the old), on the other. For people who aren't familiar with the old system, the optimal system (Clairnote DN) is better than the compromise system (Clairnote SN), in my opinion. Your solution to the dilemma: satisfy both needs, via two systems. You make good arguments for this approach.

Of course, there is really a spectrum of possible notation systems in the range between traditional notation and what one might consider an optimal solution disregarding backward-compatibility. For example, one could argue that even a chromatic staff is too big a leap for traditionalists but octave similarity is not, in which case a system like Classic Nydana (http://nydana.se/classic.html) might meet one's needs.

One drawback of Clairnote SN that I don't think you mentioned is that the use of ledger lines (where Clairnote DN doesn't have them) can work against the perception of intervals for people who are familiar with traditional notation. For example, in your first picture above, which shows the same music in TN, Clairnote SN, and Clairnote DN, the second triad (Bb major) really doesn't look like a triad in Clairnote SN. Its bottom interval looks like a fifth, not a third, because of the ledger line in between the two noteheads. Since one of your stated goals with Clairnote is to facilitate intervallic perception, this seems like a rough patch. I wonder whether there's some other way besides ledger lines to indicate the in-between pitches.

**[Paul Morris](#1100 "2018-04-11 03:49:00"):** Thanks for your always astute thoughts Doug! Indeed this is a classic dilemma... Good point that for some a chromatic staff is "a bridge too far" and that Classic Nydana is a well designed system that could suit them. Although when it comes to bilingually reading the traditional system, Classic Nydana also breaks with the traditional use of hollow and solid noteheads... Some kind of "SN" version of it would be possible, I suppose. Even closer to traditional notation are systems like Simplified Music Notation (http://www.simplifiedmusicnotation.org/). Lots of options.

Fair point about how ledger lines in Clairnote SN can affect interval appearance and recognition, especially for those that are new to the system and familiar with the traditional one. Unfortunately, when designing these systems "there's no free lunch" and there are inevitably some trade-offs, but you can still come out ahead. In Clairnote SN there is (at least) substantial consistency to the line pattern so that with a little use and familiarity you can learn to see / recognize intervals, in relation to the dominant major third spacing of the staff and ledger lines. (You can try it here: https://clairnote.org/learn/) (Other possibilities for indicating in-between pitches would be introducing different shapes or colors for the noteheads, but those would introduce different trade-offs and are arguably more dramatic breaks with the traditional system.) Thanks again for weighing in!
