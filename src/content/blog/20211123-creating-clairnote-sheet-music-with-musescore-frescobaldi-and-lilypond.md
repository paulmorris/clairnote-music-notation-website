---
title: "Creating Clairnote Sheet Music with MuseScore, Frescobaldi, and LilyPond"
author: "andres_megias"
special_byline: "By Andrés Megías (edited by Paul Morris)"
description: "Learn how to use LilyPond and Frescobaldi to automatically convert traditional sheet music from MuseScore into Clairnote music notation."
date: 2021/11/23
comment_status: open
slug: creating-clairnote-sheet-music-musescore-frescobaldi-and-lilypond
draft: false
post_type: post
tags: ["MuseScore", "Frescobaldi", "LilyPond"]
---

Although one can create sheet music in Clairnote music notation with just
LilyPond using the [clairnote.ly](https://clairnote.org/software/) file, it may
not be easy or fast if you are not familiar with LilyPond syntax, which can
take some time to learn.

Fortunately, this is not the only way to create Clairnote sheet music.
This post explains another way to easily do this task using MuseScore, a
graphical sheet music editor, and Frescobaldi, a LilyPond sheet music text
editor. They are both open source, free to download, and available for Linux,
Windows, and Mac OS.

In this post we will:

- Review how to create Clairnote sheet music with LilyPond and Frescobaldi.
- Explain how to create traditional sheet music with MuseScore.
- Show how to use Frescobaldi to convert traditional sheet music from MuseScore
  into LilyPond format and then into Clairnote music notation.
- See how to make some small changes to improve the final score.

With this approach you can work primarily with MuseScore – using its familiar
graphical user interface to enter notes and edit the music. Then you
only need to use Frescobaldi and LilyPond to convert the piece into Clairnote
music notation and make any final adjustments.

Additionally, this approach allows you to convert any sheet music that already
exists in MuseScore (or MusicXML) format into Clairnote music notation.
That includes the scores available for download from
[musescore.com](https://musescore.com), but note that not all of them can be
downloaded
([more details](https://help.musescore.com/hc/en-us/articles/209542669-Download)).

## LilyPond

[LilyPond](https://clairnote.org/software/) is a free software program for
creating scores using plain text documents written in a specific LilyPond
syntax. You can easily try it yourself with
[LilyBin + Clairnote](https://lilybin.clairnote.org), which is a version of a
web app called LilyBin that lets you edit the plain text file online.

As you will see, you use letters and numbers to denote the pitch
and duration of each note. For example, let’s consider the very first few
measures of Beethoven’s _Für Elise_, which looks like this in traditional
notation:

![Excerpt from Beethoven's Für Elise in traditional notation](/2021/11/1/fur-elise-excerpt-trad.png)

<!-- was furelise1.png -->

The corresponding LilyPond file for writing this sheet music would be:

```
\score {
  {
    \clef "treble"
    \key a \minor
    \time 3/8
    \partial 8
    e''16 dis''
    | e16'' dis'' e'' b' d'' c''
    | a'8 r16
  }
  \layout{}
}
```

Here is a brief explanation of what each line after `\score` is specifying:

- The clef of the piece (treble or G clef).
- The key (A minor).
- The time signature (3/8).
- The anacrusis measure.
- The notes of the first measure.
  - Quotation marks specify the octave.
  - Numbers specify the duration of the current and following notes.
  - The 'is' means sharp. (The default input language for note names is
    nederlands/Dutch.)
- The notes of the second measure.
  - The vertical bars (`|`) tell LilyPond to warn you if the duration of the
    notes does not match the duration of the measure.
- The notes of the third measure, which is incomplete ("r" means rest).

To convert it to Clairnote notation, one just has to add
`\include "clairnote.ly"` at the beginning of the file, which will create
the following output:

![Excerpt from Beethoven's Für Elise in Clairnote SN](/2021/11/1/fur-elise-excerpt-clairnote-sn.png)

<!-- was furelise2.png -->

## Frescobaldi

If you want to use LilyPond offline on your computer, you can use
[Frescobaldi](https://frescobaldi.org/), a free software music editor for
creating and displaying ("engraving") LilyPond files. It has a graphical user
interface that simplifies the process of making sheet music with LilyPond
– but only a little bit. The following screenshot shows Frescobaldi
with the same musical excerpt from Für Elise.

![Excerpt from Beethoven's Für Elise in Frescobaldi](/2021/11/1/fur-elise-excerpt-frescobaldi.png)

<!-- was furelise3.png -->

After clicking on the button with the LilyPond symbol (the water lily), at the
top of
the window, the sheet music is displayed in the right panel. If the file
was previously saved (third buttom from the left), a PDF file is generated in
the same folder in which the text file (with extension ".ly") is
located. Note that you will need to have the clairnote.ly file in the same
folder in which you have the LilyPond file (which in the example above is
called "furelise.ly").

(Alternatively, the clairnote.ly file can be located anywhere LilyPond or
Frescobaldi can find it
([more details](https://lilypond.org/doc/v2.22/Documentation/notation/including-lilypond-files)).
For example, in Frescobaldi if you go to "Preferences", and then "LilyPond
Preferences", you can set up a "LilyPond include path" that points to any
directory that contains the clairnote.ly file.)

Those are the basics of working with LilyPond and Frescobaldi. It may not
seem so hard, but this was just a simple and short excerpt of a piece
with only one staff. What if we want to create a full piano score, for example?
Let’s see another way to do it.

## MuseScore

[MuseScore](https://musescore.org) is a free software sheet music editor for
creating scores that has a graphical user interface, similar to a graphical
word processor, such as LibreOffice. Most people are more familiar with this
kind of user interface, so using MuseScore is pretty easy and
intuitive and there is an abundance of
[documentation and videos](https://musescore.org/en/handbook) available.

Now we are going to use as an example a piano piece called _Innocence_, written
by Friedrich Burgmüller. The following image shows this piece being edited in
MuseScore.

![Burgmüller's Innocence in MuseScore](/2021/11/1/musescore-with-innocence.png)

<!-- was innocence1.png -->

As you can see, the score is displayed visually and it fills the
majority
of the window. On the top, you can click on the buttons to select the
duration of the note to be added. Also, you can select an existing note and
change its duration by clicking on these buttons. Similarly, the pitch of the
notes can be selected and changed with the mouse. On the left, you have several
menus (palettes) with different tools and symbols, like clefs or barlines, that
can be added to the score just by clicking and draging them onto it.

## Converting to Clairnote

Once the sheet music is finished, MuseScore can export it to a PDF file,
but instead we want to convert it to Clairnote notation. To do that we first
export our score in [MusicXML](https://www.musicxml.com/) file format, which is
a standard format for digital sheet music files. MuseScore offers two
options: uncompressed MusicXML (.musicxml) or compressed MusicXML (.mxl). Either
option is fine, but for the sake of simplicity let’s select the compressed one.

Now we switch back to Frescobaldi where we will import the MusicXML file.
Click on "File" > "Import" > "Import MusicXML" and select the MusicXML file
created with MuseScore. A window with various options will pop up:

![Frescobaldi import MusicXML dialog](/2021/11/1/frescobaldi-import-musicxml.png)

<!-- was innocence2.png -->

Generally, you should use the same options shown in the image above.
Note that, in the “Command line” box, at the bottom, we have added the text
`--nrp --nsd --nsb --npb`. Here is the meaning of these options:

- `--nrp` ("no rest positions") Do not import the the exact vertical
  positions of rests.
- `--nsd` ("no stem directions") Do not import the directions of note stems.
- `--nsb` ("no system breaks") Do not import the system break points at the
  end of each staff row.
- `--npb` ("no page breaks") Do not import the page break points at the end of
  each page.

With these options LilyPond will automatically determine the best
rest positions, stem directions, system breaks, and page breaks, by taking into
account the way the music is represented in Clairnote music notation.
(See LilyPond's documentation for [musicxml2ly](http://lilypond.org/doc/v2.22/Documentation/usage/invoking-musicxml2ly).
Frescobaldi uses `musicxml2ly` when importing MusicXML files.)

Next click the "Run musicxml2ly" button.
Frescobaldi will convert the MusicXML file (.xml) into a LilyPond (.ly) file
and open it. You should then see something similar to this:

![Burgmüller's Innocence in Frescobaldi in traditional notation](/2021/11/1/frescobaldi-with-innocence-trad.png)

<!-- was innocence3.png -->

Now we just have to add the line `\include "clairnote.ly"` at the beginning of
the LilyPond file, save the file, and click the water lily button to render it.
_Voilá_! Now we
see the sheet music displayed in Clairnote notation. Moreover, a PDF file
will have been created in the working directory.

![Burgmüller's Innocence in Frescobaldi in Clairnote SN](/2021/11/1/frescobaldi-with-innocence-clairnote-sn.png)

<!-- was innocence4.png -->

## Tweaking the Appearance of the Score

The resulting sheet music should be fine, but there are usually some little
details that can be modified to make it look better.

Before starting with the edits, look at the beginning of the file, at the line
`\pointAndClickOff`. We don’t want the effect of this line, so let’s comment it
out by adding a “%” at the beginning of the line (`% \pointAndClickOff`).
This way LilyPond and Frescobaldi will ignore that line.
Now, if you place the cursor over a certain part of the sheet music in
the right-side panel and click on it, the left-side panel will jump right to
the corresponding part of the LilyPond text. This will be a very useful tool to
find and edit specific parts of the music.

(Later, after you are all done editing the file, it is best to remove the "%"
and re-generate the PDF file, because using `\pointAndClickOff` produces a
smaller PDF file that does not have any links back to the LilyPond text file.
Also, these links can be confusing for anyone who happens to click on the
symbols in the PDF file.)

Now let's start with the changes. Suppose you want to
show the name of the instrument in front of only the first system.
Unfortunately, this is one of the few things that cannot be selected with the
cursor. So, to change this you have to go to the end of the text file,
to the part below the comment `% The score definition`.
You can search for it clicking on "Edit" then "Find", or pressing
"Control" + "F" on your keyboard.

```
% The score definition
\score {
  <<

    \new PianoStaff
    <<
      \set PianoStaff.instrumentName = "Piano"
      \set PianoStaff.shortInstrumentName = "Pno"

      \context Staff = "1" <<
        \mergeDifferentlyDottedOn\mergeDifferentlyHeadedOn
        \context Voice = "PartPOneVoiceOne" {  \PartPOneVoiceOne }
      >> \context Staff = "2" <<
        \mergeDifferentlyDottedOn\mergeDifferentlyHeadedOn
        \context Voice = "PartPOneVoiceFive" {  \PartPOneVoiceFive }
      >>
    >>

  >>
  \layout {}
  % To create MIDI output, uncomment the following line:
  %  \midi {\tempo 4 = 120 }
}
```

Below the line `\new PianoStaff` you can see these lines:

```
\set PianoStaff.instrumentName = "Piano"
\set PianoStaff.shortInstrumentName = "Pno"
```

Just change the text between
double quotation marks, leaving the last one empty (`""`), and render the piece
again to see
the change. Now you will see that there is a lot of space on the left margin,
since we have deleted the "Pno." text. To change this, go to the beginning of
the text file, to the section starting with `\paper`, and reduce the value of
the variable named `short-indent` to, for example, 0.2 centimeters
(`short-indent = 0.2 \cm`).
(For more details see the LilyPond docs on
[Instrument Names](https://lilypond.org/doc/v2.22/Documentation/notation/writing-parts#index-short_002dindent)
and
[\paper variables for shifts and indents](https://lilypond.org/doc/v2.22/Documentation/notation/horizontal-spacing-paper-variables#index-short_002dindent-2).)

There are some other things that you may want to change in this section. If you
want to increase the space between systems (the pair of piano staffs), add a
variable named `system-system-spacing.padding`, and try setting it to different
values.
To get the last measure to not be displayed at the right of the
page, add a variable called `ragged-last` and assign its value to `##t`
(which means _true_). To disable it again, you can assign its value to `##f`
(which means _false_). Similarly, if you want the last system to be
displayed at the very bottom of the last page, you can add a variable called
`ragged-last-bottom` and assign its value to `##f` (false).

Below you can see this section of variables with the values we chose for this
piece.

```
#(set-global-staff-size 20)
\paper {
  paper-width = 21.01 \cm
  paper-height = 29.69 \cm
  top-margin = 1.0 \cm
  bottom-margin = 1.0 \cm
  left-margin = 1.0 \cm
  right-margin = 1.0 \cm
  indent = 1.6 \cm
  short-indent = 0.2 \cm
  system-system-spacing.padding = 0.5 \cm
  ragged-last = ##t
}
```

(For more details see the LilyPond docs on
[Fixed vertical spacing \paper variables](https://lilypond.org/doc/v2.22/Documentation/notation/fixed-vertical-spacing-paper-variables)
and
[Flexible vertical spacing \paper variables](https://lilypond.org/doc/v2.22/Documentation/notation/flexible-vertical-spacing-paper-variables).)

You may also want to change the text at the bottom of the paper, which says
something like "Music engraving by LilyPond 2.22.0" (showing the version
of LilyPond you are using). This is controlled in the section
`\header`, just above the section `\paper`. Add a variable called `tagline` and
assign it a text between double quotation marks. If you don’t want any text,
just use empty double quotation marks: `tagline = ""`.

```
\header {
  encodingsoftware =  "MuseScore 3.2.3"
  encodingdate =  "2021-10-26"
  subtitle =  "25 Easy Studies - Opus 100"
  composer =  "Friedrich Burgmüller"
  title = "Innocence"
  tagline = ""
}
```

Finally, there is a little detail about the tempo text, which in this
example is _Moderato_. If you go back to the last image, you will see that this
word is displayed just above the first note, left-aligned. But the tempo text
is usually displayed aligned a little bit more to the left, as in the
screenshot of MuseScore. To change this, we need to go to the beginning of the
section of the file where the notes of the right hand staff are specified,
To jump to the corresponding part of the LilyPond text, move the cursor over
the word “Moderato” on the right panel and click on it. Alternatively, you
can search for the word "Moderato" by using "Control" + "F".

```
PartPOneVoiceOne =  {
  \repeat volta 2 {
    \clef "treble" \key f \major \time 3/4 | % 1
    a''16 ( -4 [ _\p _\markup{ \small\italic {gracioso} } ^\markup{
      \bold {Moderato}
    } g''16 f''16 e''16 ) ] f''16 ( -4 [ e''16
```

Here, you have to remove the text `^\markup{ \bold {Moderato} }` and then add
a line with the text
`\tempo "Moderato"` after the one that starts with `\clef`.

```
PartPOneVoiceOne =  {
  \repeat volta 2 {
    \clef "treble" \key f \major \time 3/4 | % 1
    \tempo "Moderato"
    a''16 ( -4 [ _\p _\markup{ \small\italic {gracioso} }
    g''16 f''16 e''16 ) ] f''16 ( -4 [ e''16
```

This way LilyPond will treat the "Moderato" as a tempo indication instead
of as a generic "markup" text. (For more details see the LilyPond docs for
[Metronome marks](https://lilypond.org/doc/v2.22/Documentation/notation/displaying-rhythms#metronome-marks)
and
[Writing text](https://lilypond.org/doc/v2.22/Documentation/notation/writing-text).)

For this piece, the tweaks discussed above are enough, and finally we
will get a PDF file like the image below. For more complicated pieces, there
may be more details that need to be changed. Those will have to wait for
another post.

![Burgmüller's Innocence in Clairnote SN](/2021/11/1/burgmuller-innocence-clairnote-sn.png)

<!-- was innocence5.png -->

In this post we covered two ways to create sheet music in Clairnote music
notation. The first is by editing LilyPond files directly using
Frescobaldi. The second is by using MuseScore to create traditional sheet
music files and then importing them into Frescobaldi to convert them to
Clairnote. While there are benefits and drawbacks to each approach, the second
one may work better for people who prefer to use a graphical user interface
like MuseScore's. Either way, here are some links to these applications if
you would like to give it a try:

- [MuseScore](https://musescore.org)
- [Frescobaldi](https://www.frescobaldi.org/)
- [LilyPond](http://lilypond.org/)

And here are the final files for Burgmüller's _Innocence_.

- [Innocence - final PDF file](/2021/11/1/innocence.pdf)
- [Innocence - final LilyPond file](/2021/11/1/innocence.ly)

And finally, the files as they were before we made any changes:

- [Innocence - MusicXML file exported from MuseScore](/2021/11/1/innocence.mxl)
- [Innocence - LilyPond file after conversion from the MusicXML file](/2021/11/1/innocence-xml.ly)
