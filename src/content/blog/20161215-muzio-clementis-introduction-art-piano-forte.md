---
title: "Muzio Clementi’s Introduction to the Art of the Piano Forte"
link: https://clairnote.org/blog/2016/12/muzio-clementis-introduction-art-piano-forte/
author: paulmorris
special_byline: ""
description:
post_id: 3412
date: 2016/12/15
created_gmt: 2016/12/15
comment_status: closed
slug: muzio-clementis-introduction-art-piano-forte
draft: false
post_type: post
tags: ["Sheet Music", "LilyPond"]
---

While Clairnote's [Sheet Music Library](/dn/sheet-music/) now has over 500 pieces of music wouldn't it be nice to have sheet music specifically for those who are learning to play an instrument — say an introductory lesson book for piano?  Good news!  There is now just such a piano lesson book in Clairnote music notation.

Searching through [IMSLP](http://imslp.org/) to see what piano lesson books were available in the public domain, I was delighted to find one that was already available from the [Mutopia Project](http://www.mutopiaproject.org/), encoded as LilyPond files: Muzio Clementi’s _[Introduction to the Art of the Piano Forte](http://www.mutopiaproject.org/cgibin/piece-info.cgi?id=1938)._ (Many thanks to Javier Ruiz-Alma for typesetting this Mutopia Project edition!)

![Cover of Clementi Art of Playing on the Piano Forte](/images/cover-of-clementi-art-of-playing-the-piano-clairnote.png)

After some editing and updating to the latest version of LilyPond, I was able to automatically convert it to Clairnote music notation.  I omitted an introductory section titled "Music Notation" (19 pages) since it would need to be rewritten to be relevant for Clairnote.  The remaining content is still about 120 pages long, including another brief introductory section titled "Fingering," followed by fifty lessons that each focus on a different piece of music.

Originally published in 1801, its pedagogical approach is certainly dated and the learning curve is quite steep.  However, the music is high quality, including works by the following composers:  C.P.E. Bach, J.S. Bach, Beethoven, Couperin, Corelli, Cramer, Dussek, Handel, Haydn, Mozart, Paradies, Pleyel, Rameau, and Scarlatti.

You can download the PDF file for free from the [More Sheet Music](/dn/sheet-music/) page under "Collections" in A4 or Letter paper sizes.  (Also on that page are four Christmas Carols that may be of interest given that the holidays are coming up soon.)

Additionally, in the [Sheet Music Library](/dn/sheet-music/) you'll find a number of Czerny's _160 Eight-Measure Exercises_ for practicing piano, and if you play trumpet or cornet, there is Arban's _Method for the Cornet, First Studies_.  You can quickly find these kinds of works by filtering for "Technique" under styles.  Enjoy!
