---
title: "Test Draft Post"
author: paulmorris
special_byline: ""
description:
date: 2018/12/01
comment_status: "open"
slug: test-draft-post
draft: true
post_type: post
tags: ["Clairnote Website"]
---

This is a test draft post. A [Link](https://clairnote.org).
