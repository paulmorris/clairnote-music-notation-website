---
title: "An Argument for Alternative Accidental Signs"
author: paulmorris
special_byline: ""
description: "Learn about the benefits of using alternative accidental signs in Clairnote music notation and similar systems that have a chromatic staff."
date: 2021/05/27
comment_status: open
slug: an-argument-for-alternative-accidental-signs
draft: false
post_type: post
tags: ["Music Notation"]
---

Are Clairnote's alternative accidental signs just
a useless and unnecessary remnant held over from standard music notation?
Are they really needed when a note's position on the staff is sufficient to
convey which note to play on your instrument?
Wouldn't it be better to avoid using them, if at all possible?
At first glance they may seem like a pointless distraction, like visual "noise"
that just gets in the way and complicates the task of reading music.
"On the piano I'm going to play the same key whether a note's an F-sharp
or G-flat, so why clutter up the score?"

![](/images/Clairnote-accidentals-G.svg)

Of course,
one can always omit Clairnote's alternative accidental signs,
but there are also several good reasons to use them.
The most obvious is the ability to distinguish between
"enharmonic equivalents" (in 12 tone equal temperament),
e.g. the difference between F-sharp and G-flat.
That makes it possible to use standard note names without any indeterminacy,
which facilitates communication
and provides compatibility with traditional music theory.
It also preserves all the information contained in historical scores,
preventing any loss of a composer's intentions.
However, there is an even more basic function
performed by these accidental signs
that may be more relevant for some musicians,
in some situations and contexts.
This is simply the ability to easily identify notes
that are not in the current key,
regardless of their note name,
or whether they are a sharp, flat, or natural.

Is it better to indicate explicitly and directly which notes are accidentals,
(notes that fall outside of the current key),
using alternative accidental signs?
Or is it better
to let the difference between accidental and non-accidental notes
remain implicit, communicated indirectly?
After all, in Clairnote whether a given note is in the key or not
can still be determined by checking the current (alternative) key signature.
Even if the accidental signs are omitted,
that information about a note's status is not completely lost,
it's just not explicitly marked.

However, consider that indicating which notes are accidentals
also conveys which notes are _not_ accidentals.
Explicitly marking the notes that fall outside of the current key,
means that all the all the other (unmarked) notes are in the current key.
And knowing that "by default" the notes that appear on the staff
will be in the current key
is a very useful thing,
especially when reading (diatonic) music on a staff
that is not based on a diatonic scale.

Let's consider the best possible scenario for traditional music notation,
with its staff that _is_ based on a diatonic scale.
The music would be diatonic/tonal music with few if any accidentals.
The musician would already be quite experienced,
having practiced and mastered all of their scales,
having memorized how to play only the notes in the current key,
whatever it might be,
in an automatic way, via "muscle memory",
without having to think about it, without any conscious effort.
If they were a piano player, then playing in any key,
involving any number of sharps or flats (the black keys),
would be just as easy as playing in C major with no sharps or flats
(only the white keys).
At that point, the sharps or flats in the key signature
would just be a reminder of something the musician had already internalized.
Reading music in any given key would be just as easy as reading it in any other.

In this scenario there are seven notes per octave on the staff
(e.g. C, D, E, F, G, A, B for the key of C major),
and seven corresponding
notes per octave "in the musician's fingers" &mdash;
in their muscle memory and mind.
The musician can assume
that those are the only notes that they will be seeing or playing. If there is
an exception to that assumption, they will be alerted via an accidental sign
(e.g. sharp, flat, natural). This simplifies things because it means there are
five notes per octave on their instrument that they can ignore (until
there's an accidental sign). Ideally, their fingers have already learned to
ignore those notes when playing in that key,
and the notation system only presents them with
the seven notes per octave that they will be playing.
Reading music then primarily involves
only seven notes per octave &mdash;
visually on the staff, mentally in the mind, and physically/kinetically
in the relationship between fingers and instrument.

Now consider Clairnote and how things are different
with a chromatic staff.
The music may still only have seven notes per octave, but now there are twelve
note positions per octave on the staff.
The musician has to visually take in those twelve staff positions per octave,
and so, to some degree, will be mentally anticipating those twelve possible
notes, even though five of the positions are rarely used in any given key.

Perhaps that's a non-issue, especially
if the musician's fingers have already internalized how to play
using only the seven notes in the key. However, if they
could know for sure that the music had no accidentals,
they could assume that every note they saw on the
staff was one of the seven in the key, the ones their fingers are intuitively
expecting to play. That would reduce any mental uncertainty
and eliminate any slight hesitation that might occur to
recognize whether a given note was an accidental or not.

But how can a musician know there are no accidentals? Or know which notes are
accidentals when they occur?
This is where Clairnote's alternative
accidental signs come in.
When reading a score that has them
you can safely assume that all the notes you will see on the
staff will only ever be the seven in the current key, unless there's an
explicit accidental sign to indicate otherwise. That brings things much closer
to the ideal scenario for traditional music notation.
As with traditional notation, reading music is simplified
by focusing on only the seven notes per octave in the current key
&mdash; visually, mentally, and physically/kinetically.

Without the alternative accidental signs,
there are fewer symbols on the staff to read,
but reading music is arguably more
difficult since there are twelve potential notes per octave and no direct
explicit visual cue indicating whether any given note
is one of the seven in the current key —
the seven notes that experienced fingers anticipate playing.
Visually it's as if any note might be an accidental.
Perhaps some musicians can get really good at remembering which
seven of the twelve notes are in the current key (for any given key)
so that it becomes immediately obvious to them
when they see a note that it is an accidental (or not),
automatically and instantaneously, without hesitation.
However, using the alternative accidental signs already provides that advantage
for anyone.
Having some additional symbols on the staff seems like a small price to pay
for that benefit, not to mention the other benefits.

When designing or assessing a music notation system there is often
a question of what should be made explicit and what left implicit,
whether something should be indicated directly or indirectly.
These design decisions often involve trade-offs of one kind or another.
In this case explicitly and directly indicating when notes are not in the
key (and conversely that all other notes are in the key),
offers considerable benefits and few disadvantages.
And for those who disagree it is always possible to omit the accidental signs
from a score.

In closing, consider also that fewer accidental signs are needed
on Clairnote's chromatic staff than on the traditional diatonic staff.
And unlike traditional accidental signs,
musicians can ignore Clairnote's accidental signs
and still play the correct notes.
Thus they are less cluttering and burdensome than
traditional accidental signs.
For more on these points see [Accidental Signs](/accidental-signs/).

_This post was inspired by some recent exchanges on the
[Music Notation Project](http://musicnotation.org/)'s discussion group._
