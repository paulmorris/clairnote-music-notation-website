---
title: "Transposing Instruments and Clairnote Music Notation"
author: "paulmorris"
description: "Learn about how music is transposed for transposing instruments and how to simplify and improve upon this convention with Clairnote music notation."
date: 2022/12/03
comment_status: open
slug: transposing-instruments-and-clairnote-music-notation
draft: false
post_type: post
tags: [Music Notation]
---

It might seem strange but in some cases, when reading traditional music notation, the notes that musicians read on the staff are not actually the notes that are heard when musicians play them.
For example, a C on the staff might actually represent a B-flat note.

But how could that ever be a good idea or make any sense?

In this post we will cover transposing instruments and the curious convention of writing the music for these instruments so that the written notes are transposed and no longer represent the sounding notes.
We will explore a modified version of this "transposed music" convention, to be used with Clairnote music notation, that would allow more musicians to read and play music written in concert pitch, so that they are not limited to reading and playing music that has been transposed especially for their instrument.
We will see how Clairnote notation makes it possible to write music in concert pitch for more of the instruments that tend to be played more often by more people.

## What are transposing instruments?

Music is usually written in concert pitch which means the notes on the page represent the notes that are played and heard.
For example, when reading piano music a C on the staff is played as a C on the keyboard and sounds as a C.

However, by convention the music for transposing instruments is not written in concert pitch. Instead the written music is transposed so that the notes on the staff are different from the notes as they sound.
One example of a transposing instrument is the most common type of trumpet, a B-flat trumpet (or "trumpet in B-flat").
Sheet music for B-flat trumpet is transposed up a major second (two semitones) from concert pitch so that a C on the staff will actually sound as a B-flat.
The trumpet player will generally think of the note as a C, even if they are aware in the back of their mind that it is really a B-flat.

Similarly, sheet music for the less common D trumpet is transposed down a major second (two semitones) from concert pitch, so that a C on the staff actually represents a D sounding note.
The less common C trumpet, however, is not a transposing instrument because its music is written in concert pitch where the written and sounding notes are the same.

For example, in traditional notation:

![](/2022/12/transposing-instruments-chart-tn-trumpets.svg)

And in Clairnote music notation:

![](/2022/12/transposing-instruments-chart-clairnote-trumpets.svg)

There are many transposing instruments, including most trumpets, clarinets, and saxophones — instruments that are often played by beginners, students, and amateur musicians, in school and community bands and ensembles.

Notice how, as in traditional notation more generally, the note C and the key of C major are special.
The names of transposing instruments like "B-flat trumpet" reflect the note that is played when reading the note C on the staff.

## Why is music transposed for transposing instruments?

There are some historical reasons for the "transposed music" convention that are related to the way instruments were constructed differently in the past.
For example, the earliest trumpets did not have valves.
But setting aside the historical factors, what are the contemporary reasons for this convention?

Today, the primary reason that music for transposing instruments is not written in concert pitch is to make it easier for a musician to play more than one instrument within a given family of instruments.

Trumpet players typically learn to play on a B-flat trumpet, the most common type of trumpet, but certain pieces of music may call for them to play other members of the trumpet family like a piccolo trumpet, D trumpet, E-flat trumpet, or C trumpet.
They might also play a baritone horn or euphonium.
These instruments have different registers or ranges of notes that they can produce, but they are all played with the same fingering patterns.

Similarly, saxophone players typically start by learning to play an alto saxophone, but may eventually also play a soprano, tenor, or baritone saxophone.
Each type of saxophone is played with the same (or very similar) fingering patterns.

Transposing the music for these transposing instruments allows the fingering for each note on the staff to remain the same for all of the instruments in a given instrument family.
For example, a C on the staff can be played with the same fingering on any trumpet, even though the actual note that sounds is different on each instrument.

In other words, transposing the music provides consistency between the fingering patterns and the notes on the staff as a musician switches instruments.
This way musicians do not have to learn a different mapping between fingerings and notes on the staff for each instrument they play, which makes it much easier to play more than one instrument within a given instrument family.

If instead the music for these instruments was _not_ transposed, but written in concert pitch then the same note on the staff would be played with different fingerings on different instruments.
And conversely the same fingering on different instruments would correspond to different notes on the staff.
That would make things much more difficult for musicians who play more than one instrument in the same family.

In a sense, transposing the music means that the notation effectively functions more like a tablature for the instrument.
The notes on the staff no longer represent the notes that are heard, at least not directly.
Instead, they represent fingering patterns, patterns that remain consistent for any instrument in that instrument family.
They become more of an indication of how to play the notes than a representation of the notes themselves.
In other words, the notes on the staff directly indicate to the musician how to physically play the note on the instrument, and only indirectly indicate the sounding note, since the
written note must be converted mentally to concert pitch.

## Difficulties of the "transposed music" convention

The convention of transposing the music provides a clear benefit for musicians who play more than one instrument in a given family.
Typically those are professionals who have access to more than one instrument in a given family and occasion to play more than one of them.
However, it does not help those who only ever play a single transposing instrument, and actually makes things more difficult for them.
They cannot play any sheet music (in concert pitch) that they might want to play but have to obtain or create a special version of the music that is transposed for their instrument.
When transposing instruments are involved, musicians cannot simply play from the same sheet music, swap parts, or come up with a harmony line by reading a melody from another musician's score.

In some musical contexts these problems do not arise because the musician's role is to learn and play the music that is prepared (and transposed) for them ahead of time by someone else.
These problems do appear in other contexts where musicians have more agency and more of a role in determining what they will play.

## Concert pitch for common instruments?

Is there a way to keep the benefits of this "transposed music" convention while minimizing the difficulties it entails? Let's take trumpets as our example.
B-flat trumpets are much more common than C trumpets.
Students typically learn on B-flat trumpets.
Yet music is written in concert pitch for a C trumpet but not for a B-flat trumpet.

Instead, what if music was written in concert pitch for the more common B-flat trumpet and transposed down a whole step (two semitones) for the C trumpet?
Then music for a D trumpet would be transposed down a major third (four semitones). For example, in traditional notation:

![](/2022/12/transposing-instruments-chart-tn-trumpets-proposal.svg)

And in Clairnote notation:

![](/2022/12/transposing-instruments-chart-clairnote-trumpets-proposal.svg)

As we will see below, while technically this approach would be possible with traditional music notation, it is much more viable with Clairnote music notation.

In this scenario the most common type of trumpet, that is played by the most people, including beginning, student, and amateur trumpet players, would not be a transposing instrument.
The most common scenario could then be better served by the simpler convention of reading and writing music in concert pitch.
The less common scenario, where more advanced musicians play more than one kind of trumpet, could then be served by the more complex convention of transposing the music for the less common types of trumpets.

This "concert pitch for common instruments" approach would also work for other transposing instrument families.
The music for the most common instrument (e.g. alto saxophone) could be written in concert pitch, and the music for other less common instruments could be transposed. All of the benefits of transposing the music would be preserved while removing some of the difficulties, particularly for the most common scenarios.

## Home notes and keys, instruments and notation

The obvious objection to this "concert pitch for common instruments" approach derives from the way the keys of C major and A minor are the simplest keys to read in traditional music notation.

To continue with the trumpet example, in traditional notation the key signature of B-flat major has two flats while the key signature of C major has no sharps or flats, and that makes it more difficult to read music in B-flat major. But why does that matter?

Most transposing instruments have a home note and a home key that is the simplest, easiest, and most basic note and key to play on that instrument.
On a B-flat trumpet that home note and key is B-flat and B-flat major.
B-flat is the lowest and easiest note to produce when you play the trumpet with none of the valves depressed.
(This is why it is called a B-flat trumpet.)

Similarly we can say that the home note and key of traditional music notation is C and C major — the key represented most simply and directly by the staff, without any sharps or flats in the key signature.
This is why the home note on a transposing instrument is generally represented by a C on the staff.
It aligns the home note and key of the instrument with the home note and key of the staff.

It would be possible to _not_ align the home key on an instrument with the home key of the staff (e.g. if music for B-flat trumpet was written in concert pitch).
However, there are valid reasons to keep them aligned.
For example, beginners benefit from being able to start playing music in a key that is both the simplest one to read on the staff and the simplest one to play on their instrument.

But what about when music is written in a notation system like Clairnote where all the keys are equally easy to read?
Where C major is not any simpler to read than the other keys?
Where the staff does not have a home key in the way that it does in traditional notation?

With Clairnote notation the need to transpose music to align the home key of the instrument with the home key of the staff disappears.
That opens up the possibility of writing music in concert pitch for the most commonly played instrument in a given instrument family.
For example, the music for B-flat trumpet could be written in concert pitch and then transposed for the other less common trumpets.
Clairnote makes it possible to simplify the most common scenarios and reserve the more complex convention of transposing music for the less common scenarios.

## Clairnote music notation for transposing instruments

In conclusion, should music in Clairnote music notation be transposed for transposing instruments?
Yes, but there is a strong case for writing it in concert pitch for the most common and/or most beginner friendly instrument in a given instrument family (B-flat trumpet, alto saxophone) and then transposed for the other instruments in that family (D trumpet, tenor saxophone).

This provides all the benefits of the "transposed music" convention for musicians who play more than one instrument in a given family while avoiding the difficulties and complexities it entails for the more common case of those who only play the most common instrument.

As we have seen, it would be much harder to make the case for this "concert pitch for common instruments" approach in traditional music notation, given the way that traditional notation makes the key of C major easier to read than the other keys, and the appeal of having the home key of the instrument align with the home key of the notation system.

However, with Clairnote music notation where all the keys are equally easy to read, where all keys are "at home" on the staff, there are fewer constraints and the "concert pitch for common instruments" approach becomes particularly appealing and viable.
