import { defineCollection, z } from "astro:content";

// Type-check blog page frontmatter using a schema

const blog = defineCollection({
  schema: z.object({
    title: z.string(),
    // TODO: make description non-nullable and add descriptions to all blog posts.
    description: z.nullable(z.string()),
    author: z.string(),
    special_byline: z.optional(z.string()),
    tags: z.optional(z.array(z.string())),
    draft: z.optional(z.boolean()),
    comment_status: z.enum(["open", "closed"]),
    date: z
      .string()
      .or(z.date())
      .transform((val) => new Date(val)),
  }),
});

export const collections = { blog };
