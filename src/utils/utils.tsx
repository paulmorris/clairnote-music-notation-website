import { getCollection } from "astro:content";
import type { CollectionEntry } from "astro:content";
import type { SelectOption } from "../types/types";

/**
 * Like in Guile Scheme, return an array containing 'count' numbers,
 * starting from 'start' and adding 'step' each time.
 */
export const iota = (count: number, start = 0, step = 1) =>
  Array(count)
    .fill(undefined)
    .map((_, index) => start + index * step);

/**
 * Return an array of numbers from `start` to `end` (inclusive).
 */
export const range = (start: number, end: number): number[] =>
  Array.from({ length: end - start + 1 }, (_, index) => start + index);

export const capitalize = (s: string) => s.charAt(0).toUpperCase() + s.slice(1);

/**
 * Takes an integer n and returns a random integer between 0 and n - 1
 * (inclusive). For example, pass 3 and get back a 0, 1, or 2.
 */
export const randomInteger = (n: number) => {
  return Math.floor(Math.random() * n);
};

/**
 * Given an array of numbers, returns the number that's closest to a target
 * number.
 */
export const closestNumber = (numbers: number[], targetNumber: number) => {
  if (numbers.length === 0) {
    return undefined;
  }
  let closest = numbers[0];
  let smallestDiff = Infinity;

  numbers.forEach((number) => {
    const diff = Math.abs(targetNumber - number);
    if (diff < smallestDiff) {
      smallestDiff = diff;
      closest = number;
    }
  });

  return closest;
};

export function makeSelectOptions(
  selectOptions: SelectOption<string | number>[],
  initialValue?: string | number
) {
  return selectOptions.map(({ value, label }) => (
    <option value={value} selected={value === initialValue}>
      {label}
    </option>
  ));
}

/**
 * Trim whitespace from start and end of a string and replace multiple
 * whitespace characters with a single space. Optionally modify each word with
 * a mapping function.
 */
export const transformSearchQuery = (
  query: string,
  modifyWord?: (s: string) => string
): string => {
  const words1 = query.trim().split(/\s+/);

  const words2 = modifyWord ? words1.map(modifyWord) : words1;

  return words2.join(" ");
};

export const slugFromTag = (tag: string) => {
  return {
    LilyPond: "lilypond",
    MuseScore: "musescore",
    "Music Notation": "music-notation",
    "Sheet Music": "sheet-music",
    Untagged: "untagged",
    "Clairnote Website": "clairnote-website",
    Frescobaldi: "frescobaldi",
  }[tag];
};

export const yearsFromPosts = (posts: CollectionEntry<"blog">[]) => {
  const years = new Set<string>();
  posts.forEach((post) => {
    years.add(post.data.date.getFullYear().toString());
  });
  return Array.from(years);
};

export const tagsFromPosts = (posts: CollectionEntry<"blog">[]) => {
  const tags: Record<string, number> = {};

  posts.forEach((post) => {
    post.data.tags?.forEach((tag) => {
      if (tags[tag]) {
        tags[tag] += 1;
      } else {
        tags[tag] = 1;
      }
    });
  });

  return Object.entries(tags)
    .map(([key, value]) => ({
      tagName: key,
      count: value,
    }))
    .sort((a, b) => b.count - a.count);
};

export const getBlogPosts = async (
  filterFn?: (post: CollectionEntry<"blog">) => boolean
) => {
  const posts = await getCollection("blog", (post) => {
    return !post.data.draft && (filterFn ? filterFn(post) : true);
  });

  posts.sort((a, b) => b.data.date.valueOf() - a.data.date.valueOf());

  return posts;
};

export const formatDateParts = (date: Date) => {
  const year = date.getFullYear();
  const rawMonth = String(date.getMonth() + 1);
  const month = rawMonth.length === 1 ? `0${rawMonth}` : rawMonth;
  const rawDay = String(date.getDate());
  const day = rawDay.length === 1 ? `0${rawDay}` : rawDay;

  return { year, month, day };
};

export const formatDateToString = (date: Date) => {
  const { year, month, day } = formatDateParts(date);

  return `${year}-${month}-${day}`;
};

/**
 * For use with getStaticPaths(). Trailing "/" does not work here.
 */
export const blogPostToDateUrlPath = (post: CollectionEntry<"blog">) => {
  const { year, month } = formatDateParts(post.data.date);

  return `${year}/${month}/${post.slug}`;
};

/**
 * For creating links to blog posts. Use trailing "/" here.
 */
export const blogPostToFullUrlPath = (post: CollectionEntry<"blog">) =>
  `/blog/${blogPostToDateUrlPath(post)}/`;

/**
 * For TypeScript, for type narrowing on array item access.
 */
export const includes = <T extends U, U>(
  readOnlyArray: ReadonlyArray<T>,
  item: U
): item is T => {
  return readOnlyArray.includes(item as T);
};
