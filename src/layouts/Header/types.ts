import type { PageLayoutProps } from "../../types/types";

export type PrimaryMenuItemKey =
  | "about"
  | "learn"
  | "sheetMusic"
  | "software"
  | "community"
  | "blog";

export type NavItem = {
  label: string;
  mobileLabel?: string;
  dnOnly?: boolean;
} & (
  | {
      // internal links
      slug: string;
    }
  | {
      // external links
      url: string;
    }
);

export type PageLayoutPropsPlusPathname = PageLayoutProps & {
  pathname: string;
};
