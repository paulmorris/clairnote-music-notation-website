export type SnOrDnProps = {
  sn: boolean;
  dn: boolean;
  clairnoteName: "Clairnote SN" | "Clairnote DN";
  otherClairnoteName: "Clairnote DN" | "Clairnote SN";
  urlDir: "/" | "/dn/";
  otherUrlDir: "/" | "/dn/";
  lineOrSolid: "line" | "solid";
  spaceOrHollow: "space" | "hollow";
};

export type PageComponentProps = SnOrDnProps;

/**
 * Pages either have a title or a fullTitle but not both. Most have a title.
 */
export type PageLayoutProps = PageComponentProps &
  (
    | { title: string; fullTitle?: never }
    | { title?: never; fullTitle: string }
  ) & {
    description: string;
    isBlog?: boolean;
  };

/**
 * Data type for an <option> element in a <select> drop down menu.
 */
export interface SelectOption<T> {
  value: T;
  label: string;
}
