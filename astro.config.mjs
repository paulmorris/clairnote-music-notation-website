import { defineConfig } from "astro/config";
import mdx from "@astrojs/mdx";
import sitemap from "@astrojs/sitemap";
import solidJs from "@astrojs/solid-js";

// See: https://astro.build/config
export default defineConfig({
  site: "https://clairnote.org",
  integrations: [
    sitemap({
      filter: (page) =>
        page !== "https://clairnote.org/blog/tag/" &&
        !page.includes("https://clairnote.org/dn") &&
        !page.includes("https://clairnote.org/software-musescore/") &&
        !page.includes("https://clairnote.org/handwritten-sheet-music/") &&
        !page.includes("https://clairnote.org/notetrainer-application/"),
    }),
    mdx(),
    solidJs(),
  ],
  markdown: {
    // Can be 'shiki' (default), 'prism' or false to disable highlighting.
    syntaxHighlight: false,
  },
});
