\version "2.24.3"
\language "english"
% \include "clairnote-20230104.ly"
\include "clairnote.ly"

\header { tagline = "" }

#(set-global-staff-size 34)

\pointAndClickOff

\paper {
  page-breaking = #ly:one-line-auto-height-breaking
  top-system-spacing = 0
  top-margin = 1
  bottom-margin = 1
  left-margin = 1
  right-margin = 1
}

music = \relative f' {
  \time 3/4

  | gf gf gf?
  | gs gs gs?
  | g g g?

  % | gs4 cf gs? cf?

  % | cs' cs cs! cs?
  % | df df df! df?
  % | e e e! e?

  % | gs4 ef gs? ef?
  % | aqs ctqs aqs? ctqs?
  % | gqf btqf gqf? btqf?
}

\score {
  <<
    \new Staff {
      \music
    }
    \new TradStaff {
      \music
    }
  >>
  \layout {
    indent = #0
  }
}
