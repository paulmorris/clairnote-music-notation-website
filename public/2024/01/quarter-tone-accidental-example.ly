\version "2.24.3"
\language "english"
% \include "clairnote-20230104.ly"
\include "clairnote.ly"

\header { tagline = "" }

#(set-global-staff-size 34)

\pointAndClickOff

\paper {
  page-breaking = #ly:one-line-auto-height-breaking
  top-system-spacing = 0
  top-margin = 1
  bottom-margin = 1
  left-margin = 1
  right-margin = 1
}

music = \relative f' {
  \time 5/4
  | c4 cqs cs ctqs css
  | d dqf df dtqf dff
}

\score {
  <<
    \new Staff {
      \music
    }
    \new TradStaff {
      \music
    }
  >>
  \layout {
    indent = #0
  }
}
