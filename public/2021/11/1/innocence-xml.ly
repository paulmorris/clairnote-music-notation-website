\version "2.20.0"
% automatically converted by musicxml2ly from /home/andres/Música/Clairnote/innocence.mxl
\pointAndClickOff

\header {
  encodingsoftware =  "MuseScore 3.2.3"
  encodingdate =  "2021-10-26"
  subtitle =  "25 Easy Studies - Opus 100"
  composer =  "Friedrich Burgmüller"
  title =  Innocence
}

#(set-global-staff-size 20.1587428571)
\paper {

  paper-width = 21.01\cm
  paper-height = 29.69\cm
  top-margin = 1.0\cm
  bottom-margin = 2.0\cm
  left-margin = 1.0\cm
  right-margin = 1.0\cm
  indent = 1.61615384615\cm
  short-indent = 1.29292307692\cm
}
\layout {
  \context {
    \Score
    autoBeaming = ##f
  }
}
PartPOneVoiceOne =  {
  \repeat volta 2 {
    \clef "treble" \key f \major \time 3/4 | % 1
    a''16 ( -4 [ _\p _\markup{ \small\italic {gracioso} } ^\markup{
      \bold {Moderato}
    } g''16 f''16 e''16 ) ] f''16 ( -4 [ e''16
    d''16 c''16 ) ] d''16 ( -4 [ c''16 bes'16 a'16 ] | % 2
    c''8 -3 [ _\> bes'8 ) ] bes'4 _\! r4 | % 3
    g''16 ( -5 [ f''16 e''16 d''16 ] c''16 [ _\< d''16 e''16 f''16
    -1 ] g''16 [ a''16 bes''16 c'''16 ] | % 4
    bes''8 [ _\! _\> a''8 ) ] a''4 _\! r4 | % 5
    a''16 ( -5 [ _\markup{ \small\italic {cresc.} } g''16 f''16 e''16
    ] f''16 -3 [ e''16 d''16 cis''16 -2 ] d''4 ) -1 | % 6
    g''16 ( -4 [ f''16 e''16 d''16 ) ] e''16 ( -4 [ d''16 c''16 b'16
    ) ] c''16 ( -4 [ b'16 a'16 g'16 ) ] | % 7
    as'8 ( -> [ g'8 ) ] a'8 ( -> [ g'8 ) ] e''8 ( -> [ d''8 ]
  }
  \alternative {
    {
      | % 8
      c''4 ) r2
    }
    {
      | % 9
      c''8 c''8 ( [ _\markup{ \small\italic {dim.} } b'8 c''8 d''8
      c''8 ) ]
    }
  } \repeat volta 2 {
    | \barNumberCheck #10
    c''8 -. -2 [ _\markup{ \small\italic {leggiero} } c''16 ( -1 d''16
    ] e''8 ) -. [ e''16 ( -1 f''16 ] g''4 ) | % 11
    f''8 -. [ f''16 ( -1 g''16 ] a''8 ) -. [ a''16 ( -1 bes''16 ]
    c'''4 ) | % 12
    \ottava #1 _\markup{ \small\italic {cresc.} } | % 12
    c'''8 -. -2 [ c'''16 ( -1 d'''16 ] e'''8 ) -. -1 [ e'''16 ( f'''16
    ] g'''4 ) | % 13
    f'''8 -. [ f'''16 ( -1 g'''16 ] a'''8 ) -. [ a'''16 ( -1 bes'''16
    ] c''''4 ) | % 14
    c''''16 ( -5 [ _\f bes'''16 a'''16 g'''16 ] f'''16 [ e'''16 -3
    d'''16 c'''16 ] \ottava #0 bes''16 -4 [ a''16 g''16 f''16 ] | % 15
    d'''8 ) -. -5 [ d'''16 ( c'''16 ] bes''8 ) -. [ _\markup{
      \small\italic {dim.}
    } bes''16 ( -3 a''16 ] g''8 ) -. [ g''16
    ( -5 f''16 ] | % 16
    e''16 [ _\markup{ \small\italic {cresc.} } d''16 c''16 d''16 ]
    e''16 [ f''16 -1 g''16 a''16 ] bes''16 [ c'''16 -1 d'''16 e'''16
    ] | % 17
    f'''8 ) -. r8 <a f'>4 _\f r4
  }
}

PartPOneVoiceFive =  {
  \repeat volta 2 {
    \clef "bass" \key f \major \time 3/4 <f a c'>2. <f g d'>4 <f g
    d'>4 <f g d'>4 <e bes c'>2. <f a c'>4 -3 -1 <f a c'>4 <f a
    cis'>4 -1 <f a d'>2 -3 -1 <f a d'>4 <g c' e'>2 -2 -1 <g c'
    e'>4 <g b f'>4 <g b f'>4 <g b f'>4
  }
  \alternative {
    {
      <c' e'>8 -1 c'8 ( -1 [ _\markup{ \small\italic {dim.} } b8
      c'8 bes8 g8 ) ]
    }
    {
      <c' e'>8 r8 r2
    }
  } \repeat volta 2 {
    bes8 ( [ c'8 bes8 c'8 bes8 c'8 ] a8 [ c'8 a8 c'8 a8 c'8 ) ] bes8
    ( [ c'8 bes8 c'8 bes8 c'8 ] a8 [ c'8 a8 c'8 a8 c'8 ) ] <f a>2. (
    <bes d'>8 ) r8 r2 <c bes>2. ( <f a>8 ) r8 f,4 r4
  }
}


% The score definition
\score {
  <<

    \new PianoStaff
    <<
      \set PianoStaff.instrumentName = "Piano"
      \set PianoStaff.shortInstrumentName = "Pno."

      \context Staff = "1" <<
        \mergeDifferentlyDottedOn\mergeDifferentlyHeadedOn
        \context Voice = "PartPOneVoiceOne" {  \PartPOneVoiceOne }
      >> \context Staff = "2" <<
        \mergeDifferentlyDottedOn\mergeDifferentlyHeadedOn
        \context Voice = "PartPOneVoiceFive" {  \PartPOneVoiceFive }
      >>
    >>

  >>
  \layout {}
  % To create MIDI output, uncomment the following line:
  %  \midi {\tempo 4 = 120 }
}

