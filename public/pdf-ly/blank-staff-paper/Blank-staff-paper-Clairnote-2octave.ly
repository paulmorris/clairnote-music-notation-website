\version "2.19.49"
\include "clairnote.ly"

#(set-global-staff-size 40.2)

\header { tagline = "" }

\score {
  \new Staff {
    \repeat unfold 7 { s1 \break }
  }
  \layout {
    indent = 0\in
    \context {
      \Staff
      \remove "Time_signature_engraver"
      \remove "Clef_engraver"
      \remove "Bar_engraver"
    }
    \context {
      \Score
      \remove "Bar_number_engraver"
    }
  }
}

\paper {
  #(set-paper-size "letter")
  ragged-last-bottom = ##f
  line-width = 7.5 \in
  left-margin = 0.5 \in
  bottom-margin = .50 \in
  top-margin = .75 \in
}
