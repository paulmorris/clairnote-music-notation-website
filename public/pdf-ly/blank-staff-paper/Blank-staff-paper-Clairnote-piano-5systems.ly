\version "2.19.49"
\include "clairnote.ly"

staffSize = 25
pianoStaffSpacing = 10
systemsPerPage = 5

#(set-global-staff-size staffSize)

\paper {
  % #(set-paper-size "a4")
  #(set-paper-size "letter")
  ragged-last-bottom = ##f
  bottom-margin = 0.50 \in
  top-margin = 0.75 \in
  % left-margin = 0.5 \in
  % right-margin = 0.5 \in
}

\header {
  tagline = ""
}

emptyStaff = \new Staff {
  \repeat unfold #systemsPerPage {
    s1 \break
  }
}

\score {
  \new PianoStaff <<
    \emptyStaff
    \emptyStaff
  >>
  \layout {
    indent = 0 \in
    \context {
      \Staff
      \remove "Time_signature_engraver"
      \remove "Clef_engraver"
      \remove "Key_engraver"
      \override VerticalAxisGroup.staff-staff-spacing.basic-distance = #pianoStaffSpacing
    }
    \context {
      \Score
      \remove "Bar_number_engraver"
    }
  }
}
