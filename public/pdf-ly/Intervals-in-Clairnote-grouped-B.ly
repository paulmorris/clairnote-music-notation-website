\version "2.19.49"
% clairnote-type = sn
\include "clairnote.ly"

\pointAndClickOff

\header{
  % tagline = ""
  title = \markup \concat {"Intervals in " \clairnoteTypeName " Music Notation"}
}

#(set-default-paper-size "letterlandscape")

\paper {
  system-system-spacing.basic-distance = #24 % 24
  markup-system-spacing.basic-distance = #18 % 22
  indent = #0
}

% global modifications
\layout {

  \context {
    \Score
    \override SpacingSpanner #'common-shortest-duration = #(ly:make-moment 1 8 )
    \override TextScript #'extra-offset = #'(0 . 0)
  }
  \context {
    \Staff
    \remove "Accidental_engraver"
    \remove "Key_engraver"
    \remove "Time_signature_engraver"
    \remove "Clef_engraver"
    \remove "Bar_engraver"
    \override Stem.direction = 1
  }
}

expand =
#(define-music-function (mus label)
   (ly:music? string?)
   #{
     s4^\markup { \with-color #grey  \fontsize #-5 \sans { #label }}
     $mus
     \transpose c e $mus
     \transpose c gis $mus

     \transpose c d $mus
     \transpose c fis $mus
     \transpose c ais $mus

     \transpose c cis $mus
     \transpose c f $mus
     \transpose c a $mus

     \transpose c dis $mus
     \transpose c g $mus
     \transpose c b $mus
   #})

% \expand c' "Unisons (0)"

spacer = {
  % s128
  \stopStaff
  s128
  \startStaff
  \cadenzaOn
  % s128
}

{
  \cadenzaOn
  \expand <c' cis'>4 "2nds, Minor (1)"
  \spacer
  \expand <c' f'>4 "4ths, Perfect (5)"
  \spacer
  \expand <c' a'>4 "6ths, Major (9)"
}
{
  \cadenzaOn
  \expand <c' d'>4 "2nds, Major (2)"
  \spacer
  \expand <c' fis'>4 "Tritones (6)"
  \spacer
  \expand <c' ais'>4 "7ths, Minor (10)"
}
{
  \cadenzaOn
  \expand <c' dis'>4 "3rds, Minor (3)"
  \spacer
  \expand <c' g'>4 "5ths, Perfect (7)"
  \spacer
  \expand <c' b'>4 "7ths, Major (11)"
}
{
  \cadenzaOn
  \expand <c' e'>4 "3rds, Major (4)"
  \spacer
  \expand <c' gis'>4 "6ths, Minor (8)"
  \spacer
  \expand <c' c''>4 "Octaves (12)"
}

\markup \tiny {
  \vspace #4
  \wordwrap { The intervals are arranged to highlight similarities in their appearance.  Notice how the intervals in each row share certain visual characteristics. For example, the intervals on the bottom row always contain two line notes or two space notes and the notes are in the same position relative to the nearest staff line (or ledger line). }
}