\version "2.18.2"
% automatically converted from musicXML to ly format by musicxml2ly

%% additional definitions required by the score:
\language "english"
\include "clairnote-code.ly"

\paper {
  #(set-paper-size "letter" 'landscape)
}
#(set-global-staff-size 15)

\header {
  encodingsoftware = "Noteflight version 0.3.2"
  encodingdate = "2015-04-20"
  title = "Collage"
  composer = "Samuel Lam"
}

\layout {
  \context {
    \Score
    skipBars = ##t
    autoBeaming = ##f
  }
}
PartPOneVoiceOne =  \relative e'' {
  \clef "treble" \key c \major \numericTimeSignature\time 4/4 \tempo "Allegro Marcato" | % 1
  r2 r4 e8 \mf -. [ c -. ] g4 ( gs8 ) -. [
  e8 -. ] b4 ( \clef "bass" c8 ) -. [ g -. ] | % 3
  \clef "bass" e1 ~ e1 s1 \clef
  "treble" | % 6
  \clef "treble" gs'4 (a8 -.) [ e -. ] c4 ( \clef "bass" cs8 ) -. [ gs
  -. ] | % 7
  \clef "bass" e1 ~ e1 | % 9
  <es' cs'>2 \! \mf f4 \p ( g <a, df f af>1 <as d fs a> \< | % 12
  <b ef g bf>4 ) \ff -. <b ef>2 \> ( -> <c e>8 \pp [ [ <df
  f>8 \< ] ) ] <d fs> [ [ <ef g> <e gs> <f a> ] ] <gf bf> [ [
  <g b>8 ] ] <af c>4 \! | % 14
  d,4 \ff -. <g a cs> -. r2 r4 cs, \mf ( e a \< <es cs'>2. )
  <d fs gs d'>4 \f -. -> R1 | % 17
  R1 r2 \!
  \times 2/3 {ef8 [ b' g' ]}
  \ottava #1 <ds b'>4 ~ ( | % 19
  <ds b'>16 [ <e c'> <f df'> <fs d'> ] <g ef'> [ <gs
  e'>16 ) <a f'> <as fs'> ] <b g'> [ <c af'> <cs a'>
  <d bf'>16 ] <ds b'> [ <e c'> <f df'> <fs d'> ] \noBreak | % 21
  \numericTimeSignature\time 2/4 <g ef'>2
  \numericTimeSignature\time 4/4 ef'8 \mf \< -. [ bf -. ] g4 ( gs8 ) -. [ e -. ] b4 (
  c8 ) -. [ g -.  e] -. \ottava #0 f ([ df
  af8 ]) \times 2/3 {a8 ([ e cs ])}
  R1 | % 25
  r4 \! e'16 \mf \< ([ c g ]) gs ([ e b ]) \clef
  "bass" c16 \! \ff ([ g ] e4) ~ | % 26
  e1 s1 r1| % 29
  \clef "treble" <gs' e'>4 \< \ottava #1 <g' ef'> <fs' d'> \ottava #0 r r2 r4 <d,, b'>
  <cs' as'>4 \ottava #1 <c'  a'> <b' gs'> \ottava #0 r R1 | % 33
  \time 18/4  r2 \! \> \times 2/3 {r4 r <g,,, f'> ~ }<g f'>2 <fs'
  e'>2 ~
  \times 2/3 {<fs e'>2 \ottava #1 <f' ef'>4 ~}
  <f ef'>2 ~
  \times 2/3 {<f ef'>2 <e' d'>4 \! ~}
  <e d'>1 | % 34
  \numericTimeSignature\time 4/4 \ottava #0 r2. g,,8 \pp ( [ cs \< ]
  d4) gs,8 ( [ d' ] ds4) a8 ( [ ds
  ] e4) as,8 ( [ e' ] f4) -> f -> | % 37
  <fs, f'>4 \! \ff -> <f, e'> -> r2 r <ef' gf bf d> \pp ( <e g
  b ds>2 \< <f af c e> | \barNumberCheck #40
  <fs a cs es>4) \ff -. <g, cs>2 -> \> ( <af d>8 \! \pp [ <a
  ds>8 \< ] <bf e> [ <b es> <c fs> <df g> ] <d gs>
  [ <ef a>8 ] <e as>4) \! | % 42
  <ds b'>4 \ff -. -. <f g b> -. r2
  r4 b, \p ( d \< g )
  <gs e'>2. <f a b f'>4 \! \f -. -> R1 | % 45
  R1 r2 \times 2/3 {
    gf8 [ d' bf' ]
  }
  <fs d'>4 -> | % 47
  <af, bf>8 \p \< ([ <a b> <bf c>  <b cs> ] <c d> [
  <df ef>8 ] d'4) \! | % 49
  <d, e>8 \mf \< ([ <ef f> <e fs> <f g> ] <gf af> [ <g
  a>8 ] d'4) \! | \barNumberCheck #50
  <af bf>8 \f \< ( [ [ <a b> <bf c> ) <b cs> ] ] <c d> \ottava #1 d'4. \! \ottava #0 b,,8
  ( [ a8 \> g f ] ef [ df ) ] e'' \mf ( [ c \! ]
  g4 \>) gs8 ( [e8] b4) c8 ( [ g ] e [ b ] gs2. ~ gs8 \! [ a \ppp bf b ] c2
  ~ c8) [ df ( d ef ] e2 ~ e8) [ f ( gf g ] af2 ~ af4) s2
  gs'4 ~ -> | % 58
  gs4 ^\markup {\bold "Accel."}
  s2 af4 ~ ->  af \! s2 af4 ~ ->
  af s4 r \ottava #1 <b b'> \! \fff ->
  r <b b'> -> r <b b'> ^\markup {\bold "Molto rit."} -> | % 62
  \clef "bass" \ottava #-1 a,,,,,1 \glissando | % 63
  \clef "treble" \ottava #2 c'''''''4 -> -- \ottava #0 <cs,,,, ds fs gs as cs ds fs gs as cs> -. -!
  -- -> -^ \clef "bass" c8 -. -^ [ g -. -^ c, -. -^ ] r
  \bar "|."
}
PartPOneVoiceTwo =  \relative e {
  \clef "treble" \key c \major \numericTimeSignature\time 4/4 | % 1
  s1*2 | % 3
  \clef "bass" s4*3 s4 s4 gs \p \< ( b e <bs gs'>2.) \! \clef
  "treble" e'8 \mf -. [ cs -. ] \!| % 6
  s1 | % 7
  \clef "bass" s1 s4 gs, \p \< ( b e ) \! \clef "treble"
  | % 9
  s1*5 | % 14
  s1*7 | % 21
  \numericTimeSignature\time 2/4 s2
  \numericTimeSignature\time 4/4
  s1*4 | % 26
  s4 gs, \p ( b \< e ) <bs gs'>2. \! r4 | % 28
  s1*5 \numericTimeSignature\time 18/4 s1*4 s2 | % 34
  \numericTimeSignature\time 4/4 s2.cs8 ([d] ds4 ) d8 ([ds] e4) ds8 ([e
  ] f4) e8 ([es] fs4) -> fs -> | % 37
  s1*3 | \barNumberCheck #40
  s2. s8  s8*7 s4 | % 42
  s1*6 | % 48
  d8 ([ds e es] fs [g] fs'4) | % 49
  gs,8 ([a as b] c [cs] fs4) \barNumberCheck #50
  d8 [ ds e f ] fs s s4 as,8 [ gs fs e ]
  d8 [ c ] <as b>4 g'8 ( [ cs ] d4) b,8 ( [ f' ] fs4) s1 s2 \!
  s4 <ef, af> ~ <ef af> s2 <gs b>4 ~ <gs b> s2 <f b>4 ~
  (<f b>8 [ <gf c> \< <g cs> <af d> ] <a ds>2 ~ | % 58
  <a ds>8) [<bf e> (<b f'> <c fs>] <df g>2 ~ <df g>8)
  [<d gs> (<ef a> <e as>] <f b>2 ~ <f b>8) [<gf c>
  (<g cs> <af d>] <a ds>2 \!) | % 62
  s1*3 | % 63
}
PartPOneVoiceThree =  \relative b, {
  \key c \major \clef "bass" \numericTimeSignature\time 4/4 | % 1
  R1*3 r4 b \sustainOn ( gs e
  <gs,gs'>2. ) \sustainOff r4 R1*2 r4 b' \sustainOn ( gs e  | % 9
  <cs, cs'>2 ) \sustainOff s2 <a' a'>1 \sustainOn ( <gs gs'> 1 | % 12
  <g g'>4 ) \sustainOff -. g''2 ( -> gf8 [ f ] e [ ef d df ] c [ b
  ) ] bf4 | % 14
  <f' af>4 -. a, -. s2 -> s4 e' (cs a <cs, cs'>2. <c c'>4 \sustainOff -> ) | % 17
  bf'8 ([ b c cs ] d [ds e f] <cs fs> [<d g> \! <ds gs>
  <e a>] <f bf>2) | % 19
  <b, ef g b>4 -. -> s2. r1 | % 21
  \numericTimeSignature\time 2/4 r2
  \numericTimeSignature\time 4/4
  r1 r | % 24
  \times 2/3 {d'8 ([ bf f ])}
  gf16 ([ df bf ]) b ([ g d ]) \ottava #-1 ef ([ b g ]) gs
  ([ e16 b ]) | % 25
  s1 | % 26
  r4 \ottava #0 b'' ( gs ) e <gs, gs'>2. \ottava #-1 <c,
  af'>4 ~ -> \sustainOn <c af'> \ottava #0 <b' g'> <bf' gf'> <a' f'>
  r2 r4 \sustainOff \ottava #-1 <f,, d'> ~ -> \sustainOn
  <f d'> \ottava #0 <e' cs'> <ef' c'> r r2 r4 \sustainOff \ottava #-1 <bf,, af'> \sustainOn ~ -> <bf af'>1 | % 33
  \time 18/4 \ottava #0 \times 2/3 {<a' g'>2 <af' gf'>4 ~}
  \times 2/3 {<af gf'>2 r4} r1 r r1. | % 34
  \numericTimeSignature\time 4/4 R1*3 \sustainOff
  | % 37
  r2 \sustainOn <e ef'>4 -> \ottava #-1 <ds, d'> -> <df df'>2 -> \ottava #0 <af'' ef' gf>
  (<g d' f> <gf df' ff> | \barNumberCheck #40
  <a,a'>4) \sustainOff -. <a' a'>2 \sustainOn -> (<bf bf'>8 [<b b'>] <c
  c'> [<cs cs'> <d d'> <ef ef'> ] <e e'> [<f f'>] <fs fs'>4) | % 42
  <b, b'>4 \sustainOff -. <g g'> -. s2 s4 d' ( b g )
  <e e'>2. <ef ef'>4 -> | % 45
  cs'8 ( [ d ds e ] f [ fs g gs]
  <d a'> [<ef bf'> <e b'> <f c'> ] <fs cs'>2) | % 47
  <d gf bf d>4 -. -> r r2 | % 48
  R1 | % 49
  R1 | \barNumberCheck #50
  r2 r4 \ottava #-1 d,, \ottava #0 R1*2 s1 s2. c''4
  ~ c s2 b4 ~ b s2 <g d'>4 ~
  <g d'>8 [<af ef'> (<a e'>
  <bf f'>8] <b fs'>2 ~ | % 58
  <b fs'>8) [<c g'> (<df af'> <d a'>] <ef bf'>2 ~
  <ef bf'>8) [<e b'> (<f c'> <fs cs'> ] <g d'>2 ~
  <g d'>8) [<af ef'> (<a e'> <bf f'>8 ]
  <b fs'>2)
  r4
  % \ottava #-1
  <b,,, b'> -> r <b b'> ->
  bf1 \glissando \clef "treble" \ottava #2 bf'''''''4 -> -- \sustainOff \clef "bass" \ottava #0 <c,,,,,, d e f g a b c d e f g a b c> -. -! -- -> -^
  \ottava #-1 <c c'>8 -. -^ [<g g'> -. -^ <c, c'> -. -^ ] \ottava #0 r \bar "|." | % 63
}
PartPOneVoiceFour =  \relative a,, {
  \key c \major \numericTimeSignature\time 4/4 | % 1
  s1*8 | % 9
  s1*5 s2 <a a'>2 \sustainOn ~ | %14
  <a a'>1 s1 | % 17
  f'8 \p \< ([ fs g gs ] a [ as b c] fs, [ f e ef ]
  \times 2/3 {d8 [df c]}
  b16 [ bf a af ] | % 19
  \times 2/3  {<g g'>8) \! \ff -. -> [ g' g' ]}
  \times 2/3  {ef8 [ b' g' ]}
  s2 s1 | % 21
  \numericTimeSignature\time 2/4 s2
  \numericTimeSignature\time 4/4 s1*3
  c,,,,1 -. -> ~| % 26
  c s1*6 \numericTimeSignature\time 18/4 s1*4 s2 | % 34
  \numericTimeSignature\time 4/4 s1*8 | % 42
  s2 <g' g'>2 -> ~ <g g'>1 | % 44
  s1 fs'8 \p \< ([g gs a] bf [b c cs] a [gs g fs]
  \times 2/3 {f8 [ e ds ]}
  d16 [ cs c b ] | % 47
  \times 2/3  {<bf bf'>8) \! \ff -. -> [bf' bf'\sustainOn]}
  \times 2/3  {gf8 [ d' bf' ]}
  s2 | % 48
  s1 \sustainOff | % 49
  s1 | \barNumberCheck #50
  s1*3 r4 f,8 ( [ d ] g,2 ~
  g8 [gf f e] ef2 ~ ef8) [d (df c] b2 ~ b8) [bf (a af] g2 \sustainOn ~
  g4) s2 g4 -> ~ | % 58
  g s2 g4 -> ~
  g s2 g4 -> ~
  g s r4 \sustainOn \ottava #-1 <b, b'> -> s1*3 | % 63
}
% The score definition
\score {
  <<
    \new PianoStaff
    <<
      \set PianoStaff.instrumentName = "Piano"
      \context Staff = "1" \with {
        % \remove "Accidental_engraver"
      }
      <<
        \context Voice = "PartPOneVoiceOne" { \voiceOne \PartPOneVoiceOne }
        \context Voice = "PartPOneVoiceTwo" { \voiceTwo \PartPOneVoiceTwo }
      >>
      \context Staff = "2" \with {
        % \remove "Accidental_engraver"
      }
      <<
        \context Voice = "PartPOneVoiceThree" { \voiceOne \PartPOneVoiceThree }
        \context Voice = "PartPOneVoiceFour" { \voiceTwo \PartPOneVoiceFour }
      >>
    >>
  >>
  \layout {}
  % To create MIDI output, uncomment the following line:
  % \midi {}
}

