\version "2.20.0"
% automatically converted by musicxml2ly from /home/andres/Música/Clairnote/tarantella.mxl
\pointAndClickOff

\header {
  encodingsoftware =  "MuseScore 3.2.3"
  encodingdate =  "2021-12-08"
  subtitle =  "Opus 100 - 25 Easy Studies"
  composer =  "Friedrich Burgmüller"
  title =  Tarantela
}

#(set-global-staff-size 20.1587428571)
\paper {

  paper-width = 21.01\cm
  paper-height = 29.69\cm
  top-margin = 1.0\cm
  bottom-margin = 2.0\cm
  left-margin = 1.0\cm
  right-margin = 1.0\cm
  indent = 1.61615384615\cm
  short-indent = 1.29292307692\cm
}
\layout {
  \context {
    \Score
    autoBeaming = ##f
  }
}
PartPOneVoiceOne =  {
  \repeat volta 2 {
    \clef "treble" \key f \major \time 6/8 | % 1
    \tempo "Allegro" 4.=160 | % 1
    d'8 ( [ _\f e'8 _\< f'8 ) ] f'8 ( [ g'8 a'8 ) ] | % 2
    a'8 ( [ _\! _\sf _\> g'8 f'8 ) ] f'8 ( [ e'8 d'8 ) ] | % 3
    d'8 ( [ _\! _\< e'8 f'8 ) ] f'8 ( [ g'8 a'8 ) ] | % 4
    a'8 ( [ _\! _\sf _\> g'8 f'8 ) ] f'8 ( [ e'8 d'8 ) ] | % 5
    d'4 _\! r8 r4. | % 6
    <f' d''>4 -> _\f r8 r4. | % 7
    <e' d''>4 -> r8 r4. | % 8
    <g' cis''>4 -> r8 r4 ^\fermata s8 \repeat volta 2 {
      | % 9
      a''8 _\p s8*5 | \barNumberCheck #10
      a''8 ( [ g''8 f''8 ) ] f''8 ( [ e''8 d''8 ) ] | % 11
      d''8 ( [ e''8 f''8 ) ] f''8 ( [ g''8 a''8 ] | % 12
      bes''8 ) [ f''8 \rest bes''8 ( ] d'''8 ) [ f''8 \rest d'''8
      ( ] | % 13
      a''4. ) ~ a''4 a''8 | % 14
      a''8 ( [ _\markup{ \small\italic {cresc.} } g''8 f''8 ) ]
      f''8 ( [ e''8 d''8 ] | % 15
      c''8 ) ( [ d''8 e''8 ] f''8 [ g''8 a''8 ] | % 16
      c'''8 ) [ f''8 \rest bes''8 ( ] g''8 ) [ f''8 \rest c'''8 (
      ] | % 17
      f''4. ) f'''8 -. r8
    }
    s8 \repeat volta 2 {
      | % 18
      a''8 ( _\p s8*5 | % 19
      g''8 ) [ _\markup{ \small\italic {leggiero} } f''8 \rest g''8
      ( ] e'''8 ) [ f''8 \rest e'''8 ( ] | \barNumberCheck #20
      d'''8 ) [ f''8 \rest d'''8 ( ] f''8 ) [ f''8 \rest f''8 ( ]
      | % 21
      e''8 ) [ f''8 \rest e''8 ( ] a''8 ) [ f''8 \rest a''8 ( ] | % 22
      f''4 -> e''8 d''8 ) [ f''8 \rest a''8 ( ] | % 23
      g''8 ) [ _\markup{ \small\italic {cresc.} } f''8 \rest g''8
      ( ] e'''8 ) [ f''8 \rest e'''8 ( ] | % 24
      d'''8 ) [ f''8 \rest d'''8 ( ] f''8 ) [ f''8 \rest f''8 ] | % 25
      a''4 ( g''8 ) \acciaccatura { a''8 } g''4 ( f''8 | % 26
      e''4. ) ~ e''4 a''8 -. _\f | % 27
      a''8 ( [ g''8 f''8 ) ] f''8 ( [ e''8 d''8 ) ] | % 28
      d''8 ( [ e''8 f''8 ) ] f''8 ( [ g''8 a''8 ] | % 29
      bes''8 ) [ f''8 \rest bes''8 ( ] d'''8 ) [ f''8 \rest d'''8
      ( ] | \barNumberCheck #30
      a''4. ) ~ a''4 a''8 -. | % 31
      bes''8 ( [ _\markup{ \small\italic {cresc.} } a''8 g''8 ) ]
      g''8 ( [ f''8 e''8 ] | % 32
      f''8 ) ( [ g''8 a''8 ] d''8 ) [ f''8 \rest d''8 ( ] | % 33
      cis''8 ) [ f''8 \rest cis''8 ( ] a''8 ) [ f''8 \rest a''8 (
      ] | % 34
      d''4. ) ( d'''8 ) -. r8
    }
    s8 | % 35
    r8 s8*5 \repeat volta 2 {
      | % 36
      \key d \major | % 36
      a'4 -. _\p r8 b'4 -. r8 | % 37
      cis''4 -. _\markup{ \small\italic {cresc.} } r8 d''4 -. r8 | % 38
      e''4 -. r8 fis''4 -. r8 | % 39
      fis''4. ( _\sf _\> b'4. ) | \barNumberCheck #40
      fis''4 -. _\! _\p r8 e''4 -. r8 | % 41
      d''4 -. r8 cis''4 -. r8 | % 42
      b'4 -. r8 cis''4 -. r8 | % 43
      \acciaccatura { b'16 [ cis''16 ] } b'4. _\> a'4 r8 | % 44
      \acciaccatura { a'8 } a'4 _\! r8 \acciaccatura { b'8 } b'4 r8
      | % 45
      \acciaccatura { cis''8 } cis''4 _\markup{
        \small\italic
        {cresc.}
      } r8 \acciaccatura { d''8 } d''4 r8 | % 46
      \acciaccatura { e''8 } e''4 r8 \acciaccatura { fis''8 }
      fis''4 r8 | % 47
      \acciaccatura { fis''8 } a''4. ( _\sf _\> g''4. ) | % 48
      d''8 -. [ _\! _\p f''8 \rest _\markup{
        \small\italic
        {leggiero}
      } d''8 ( ] e''8 ) [ f''8 \rest e''8 ( ] | % 49
      fis''8 ) [ f''8 \rest fis''8 ( ] a''8 ) [ f''8 \rest a''8 (
      ] | \barNumberCheck #50
      e''8 ) [ f''8 \rest e''8 ( ] a''8 ) [ f''8 \rest a''8 ( ]
    }
    \alternative {
      {
        | % 51
        d''2. )
      }
      {
        | % 52
        d''4. ~ d''4
      }
    } s8 \repeat volta 2 {
      | % 53
      \key f \major | % 53
      a''8 -. _\f s8*5 | % 54
      a''8 ( [ g''8 f''8 ) ] f''8 ( [ e''8 d''8 ) ] | % 55
      d''8 ( [ e''8 f''8 ) ] f''8 ( [ g''8 a''8 ] | % 56
      bes''8 ) [ f''8 \rest bes''8 ( ] d'''8 ) [ f''8 \rest d'''8
      ( ] | % 57
      a''4. ) ~ a''4 a''8 -. | % 58
      bes''8 ( [ _\markup{ \small\italic {cresc.} } a''8 g''8 ) ]
      g''8 ( [ f''8 e''8 ] | % 59
      f''8 ) ( [ g''8 a''8 ] d''8 ) [ f''8 \rest d''8 ( ] |
      \barNumberCheck #60
      cis''8 ) [ f''8 \rest cis''8 ( ] a''8 ) [ f''8 \rest a''8 (
      ]
    }
    \alternative {
      {
        | % 61
        d''4. ) ( d'''8 ) -. r8
      }
    } s8
  }
  \alternative {
    {
      | % 62
      d''8 ( [ _\< e''8 f''8 ) ] f''8 ( [ g''8 a''8 ) ]
    }
  } | % 63
  a''8 ( [ _\! _\sf g''8 _\> f''8 ] e''8 [ f''8 e''8 ] | % 64
  d''8 ) ( [ _\! _\< e''8 f''8 ) ] f''8 ( [ g''8 a''8 ) ] | % 65
  a''8 ( [ _\! _\sf g''8 _\> f''8 ] e''8 [ f''8 e''8 ] | % 66
  d''8 ) ( [ _\! e''8 f''8 ) ] f''8 ( [ gis''8 a''8 ) ] | % 67
  a''8 ( [ cis'''8 d'''8 ) ] d'''8 ( [ f'''8 e'''8 ] | % 68
  \ottava #1 _\markup{ \small\italic {dimin. e poco riten.} } | % 68
  d'''8 ) ( [ e'''8 f'''8 ) ] f'''8 ( [ gis'''8 a'''8 ) ] | % 69
  a'''8 ( [ cis''''8 d''''8 ) ] d''''8 ( [ f''''8 e''''8 ] |
  \barNumberCheck #70
  d''''4 ) \ottava #0 r8 r4. | % 71
  <f' d''>4 ^\markup{ \italic {a tempo} } _\f r8 <f' d''>4 r8 | % 72
  <f' d''>2. \bar "|."
}

PartPOneVoiceFive =  {
  \repeat volta 2 {
    \clef "bass" \key f \major \time 6/8 d8 ( [ e8 f8 ) ] f8 ( [ g8
    a8 ) ] a8 ( [ g8 f8 ) ] f8 ( [ e8 d8 ) ] d8 ( [ e8 f8 ) ] f8 ( [
    g8 a8 ) ] a8 ( [ g8 f8 ) ] f8 ( [ e8 d8 ) ] d4 r8 r4. <bes d'>4
    r8 r4. <g bes>4 r8 r4. <a e'>4 r8 r4 s8 \repeat volta 2 {
      r8 s8*5 <d f a>4. <d f a>4. <d f a>4. <d f a>4. <d g bes>4.
      <d g bes>4. <d f a>4. <d f a>4. <c f a>4. <c f a>4. <c f a>4.
      <c f a>4. <c e bes>4. <c e bes>4. <f a>4. ~ ~ <f a>8 r8
    }
    s8 \repeat volta 2 {
      r8 s8*5 | % 19
      \clef "treble" <a cis' e'>4. <a cis' e'>4. <a d' f'>4. <a d'
      f'>4. <a cis' g'>4. <a cis' g'>4. <a d' f'>4. <a d' f'>4.
      a8 ( [ cis'8 e'8 ] a8 [ cis'8 e'8 ] a8 [ d'8 f'8 ] a8 [ d'8
      f'8 ] a8 [ cis'8 e'8 ] a8 [ d'8 f'8 ] a8 [ cis'8 e'8 ] a4 )
      r8 | % 27
      \clef "bass" <d f a>4. <d f a>4. <d f a>4. <d f a>4. <d g
      bes>4. <d g bes>4. <d f a>4. <d f a>4. | % 31
      \clef "treble" g8 ( [ bes8 e'8 ] g8 [ bes8 e'8 ] a8 [ d'8 f'8
      ] a8 [ d'8 f'8 ] a8 [ e'8 g'8 ] a8 [ e'8 g'8 ] a8 [ d'8 f'8
      ] d'8 ) -. r8
    }
    s8 r8 s8*5 \repeat volta 2 {
      | % 36
      \clef "bass" \key d \major <d fis a>4 -. r8 <d fis a>4 -. r8
      <d fis a>4 -. r8 <d fis a>4 -. r8 <d fis a>4 -. r8 <d fis a>4
      -. r8 <d gis b>4 -. r8 <d gis b>4 -. r8 <d gis b>4 -. r8 <d
      gis b>4 -. r8 <d g a>4 -. r8 <d g a>4 -. r8 <d g a>4 -.
      r8 <d g a>4 -. r8 <d fis a>4 -. r8 <d fis a>4 -. r8 <d fis
      a>4 -. r8 <d fis a>4 -. r8 <d fis a>4 -. r8 <d fis a>4
      -. r8 <d fis a>4 -. r8 <d fis a>4 -. r8 <d g b>4 -. r8 <d g
      b>4 -. r8 <d g bes>4 -. r8 <d g bes>4 -. r8 <d fis a>4
      -. r8 <d fis a>4 -. r8 <d g a>4 -. r8 <d g a>4 -. r8
    }
    \alternative {
      {
        <d fis a>4 -. r8 <d fis a>4 -. r8
      }
      {
        <d fis a>4 -. r8 <d fis a>4 -.
      }
    } s8 \repeat volta 2 {
      | % 53
      \key f \major r8 s8*5 <d f a>4. <d f a>4. <d f a>4. <d f a>4.
      <d g bes>4. <d g bes>4. <d f a>4. <d f a>4. | % 58
      \clef "treble" g8 ( [ bes8 e'8 ] g8 [ bes8 e'8 ] a8 [ d'8 f'8
      ] a8 [ d'8 f'8 ] a8 [ e'8 g'8 ] a8 [ e'8 g'8 ]
    }
    \alternative {
      {
        a8 [ d'8 f'8 ] d'8 ) -. r8
      }
    } s8
  }
  \alternative {
    {
      | % 62
      \clef "treble" <d' f'>4 r8 r4.
    }
  } <a cis' g'>4. <a cis' g'>4. <d' f'>4 r8 r4. <a cis' g'>4. <a
  cis' g'>4. <d' f'>4 r8 r4. | % 67
  \clef "bass" <d f a>4. <d f a>4. <d f a>2. <d f a>4. <d f a>4. <d f
  a>4 r8 r4. <d a>4 r8 <d a>4 r8 <d a>2. \bar "|."
}


% The score definition
\score {
  <<

    \new PianoStaff
    <<
      \set PianoStaff.instrumentName = "Piano"
      \set PianoStaff.shortInstrumentName = "Pno."

      \context Staff = "1" <<
        \mergeDifferentlyDottedOn\mergeDifferentlyHeadedOn
        \context Voice = "PartPOneVoiceOne" {  \PartPOneVoiceOne }
      >> \context Staff = "2" <<
        \mergeDifferentlyDottedOn\mergeDifferentlyHeadedOn
        \context Voice = "PartPOneVoiceFive" {  \PartPOneVoiceFive }
      >>
    >>

  >>
  \layout {}
  % To create MIDI output, uncomment the following line:
  %  \midi {\tempo 4 = 240 }
}

