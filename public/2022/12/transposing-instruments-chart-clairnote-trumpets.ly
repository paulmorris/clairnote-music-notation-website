\version "2.22.2"
% clairnote-type = "dn"
\include "clairnote.ly"
\language "english"
#(set-default-paper-size "letter")

% for properly scaled svg images for clairnote.org
#(set-global-staff-size 34)

\pointAndClickOff

\layout {
  % \override Score.BarNumber.break-visibility = #all-invisible

  \context {
    \Staff
    % \override StaffSymbol.line-positions = #'(-8 -4 0 4 8)
  }
}

\header {
  tagline = ""
}

#(set! paper-alist
       (cons
        '("my size" . (cons
                       (* 7.8 in) ; width
                       (* 4.5 in) ; height
                       ))
        paper-alist))

\paper {
  % page-breaking = #ly:one-line-auto-height-breaking
  #(set-paper-size "my size")
  top-system-spacing = 0
  top-margin = 0
  bottom-margin = 0
  left-margin = 1
  right-margin = 1
}

drawClairnoteStaff =
#(define-music-function (note) (ly:music?)
   #{
     \new Staff \with {
       \omit TimeSignature
     }{
       s4. #note s4.
     }
   #})

drawTradStaff =
#(define-music-function (note) (ly:music?)
   #{
     \new TradStaff \with {
       \omit TimeSignature
     }{ s4. #note s4. }
   #})

drawBothStaves =
#(define-music-function (note) (ly:music?)
   #{
     <<
       \new TradStaff \with {
         \omit TimeSignature
       }{ s4. #note s4. }
       \new Staff \with {
         \omit TimeSignature
       }{ s4. #note s4. }
     >>
   #})

% drawStaves = #drawTradStaff
drawStaves = #drawClairnoteStaff
% drawStaves = #drawBothStaves

\markuplist {
  \override #'(padding . 2)
  \override #'(baseline-skip . 2.2)
  \table
  #'(-1 0 0 -1)
  \tiny {
    % \underline
    \bold
    {
      "Instrument"
      \center-column {
        \line {"Note Heard"}
        \line {"(Concert Pitch)"}
      }
      \center-column {
        \line {"Interval of"}
        \line {"Transposition"}
      }
      \center-column {
        \line {"Note Written"}
      }
    }

    " " " " " " " "

    "\"B-flat\" Trumpet"
    \score { \drawStaves bf4 }
    \center-column {
      \line {"Up a" }
      \line {"Major Second"}
    }
    \score { \drawStaves c'4 }


    " " " " " " " "

    "\"C\" Trumpet"
    \score { \drawStaves c'4 }
    "(None)"
    \score { \drawStaves c'4 }

    " " " " " " " "

    "\"D\" Trumpet"
    \score { \drawStaves d'4 }
    \center-column {
      \line {"Down a"}
      \line {"Major Second"}
    }
    \score { \drawStaves c'4 }

%{

    " " " " " " " "

    "\"E-flat\" Trumpet"
    \score { \drawStaves ef'4 }
    \center-column {
      \line {"Down a"}
      \line {"Minor Third"}
    }
    \score { \drawStaves c'4 }

    " " " " " " " "

    \column {
      \line {"\"A\" Piccolo"}
      \line {"Trumpet"}
    }
    \score { \drawStaves a'4 }
    \center-column {
      \line {"Down a"}
      \line {"Major Sixth"}
    }
    \score { \drawStaves c'4 }

    " " " " " " " "

    \column {
      \line {"\"B-flat\" Piccolo"}
      \line {"Trumpet"}
    }
    \score { \drawStaves bf'4 }
    \center-column {
      \line {"Down a"}
      \line {"Minor Seventh"}
    }
    \score { \drawStaves c'4 }
    %}

  }
}