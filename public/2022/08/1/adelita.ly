\version "2.20.0"
% automatically converted by musicxml2ly from /home/andres/Música/Clairnote/adelita.mxl
%\pointAndClickOff
\include "clairnote.ly"

\header {
  encodingsoftware =  "MuseScore 3.2.3"
  encodingdate =  "2022-03-20"
  subtitle =  "Mazurka"
  composer =  "Francisco Tárrega"
  title =  "Adelita"
  tagline =  ""
}

#(set-global-staff-size 21)
\paper {
  paper-width = 21.01\cm
  paper-height = 29.69\cm
  top-margin = 1.0\cm
  bottom-margin = 1.0\cm
  left-margin = 1.0\cm
  right-margin = 1.0\cm
  indent = 1.6\cm
  short-indent = 0.2\cm
  system-system-spacing.padding = 1.1\cm
  ragged-last-bottom = ##t
  ragged-last = ##t
}

\layout {
  \context {
    \Score
    autoBeaming = ##f
  }
}

PartPOneVoiceOne =  {
  \override TextSpanner.bound-details.left.stencil-align-dir-y = #CENTER
  \textSpannerDown
  \repeat volta 2 {
    \key e \minor \time 3/4 | % 1
    \tempo "Andante"
    e''8 ( [ _\p dis''8 ) ] b'2 -> | % 2
    d''8 ( [ c''8 ) ] fis'2\2 -> | % 3
    \override TextSpanner.bound-details.left.text = \markup{C\tiny{VII} "  "}
    \tweak Y-offset -10 \startTextSpan
    c''8 ( [  b'8 ) ] dis'4\3 -> g'4\2 \stopTextSpan |% 4
    \grace { fis'16\2 ( [ g'16\2 ] } fis'8\2 ) -> [ e'8 ] b8 [ e'8 ] g'8 [
    b'8 \glissando ] | % 5
    e''8 ( -> [ dis''8 ) ] b'2 -> | % 6
    d''16 [ e''16 c''8 ] fis'2\2 -> | % 7
    \override TextSpanner.bound-details.left.text = \markup{C\tiny{VII} "  "}
    \tweak Y-offset -10 \startTextSpan
    c''8 ( [  b'8 ) ] dis'4\3 -> g'4\2 \glissando \stopTextSpan _\markup{
      \small\italic {rit.}
    } | % 8
    <e'>2.\2
  }
  \repeat volta 2 {
    | % 9
    \key e \major | % 9
    \override TextSpanner.bound-details.left.text = \markup{C\tiny{IV} "  "}
    \tweak Y-offset -10 \startTextSpan
    gis'4. _\f  b'8 e'8\2 [ fis'8\2 ] | \barNumberCheck #10
    gis'4. b'8 e'8\2 [ fis'8\2 ] | % 11
    gis'8 \stopTextSpan [ b'8 \glissando ] e''8 [ dis''8 ] \acciaccatura {
      dis''16
      [ e''16 ]
    }
    \override TextSpanner.bound-details.left.text = \markup{C\tiny{IX} "  "}
    \tweak Y-offset -10 \startTextSpan
    dis''8 [  cis''8 ] \stopTextSpan | % 12
    \acciaccatura { cis''16 [ dis''16 ] }
    \override TextSpanner.bound-details.left.text = \markup{C\tiny{VII} "  "}
    \tweak Y-offset -11 \startTextSpan
    cis''8 [ _\markup{
      \small\italic {rit.}
    } _\>  b'8 ] a'8\2 [ fis'8\2 ]
    dis'8\3 [ b8\4 \glissando ] \stopTextSpan | % 13
    gis4.\4 _\! ^\markup{ \italic {a tempo} } b8\3 e8 [ fis8 ] | % 14
    gis8 [ b8\3 \glissando ] _\markup{ \small\italic {tenuto} } gis'8\3
    [ dis'8\3 ] \acciaccatura { e'16\3 [ fis'16\3 ] }
    \override TextSpanner.bound-details.left.text = \markup{C\tiny{VIII} "  "}
    \tweak Y-offset -10 \startTextSpan
    e'8.\3 ^\fermata  _\> ais16 \stopTextSpan | % 15
    b8. [ _\! _\p <a cis'>16 ] <gis b>4 _\markup{
      \small\italic
      {rit.}
    } 
    \override TextSpanner.bound-details.left.text = \markup{C\tiny{II} "  "}
    \tweak Y-offset -10 \startTextSpan
    <fis a dis'>4 | \stopTextSpan % 16
    <gis b e'>2 ^ "D.C. al Fine" r4
  }
}

PartPOneVoiceThree =  {
  \repeat volta 2 {
    \key e \minor \time 3/4 f2 \rest <e'\3 g'\2>4 f2
    \rest <c'\4 e'\3>4 f2 \rest <a\4 dis'\3>4 s2. f2 \rest <e'\3 g'\2>4 f2 \rest
    <c'\4 e'\3>4 f2 \rest <a\4 dis'\3>4 \stemUp <g\4 b\3>2. \stemDown
  }
  \repeat volta 2 {
    | % 9
    \key e \major e4 \rest <gis\4 b\3 e'\2>4 cis'4\3 e4 \rest <gis\4 b\3 e'\2>4
    cis'4\3 <b\3 e'\2>4 e4 \rest e'4\3 s2. d,4 \rest <b e'>4 <cis' e'>4 s4*9
  }
}

PartPOneVoiceTwo =  {
  \repeat volta 2 {
    \key e \minor \time 3/4
    \stemDown \shiftOff
    e,2. a,2. b,2.\6 e,2. e,2.
    a,2. b,2.\6 c,4 \rest e,2 ^ "Fine"
  }
  \repeat volta 2 {
     | % 9
    \key e \major e,2 a,4 e,2 a,4 e,2 fis4\5 <b,\6 a\4 dis'\3>2 f,4 \rest e,2 a,4
    e,2 <c\6 ais\4>4 b,2 b,4 e4 e,2
  }
}


% The score definition
\score {
    <<     
        \new StaffGroup = "tab with traditional"
        <<        
            \set StaffGroup.instrumentName = "Guitar"           
            \new Staff = "guitar traditional" \with {\cnStaffOctaveSpan 3}  <<         
                \mergeDifferentlyDottedOn \mergeDifferentlyHeadedOn
                \clef "treble_8"
                \context Voice = "PartPOneVoiceOne" { \voiceOne \PartPOneVoiceOne }
                \context Voice = "PartPOneVoiceThree" { \voiceTwo \PartPOneVoiceThree }
                \context Voice = "PartPOneVoiceTwo" { \voiceThree \PartPOneVoiceTwo }
                >>           
            \new TabStaff = "guitar tab" << 
                \set Staff.stringTunings = \stringTuning <e, a, d g b e'>
                \override TextScript.font-series = #'sans
                \clef "tab"
                \context TabVoice = "PartPOneVoiceOne" { \voiceOne \PartPOneVoiceOne }
                \context TabVoice = "PartPOneVoiceThree" { \voiceTwo \PartPOneVoiceThree }
                \context TabVoice = "PartPOneVoiceTwo" { \voiceThree \PartPOneVoiceTwo }
                >>         
            >>   
        >>
  \layout {
    \context {
      \Score
      \override StaffGrouper.staff-staff-spacing.padding = 0.2\cm
      \omit Voice.StringNumber
    }
  }
}

