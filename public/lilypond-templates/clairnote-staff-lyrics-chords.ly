\version "2.19.49"
% clairnote-type = dn
\include "clairnote.ly"

#(set-default-paper-size "letter")

\header {
  title = "Clairnote Template with Single Staff, Lyrics, and Chords"
  tagline = \markup \teeny {Clairnote Music Notation   (clairnote.org)   Music engraving by LilyPond   (www.lilypond.org)}
}

melody = \relative c' {
  \clef treble
  \key c \major
  \time 4/4

  a4 b c d
}

text = \lyricmode {
  Aaa Bee Cee Dee
}

harmonies = \chordmode {
  a2 c
}

\score {
  <<

    \new ChordNames {
      \set chordChanges = ##t
      \harmonies
    }

    \new Staff \with {
      % \remove "Accidental_engraver"
      % \remove "Key_engraver"
    } {
      \new Voice = "one" { \melody }
    }

    \new Lyrics \lyricsto "one" \text

  >>
  \layout { }
  \midi { }
}