\version "2.19.49"
% clairnote-type = dn
\include "clairnote.ly"

#(set-default-paper-size "letter")

\header {
  title = "Clairnote Template with Single Staff"
  tagline = \markup \teeny {Clairnote Music Notation   (clairnote.org)   Music engraving by LilyPond   (www.lilypond.org)}
}

melody = \relative c' {
  \clef treble
  \key c \major
  \time 4/4
  a4 b c d
}

\score {

  \new Staff \with {
    % \remove "Accidental_engraver"
    % \remove "Key_engraver"
  } {
    \melody
  }

  \layout { }
  \midi { }
}