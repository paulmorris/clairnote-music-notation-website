\version "2.19.49"
% clairnote-type = dn
\include "clairnote.ly"

#(set-default-paper-size "letter")

\header {
  title = "Clairnote Template for Solo Piano"
  tagline = \markup \teeny {Clairnote Music Notation   (clairnote.org)   Music engraving by LilyPond   (www.lilypond.org)}
}

upper = \relative c'' {
  \clef treble
  \key c \major
  \time 4/4
  a4 b c d e f g a
}

lower = \relative c {
  \clef bass
  \key c \major
  \time 4/4
  a2 c e g
}

\score {
  \new PianoStaff
  <<
    \set PianoStaff.instrumentName = #"Piano"

    \new Staff = upper \with {
      % \remove "Accidental_engraver"
      % \remove "Key_engraver"
    } {
      \upper
    }

    \new Staff = lower \with {
      % \remove "Accidental_engraver"
      % \remove "Key_engraver"
    } {
      \lower
    }

  >>
  \layout { }
  \midi { }
}