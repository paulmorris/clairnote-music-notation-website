\version "2.19.49"
% clairnote-type = dn
\include "clairnote.ly"

#(set-default-paper-size "letter")

\header {
  title = "Clairnote Template with Piano Staff, Melody, Lyrics"
  tagline = \markup \teeny {Clairnote Music Notation   (clairnote.org)   Music engraving by LilyPond   (www.lilypond.org)}
}

melody = \relative c'' {
  \clef treble
  \key c \major
  \time 4/4
  a b c d
}

text = \lyricmode {
  Aaa Bee Cee Dee
}

upper = \relative c'' {
  \clef treble
  \key c \major
  \time 4/4
  a4 b c d
}

lower = \relative c {
  \clef bass
  \key c \major
  \time 4/4
  a2 c
}

\score {
  <<

    \new Staff = "melody staff" \with {
      % \remove "Accidental_engraver"
      % \remove "Key_engraver"
    } {
      \new Voice = "mel" { \melody }
    }

    \new Lyrics \lyricsto mel \text
    \new PianoStaff <<

      \new Staff = "upper" \with {
        % \remove "Accidental_engraver"
        % \remove "Key_engraver"
      } {
        \upper
      }

      \new Staff = "lower" \with {
        % \remove "Accidental_engraver"
        % \remove "Key_engraver"
      } {
        \lower
      }

    >>
  >>

  \layout {
    \context { \Staff \RemoveEmptyStaves }
  }
  \midi { }
}