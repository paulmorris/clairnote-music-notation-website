# Clairnote Music Notation Website

[https://clairnote.org](https://clairnote.org)

Initially based on the default Astro blog boilerplate.

## Generating SVG images from LilyPond (.ly) files

See notes in `/src/images/README.md`.

## Sheet Music Search Index

A `lunr` search index needs to be pre-built, to prevent runtime code
from having to build it over and over. Build it by running:

`build-sheet-music-index`

The relevant sheet music data files will be read and a search index created.
The index is then imported for use on the "Sheet Music" page.
